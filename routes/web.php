<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Route::get('/', 'HomeController@homePage');//вывод страницы "Онлайн платформа фінансово-кредитнгої підтримки суб'єктів МСП м.Києва"
Route::post('/auth', 'HomeController@auth');//авторизация
Route::get('/logout/{userId}', 'HomeController@logout');//выход
Route::get('/download/{where}/{nameFile}', 'HomeController@download');//обработка скачивания
Route::get('/delete/{category}/{id}', 'HomeController@deleteFile');//обработка скачивания

Route::post('/bank/edit', 'HomeController@bankEdit');//изминение названия
Route::get('/404', 'HomeController@notFoundPage');//404
Route::post('/deleteImage', 'HomeController@deleteImage');//удаление картинки

Route::get('/regulatoryDocuments', 'RegulatoryDocumentsController@regulatoryDocumentsPage');//вывод страницы "Нормативно-розпорядчі документи щодо Програми ФКП суб'єктів МСП"
Route::get('/regulatoryDocuments/{category}', 'RegulatoryDocumentsController@regulatoryDocumentsLorePage');//вывод страницы со списком регуляторных документов
Route::post('/regulatoryDocuments/{category}/add', 'RegulatoryDocumentsController@regulatoryDocumentsLoreAdd');//обработка добавления регуляторного документа

Route::get('/gufAndPartnerBanks', 'GufAndPartnerBanksController@gufAndPartnerBanksPage');//вывод страницы "Німецько-український фонд та банки-партнери"
Route::get('/gufAndPartnerBanks/guf', 'GufAndPartnerBanksController@gufPage');//вывод страницы "Німецько-український фонд та банки-партнери"
Route::post('/gufAndPartnerBanks/guf/addInformation', 'GufAndPartnerBanksController@gufAddInformation');//обработка добавления информации
Route::post('/gufAndPartnerBanks/guf/editInformation', 'GufAndPartnerBanksController@gufEditInformation');//обработка редактирования информации
Route::get('/gufAndPartnerBanks/guf/information', 'GufAndPartnerBanksController@gufInformationPage');//вывод страницы "Інформація"
Route::get('/gufAndPartnerBanks/guf/contract', 'GufAndPartnerBanksController@gufContractPage');//вывод страницы "Догорів ДПРП з НУФ"
Route::post('/gufAndPartnerBanks/guf/contractsDPRPWithGuf/add', 'GufAndPartnerBanksController@gufContractAdd');//добавление договора "Догорів ДПРП з НУФ"
Route::get('/gufAndPartnerBanks/partnerBanks', 'GufAndPartnerBanksController@partnerBanksPage');//вывод страницы "Банки-партнери"
Route::post('/gufAndPartnerBanks/partnerBanks/add', 'GufAndPartnerBanksController@partnerBanksAdd');//обработка добавления банка-партнера
Route::get('/gufAndPartnerBanks/partnerBanks/bank/{id}', 'GufAndPartnerBanksController@partnerBanksBankPage');//вывод страницы "Банки-партнери"
Route::get('/gufAndPartnerBanks/partnerBanks/bank/{id}/information', 'GufAndPartnerBanksController@partnerBanksBankInformationPage');//вывод страницы "Банка с кнопками Інформація, Догорів ДПРП з банком, Договір банку з НУФ"
Route::post('/gufAndPartnerBanks/partnerBanks/bank/{id}/addInformation', 'GufAndPartnerBanksController@partnerBanksBankAddInformation');//обработка добавления информации
Route::post('/gufAndPartnerBanks/partnerBanks/bank/{id}/editInformation', 'GufAndPartnerBanksController@partnerBanksBankEditInformation');//обработка редактирования информации
Route::get('/gufAndPartnerBanks/partnerBanks/bank/{id}/contractGUF', 'GufAndPartnerBanksController@partnerBanksBankContractGUFPage');//вывод страницы "Договір банку з НУФ"
Route::post('/gufAndPartnerBanks/partnerBanks/bank/{id}/contractGUF/add', 'GufAndPartnerBanksController@partnerBanksBankContractGUFAdd');//обработка добавления договора "Договір банку з НУФ"
Route::get('/gufAndPartnerBanks/partnerBanks/bank/{id}/contractDPRP', 'GufAndPartnerBanksController@partnerBanksBankContractDPRPPage');//вывод страницы "Договір ДПРП з банком"
Route::post('/gufAndPartnerBanks/partnerBanks/bank/{id}/contractDPRP/add', 'GufAndPartnerBanksController@partnerBanksBankContractDPRPAdd');//обработка добавления договора "Договір ДПРП з банком"

Route::post('/addBankImage/{id_bank}', 'GufAndPartnerBanksController@addBankImage');//добавление картинки
Route::post('/addGufImage', 'GufAndPartnerBanksController@addGufImage');//добавление картинки

Route::get('/registerOfApplicants', 'RegisterOfApplicantsController@registerOfApplicantsPage');//вывод страницы "Реєстр* погодження позичальників, які претендують на ФКП"
Route::get('/registerOfApplicants/commission', 'RegisterOfApplicantsController@registerOfApplicantsCommissionPage');//вывод страницы "Комісія по погодженню надання ФКП суб'єктам МСП"
Route::get('/registerOfApplicants/commission/{category}', 'RegisterOfApplicantsController@registerOfApplicantsCommissionComProtPage');//вывод страницы "Протоколи" и "Наказ"
Route::post('/registerOfApplicants/commission/{category}/add', 'RegisterOfApplicantsController@registerOfApplicantsCommissionComProtAdd');//добавление "Протоколи" и "Наказ"
Route::post('/registerOfApplicants/bank/add', 'RegisterOfApplicantsController@registerOfApplicantsBanksAdd');//обработка добавления банка
Route::get('/registerOfApplicants/generalRegister', 'RegisterOfApplicantsController@registerOfApplicantsGeneralRegisterPage');//вывод страницы "Загальний реєстр"
Route::get('/registerOfApplicants/generalRegister/search', 'RegisterOfApplicantsController@registerOfApplicantsGeneralRegisterSearchPage');//поиск по таблице
Route::get('/registerOfApplicants/bank/{id}/register', 'RegisterOfApplicantsController@registerOfApplicantsBankRegisterPage');//вывод страницы "Реєстр погодження позичальників, які можуть претендувати на ФКП"
Route::post('/registerOfApplicants/addRow', 'RegisterOfApplicantsController@registerOfApplicantsAddRow');//обработка добавления строки
Route::post('/registerOfApplicants/deleteRow', 'RegisterOfApplicantsController@registerOfApplicantsDeleteRow');//обработка удаления строки
Route::post('/registerOfApplicants/updateRow', 'RegisterOfApplicantsController@registerOfApplicantsUpdateRow');//обработка обновления строки
Route::post('/registerOfApplicants/updateRowKved', 'RegisterOfApplicantsController@registerOfApplicantsUpdateRowKved');//обработка обновления строки
Route::post('/registerOfAplicants/updateCol', 'RegisterOfApplicantsController@updateCol');//сохрание отчета

Route::get('/registerOfBorrowers', 'RegisterOfBorrowersController@registerOfBorrowersPage');//вывод страницы "Реєстр** позичальників на надання ФКП"
Route::get('/registerOfBorrowers/generalRegister', 'RegisterOfBorrowersController@registerOfBorrowersGeneralRegisterPage');//вывод страницы "Загальний реєстр"
Route::post('/registerOfBorrowers/bank/add', 'RegisterOfBorrowersController@registerOfBorrowersBanksAdd');//обработка добавления банка
Route::get('/registerOfBorrowers/bank/{id}/register', 'RegisterOfBorrowersController@registerOfBorrowersBankRegisterPage');//вывод страницы "Реєстр позичальників на надання ФКП" (для каждого банка)
Route::post('/registerOfBorrowers/addRow', 'RegisterOfBorrowersController@registerOfBorrowersAddRow');//обработка добавления строки
Route::post('/registerOfBorrowers/deleteRow', 'RegisterOfBorrowersController@registerOfBorrowersDeleteRow');//обработка удаления строки
Route::post('/registerOfBorrowers/updateRow', 'RegisterOfBorrowersController@registerOfBorrowersUpdateRow');//обработка обновления строки
Route::get('/registerOfBorrowers/generalRegister/search', 'RegisterOfBorrowersController@registerOfBorrowersGeneralRegisterSearchPage');//поиск по таблице
Route::post('/registerOfBorrowers/updateRowKved', 'RegisterOfBorrowersController@registerOfBorrowersUpdateRowKved');//обработка обновления строки

Route::get('/performanceOfImplementation', 'PerformanceOfImplementationController@performanceOfImplementationPage');//вывод страницы "Інформація щодо показників результативності впроваждення ФКП"
Route::get('/performanceOfImplementation/generalInformation', 'PerformanceOfImplementationController@performanceOfImplementationGeneralInformationPage');//вывод страницы "ЗАГАЛЬНА ІНФОРМАЦІЯ ЩОДО ПОКАЗНИКІВ РЕЗУЛЬТАТИВНОСТІ провадження ФКП"
Route::post('/performanceOfImplementation/bank/add', 'PerformanceOfImplementationController@performanceOfImplementationBanksAdd');//обработка добавления банка
Route::get('/performanceOfImplementation/bank/{id}/information', 'PerformanceOfImplementationController@performanceOfImplementationBankInformationPage');//вывод страницы " ІНФОРМАЦІЯ ЩОДО ПОКАЗНИКІВ РЕЗУЛЬТАТИВНОСТІ провадження ФКП"(по банкам)
Route::post('/performanceOfImplementation/addRow', 'PerformanceOfImplementationController@performanceOfImplementationAddRow');//обработка добавления строки
Route::post('/performanceOfImplementation/deleteRow', 'PerformanceOfImplementationController@performanceOfImplementationDeleteRow');//обработка удаления строки
Route::post('/performanceOfImplementation/updateRow', 'PerformanceOfImplementationController@performanceOfImplementationUpdateRow');//обработка обновления строки
Route::get('/performanceOfImplementation/generalInformation/search', 'PerformanceOfImplementationController@performanceOfImplementationSearchPage');//поиск по таблице
Route::post('/performanceOfImplementation/updateRowKved', 'PerformanceOfImplementationController@performanceOfImplementationUpdateRowKved');//обработка обновления строки

Route::get('/breached', 'BreachedController@breachedPage');//вывод страницы "Суб'єкти МСП, які порушили умови кредитних договорів"
Route::post('/breached/add', 'BreachedController@breachedAdd');//обработка формы добавления файла

Route::get('/refusals', 'RefusalsController@refusalsPage');//вывод страницы "Суб'єкти МСП, яким відмовиили у ФКП"
Route::post('/refusals/add', 'RefusalsController@refusalsAdd');//вывод страницы "Суб'єкти МСП, яким відмовиили у ФКП"

Route::post('/getBools', 'HomeController@getBools');//выбрать данные с таблицы `bool`
Route::post('/getPersonal', 'HomeController@getPersonal');//выбрать данные с таблицы `Presonal`
Route::post('/getStacking', 'HomeController@getStacking');//выбрать данные с таблицы `Stacking`
Route::post('/getTotal', 'HomeController@getTotal');//выбрать данные с таблицы `Total`
Route::post('/getTerm', 'HomeController@getTerm');//выбрать данные с таблицы `Term`
Route::post('/getBank', 'HomeController@getBank');//выбрать данные с таблицы `Users` только банки
Route::post('/getRepaymentSchedule', 'HomeController@getRepaymentSchedule');//выбрать данные с таблицы `RepaymentSchedule`
Route::post('/getSizeBorrower', 'HomeController@getSizeBorrower');//выбрать данные с таблицы `Size-Borrower`
Route::post('/getTarget', 'HomeController@getTarget');//выбрать данные с таблицы `Target`
Route::post('/getKved', 'HomeController@getKved');//получение группы или раздела КВЕД 2010

Route::get('/admin', 'AdminController@adminPage');//вывод страницы 'администрирование'
Route::post('/admin/createBank', 'AdminController@createBank');//создание банка
Route::post('/admin/editBank', 'AdminController@editBank');//создание банка
Route::post('/admin/deleteBank', 'AdminController@deleteBank');//удаление банка
Route::post('/borrowers/default', 'AdminController@borrowersDefault');//установление значения по умолчанию

Route::post('/admin/createTarget', 'AdminController@createTarget');//создание цели
Route::post('/admin/editTarget', 'AdminController@editTarget');//редактирование цели
Route::post('/admin/deleteTarget', 'AdminController@deleteTarget');//удаление цели

Route::post('/admin/createKved', 'AdminController@createKved');//создание КВЕД 2010
Route::post('/admin/deleteKved', 'AdminController@deleteKved');//удаление КВЕД 2010
Route::post('/admin/addGroup', 'AdminController@addGroup');//добавление группы КВЕД 2010
Route::post('/admin/editGroup', 'AdminController@editGroup');//редактирование группы КВЕД 2010
Route::post('/admin/removeGroup', 'AdminController@removeGroup');//удаление группы КВЕД 2010
Route::post('/admin/removeClas', 'AdminController@removeClas');//удаление класса КВЕД 2010

Route::get('/infographics', 'InfographicsController@infographicsPage');//вывод страницы "Інфографіка"
Route::post('/addInfographicsImage', 'InfographicsController@addImage');//добавление картинки
Route::post('/infographics/editInformation', 'InfographicsController@editInformation');//обработка редактирования информации

Route::get('/examples', 'ExamplesController@examplesPage');//вывод страницы "Цікаві приклади суб'єктів МСП, які отримали ФКП"
Route::post('/addExamplesImage', 'ExamplesController@addImage');//добавление картинки
Route::post('/examples/editInformation', 'ExamplesController@editInformation');//обработка редактирования информации

/*Графики*/
Route::get('/chart_1', 'RegisterOfBorrowersController@chart_1');//график 1
Route::get('/chart_4_5_6_7', 'RegisterOfBorrowersController@chart_4_5_6_7');//график "структура кредитів по цілях", "сума кредитів по цілях", " сума ФКП по цілях"
Route::get('/chart_8_9_10', 'RegisterOfBorrowersController@chart_8_9_10');//график "структура кредитів по цілях", "сума кредитів по цілях", " сума ФКП по цілях"
Route::get('/chart_11_12_13', 'RegisterOfBorrowersController@chart_11_12_13');//график "сума кредитів по банках","сума ФКП по банках", "кількість підприємств, які отпримали кредити по банках",

Route::get('/performance/chart_1_3_5', 'PerformanceOfImplementationController@chart_1_3_5');//график 1, 3 и 5
Route::get('/performance/chart_2_4_6', 'PerformanceOfImplementationController@chart_2_4_6');//график 2, 4 и 6
Route::get('/performance/chart_7_8_9', 'PerformanceOfImplementationController@chart_7_8_9');//график 7, 8 и 9
Route::get('/performance/chart_10_11_12', 'PerformanceOfImplementationController@chart_10_11_12');//график 10, 11 и 12
