<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use DB;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \App\Console\Commands\ReportMonthBorrowers::class,
        \App\Console\Commands\ReportQuartalBorrowers::class,
        \App\Console\Commands\ReportYearBorrowers::class,
        \App\Console\Commands\ReportMonthApplicants::class,
        \App\Console\Commands\ReportQuartalApplicants::class,
        \App\Console\Commands\ReportYearApplicants::class,
        \App\Console\Commands\ReportMonthPerformance::class,
        \App\Console\Commands\ReportQuartalPerformance::class,
        \App\Console\Commands\ReportYearPerformance::class,
        \App\Console\Commands\DeleteBank::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {        
        //$schedule->command('inspire')->everyMinute();
        //$schedule->command('report:reportMonth')->monthlyOn(1, '00:10');//сохраняет отчет за месяц, запускается 1 числа и берет записи за предыдущий месяц
        $schedule->command('report:reportMonthBorrowers')->monthlyOn(1, '04:00');//сохраняет отчет за месяц, запускается 1 числа и берет записи за предыдущий месяц
        $schedule->command('report:reportYearBorrowers')->monthlyOn(1, '04:00');//сохраняет отчет за месяц, запускается 1 числа и берет записи за предыдущий месяц
        $schedule->command('report:reportQuartalBorrowers')->monthlyOn(1, '04:00');//сохраняет отчет за месяц, запускается 1 числа и берет записи за предыдущий месяц

        $schedule->command('report:reportMonthApplicants')->monthlyOn(1, '04:00');//сохраняет отчет за месяц, запускается 1 числа и берет записи за предыдущий месяц
        $schedule->command('report:reportYearApplicants')->monthlyOn(1, '04:00');//сохраняет отчет за месяц, запускается 1 числа и берет записи за предыдущий месяц
        $schedule->command('report:reportQuartalApplicants')->monthlyOn(1, '04:00');//сохраняет отчет за месяц, запускается 1 числа и берет записи за предыдущий месяц

        $schedule->command('report:reportMonthPerformance')->monthlyOn(1, '04:00');//сохраняет отчет за месяц, запускается 1 числа и берет записи за предыдущий месяц
        $schedule->command('report:reportYearPerformance')->monthlyOn(1, '04:00');//сохраняет отчет за месяц, запускается 1 числа и берет записи за предыдущий месяц
        $schedule->command('report:reportQuartalPerformance')->monthlyOn(1, '04:00');//сохраняет отчет за месяц, запускается 1 числа и берет записи за предыдущий месяц

        $schedule->command('delete:deleteBank')->dailyAt('02:30');//сохраняет отчет за месяц, запускается 1 числа и берет записи за предыдущий месяц
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    /*protected function commands()
    {
        require base_path('routes/console.php');
    }*/
}
