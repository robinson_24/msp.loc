<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\ReportController;

class ReportYearBorrowers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'report:reportYearBorrowers';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'check of lots for the first time';

    private $report;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(
        ReportController $report
    )
    {
        parent::__construct();
        $this->report = $report;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->report->cronReportYearBorrowers();
    }
}