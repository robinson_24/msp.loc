<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\DeleteBankController;

class DeleteBank extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'delete:deleteBank';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'check of lots for the first time';

    private $deleteBank;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(
        DeleteBankController $deleteBank
    )
    {
        parent::__construct();
        $this->deleteBank = $deleteBank;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->deleteBank->cronDeleteBank();
    }
}