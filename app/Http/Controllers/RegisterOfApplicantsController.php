<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Contracts\CommissionInterface;
use App\Http\Controllers\Contracts\BanksInterface;
use App\Http\Controllers\Contracts\UsersInterface;
use App\Http\Controllers\Contracts\ApplicantsRegisterInterface;
use App\Http\Controllers\Contracts\PersonalInterface;
use App\Http\Controllers\Contracts\TotalInterface;
use App\Http\Controllers\Contracts\RepaymentScheduleInterface;
use App\Http\Controllers\Contracts\ReportsInterface;
use App\Http\Controllers\Contracts\KvedInterface;
use Session;
use Request;
use Route;
use Validator;
use App\Libraries\GeneralFunctions;
use Excel;
//use PHPExcel_Style_Border;

class RegisterOfApplicantsController extends Controller 
{

    const COMMANDS = 'commands';
    const PROTOCOLS = 'protocols';

    private $commission;
    private $banks;
    private $users;
    private $applicantsRegister;
    private $personal;
    private $total;
    private $repayment_schedule;
    private $reports;
    private $kved;
    private $nameFileReport;

    public function __construct(
        BanksInterface $banks,
        UsersInterface $users,
        ApplicantsRegisterInterface $applicantsRegister,
        CommissionInterface $commission,
        PersonalInterface $personal,
        TotalInterface $total,
        RepaymentScheduleInterface $repayment_schedule,
        ReportsInterface $reports,
        KvedInterface $kved

    ){
        $this->request = Request::all();
        $this->banks = $banks;
        $this->users = $users;
        $this->applicantsRegister = $applicantsRegister;
        $this->commission = $commission;
        $this->personal = $personal;
        $this->total = $total;
        $this->repayment_schedule = $repayment_schedule;
        $this->reports = $reports;
        $this->kved = $kved;

    }

    public function registerOfApplicantsPage() //вывод страницы "Реєстр* погодження позичальників, які претендують на ФКП"
    {
        if(Session::has('userId')){
            $data = [];

            $where = ['banks.category' => 'registerOfApplicants'];
            $banks = $this->banks->getBanksJoin($where);
            if(count($banks)){
                $data['banks'] = $banks;
            } else{
                $data['banks'] = '';
            }


            if(Session::get('status') == 'departament'){
                $whereUser = ['users.status' => 'bank', 'users.is_delete' => 0];
                $whereUserBank = 'registerOfApplicants';
                $users = $this->users->getUsersBankJoin($whereUser, $whereUserBank);
                if(count($users)){
                    $data['users'] = $users;
                }
            }

            $data = array_merge($data, array(
                'title' => 'Реєстр* погодження позичальників, які претендують на ФКП'
            ));
            return view('/registerOfApplicants/registerOfApplicants', $data);
        }else{
            return redirect('/');
        }
    }

    public function registerOfApplicantsBanksAdd()//автоматическое добавление банка
    {

        $data = [];
        $data['id_bank'] = $_POST['id_bank'];
        $data['category'] = 'registerOfApplicants';

        $result = $this->banks->addBanks($data);

        return json_encode($result);
    }

    public function registerOfApplicantsCommissionPage() //вывод страницы "Комісія по погодженню надання ФКП суб'єктам МСП"
    {
        if(Session::has('userId')){
            $data = [];
            $data = array_merge($data, array(
                'userId' => Session::get('userId'),
                'title' => 'Комісія по погодженню надання ФКП суб\'єктам МСП'
            ));
            return view('/registerOfApplicants/commission', $data);
        }else{
            return redirect('/');
        }
    }

    public function registerOfApplicantsCommissionComProtPage() //вывод страницы "Наказ" или "Протоклы"
    {
        if(Session::has('userId')){
            if(Route::input('category') && is_string(Route::input('category'))){
                $data = [];
                $category = Route::input('category');
                $where = ['commission.category' => $category];
                $comProt = $this->commission->getCommission($where);
                if(count($comProt)){
                    $data['comProt'] = $comProt;
                }

                if($category == self::COMMANDS){
                    $data['title'] = 'Наказ';
                }
                if($category == self::PROTOCOLS){
                    $data['title'] = 'Протоколи';
                }

                $data = array_merge($data, array(
                    'category' => $category
                ));

                return view('/registerOfApplicants/comprot', $data);
            } else {
                return redirect('404');
            }
        } else{
            return redirect('/');
        }
    }

    public function registerOfApplicantsCommissionComProtAdd()// добавление "Наказ" или "Протоклы"
    {
        if(Session::has('userId')){
            if(Route::input('category') && is_string(Route::input('category'))){

                $category = Route::input('category');

                unset($this->request['_token']);

                $validator = Validator::make(
                    $this->request,[
                        'name' => 'required'
                    ],[
                        'name.required' => 'Поле має бути обов\'язково заповнене.'
                    ]
                );

                if($validator->fails()){
                    return redirect('/registerOfApplicants/commission'.'/'.$category)->with('error', $validator->messages());
                }else{
                    $data = [];

                    if(!empty($this->request['pdf'])){
                        $pathPDF = 'files/registerOfApplicants/commission/'.$category.'/';
                        $filePDF = 'pdf';
                        $resultPDF = GeneralFunctions::getNameFile($pathPDF, $filePDF);
                        if($resultPDF != false){
                            $data['url_pdf'] = $resultPDF;
                        }
                    }

                    $data = array_merge($data, array(
                        'category' => $category,
                        'name' => $this->request['name']
                    ));
                    ;

                    $this->commission->addCommission($data);

                    return redirect('/registerOfApplicants/commission'.'/'.$category);
                }

            }else{
                return redirect('404');
            }
        }else{
            return redirect('/');
        }
    }

    public function registerOfApplicantsGeneralRegisterPage() //вывод страницы "ЗАГАЛЬНИЙ РЕЄСТР"
    {
        if(Session::has('userId')){
            $data = [];

            if(Session::get('status') == 'departament'){

                $generalRegister = $this->applicantsRegister->getApplicantsRegisterJoin(null, true);
                if(count($generalRegister)){
                    foreach ($generalRegister as $key => $value) {
                        if($key == 'applicantsRegisterJoin'){
                            $data['generalRegister'] = $generalRegister['applicantsRegisterJoin'];
                        } else{
                            $data[$key] = $value;
                        }
                    }
                }

                $data['reports'] = $this->reports->getReports(['reports.category' => 'registerOfApplicants']);

            }

            $data = array_merge($data, array(
                'idBank' =>Session::get('userId'),
                'title' => 'ЗАГАЛЬНИЙ РЕЄСТР',
                'subTitle' => 'погодження позичальників, які можуть претендувати на ФКП'
            ));
            return view('/registerOfApplicants/generalRegister', $data);
        }else{
            return redirect('/');
        }
    }

    public function registerOfApplicantsBankRegisterPage() //вывод страницы "РЕЄСТР погодження позичальників, які можуть претендувати на ФКП"
    {
        if(Session::has('userId')){
            if(Route::input('id') && is_numeric(Route::input('id'))){
                $data = [];
                $idBank = Route::input('id');
                
                $whereBank = ['banks.id_bank' => $idBank];
                $bank = $this->banks->getBanksJoin($whereBank);
                if(count($bank)){

                    $where = [];
                    $where[] = [
                        'field'  => 'applicantsRegister.id_bank',
                        'symbol' => '=',
                        'value'  => $idBank
                    ];
                    $applicantsRegister = $this->applicantsRegister->getApplicantsRegisterJoin($where);
                    if(count($applicantsRegister)){
                        $data['applicantsRegister'] = $applicantsRegister['applicantsRegisterJoin'];
                    }

                    $data = array_merge($data, array(
                        'name' => $bank[0]->name,
                        'title' => 'РЕЄСТР',
                        'subTitle' => 'погодження позичальників, які можуть претендувати на ФКП',
                        'idBank' => $idBank
                    ));
                    
                    return view('/registerOfApplicants/register', $data);

                } else{
                    return redirect('404');
                }
            } else{
                return redirect('404');
            }
        }else{
            return redirect('/');
        }
    }

    public function registerOfApplicantsAddRow()//добавление строки в реестре
    {
        if(Session::has('userId') && Session::get('status') == 'bank'){
            $data['id_bank'] = Session::get('userId');
            $data['col_24'] = Date('Y-m-d');
            $result = $this->applicantsRegister->addApplicantsRegister($data);
            return json_encode($result);
        } else{
            return json_encode('false');
        }
    }

    public function registerOfApplicantsDeleteRow()//удаление строки в реестре
    {
        if(Session::has('userId') && Session::get('status') == 'bank'){
            $id = $_POST['id'];
            $where = [
                'id'      => $id,
                'id_bank' => Session::get('userId')
            ];
            try {
                $result = $this->applicantsRegister->getApplicantsRegister($where);

                $this->repayment_schedule->deleteRepaymentSchedule([
                        'repayment_schedule.id' => $result[0]['col_17'],
                        'repayment_schedule.is_default' => 0
                    ]);

                $this->applicantsRegister->deleteApplicantsRegister($where);
            } catch (\Exception $e) {
                }
            return json_encode('true');
        }
    }

    public function registerOfApplicantsUpdateRow()//обновление строки в реестре
    {
        if(Session::has('userId') && Session::get('status') == 'bank'){
            $id = $_POST['id'];
            $val = $_POST['val'];
            $col = $_POST['col'];
            $idOld = $_POST['idOld'];
            if($idOld != null){
                $this->repayment_schedule->deleteRepaymentSchedule([
                    'repayment_schedule.id' => $idOld,
                    'repayment_schedule.is_default' => 0
                ]);
            }

            $id_bank = Session::get('userId');

            $where = [
                'id' => $id,
                'id_bank' => $id_bank
            ];
            $data = [
                'col_'.$col => $val
            ];

            $result = $this->applicantsRegister->updateApplicantsRegister($where, $data);
            return json_encode($result);
        }
    }

    public function registerOfApplicantsUpdateRowKved()//обновление строки в реестре
    {
        if(Session::has('userId') && Session::get('status') == 'bank'){
            $id = $_POST['id'];
            $col = $_POST['col'];
            $section = $_POST['idSection'];
            $group = $_POST['idGroup'];
            $clas = $_POST['idClas'];

            $id_bank = Session::get('userId');

            $where = [
                'id' => $id,
                'id_bank' => $id_bank
            ];
            $data = [
                'col_'.$col.'_section' => $section,
                'col_'.$col.'_group' => $group,
                'col_'.$col.'_clas' => $clas
            ];

            $result = $this->applicantsRegister->updateApplicantsRegister($where, $data);
            return json_encode($result);
        }
    }

    public function registerOfApplicantsGeneralRegisterSearchPage()//поиск по реестру
    {
        if(!empty($_GET)){
            $data = [];
            $where = [];
            $between = [];
            $whereYear = [];
            $whereMonth = [];
            $whereQuarte = [];

            if(isset($_GET['field_1']) && isset($_GET['val_1']) && isset($_GET['type_1'])){
                switch ($_GET['type_1']) {
                    case 'stacking':
                        $where[] = [
                            'field'  => 'applicantsRegister.'.$_GET['field_1'],
                            'symbol' => '=',
                            'value'  => $_GET['val_1']
                        ];
                        break;

                    case 'target':
                        $where[] = [
                            'field'  => 'applicantsRegister.'.$_GET['field_1'],
                            'symbol' => '=',
                            'value'  => $_GET['val_1']
                        ];
                        break;

                    case 'term':
                        $where[] = [
                            'field'  => 'applicantsRegister.'.$_GET['field_1'],
                            'symbol' => '=',
                            'value'  => $_GET['val_1']
                        ];
                        break;

                    case 'kved':
                        $vals = explode('_', $_GET['val_1']);
                        if($vals[0] != 0){
                            $where[] = [
                                'field'  => 'applicantsRegister.'.$_GET['field_1'].'_section',
                                'symbol' => '=',
                                'value'  => $vals[0]
                            ];
                        }
                        if($vals[1] != 0 && $vals[1] != 'undefined'){
                            $where[] = [
                                'field'  => 'applicantsRegister.'.$_GET['field_1'].'_group',
                                'symbol' => '=',
                                'value'  => $vals[1]
                            ];
                        }
                        if($vals[2] != 0 && $vals[2] != 'undefined'){
                            $where[] = [
                                'field'  => 'applicantsRegister.'.$_GET['field_1'].'_clas',
                                'symbol' => '=',
                                'value'  => $vals[2]
                            ];
                        }
                        break;

                    case 'bank':
                        $where[] = [
                            'field'  => 'applicantsRegister.'.$_GET['field_1'],
                            'symbol' => '=',
                            'value'  => $_GET['val_1']
                        ];
                        break;

                    case 'date':
                        $where[] = [
                            'field'  => 'applicantsRegister.'.$_GET['field_1'],
                            'symbol' => '=',
                            'value'  => date("Y-m-d", strtotime($_GET['val_1']))
                        ];
                        break;

                    case 'text':
                        $where[] = [
                            'field'  => 'applicantsRegister.'.$_GET['field_1'],
                            'symbol' => 'like',
                            'value'  => '%'.$_GET['val_1'].'%'
                        ];
                        break;

                    case 'personal':
                        preg_match_all("/\d+/", $_GET['val_1'], $matches);
                        if(count($matches[0]) == 2){
                            $between = [
                                'applicantsRegister.'.$_GET['field_1'] => array(intval($matches[0][0]), intval($matches[0][1]))
                            ];
                        } else if(count($matches[0]) == 1 && intval($matches[0][0]) == 50){
                            $where[] = [
                                'field'  => 'applicantsRegister.'.$_GET['field_1'],
                                'symbol' => '<',
                                'value'  => intval($matches[0][0])
                            ];
                        } else if(count($matches[0]) == 1 && intval($matches[0][0]) == 250){
                            $where[] = [
                                'field'  => 'applicantsRegister.'.$_GET['field_1'],
                                'symbol' => '>',
                                'value'  => intval($matches[0][0])
                            ];
                        }
                        break;
                    
                    case 'total':
                        preg_match_all("/\d+/", $_GET['val_1'], $matches);
                        if(count($matches[0]) == 2){
                            $between = [
                                'applicantsRegister.'.$_GET['field_1'] => array(intval($matches[0][0])*1000, intval($matches[0][1])*1000)
                            ];
                        } else if(count($matches[0]) == 1 && intval($matches[0][0]) == 100){
                            $where[] = [
                                'field'  => 'applicantsRegister.'.$_GET['field_1'],
                                'symbol' => '<',
                                'value'  => intval($matches[0][0])*1000
                            ];
                        } else if(count($matches[0]) == 1 && intval($matches[0][0]) == 5000){
                            $where[] = [
                                'field'  => 'applicantsRegister.'.$_GET['field_1'],
                                'symbol' => '>',
                                'value'  => intval($matches[0][0])*1000
                            ];
                        }
                        break;

                    case 'dateSelect':
                        if($_GET['val_1'] == 'щоквартально'){
                            $whereYear = ['applicantsRegister.created_at' => date('Y')];
                            if(date('m') == '01'|| date('m') == '02'|| date('m') == '03'){
                                $whereQuarte = array([
                                    'field' => 'applicantsRegister.created_at',
                                    'symbol' => '>=',
                                    'value' => '01'
                                ], [
                                    'field' => 'applicantsRegister.created_at',
                                    'symbol' => '<=',
                                    'value' => '03'
                                ]);
                            } elseif(date('m') == '04'|| date('m') == '04'|| date('m') == '06'){
                                $whereQuarte = array([
                                    'field' => 'applicantsRegister.created_at',
                                    'symbol' => '>=',
                                    'value' => '04'
                                ], [
                                    'field' => 'applicantsRegister.created_at',
                                    'symbol' => '<=',
                                    'value' => '06'
                                ]);
                            } elseif(date('m') == '07'|| date('m') == '08'|| date('m') == '09'){
                                $whereQuarte = array([
                                    'field' => 'applicantsRegister.created_at',
                                    'symbol' => '>=',
                                    'value' => '07'
                                ], [
                                    'field' => 'applicantsRegister.created_at',
                                    'symbol' => '<=',
                                    'value' => '09'
                                ]);
                            } elseif(date('m') == '10'|| date('m') == '11'|| date('m') == '12'){
                                $whereQuarte = array([
                                    'field' => 'applicantsRegister.created_at',
                                    'symbol' => '>=',
                                    'value' => '10'
                                ], [
                                    'field' => 'applicantsRegister.created_at',
                                    'symbol' => '<=',
                                    'value' => '12'
                                ]);
                            }
                        } elseif($_GET['val_1'] == 'щорічно'){
                            $whereYear = ['applicantsRegister.created_at' => date('Y')];
                        } elseif($_GET['val_1'] == 'щомісячно'){
                            $whereYear = ['applicantsRegister.created_at' => date('Y')];
                            $whereMonth = ['applicantsRegister.created_at' => date('m')];
                        } else{
                            $where[] = [
                                'field'  => 'applicantsRegister.col_24',
                                'symbol' => '=',
                                'value'  => date("Y-m-d", strtotime($_GET['val_1']))
                            ];
                        }
                        break;

                    default:
                        # code...
                        break;
                }
            }

            if(isset($_GET['field_2']) && isset($_GET['val_2']) && isset($_GET['type_2'])){
                switch ($_GET['type_2']) {
                    case 'stacking':
                        $where[] = [
                            'field'  => 'applicantsRegister.'.$_GET['field_2'],
                            'symbol' => '=',
                            'value'  => $_GET['val_2']
                        ];
                        break;

                    case 'term':
                        $where[] = [
                            'field'  => 'applicantsRegister.'.$_GET['field_2'],
                            'symbol' => '=',
                            'value'  => $_GET['val_2']
                        ];
                        break;

                    case 'kved':
                        $vals = explode('_', $_GET['val_2']);
                        if($vals[0] != 0){
                            $where[] = [
                                'field'  => 'applicantsRegister.'.$_GET['field_2'].'_section',
                                'symbol' => '=',
                                'value'  => $vals[0]
                            ];
                        }
                        if($vals[1] != 0 && $vals[1] != 'undefined'){
                            $where[] = [
                                'field'  => 'applicantsRegister.'.$_GET['field_2'].'_group',
                                'symbol' => '=',
                                'value'  => $vals[1]
                            ];
                        }
                        if($vals[2] != 0 && $vals[2] != 'undefined'){
                            $where[] = [
                                'field'  => 'applicantsRegister.'.$_GET['field_2'].'_clas',
                                'symbol' => '=',
                                'value'  => $vals[2]
                            ];
                        }
                        break;

                    case 'bank':
                        $where[] = [
                            'field'  => 'applicantsRegister.'.$_GET['field_2'],
                            'symbol' => '=',
                            'value'  => $_GET['val_2']
                        ];
                        break;

                    case 'date':
                        $where[] = [
                            'field'  => 'applicantsRegister.'.$_GET['field_2'],
                            'symbol' => '=',
                            'value'  => date("Y-m-d", strtotime($_GET['val_2']))
                        ];
                        break;

                    case 'text':
                        $where[] = [
                            'field'  => 'applicantsRegister.'.$_GET['field_2'],
                            'symbol' => 'like',
                            'value'  => '%'.$_GET['val_2'].'%'
                        ];
                        break;

                    case 'personal':
                        preg_match_all("/\d+/", $_GET['val_2'], $matches);
                        if(count($matches[0]) == 2){
                            $between = [
                                'applicantsRegister.'.$_GET['field_2'] => array(intval($matches[0][0]), intval($matches[0][1]))
                            ];
                        } else if(count($matches[0]) == 1 && intval($matches[0][0]) == 50){
                            $where[] = [
                                'field'  => 'applicantsRegister.'.$_GET['field_2'],
                                'symbol' => '<',
                                'value'  => intval($matches[0][0])
                            ];
                        } else if(count($matches[0]) == 1 && intval($matches[0][0]) == 250){
                            $where[] = [
                                'field'  => 'applicantsRegister.'.$_GET['field_2'],
                                'symbol' => '>',
                                'value'  => intval($matches[0][0])
                            ];
                        }
                        break;
                    
                    case 'total':
                        preg_match_all("/\d+/", $_GET['val_2'], $matches);
                        if(count($matches[0]) == 2){
                            $between = [
                                'applicantsRegister.'.$_GET['field_2'] => array(intval($matches[0][0])*1000, intval($matches[0][1])*1000)
                            ];
                        } else if(count($matches[0]) == 1 && intval($matches[0][0]) == 100){
                            $where[] = [
                                'field'  => 'applicantsRegister.'.$_GET['field_2'],
                                'symbol' => '<',
                                'value'  => intval($matches[0][0])*1000
                            ];
                        } else if(count($matches[0]) == 1 && intval($matches[0][0]) == 5000){
                            $where[] = [
                                'field'  => 'applicantsRegister.'.$_GET['field_2'],
                                'symbol' => '>',
                                'value'  => intval($matches[0][0])*1000
                            ];
                        }
                        break;

                    case 'dateSelect':
                        if($_GET['val_2'] == 'щоквартально'){
                            $whereYear = ['applicantsRegister.created_at' => date('Y')];
                            if(date('m') == '01'|| date('m') == '02'|| date('m') == '03'){
                                $whereQuarte = array([
                                    'field' => 'applicantsRegister.created_at',
                                    'symbol' => '>=',
                                    'value' => '01'
                                ], [
                                    'field' => 'applicantsRegister.created_at',
                                    'symbol' => '<=',
                                    'value' => '03'
                                ]);
                            } elseif(date('m') == '04'|| date('m') == '04'|| date('m') == '06'){
                                $whereQuarte = array([
                                    'field' => 'applicantsRegister.created_at',
                                    'symbol' => '>=',
                                    'value' => '04'
                                ], [
                                    'field' => 'applicantsRegister.created_at',
                                    'symbol' => '<=',
                                    'value' => '06'
                                ]);
                            } elseif(date('m') == '07'|| date('m') == '08'|| date('m') == '09'){
                                $whereQuarte = array([
                                    'field' => 'applicantsRegister.created_at',
                                    'symbol' => '>=',
                                    'value' => '07'
                                ], [
                                    'field' => 'applicantsRegister.created_at',
                                    'symbol' => '<=',
                                    'value' => '09'
                                ]);
                            } elseif(date('m') == '10'|| date('m') == '11'|| date('m') == '12'){
                                $whereQuarte = array([
                                    'field' => 'applicantsRegister.created_at',
                                    'symbol' => '>=',
                                    'value' => '10'
                                ], [
                                    'field' => 'applicantsRegister.created_at',
                                    'symbol' => '<=',
                                    'value' => '12'
                                ]);
                            }
                        } elseif($_GET['val_2'] == 'щорічно'){
                            $whereYear = ['applicantsRegister.created_at' => date('Y')];
                        } elseif($_GET['val_2'] == 'щомісячно'){
                            $whereYear = ['applicantsRegister.created_at' => date('Y')];
                            $whereMonth = ['applicantsRegister.created_at' => date('m')];
                        } else{
                            $where[] = [
                                'field'  => 'applicantsRegister.col_24',
                                'symbol' => '=',
                                'value'  => date("Y-m-d", strtotime($_GET['val_2']))
                            ];
                        }
                        break;

                    default:
                        # code...
                        break;
                }
            }

            if(count($where) == 0){
                $where = null;
            }

            if(count($between) == 0){
                $between = null;
            }


            $generalRegister = $this->applicantsRegister->getApplicantsRegisterJoin($where, true, $between, $whereYear, $whereMonth, $whereQuarte);
            if(count($generalRegister)){
                foreach ($generalRegister as $key => $value) {
                    if($key == 'applicantsRegisterJoin'){
                        $data['generalRegister'] = $generalRegister['applicantsRegisterJoin'];
                    } else{
                        $data[$key] = $value;
                    }
                    
                }
            }
            $data = array_merge($data, array(
                'idBank'   => Session::get('userId'),
                'title'    => 'ЗАГАЛЬНИЙ РЕЄСТР',
                'subTitle' => 'погодження позичальників, які можуть претендувати на ФКП'
            ));
            return view('/registerOfApplicants/generalRegister', $data);
        } else{
            return redirect('/registerOfApplicants/generalRegister');
        }
    }

    public function saveReport($where = null, $sum = null, $between = null, $whereYear = null, $whereMonth = null, $whereQuarte = null, $name = null)
    {
        $data = [];

        $generalRegister = $this->applicantsRegister->getApplicantsRegisterJoin($where, $sum, $between, $whereYear, $whereMonth, $whereQuarte);
        if(count($generalRegister)){
            foreach ($generalRegister as $key => $value) {
                if($key == 'applicantsRegisterJoin'){
                    $data['generalRegister'] = $generalRegister['applicantsRegisterJoin'];
                } else{
                    $data[$key] = $value;
                }
            }
        }

        $nameFile = self::ExcelReport($data);

        $value = [
            'name' => $name,
            'category' => 'registerOfApplicants',
            'url' => $nameFile
        ];
        $this->reports->addReports($value);
        
        return redirect('/registerOfApplicants/generalRegister');

    }

    public function updateCol()//обновление базы если ввели что-то новое
    {
        if(Session::has('userId') && Session::get('status') == 'bank'){
            $id = $_POST['id'];
            $val = GeneralFunctions::trimString($_POST['val'], true);
            $col = $_POST['col'];
            $idOld = $_POST['idOld'];
            
            if($idOld != 1 || $idOld != 2 || $idOld != 3 || $idOld != null){
                $this->repayment_schedule->deleteRepaymentSchedule([
                    'repayment_schedule.id' => $idOld,
                    'repayment_schedule.is_default' => 0
                ]);

                $id_bank = Session::get('userId');

                $value = [
                    'repayment_schedule.repayment_schedule' => $val,
                    'is_default' => 0
                ];
                $result = $this->repayment_schedule->addRepaymentSchedule($value);
            } elseif($idOld == null){
                $value = [
                    'repayment_schedule.repayment_schedule' => $val,
                    'is_default' => 0
                ];
                $result = $this->repayment_schedule->addRepaymentSchedule($value);
            }

            $where = [
                'id' => $id,
                'id_bank' => $id_bank
            ];
            $data = [
                'col_'.$col => $result['id']
            ];

            $this->applicantsRegister->updateApplicantsRegister($where, $data);
            return json_encode($result['id']);
        }
    }

    public function ExcelReport($data)
    {
        $nameFile = md5(microtime());

        Excel::create($nameFile, function($excel) use ($data) {

            $excel->sheet('ЗАГАЛЬНИЙ РЕЄСТР', function($sheet) use ($data) {
                $sheet->setStyle(array(
                    'font' => array(
                        'name' => 'Arial',
                        'size' => 12
                    ),
                ));

                $sheet->setAutoSize(true);
                //$cell->setAutoSize(true);

                $sheet->mergeCells('A1:Y1');
                $sheet->cell('A1', function ($cell) {
                    $cell->setValue('ЗАГАЛЬНИЙ РЕЄСТР');
                    $cell->setAlignment('center');
                    $cell->setValignment('center');

                });

                $sheet->mergeCells('A2:Y2');
                $sheet->cell('A2', function ($cell) {
                    $cell->setValue('погодження позичальників, які можуть претендувати на ФКП');
                    $cell->setAlignment('center');
                    $cell->setValignment('center');
                });

                $sheet->mergeCells('A3:A5');
                $sheet->cell('A3', function ($cell) {
                    $cell->setValue('№');
                    $cell->setAlignment('center');
                    $cell->setValignment('center');
                });

                $sheet->mergeCells('B3:B5');
                $sheet->cell('B3', function ($cell) {
                    $cell->setValue('Назва');
                    $cell->setAlignment('center');
                    $cell->setValignment('center');
                });

                $sheet->mergeCells('C3:C5');
                $sheet->cell('C3', function ($cell) {
                    $cell->setValue('ЄДРПОУ');
                    $cell->setAlignment('center');
                    $cell->setValignment('center');
                });

                $sheet->mergeCells('D3:E3');
                $sheet->cell('D3', function ($cell) {
                    $cell->setValue('Критерій 1');
                    $cell->setAlignment('center');
                    $cell->setValignment('center');
                });
                $sheet->mergeCells('D4:D5');
                $sheet->cell('D4', function ($cell) {
                    $cell->setValue('Місце державної реєстрації');
                    $cell->setAlignment('center');
                    $cell->setValignment('center');
                });
                $sheet->mergeCells('E4:E5');
                $sheet->cell('E4', function ($cell) {
                    $cell->setValue('Розміщення виробничих потужностей');
                    $cell->setAlignment('center');
                    $cell->setValignment('center');
                });

                $sheet->cell('F3', function ($cell) {
                    $cell->setValue('Критерій 2');
                    $cell->setAlignment('center');
                    $cell->setValignment('center');
                });
                $sheet->mergeCells('F4:F5');
                $sheet->cell('F4', function ($cell) {
                    $cell->setValue('КВЕД 2010 /розділ/група/клас');
                    $cell->setAlignment('center');
                    $cell->setValignment('center');
                });

                $sheet->cell('G3', function ($cell) {
                    $cell->setValue('Критерій 3');
                    $cell->setAlignment('center');
                    $cell->setValignment('center');
                });
                $sheet->mergeCells('G4:G5');
                $sheet->cell('G4', function ($cell) {
                    $cell->setValue('Ціль кредиту');
                    $cell->setAlignment('center');
                    $cell->setValignment('center');
                });

                $sheet->cell('H3', function ($cell) {
                    $cell->setValue('Критерій 4');
                    $cell->setAlignment('center');
                    $cell->setValignment('center');
                });
                $sheet->mergeCells('H4:H5');
                $sheet->cell('H4', function ($cell) {
                    $cell->setValue('Особистий вклад МСП');
                    $cell->setAlignment('center');
                    $cell->setValignment('center');
                });

                $sheet->mergeCells('I3:I5');
                $sheet->cell('I3', function ($cell) {
                    $cell->setValue('Відсутність просроченого боргу');
                    $cell->setAlignment('center');
                    $cell->setValignment('center');
                });

                $sheet->mergeCells('J3:J5');
                $sheet->cell('J3', function ($cell) {
                    $cell->setValue('Незалежність МСП');
                    $cell->setAlignment('center');
                    $cell->setValignment('center');
                });

                $sheet->mergeCells('K3:K5');
                $sheet->cell('K3', function ($cell) {
                    $cell->setValue('Річний дохід, тис.грн');
                    $cell->setAlignment('center');
                    $cell->setValignment('center');
                });

                $sheet->mergeCells('L3:L5');
                $sheet->cell('L3', function ($cell) {
                    $cell->setValue('Чисельність персоналу, одиниць');
                    $cell->setAlignment('center');
                    $cell->setValignment('center');
                });

                $sheet->mergeCells('M3:V3');
                $sheet->cell('M3', function ($cell) {
                    $cell->setValue('Кредитний догорів');
                    $cell->setAlignment('center');
                    $cell->setValignment('center');
                });

                $sheet->mergeCells('M4:M5');
                $sheet->cell('M4', function ($cell) {
                    $cell->setValue('Номер');
                    $cell->setAlignment('center');
                    $cell->setValignment('center');
                });

                $sheet->mergeCells('N4:N5');
                $sheet->cell('N4', function ($cell) {
                    $cell->setValue('Дата');
                    $cell->setAlignment('center');
                    $cell->setValignment('center');
                });

                $sheet->mergeCells('O4:O5');
                $sheet->cell('O4', function ($cell) {
                    $cell->setValue('Строк');
                    $cell->setAlignment('center');
                    $cell->setValignment('center');
                });

                $sheet->mergeCells('P4:P5');
                $sheet->cell('P4', function ($cell) {
                    $cell->setValue('Сума, грн');
                    $cell->setAlignment('center');
                    $cell->setValignment('center');
                });

                $sheet->mergeCells('Q4:Q5');
                $sheet->cell('Q4', function ($cell) {
                    $cell->setValue('Графік погашення');
                    $cell->setAlignment('center');
                    $cell->setValignment('center');
                });

                $sheet->mergeCells('R4:R5');
                $sheet->cell('R4', function ($cell) {
                    $cell->setValue('Кінцеве погашення');
                    $cell->setAlignment('center');
                    $cell->setValignment('center');
                });

                $sheet->mergeCells('S4:S5');
                $sheet->cell('S4', function ($cell) {
                    $cell->setValue('Ставка, %');
                    $cell->setAlignment('center');
                    $cell->setValignment('center');
                });

                $sheet->mergeCells('T4:V4');
                $sheet->cell('T4', function ($cell) {
                    $cell->setValue('ФКП');
                    $cell->setAlignment('center');
                    $cell->setValignment('center');
                });
                $sheet->cell('T5', function ($cell) {
                    $cell->setValue('Щомісячна сума, грн');
                    $cell->setAlignment('center');
                    $cell->setValignment('center');
                });
                $sheet->cell('U5', function ($cell) {
                    $cell->setValue('Період виплат (не більше 24 міс.)');
                    $cell->setAlignment('center');
                    $cell->setValignment('center');
                });
                $sheet->cell('V5', function ($cell) {
                    $cell->setValue('Загальна сума, грн');
                    $cell->setAlignment('center');
                    $cell->setValignment('center');
                });

                $sheet->mergeCells('W3:W5');
                $sheet->cell('W3', function ($cell) {
                    $cell->setValue('Результат розгляду (*)');
                    $cell->setAlignment('center');
                    $cell->setValignment('center');
                });
                $sheet->mergeCells('X3:X5');
                $sheet->cell('X3', function ($cell) {
                    $cell->setValue('Банк-партнер');
                    $cell->setAlignment('center');
                    $cell->setValignment('center');
                });
                $sheet->mergeCells('Y3:Y5');
                $sheet->cell('Y3', function ($cell) {
                    $cell->setValue('Дата заповнення реєстру');
                    $cell->setAlignment('center');
                    $cell->setValignment('center');
                });

                //$sheet->setAutoSize(true);

                $sheet->row(6, [
                    1,
                    2,
                    3,
                    4,
                    5,
                    6,
                    7,
                    8,
                    9,
                    10,
                    11,
                    12,
                    13,
                    14,
                    15,
                    16,
                    17,
                    18,
                    19,
                    20,
                    21,
                    22,
                    23,
                    24,
                    25
                ]);
                $sheet->row(6, function ($row) {
                    $row->setAlignment('center');
                    $row->setValignment('center');
                });

                if (count($data['generalRegister'])) {
                    for ($i = 0; $i < count($data['generalRegister']); $i++) {
                        $sheet->row($i + 7, [
                            $i + 1,
                            $data['generalRegister'][$i]->col_2,
                            $data['generalRegister'][$i]->col_3,
                            $data['generalRegister'][$i]->col_4,
                            $data['generalRegister'][$i]->col_5,
                            $data['generalRegister'][$i]->col_6_section.' / '.$data['generalRegister'][$i]->col_6_group.' / '.$data['generalRegister'][$i]->col_6_clas,
                            $data['generalRegister'][$i]->col_7,
                            $data['generalRegister'][$i]->col_8,
                            $data['generalRegister'][$i]->col_9,
                            $data['generalRegister'][$i]->col_10,
                            $data['generalRegister'][$i]->col_11 / 1000,
                            $data['generalRegister'][$i]->col_12,
                            $data['generalRegister'][$i]->col_13,
                            $data['generalRegister'][$i]->col_14,
                            $data['generalRegister'][$i]->col_15,
                            $data['generalRegister'][$i]->col_16,
                            $data['generalRegister'][$i]->col_17,
                            \Carbon\Carbon::parse($data['generalRegister'][$i]->col_18)->format('d.m.Y'),
                            $data['generalRegister'][$i]->col_19,
                            $data['generalRegister'][$i]->col_20,
                            $data['generalRegister'][$i]->col_21,
                            $data['generalRegister'][$i]->col_22,
                            $data['generalRegister'][$i]->col_23,
                            $data['generalRegister'][$i]->name,
                            \Carbon\Carbon::parse($data['generalRegister'][$i]->col_24)->format('d.m.Y')
                        ]);

                        if ($i == count($data['generalRegister']) - 1) {
                            $sheet->row($i + 7, [
                                '',
                                '',
                                '',
                                '',
                                '',
                                '',
                                '',
                                '',
                                '',
                                '',
                                $data['sum_11'] / 1000,
                                $data['sum_12'],
                                '',
                                '',
                                '',
                                $data['sum_16'],
                                '',
                                '',
                                '',
                                $data['sum_20'],
                                '',
                                $data['sum_22'],
                                '',
                                '',
                                ''
                            ]);
                        }
                    }
                } else{
                    $sheet->mergeCells('A7:Y7');
                    $sheet->cell('A7', function($cell) {
                        $cell->setValue('Інформації немає');
                        $cell->setAlignment('center');
                        $cell->setValignment('center');
                    });
                }
            });
        
        })->store('xls', 'public/files/reports/');
        
        return $nameFile.'.xls';
    }
}