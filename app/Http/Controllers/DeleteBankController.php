<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Contracts\UsersInterface;
use App\Http\Controllers\Contracts\BanksInterface;
use App\Http\Controllers\Contracts\ApplicantsRegisterInterface;
use App\Http\Controllers\Contracts\BorrowersRegisterInterface;
use App\Http\Controllers\Contracts\PerformanceInterface;
use App\Http\Controllers\Contracts\ContractsBankWithGufInterface;
use App\Http\Controllers\Contracts\ContractsBankWithDPRPInterface;
use App\Http\Controllers\Contracts\InformationInterface;
use App\Http\Controllers\Contracts\ImagesInterface;
use App\Http\Controllers\Contracts\RepaymentScheduleInterface;
/*use App\Http\Controllers\RegisterOfApplicantsController;
use App\Http\Controllers\RegisterOfBorrowersController;
use App\Http\Controllers\PerformanceOfImplementationController;
use App\Http\Controllers\Contracts\DefsInterface;*/

class DeleteBankController extends Controller
{
    private $users;
    private $banks;
    private $applicantsRegister;
    private $borrowersRegister;
    private $performance;
    private $contractsBankWithGuf;
    private $contractsBankWithDPRP;
    private $information;
    private $images;
    private $repayment_schedule;
    /*private $performanceController;
    private $borrowersController;
    private $applicantsController;*/

    public function __construct(
        UsersInterface $users,
        BanksInterface $banks,
        ApplicantsRegisterInterface $applicantsRegister,
        BorrowersRegisterInterface $borrowersRegister,
        PerformanceInterface $performance,
        ContractsBankWithGufInterface $contractsBankWithGuf,
        ContractsBankWithDPRPInterface $contractsBankWithDPRP,
        InformationInterface $information,
        ImagesInterface $images,
        RepaymentScheduleInterface $repayment_schedule
        /*RegisterOfBorrowersController $borrowersController,
        RegisterOfApplicantsController $applicantsController,
        PerformanceOfImplementationController $performanceController*/
    ){
        $this->users = $users;
        $this->banks = $banks;
        $this->applicantsRegister = $applicantsRegister;
        $this->borrowersRegister = $borrowersRegister;
        $this->performance = $performance;
        $this->contractsBankWithDPRP = $contractsBankWithDPRP;
        $this->contractsBankWithGuf = $contractsBankWithGuf;
        $this->information = $information;
        $this->images = $images;
        $this->repayment_schedule = $repayment_schedule;
        /*$this->performanceController = $performanceController;
        $this->borrowersController = $borrowersController;
        $this->applicantsController = $applicantsController;*/
    }

    public function cronDeleteBank()
    {
$begin = time();
        $users = $this->users->getUsers(['users.is_delete' => 1, 'users.status' => 'bank']);
        if(count($users)){
            for($i = 0; $i < count($users); $i++){
                $idBank = $users[$i]['id'];
                
                $registerOfApplicants = $this->applicantsRegister->getApplicantsRegister(['applicantsRegister.id_bank' => $idBank]);
                if(count($registerOfApplicants)){
                    for ($j = 0; $j < count($registerOfApplicants); $j++) { 
                        if($registerOfApplicants[$j]['col_17'] > 3){
                            $this->repayment_schedule->deleteRepaymentSchedule(['repayment_schedule.id' => $registerOfApplicants[$j]['col_17']]);//удаление с таблицы 'repayment_schedule'
                        }
                    }
                    $this->applicantsRegister->deleteApplicantsRegister(['applicantsRegister.id_bank' => $idBank]);//удаление с таблицы 'applicantsRegister'
                }

                $contractsBankWithGuf = $this->contractsBankWithGuf->getContractsBankWithGuf(['contracts_bank_with_Guf.id_bank' => $idBank]);
                if(count($contractsBankWithGuf)){
                    for ($j = 0; $j < count($contractsBankWithGuf); $j++) { 
                        if(file_exists('files/contracts/contractsBankWithGuf/'.$contractsBankWithGuf[$j]['url_pdf'])){
                            unlink('files/contracts/contractsBankWithGuf/'.$contractsBankWithGuf[$j]['url_pdf']);//удаление файла
                        }
                    }
                    $this->contractsBankWithGuf->deleteContractsBankWithGuf(['contracts_bank_with_Guf.id_bank' => $idBank]);//удаление с таблицы 'contracts_bank_with_Guf'
                }

                $contractsBankWithDPRP = $this->contractsBankWithDPRP->getContractsBankWithDPRP(['contracts_bank_with_DPRP.id_bank' => $idBank]);
                if(count($contractsBankWithDPRP)){
                    for ($j = 0; $j < count($contractsBankWithDPRP); $j++) { 
                        if(file_exists('files/contracts/contractsBankWithDPRP/'.$contractsBankWithDPRP[$j]['url_pdf'])){
                            unlink('files/contracts/contractsBankWithDPRP/'.$contractsBankWithDPRP[$j]['url_pdf']);//удаление файла
                        }
                    }
                    $this->contractsBankWithDPRP->deleteContractsBankWithDPRP(['contracts_bank_with_DPRP.id_bank' => $idBank]);//удаление с таблицы 'contracts_bank_with_DPRP'
                }

                $information = $this->information->getInformation(['information.id_bank' => $idBank]);
                if(count($information)){
                    for ($j = 0; $j < count($information); $j++) { 
                        $images = $this->images->getImages(['images.id_information' => $information[$j]['id']]);
                        if(count($images)){
                            for ($m = 0; $m < count($images); $m++) { 
                                if(file_exists('images/information/'.$images[$m]['url'])){
                                    $result = unlink('images/information/'.$images[$m]['url']);//удаление картинки
                                }
                                if($result){
                                    $this->images->deleteImages(['images.id' => $images[$m]['id']]);//удаление с таблицы 'images'
                                }
                            }
                        }
                    }
                    $this->information->deleteInformation(['information.id_bank' => $idBank]);//удаление с таблицы 'information'
                }

                $this->banks->deleteBanks(['banks.id_bank' => $idBank]);//удаление с таблицы 'banks'
                $this->users->deleteUsers(['users.id' => $idBank]);//удаление с таблицы 'users'
                $this->borrowersRegister->deleteBorrowersRegister(['borrowersRegister.id_bank' => $idBank]);//удаление с таблицы 'borrowersRegister'
                $this->performance->deletePerformance(['performance.id_bank' => $idBank]);//удаление с таблицы 'performance'
            }
        }
    }

}