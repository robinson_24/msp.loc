<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Contracts\BanksInterface;
use App\Http\Controllers\Contracts\UsersInterface;
use App\Http\Controllers\Contracts\BoolsInterface;
use App\Http\Controllers\Contracts\PersonalInterface;
use App\Http\Controllers\Contracts\TotalInterface;
use App\Http\Controllers\Contracts\TermInterface;
use App\Http\Controllers\Contracts\SizeBorrowerInterface;
use App\Http\Controllers\Contracts\StackingInterface;
use App\Http\Controllers\Contracts\RepaymentScheduleInterface;
use App\Http\Controllers\Contracts\TargetInterface;
use App\Http\Controllers\Contracts\KvedInterface;
use App\Http\Controllers\Contracts\ImagesInterface;

use App\Http\Controllers\Contracts\RegulatoryDocumentsInterface;
use App\Http\Controllers\Contracts\RefusalsInterface;
use App\Http\Controllers\Contracts\CommissionInterface;
use App\Http\Controllers\Contracts\ContractsBankWithDPRPInterface;
use App\Http\Controllers\Contracts\ContractsBankWithGufInterface;
use App\Http\Controllers\Contracts\ContractsDPRPWithGufInterface;
use App\Http\Controllers\Contracts\BreachedInterface;

use Session;
use Request;
use Route;
use Response;
use Validator;
use App\Libraries\GeneralFunctions;
//use Excel;

class HomeController extends Controller 
{

    const DOWNLOADLAWS                  = '/files/regulatoryDocuments/laws/';
    const DOWNLOADORDERS                = '/files/regulatoryDocuments/orders/';
    const DOWNLOADRESOLUTIONS           = '/files/regulatoryDocuments/resolutions/';
    const DOWNLOADETCS                  = '/files/regulatoryDocuments/etcs/';
    const DOWNLOADCONTRACTSDPRPWITHGUF  = '/files/contracts/contractsDPRPWithGuf/';
    const DOWNLOADCONTRACTSBANKWITHGUF  = '/files/contracts/contractsBankWithGuf/';
    const DOWNLOADCONTRACTSBANKWITHDPRP = '/files/contracts/contractsBankWithDPRP/';
    const DOWNLOADCOMMANDS              = '/files/registerOfApplicants/commission/commands/';
    const DOWNLOADPROTOCOLS             = '/files/registerOfApplicants/commission/protocols/';
    const DOWNLOADBREACHED              = '/files/breached/';
    const DOWNLOADREFUSALS              = '/files/refusals/';
    const DOWNLOADREPORT                = '/files/reports/';

    private $banks;
    private $users;
    private $bools;
    private $personal;
    private $term;
    private $sizeBorrower;
    private $total;
    private $stacking;
    private $repaymentSchedule;
    private $target;
    private $kved;
    private $images;

    private $regulatorydocuments;
    private $refusals;
    private $commission;
    private $contractsbankwithdprp;
    private $contractsbankwithguf;
    private $contractsdprpwithguf;
    private $breached;

    public function __construct(
        UsersInterface $users,
        BanksInterface $banks,
        BoolsInterface $bools,
        PersonalInterface $personal,
        TermInterface $term,
        SizeBorrowerInterface $sizeBorrower,
        TotalInterface $total,
        StackingInterface $stacking,
        RepaymentScheduleInterface $repaymentSchedule,
        TargetInterface $target,
        KvedInterface $kved,
        ImagesInterface $images,
        RegulatoryDocumentsInterface $regulatorydocuments,
        RefusalsInterface $refusals,
        CommissionInterface $commission,
        ContractsBankWithDPRPInterface $contractsbankwithdprp,
        ContractsBankWithGufInterface $contractsbankwithguf,
        ContractsDPRPWithGufInterface $contractsdprpwithguf,
        BreachedInterface $breached
    ){
        $this->request = Request::all();
        $this->users = $users;
        $this->banks = $banks;
        $this->bools = $bools;
        $this->personal = $personal;
        $this->term = $term;
        $this->sizeBorrower = $sizeBorrower;
        $this->total = $total;
        $this->stacking = $stacking;
        $this->repaymentSchedule = $repaymentSchedule;
        $this->target = $target;
        $this->kved = $kved;
        $this->images = $images;
        $this->regulatorydocuments = $regulatorydocuments;
        $this->refusals = $refusals;
        $this->commission = $commission;
        $this->contractsbankwithdprp = $contractsbankwithdprp;
        $this->contractsbankwithguf = $contractsbankwithguf;
        $this->contractsdprpwithguf = $contractsdprpwithguf;
        $this->breached = $breached;
    }

    public function auth()//авторизация
    {
        if(!Session::has('userId')){
            $user = $this->request['user'];
            $password = $this->request['password'];

            $validator = Validator::make(
                [
                    'user' => $user,
                    'password' => $password
                ],[
                    'user' => 'required',
                    'password' => 'required'
                ],[
                    'user.required' => 'Поле "Користувач" має бути заповненим',
                    'password.required' => 'Поле "Пароль" має бути заповненим'
                ]
            );

            if($validator->fails()){
                return redirect('/')->with('error', 'Щось пішло не так');
            } else{

//$salt          = GeneralFunctions::createSalt();
//$password2      = GeneralFunctions::genPassword($password, $salt);
//
//dd($salt.'    '.$password2);

                $users = $this->users->getUsers(['login' => $user]);

                $message = 'Невірний користувач або пароль';

                if (count($users)) {
                    $users = $users[0];
                    $salt = $users['salt'];
                    $password = GeneralFunctions::genPassword($password, $salt);

                    if ($users['password'] == $password) {
                        Session::put('userId', $users['id']);
                        Session::put('status', $users['status']);
                        Session::put('login', $users['login']);
                        Session::put('name', $users['name']);

                        return redirect('/');
                    } else {
                        return redirect('/')->with('error', $message);
                    }
                } else {
                    return redirect('/')->with('error', $message);
                }
            }

        } else{
            return redirect('/')->with('error', 'Ви уже авторизовані');
        }
    }

    public function logout()//выход
    {
        if(Session::has('userId')){
            if(Session::get('userId') == Route::input('userId')){
                Session::forget('userId');
                Session::forget('status');
                Session::forget('login');
                Session::forget('name');

                return redirect('/');

            } else{
                return redirect('/');
            }
        }else{
            return redirect('/');
        }
    }

    public function homePage() //вывод страницы  "Онлайн платформа фінансово-кредитнгої підтримки суб'єктів МСП м.Києва"
    {
        $data = [];
        $data = array_merge($data, array(
            'title' => 'Он-лайн платформа фінансово-кредитної підтримки суб\'єктів МСП м.Києва',
            'error' => Session::has('error') ? Session::get('error') : null,
            'errorValid' => Session::has('errorValid') ? Session::get('errorValid') : null
        ));
        return view('/home', $data);
    }

    public function infographicsPage() //вывод страницы "Інфографіка"
    {
        if(Session::has('userId')){
            $data = [];
            $data = array_merge($data, array(
                'title' => 'Інфографіка'
            ));
            return view('/infographics', $data);
        } else{
            return redirect('/');
        }
    }

    public function download()
    {
        if(Session::has('userId')){
            if(Route::input('where') && Route::input('nameFile')){

                $path = Route::input('where');
                $nameFile = Route::input('nameFile');

                if($path == 'laws'){
                        $headers = array(
                            'Content-Type: application/pdf',
                        );
                    return response()->download(public_path().self::DOWNLOADLAWS.$nameFile, $nameFile, $headers);
                }
                if($path == 'orders'){
                        $headers = array(
                            'Content-Type: application/pdf',
                        );
                    return response()->download(public_path().self::DOWNLOADORDERS.$nameFile, $nameFile, $headers);
                }
                if($path == 'resolutions'){
                        $headers = array(
                            'Content-Type: application/pdf',
                        );
                    return response()->download(public_path().self::DOWNLOADRESOLUTIONS.$nameFile, $nameFile, $headers);
                }
                if($path == 'etcs'){
                        $headers = array(
                            'Content-Type: application/pdf',
                        );
                    return response()->download(public_path().self::DOWNLOADETCS.$nameFile, $nameFile, $headers);
                }

                if($path == 'contractsDPRPWithGuf'){
                        $headers = array(
                            'Content-Type: application/pdf',
                        );
                    return response()->download(public_path().self::DOWNLOADCONTRACTSDPRPWITHGUF.$nameFile, $nameFile, $headers);
                }

                if($path == 'contractsBankWithGuf'){
                        $headers = array(
                            'Content-Type: application/pdf',
                        );
                    return response()->download(public_path().self::DOWNLOADCONTRACTSBANKWITHGUF.$nameFile, $nameFile, $headers);
                }

                if($path == 'contractsBankWithDPRP'){
                        $headers = array(
                            'Content-Type: application/pdf',
                        );
                    return response()->download(public_path().self::DOWNLOADCONTRACTSBANKWITHDPRP.$nameFile, $nameFile, $headers);
                }

                if($path == 'commands'){
                        $headers = array(
                            'Content-Type: application/pdf',
                        );
                    return response()->download(public_path().self::DOWNLOADCOMMANDS.$nameFile, $nameFile, $headers);
                }

                if($path == 'protocols'){
                        $headers = array(
                            'Content-Type: application/pdf',
                        );
                    return response()->download(public_path().self::DOWNLOADPROTOCOLS.$nameFile, $nameFile, $headers);
                }

                if($path == 'breached'){
                        $headers = array(
                            'Content-Type: application/pdf',
                        );
                    return response()->download(public_path().self::DOWNLOADBREACHED.$nameFile, $nameFile, $headers);
                }

                if($path == 'refusals'){
                        $headers = array(
                            'Content-Type: application/pdf',
                        );
                    return response()->download(public_path().self::DOWNLOADREFUSALS.$nameFile, $nameFile, $headers);
                }

                if($path == 'reports'){
                        $headers = array();
                    return response()->download(public_path().self::DOWNLOADREPORT.$nameFile, $nameFile, $headers);
                }

            } else{
                return redirect('404');
            }
        } else{
            return redirect('/');
        }
    }

    public function bankEdit()
    {
        $name = GeneralFunctions::trimString($_POST['name'], true);
        $id = $_POST['id'];

        $value = [];

        $where = ['users.id' => $id];
        $value = ['users.name' => $name];

        $result = $this->users->updateUsers($where, $value);

        return json_encode($result);
    }

    public function notFoundPage()
    {
        if(Session::has('userId')){
            $data = [];
            $data = array_merge($data, array(
                'title' => 'Сторінку не знайдено'
            ));
            return view('404', $data);
        }else{
            return redirect('/');
        }
    }

    public function getBools()
    {
        $result = $this->bools->getBools();
        return json_encode($result);
    }

    public function getPersonal()
    {
        $result = $this->personal->getPersonal();
        return json_encode($result);
    }

    public function getStacking()
    {
        $result = $this->stacking->getStacking();
        return json_encode($result);
    }

    public function getTotal()
    {
        $result = $this->total->getTotal();
        return json_encode($result);
    }

    public function getTerm()
    {
        $result = $this->term->getTerm();
        return json_encode($result);
    }

    public function getSizeBorrower()
    {
        $result = $this->sizeBorrower->getSizeBorrower();
        return json_encode($result);
    }
    
    public function getTarget()
    {
        $result = $this->target->getTarget();
        return json_encode($result);
    }

    public function getRepaymentSchedule()
    {
        $result = $this->repaymentSchedule->getRepaymentSchedule(['is_default' => 1]);
        return json_encode($result);
    }

    public function getBank()
    {
        $result = $this->banks->getBanksJoin(['banks.category' => $_POST['category']]);
        return json_encode($result);
    }

    public function getKved()
    {
        $where = [];
        unset($_POST['_token']);
        if(count($_POST)){
            foreach ($_POST as $key => $value) {
                $where['kved.'.$key] = $value;
            }
        } else{
            $where = null;
        }

        $order = 'kved.kved';
        $result = $this->kved->getKved($where, $order);

        return json_encode($result);
    }

    public function deleteImage()
    {
        $id = $_POST['id'];
        $url = $_POST['url'];
        try {
            $result = $this->images->deleteImages(['images.id' => $id]);

            if(file_exists($url)){
                unlink($url);
            }
        } catch (\Exception $e) {
        }
        
        return json_encode('true');
    }

    public function deleteFile($category, $id)//удаление файлов Департаментом
    {
        if(Session::has('userId')){
            if(Session::get('status') == 'departament'){
                
                if($category == 'laws'){
                    $result = $this->regulatorydocuments->getRegulatoryDocuments(['regulatory_documents.id' => $id]);
                    if(count($result)){
                        try {
                            if($result[0]->url_pdf){
                                if(file_exists(public_path().self::DOWNLOADLAWS.$result[0]->url_pdf)){
                                    unlink(public_path().self::DOWNLOADLAWS.$result[0]->url_pdf);
                                }
                            }
                            if($result[0]->url_word){
                                if(file_exists(public_path().self::DOWNLOADLAWS.$result[0]->url_word)){
                                    unlink(public_path().self::DOWNLOADLAWS.$result[0]->url_word);
                                }
                            }
                            $this->regulatorydocuments->deleteRegulatoryDocuments(['regulatory_documents.id' => $id]);
                            return redirect()->back();
                        } catch (\Exception $e) {
                        }
                    }
                }
                if($category == 'orders'){
                    $result = $this->regulatorydocuments->getRegulatoryDocuments(['regulatory_documents.id' => $id]);
                    if(count($result)){
                        try {
                            if($result[0]->url_pdf){
                                if(file_exists(public_path().self::DOWNLOADORDERS.$result[0]->url_pdf)){
                                    unlink(public_path().self::DOWNLOADORDERS.$result[0]->url_pdf);
                                }
                            }
                            if($result[0]->url_word){
                                if(file_exists(public_path().self::DOWNLOADORDERS.$result[0]->url_word)){
                                    unlink(public_path().self::DOWNLOADORDERS.$result[0]->url_word);
                                }
                            }
                            $this->regulatorydocuments->deleteRegulatoryDocuments(['regulatory_documents.id' => $id]);
                            return redirect()->back();
                        } catch (\Exception $e) {
                        }
                    }
                }
                if($category == 'resolutions'){
                    $result = $this->regulatorydocuments->getRegulatoryDocuments(['regulatory_documents.id' => $id]);
                    if(count($result)){
                        try {
                            if($result[0]->url_pdf){
                                if(file_exists(public_path().self::DOWNLOADRESOLUTIONS.$result[0]->url_pdf)){
                                    unlink(public_path().self::DOWNLOADRESOLUTIONS.$result[0]->url_pdf);
                                }
                            }
                            if($result[0]->url_word){
                                if(file_exists(public_path().self::DOWNLOADRESOLUTIONS.$result[0]->url_word)){
                                    unlink(public_path().self::DOWNLOADRESOLUTIONS.$result[0]->url_word);
                                }
                            }
                            $this->regulatorydocuments->deleteRegulatoryDocuments(['regulatory_documents.id' => $id]);
                            return redirect()->back();
                        } catch (\Exception $e) {
                        }
                    }
                }
                if($category == 'etcs'){
                    $result = $this->regulatorydocuments->getRegulatoryDocuments(['regulatory_documents.id' => $id]);
                    if(count($result)){
                        try {
                            if($result[0]->url_pdf){
                                if(file_exists(public_path().self::DOWNLOADETCS.$result[0]->url_pdf)){
                                    unlink(public_path().self::DOWNLOADETCS.$result[0]->url_pdf);
                                }
                            }
                            if($result[0]->url_word){
                                if(file_exists(public_path().self::DOWNLOADETCS.$result[0]->url_word)){
                                    unlink(public_path().self::DOWNLOADETCS.$result[0]->url_word);
                                }
                            }
                            $this->regulatorydocuments->deleteRegulatoryDocuments(['regulatory_documents.id' => $id]);
                            return redirect()->back();
                        } catch (\Exception $e) {
                        }
                    }
                }

                if($category == 'contractsDPRPWithGuf'){
                    $result = $this->contractsdprpwithguf->getContractsDPRPWithGuf(['contracts_DPRP_with_Guf.id' => $id]);
                    if(count($result)){
                        try {
                            if($result[0]->url_pdf){
                                if(file_exists(public_path().self::DOWNLOADCONTRACTSDPRPWITHGUF.$result[0]->url_pdf)){
                                    unlink(public_path().self::DOWNLOADCONTRACTSDPRPWITHGUF.$result[0]->url_pdf);
                                }
                            }
                            if($result[0]->url_word){
                                if(file_exists(public_path().self::DOWNLOADCONTRACTSDPRPWITHGUF.$result[0]->url_word)){
                                    unlink(public_path().self::DOWNLOADCONTRACTSDPRPWITHGUF.$result[0]->url_word);
                                }
                            }
                            $this->contractsdprpwithguf->deleteContractsDPRPWithGuf(['contracts_DPRP_with_Guf.id' => $id]);
                            return redirect()->back();
                        } catch (\Exception $e) {
                        }
                    }
                }
                if($category == 'contractsBankWithGuf'){
                    $result = $this->contractsbankwithguf->getContractsBankWithGuf(['contracts_bank_with_Guf.id' => $id]);
                    if(count($result)){
                        try {
                            if($result[0]->url_pdf){
                                if(file_exists(public_path().self::DOWNLOADCONTRACTSBANKWITHGUF.$result[0]->url_pdf)){
                                    unlink(public_path().self::DOWNLOADCONTRACTSBANKWITHGUF.$result[0]->url_pdf);
                                }
                            }
                            if($result[0]->url_word){
                                if(file_exists(public_path().self::DOWNLOADCONTRACTSBANKWITHGUF.$result[0]->url_word)){
                                    unlink(public_path().self::DOWNLOADCONTRACTSBANKWITHGUF.$result[0]->url_word);
                                }
                            }
                            $this->contractsbankwithguf->deleteContractsBankWithGuf(['contracts_bank_with_Guf.id' => $id]);
                            return redirect()->back();
                        } catch (\Exception $e) {
                        }
                    }
                }
                if($category == 'contractsBankWithDPRP'){
                    $result = $this->contractsbankwithdprp->getContractsBankWithDPRP(['contracts_bank_with_DPRP.id' => $id]);
                    if(count($result)){
                        try {
                            if($result[0]->url_pdf){
                                if(file_exists(public_path().self::DOWNLOADCONTRACTSBANKWITHDPRP.$result[0]->url_pdf)){
                                    unlink(public_path().self::DOWNLOADCONTRACTSBANKWITHDPRP.$result[0]->url_pdf);
                                }
                            }
                            if($result[0]->url_word){
                                if(file_exists(public_path().self::DOWNLOADCONTRACTSBANKWITHDPRP.$result[0]->url_word)){
                                    unlink(public_path().self::DOWNLOADCONTRACTSBANKWITHDPRP.$result[0]->url_word);
                                }
                            }
                            $this->contractsbankwithdprp->deleteContractsBankWithDPRP(['contracts_bank_with_DPRP.id' => $id]);
                            return redirect()->back();
                        } catch (\Exception $e) {
                        }
                    }
                }

                if($category == 'commands'){
                    $result = $this->commission->getCommission(['commission.id' => $id]);
                    if(count($result)){
                        try {
                            if($result[0]->url_pdf){
                                if(file_exists(public_path().self::DOWNLOADCOMMANDS.$result[0]->url_pdf)){
                                    unlink(public_path().self::DOWNLOADCOMMANDS.$result[0]->url_pdf);
                                }
                            }
                            if($result[0]->url_word){
                                if(file_exists(public_path().self::DOWNLOADCOMMANDS.$result[0]->url_word)){
                                    unlink(public_path().self::DOWNLOADCOMMANDS.$result[0]->url_word);
                                }
                            }
                            $this->commission->deleteCommission(['commission.id' => $id]);
                            return redirect()->back();
                        } catch (\Exception $e) {
                        }
                    }
                }
                if($category == 'protocols'){
                    $result = $this->commission->getCommission(['commission.id' => $id]);
                    if(count($result)){
                        try {
                            if($result[0]->url_pdf){
                                if(file_exists(public_path().self::DOWNLOADPROTOCOLS.$result[0]->url_pdf)){
                                    unlink(public_path().self::DOWNLOADPROTOCOLS.$result[0]->url_pdf);
                                }
                            }
                            if($result[0]->url_word){
                                if(file_exists(public_path().self::DOWNLOADPROTOCOLS.$result[0]->url_word)){
                                    unlink(public_path().self::DOWNLOADPROTOCOLS.$result[0]->url_word);
                                }
                            }
                            $this->commission->deleteCommission(['commission.id' => $id]);
                            return redirect()->back();
                        } catch (\Exception $e) {
                        }
                    }
                }

                if($category == 'breached'){
                    $result = $this->breached->getBreached(['breached.id' => $id]);
                    if(count($result)){
                        try {
                            if($result[0]->url_pdf){
                                if(file_exists(public_path().self::DOWNLOADBREACHED.$result[0]->url_pdf)){
                                    unlink(public_path().self::DOWNLOADBREACHED.$result[0]->url_pdf);
                                }
                            }
                            if($result[0]->url_word){
                                if(file_exists(public_path().self::DOWNLOADBREACHED.$result[0]->url_word)){
                                    unlink(public_path().self::DOWNLOADBREACHED.$result[0]->url_word);
                                }
                            }
                            $this->breached->deleteBreached(['breached.id' => $id]);
                            return redirect()->back();
                        } catch (\Exception $e) {
                        }
                    }
                }

                if($category == 'refusals'){
                    $result = $this->refusals->getRefusals(['refusals.id' => $id]);
                    if(count($result)){
                        try {
                            if($result[0]->url_pdf){
                                if(file_exists(public_path().self::DOWNLOADREFUSALS.$result[0]->url_pdf)){
                                    unlink(public_path().self::DOWNLOADREFUSALS.$result[0]->url_pdf);
                                }
                            }
                            if($result[0]->url_word){
                                if(file_exists(public_path().self::DOWNLOADREFUSALS.$result[0]->url_word)){
                                    unlink(public_path().self::DOWNLOADREFUSALS.$result[0]->url_word);
                                }
                            }
                            $this->refusals->deleteRefusals(['refusals.id' => $id]);
                            return redirect()->back();
                        } catch (\Exception $e) {
                        }
                    }
                }
            }else{
                return redirect('404');
            }
        }else{
            return redirect('404');
        }
    }

}