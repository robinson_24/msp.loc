<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Contracts\InformationInterface;
use App\Http\Controllers\Contracts\ImagesInterface;
use Session;
use Request;
use Route;
use Validator;
use App\Libraries\GeneralFunctions;

class InfographicsController extends Controller 
{

    private $information;
    private $images;

    public function __construct(
        InformationInterface $information,
        ImagesInterface $images
    ){
        $this->request = Request::all();
        $this->information = $information;
        $this->images = $images;
    }

    public function infographicsPage() //вывод страницы "Цікаві приклади суб'єктів МСП, які отримали ФКП"
    {
        if(Session::has('userId')){
            $data = [];

            $whereInformation = ['information.category' => 'infographics'];

            $information = $this->information->getInformationJoin($whereInformation);

            if(count($information)){
                $data['information'] = $information[0];
                $data['images'] = $information;
            }

            $data = array_merge($data, array(
                'title' => 'Інфографіка',
                'success' => Session::has('success') ? Session::get('success') : null,
                'error' => Session::has('error') ? Session::get('error') : null
            ));
            return view('/infographics', $data);
        } else{
            return redirect('/');
        }
    }

    public function addImage()//добавление картинки
    {
        $idInformation = $_POST['idInformation'];
        $path = 'images/information/';
        $file = 'img';
        try {
            
            $result = GeneralFunctions::getNameFile($path, $file);

            if($result != false){
                $data = [
                    'images.id_information' => $idInformation,
                    'images.url' => $result
                ];

                $addImage = $this->images->addImages($data);

            }
        } catch (\Exception $e) {
        }
        
        return redirect('/infographics');
    }

    public function editInformation()
    {
        if(Session::has('userId')){
            if(Session::get('status') == 'departament'){
                $where = [];
                $data = [];

                $where = ['information.id' => $_POST['idInformation']];
                
                $data = [
                    'body' => $_POST['editorInformation']
                ];

                $result = $this->information->updateInformation($where, $data);
                if($result){
                    return redirect('/infographics')->with('success', 'Інформацію успішно збережено');
                }else{
                    return redirect('/infographics')->with('error', 'Щось пішло не так');
                }
            }else{
                return redirect('404');
            }
        } else{
            return redirect('/');
        }
    }
}