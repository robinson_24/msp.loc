<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Controllers\RegisterOfApplicantsController;
use App\Http\Controllers\RegisterOfBorrowersController;
use App\Http\Controllers\PerformanceOfImplementationController;
use App\Http\Controllers\Contracts\DefsInterface;

class ReportController extends Controller
{
    private $performanceController;
    private $borrowersController;
    private $applicantsController;

    public function __construct(
        RegisterOfBorrowersController $borrowersController,
        RegisterOfApplicantsController $applicantsController,
        PerformanceOfImplementationController $performanceController
    ){
        $this->performanceController = $performanceController;
        $this->borrowersController = $borrowersController;
        $this->applicantsController = $applicantsController;
    }

    public function cronReportMonthBorrowers()//берем записи за предыдущий месяц
    {

        $whereYear = ['borrowersRegister.created_at' => date('Y', strtotime('-1 months'))];
        $whereMonth = ['borrowersRegister.created_at' => date('m', strtotime('-1 months'))];

        $name = 'Звіт за '.date('m', strtotime('-1 months')).'.'.date('Y', strtotime('-1 months')).' станом на 01.'.date('m').'.'.date('Y');
        $this->borrowersController->saveReport(null, $whereYear, $whereMonth, null, $name);
    }

    public function cronReportQuartalBorrowers()//берем записи за предыдущий квартал
    {
        if(date('m') == '01' || date('m') == '04' || date('m') == '07' || date('m') == '10'){
            $whereYear = ['borrowersRegister.created_at' => date('Y', strtotime('-1 months'))];

            $quarte = 0;

            if(date('m') == '01'){
                $quarte = 4;
            } elseif(date('m') == '04'){
                $quarte = 1;
            } elseif(date('m') == '07'){
                $quarte = 2;
            } elseif(date('m') == '10'){
                $quarte = 3;
            }

            $whereQuarte = [];

            $whereQuarte = array([
                'field' => 'borrowersRegister.created_at',
                'symbol' => '>=',
                'value' => date('m', strtotime('-3 months'))
            ], [
                'field' => 'borrowersRegister.created_at',
                'symbol' => '<=',
                'value' => date('m', strtotime('-1 months'))
            ]);
            $name = 'Звіт за '.$quarte.' квартал станом на 01.'.date('m').'.'.date('Y');
            $this->borrowersController->saveReport(null, $whereYear, null, $whereQuarte, $name);
        }
    }

    public function cronReportYearBorrowers()//берем записи за предыдущий год
    {
        if(date('m') == '01'){
            $whereYear = ['borrowersRegister.created_at' => date('Y', strtotime('-1 months'))];
            $name = 'Звіт за '.date('Y', strtotime('-1 months')).' рік станом на 01.01.'.date('Y');
            $this->borrowersController->saveReport(null, $whereYear, null, null, $name);
        }
    }

    public function cronReportMonthApplicants()//берем записи за предыдущий месяц
    {

        $whereYear = ['applicantsRegister.created_at' => date('Y', strtotime('-1 months'))];
        $whereMonth = ['applicantsRegister.created_at' => date('m', strtotime('-1 months'))];

        $name = 'Звіт за '.date('m', strtotime('-1 months')).'.'.date('Y', strtotime('-1 months')).' станом на 01.'.date('m').'.'.date('Y');
        $this->applicantsController->saveReport(null, true, null, $whereYear, $whereMonth, null, $name);
    }

    public function cronReportQuartalApplicants()//берем записи за предыдущий квартал
    {
        if(date('m') == '01' || date('m') == '04' || date('m') == '07' || date('m') == '10'){
            $whereYear = ['applicantsRegister.created_at' => date('Y', strtotime('-1 months'))];

            $quarte = 0;

            if(date('m') == '01'){
                $quarte = 4;
            } elseif(date('m') == '04'){
                $quarte = 1;
            } elseif(date('m') == '07'){
                $quarte = 2;
            } elseif(date('m') == '10'){
                $quarte = 3;
            }

            $whereQuarte = [];

            $whereQuarte = array([
                'field' => 'applicantsRegister.created_at',
                'symbol' => '>=',
                'value' => date('m', strtotime('-3 months'))
            ], [
                'field' => 'applicantsRegister.created_at',
                'symbol' => '<=',
                'value' => date('m', strtotime('-1 months'))
            ]);
            $name = 'Звіт за '.$quarte.' квартал станом на 01.'.date('m').'.'.date('Y');
            $this->applicantsController->saveReport(null, true, null, $whereYear, null, $whereQuarte, $name);
        }
    }

    public function cronReportYearApplicants()//берем записи за предыдущий год
    {
        if(date('m') == '01'){
            $whereYear = ['applicantsRegister.created_at' => date('Y', strtotime('-1 months'))];
            $name = 'Звіт за '.date('Y', strtotime('-1 months')).' рік станом на 01.01.'.date('Y');
            $this->applicantsController->saveReport(null, true, null, $whereYear, null, null, $name);
        }
    }

    public function cronReportMonthPerformance()//берем записи за предыдущий месяц
    {

        $whereYear = ['performance.created_at' => date('Y', strtotime('-1 months'))];
        $whereMonth = ['performance.created_at' => date('m', strtotime('-1 months'))];

        $name = 'Iнформацiя за '.date('m', strtotime('-1 months')).'.'.date('Y', strtotime('-1 months')).' станом на 01.'.date('m').'.'.date('Y');
        $this->performanceController->saveReport(null, true, $whereYear, $whereMonth, null, $name);
    }

    public function cronReportQuartalPerformance()//берем записи за предыдущий квартал
    {
        if(date('m') == '01' || date('m') == '04' || date('m') == '07' || date('m') == '10'){
            $whereYear = ['performance.created_at' => date('Y', strtotime('-1 months'))];

            $quarte = 0;

            if(date('m') == '01'){
                $quarte = 4;
            } elseif(date('m') == '04'){
                $quarte = 1;
            } elseif(date('m') == '07'){
                $quarte = 2;
            } elseif(date('m') == '10'){
                $quarte = 3;
            }

            $whereQuarte = [];

            $whereQuarte = array([
                'field' => 'performance.created_at',
                'symbol' => '>=',
                'value' => date('m', strtotime('-3 months'))
            ], [
                'field' => 'performance.created_at',
                'symbol' => '<=',
                'value' => date('m', strtotime('-1 months'))
            ]);
            $name = 'Iнформацiя за '.$quarte.' квартал станом на 01.'.date('m').'.'.date('Y');
            $this->performanceController->saveReport(null, true, $whereYear, null, $whereQuarte, $name);
        }
    }

    public function cronReportYearPerformance()//берем записи за предыдущий год
    {
        if(date('m') == '01'){
            $whereYear = ['performance.created_at' => date('Y', strtotime('-1 months'))];
            $name = 'Iнформацiя за '.date('Y', strtotime('-1 months')).' рік станом на 01.01.'.date('Y');
            $this->performanceController->saveReport(null, true, $whereYear, null, null, $name);
        }
    }
}