<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Contracts\BreachedInterface;
use Session;
use Request;
use Route;
use Validator;
use App\Libraries\GeneralFunctions;

class BreachedController extends Controller 
{

    //const LAWS = 'laws';
    //const RESOLUTIONS = 'resolutions';
    //const ORDERS = 'orders';
    //const ETCS = 'etcs';

    private $breached;

    public function __construct(
        BreachedInterface $breached
    ){
        $this->request = Request::all();
        $this->breached = $breached;
    }

    public function breachedPage() //вывод страницы "Суб'єкти МСП, які порушили умови кредитних договорів"
    {
        if(Session::has('userId')){
            $data = [];

            $breached = $this->breached->getBreached();
            if(count($breached)){
                $data['breached'] = $breached;
            }

            $data = array_merge($data, array(
                'title' => 'Суб\'єкти МСП, які порушили умови кредитних договорів'
            ));
            return view('/breached', $data);
        } else{
            return redirect('/');
        }
    }

    public function breachedAdd() //добавление документа
    {
        if(Session::has('userId')){
                unset($this->request['_token']);

                $validator = Validator::make(
                    [
                        'name' => $this->request['name']
                    ],[
                        'name' => 'required'
                    ],[
                        'name.required' => 'Поле має бути обов\'язково заповнене.'
                    ]
                );

                if($validator->fails()){
                    return redirect('/breached')->with('error', $validator->messages());
                }else{
                    $data = [];

                    if(!empty($this->request['pdf'])){
                        $pathPDF = 'files/breached/';
                        $filePDF = 'pdf';
                        $resultPDF = GeneralFunctions::getNameFile($pathPDF, $filePDF);
                        if($resultPDF != false){
                            $data['url_pdf'] = $resultPDF;
                        }
                    }
                    if(!empty($this->request['word'])){
                        $pathWORD = 'files/breached/';
                        $fileWORD = 'word';
                        $resultWORD = GeneralFunctions::getNameFile($pathWORD, $fileWORD);
                        if($resultWORD != false){
                            $data['url_word'] = $resultWORD;
                        }
                    }

                    $data = array_merge($data, array(
                        'name' => $this->request['name']
                    ));
                    ;

                    $this->breached->addBreached($data);

                    return redirect('/breached');
                }
        }else{
            return redirect('/');
        }
    }



}