<?php
namespace app\Http\Controllers\Repositories;

use App\Http\Controllers\Contracts\BorrowersRegisterInterface;
use App\Eloquent\BorrowersRegister;
use App\Eloquent\Users;
use App\Eloquent\Term;
use App\Eloquent\Defs;
use App\Eloquent\Kved;
use DB;
use Carbon;

class BorrowersRegisterRepository implements BorrowersRegisterInterface
{
    public function getBorrowersRegister($where = null, $order = null, $limit = null, $offset = null, $fields = null)
    {
        $borrowersRegister = new BorrowersRegister();

        if (!empty($where)) {
            foreach ($where as $attribute => $value) {
                $borrowersRegister = $borrowersRegister->where($attribute, $value);
            }
        }

        if (count($fields)) {
            foreach ($fields as $field) {
                $borrowersRegister = $borrowersRegister->addSelect($field);
            }
        }

        if (!empty($order)) {
            $borrowersRegister = $borrowersRegister->orderBy($order);
        }

        if (!empty($limit)) {
            $borrowersRegister = $borrowersRegister->take($limit);
        }

        if (!empty($offset)) {
            $borrowersRegister = $borrowersRegister->skip($offset);
        }

        return $borrowersRegister->get();
    }

    public function getBorrowersRegisterJoin($where = null, $whereYear = null, $whereMonth = null, $whereQuarte = null)
    {
        $borrowersRegisterJoin = new BorrowersRegister();

        if (!empty($where)) {
            for ($i = 0; $i < count($where); $i++) {
                $borrowersRegisterJoin = $borrowersRegisterJoin->where($where[$i]['field'], $where[$i]['symbol'], $where[$i]['value']);
            }
        }

        if(!empty($whereYear)){
            foreach ($whereYear as $key => $value) {
                $borrowersRegisterJoin = $borrowersRegisterJoin->whereYear($key, $value);
            }
        }

        if(!empty($whereMonth)){
            foreach ($whereMonth as $key => $value) {
                $borrowersRegisterJoin = $borrowersRegisterJoin->whereMonth($key, $value);
            }
        }

        if(!empty($whereQuarte) && !empty($whereYear)){
            for($i = 0; $i < count($whereQuarte); $i++){
                $borrowersRegisterJoin = $borrowersRegisterJoin->whereMonth($whereQuarte[$i]['field'], $whereQuarte[$i]['symbol'], $whereQuarte[$i]['value']);
            }
        }

        $borrowersRegisterJoin = $borrowersRegisterJoin
            ->leftJoin('users', 'borrowersRegister.id_bank', '=', 'users.id')
            ->leftJoin('term', 'borrowersRegister.col_9', '=', 'term.id')
            ->leftJoin('target', 'borrowersRegister.col_5', '=', 'target.id')
            ->leftJoin('kved as kved_section', 'borrowersRegister.col_4_section', '=', 'kved_section.id')
            ->leftJoin('kved as kved_group', 'borrowersRegister.col_4_group', '=', 'kved_group.id')
            ->leftJoin('kved as kved_clas', 'borrowersRegister.col_4_clas', '=', 'kved_clas.id')
            ->where('users.is_delete', '=', 0)
            ->select('borrowersRegister.*', 'users.name as name', 'term.term as col_9', 'target.target as col_5',
                'kved_section.kved as col_4_section', 'kved_group.kved as col_4_group', 'kved_clas.kved as col_4_clas')
            ->orderBy('borrowersRegister.created_at', 'desc')
            ->get();

        return $borrowersRegisterJoin;
    }

    public function addBorrowersRegister($where)
    {
        $borrowersRegister = new BorrowersRegister();

        foreach ($where as $attribute => $value) {
            $borrowersRegister->$attribute = $value;
        }

        $borrowersRegister->save();

        return $borrowersRegister;
    }

    public function updateBorrowersRegister($where, $values)
    {
        $borrowersRegister = new BorrowersRegister();

        foreach ($where as $attribute => $value) {
            $borrowersRegister = $borrowersRegister->where($attribute, $value);
        }

        $borrowersRegister = $borrowersRegister->update($values);

        return $borrowersRegister;
    }

    public function deleteBorrowersRegister($where)
    {
        $borrowersRegister = new BorrowersRegister();

        foreach ($where as $attribute => $value) {
            $borrowersRegister = $borrowersRegister->where($attribute, $value);
        }

        $borrowersRegister->delete();
    }

    public function getChart_8_9_10()
    {
        $borrowersRegister = new BorrowersRegister();

        $borrowersRegister = $borrowersRegister
                 ->select('target.target as target',
                    DB::raw('count(*) as count'),
                    DB::raw('sum(borrowersRegister.col_10) as sumCredits'),
                    DB::raw('sum(borrowersRegister.col_14) as sumFKP'))
                 ->groupBy('col_5')
                 ->leftJoin('target', 'borrowersRegister.col_5', '=', 'target.id')
                 ->leftJoin('users', 'borrowersRegister.id_bank', '=', 'users.id')
                 ->where('users.is_delete', 0)
                 ->whereNotNull('borrowersRegister.col_5')
                 ->get();

        return $borrowersRegister;//->get();
    }

    public function getChart_1()
    {
        $borrowersRegister = new BorrowersRegister();

        $borrowersRegister = $borrowersRegister
                ->select(DB::raw('count(*) as count'),
                     DB::raw('YEAR(borrowersRegister.col_7) as year, MONTH(borrowersRegister.col_7) as month'))
                ->groupBy('year', 'month')
                ->leftJoin('users', 'borrowersRegister.id_bank', '=', 'users.id')
                ->where('users.is_delete', 0)
                ->whereNotNull('borrowersRegister.col_7')
                ->get();
        return $borrowersRegister;//->get();
    }

    public function getChart_11_12_13()
    {
        $borrowersRegister = new BorrowersRegister();

        $borrowersRegister = $borrowersRegister
                 ->select('users.name as name', 
                    DB::raw('count(*) as count'),
                    DB::raw('sum(borrowersRegister.col_10) as sumCredits'),
                    DB::raw('sum(borrowersRegister.col_14) as sumFKP'))
                 ->groupBy('id_bank')
                 ->leftJoin('users', 'borrowersRegister.id_bank', '=', 'users.id')
                 ->where('users.is_delete', 0)
                 ->get();

        return $borrowersRegister;//->get();
    }

    public function getChart_4_5_6_7()
    {
        $borrowersRegister = new BorrowersRegister();

        $borrowersRegister = $borrowersRegister
                 ->select('kved.kved as kved', 
                    DB::raw('count(*) as countCredits'),
                    DB::raw('count(*) as countFKP'),
                    DB::raw('sum(borrowersRegister.col_10) as sumCredits'),
                    DB::raw('sum(borrowersRegister.col_14) as sumFKP'))
                 ->groupBy('col_4_section')
                 ->leftJoin('kved', 'borrowersRegister.col_4_section', '=', 'kved.id')
                 ->leftJoin('users', 'borrowersRegister.id_bank', '=', 'users.id')
                 ->where('users.is_delete', 0)
                 ->whereNotNull('borrowersRegister.col_4_section')
                 ->get();

        return $borrowersRegister;//->get();
    }

}