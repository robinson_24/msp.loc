<?php
namespace app\Http\Controllers\Repositories;

use App\Http\Controllers\Contracts\ApplicantsRegisterInterface;
use App\Eloquent\ApplicantsRegister;
use App\Eloquent\Users;
use App\Eloquent\Bools;
use App\Eloquent\Term;
use App\Eloquent\Stacking;
use App\Eloquent\RepaymentSchedule;
use App\Eloquent\Kved;
use DB;

class ApplicantsRegisterRepository implements ApplicantsRegisterInterface
{
    public function getApplicantsRegister($where = null, $order = null, $limit = null, $offset = null, $fields = null)
    {
        $applicantsRegister = new ApplicantsRegister();

        if (!empty($where)) {
            foreach ($where as $attribute => $value) {
                $applicantsRegister = $applicantsRegister->where($attribute, $value);
            }
        }

        if (count($fields)) {
            foreach ($fields as $field) {
                $applicantsRegister = $applicantsRegister->addSelect($field);
            }
        }

        if (!empty($order)) {
            $applicantsRegister = $applicantsRegister->orderBy($order);
        }

        if (!empty($limit)) {
            $applicantsRegister = $applicantsRegister->take($limit);
        }

        if (!empty($offset)) {
            $applicantsRegister = $applicantsRegister->skip($offset);
        }

        return $applicantsRegister->get();
    }

    public function getApplicantsRegisterJoin($where = null, $sum = null, $between = null, $whereYear = null, $whereMonth = null, $whereQuarte = null)
    {
        $applicantsRegisterJoin = new ApplicantsRegister();

        $data = [];

        if (!empty($where)) {
            for ($i = 0; $i < count($where); $i++) { 
                $applicantsRegisterJoin = $applicantsRegisterJoin->where($where[$i]['field'], $where[$i]['symbol'], $where[$i]['value']);
            }
        }

        if(!empty($between)){
            foreach ($between as $key => $value) {
                $applicantsRegisterJoin = $applicantsRegisterJoin->whereBetween($key, $value);
            }
        }

        if(!empty($whereYear)){
            foreach ($whereYear as $key => $value) {
                $applicantsRegisterJoin = $applicantsRegisterJoin->whereYear($key, $value);
            }
        }

        if(!empty($whereMonth)){
            foreach ($whereMonth as $key => $value) {
                $applicantsRegisterJoin = $applicantsRegisterJoin->whereMonth($key, $value);
            }
        }

        if(!empty($whereQuarte) && !empty($whereYear)){
            $applicantsRegisterJoin = $applicantsRegisterJoin->whereYear($key, $value);
            for($i = 0; $i < count($whereQuarte); $i++){
                $applicantsRegisterJoin = $applicantsRegisterJoin->whereMonth($whereQuarte[$i]['field'], $whereQuarte[$i]['symbol'], $whereQuarte[$i]['value']);
            }
        }

        $applicantsRegisterJoin = $applicantsRegisterJoin
            ->leftJoin('users', 'applicantsRegister.id_bank', '=', 'users.id')
            ->leftJoin('bools as bools_9', 'applicantsRegister.col_9', '=', 'bools_9.id')
            ->leftJoin('bools as bools_10', 'applicantsRegister.col_10', '=', 'bools_10.id')
            ->leftJoin('bools as bools_23', 'applicantsRegister.col_23', '=', 'bools_23.id')
            ->leftJoin('kved as kved_section', 'applicantsRegister.col_6_section', '=', 'kved_section.id')
            ->leftJoin('kved as kved_group', 'applicantsRegister.col_6_group', '=', 'kved_group.id')
            ->leftJoin('kved as kved_clas', 'applicantsRegister.col_6_clas', '=', 'kved_clas.id')
            ->leftJoin('term', 'applicantsRegister.col_15', '=', 'term.id')
            ->leftJoin('target', 'applicantsRegister.col_7', '=', 'target.id')
            ->leftJoin('stacking', 'applicantsRegister.col_14', '=', 'stacking.id')
            ->leftJoin('repayment_schedule', 'applicantsRegister.col_17', '=', 'repayment_schedule.id')
            ->where('users.is_delete', '=', 0)
            ->select('applicantsRegister.*', 'users.name as name', 'bools_9.bools as col_9',
                'bools_10.bools as col_10', 'bools_23.bools as col_23', 'term.term as col_15',
                'target.target as col_7', 'stacking.stacking as col_14',
                'repayment_schedule.repayment_schedule as col_17',
                'repayment_schedule.id as repayment_schedule_id', 'kved_section.kved as col_6_section',
                'kved_group.kved as col_6_group', 'kved_clas.kved as col_6_clas')
            ->orderBy('applicantsRegister.col_24', 'desc')
            ->get();

        if(!empty($sum)){
            $data['sum_11'] = $applicantsRegisterJoin->sum('col_11');
            $data['sum_12'] = $applicantsRegisterJoin->sum('col_12');
            $data['sum_16'] = $applicantsRegisterJoin->sum('col_16');
            $data['sum_20'] = $applicantsRegisterJoin->sum('col_20');
            $data['sum_22'] = $applicantsRegisterJoin->sum('col_22');
        }

        $data = array_merge($data, array(
            'applicantsRegisterJoin' => $applicantsRegisterJoin
        ));

        return $data;
    }

    public function addApplicantsRegister($where)
    {
        $applicantsRegister = new ApplicantsRegister();

        foreach ($where as $attribute => $value) {
            $applicantsRegister->$attribute = $value;
        }

        $applicantsRegister->save();

        return $applicantsRegister;
    }

    public function updateApplicantsRegister($where, $values)
    {
        $applicantsRegister = new ApplicantsRegister();

        foreach ($where as $attribute => $value) {
            $applicantsRegister = $applicantsRegister->where($attribute, $value);
        }

        $applicantsRegister = $applicantsRegister->update($values);

        return $applicantsRegister;
    }

    public function deleteApplicantsRegister($where)
    {
        $applicantsRegister = new ApplicantsRegister();

        foreach ($where as $attribute => $value) {
            $applicantsRegister = $applicantsRegister->where($attribute, $value);
        }

        $applicantsRegister->delete();
    }

}