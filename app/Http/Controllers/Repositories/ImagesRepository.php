<?php
namespace app\Http\Controllers\Repositories;

use App\Http\Controllers\Contracts\ImagesInterface;
use App\Eloquent\Images;
use DB;

class ImagesRepository implements ImagesInterface
{
    public function getImages($where = null)
    {
        $images = new Images();

        if (!empty($where)) {
            foreach ($where as $attribute => $value) {
                $images = $images->where($attribute, $value);
            }
        }
        
        return $images->get();
    }

    public function addImages($where)
    {
        $images = new Images();

        foreach ($where as $attribute => $value) {
            $images->$attribute = $value;
        }

        $images->save();

        return $images;
    }
    
    public function deleteImages($where)
    {
        $images = new Images();

        foreach ($where as $attribute => $value) {
            $images = $images->where($attribute, $value);
        }

        $images->delete();
    }
}