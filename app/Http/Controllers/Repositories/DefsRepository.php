<?php
namespace app\Http\Controllers\Repositories;

use App\Http\Controllers\Contracts\DefsInterface;
use App\Eloquent\Defs;
//use App\Eloquent\Users;
use DB;

class DefsRepository implements DefsInterface
{
    public function getDefs($where = null, $order = null, $limit = null, $offset = null, $fields = null)
    {
        $defs = new Defs();

        if (!empty($where)) {
            foreach ($where as $attribute => $value) {
                $defs = $defs->where($attribute, $value);
            }
        }

        if (count($fields)) {
            foreach ($fields as $field) {
                $defs = $defs->addSelect($field);
            }
        }

        if (!empty($order)) {
            $defs = $defs->orderBy($order);
        }

        if (!empty($limit)) {
            $defs = $defs->take($limit);
        }

        if (!empty($offset)) {
            $defs = $defs->skip($offset);
        }

        return $defs->get();
    }

    public function getDefsNowYear($where = null)
    {
        $defs = new Defs();

        if (!empty($where)) {
            foreach ($where as $attribute => $value) {
                $defs = $defs->where($attribute, $value);
            }
        }

        $defs = $defs->whereYear('created_at', date('Y'));

        return $defs->get();
    }

    /*public function getDefsJoin($where = null)
    {
        $getDefsJoin = new Defs();

        if (!empty($where)) {
            foreach ($where as $attribute => $value) {
                $getDefsJoin = $getDefsJoin->where($attribute, $value);
            }
        }

        $getDefsJoin = $getDefsJoin
            ->leftJoin('users', 'defs.id_bank', '=', 'users.id')
            ->where('users.is_delete', '=', 0)
            ->select('defs.category', 'users.name', 'users.id');

        return $getDefsJoin->get();
    }*/

    public function addDefs($where)
    {
        $defs = new Defs();

        foreach ($where as $attribute => $value) {
            $defs->$attribute = $value;
        }

        $defs->save();

        return $defs;
    }

    public function updateDefs($where, $values)
    {
        $defs = new Defs();

        foreach ($where as $attribute => $value) {
            $defs = $defs->where($attribute, $value);
        }

        $defs = $defs->update($values);

        return $defs;
    }

}