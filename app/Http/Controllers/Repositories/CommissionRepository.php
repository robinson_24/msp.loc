<?php
namespace app\Http\Controllers\Repositories;

use App\Http\Controllers\Contracts\CommissionInterface;
use App\Eloquent\Commission;
use DB;

class CommissionRepository implements CommissionInterface
{
    public function getCommission($where = null, $order = null, $limit = null, $offset = null, $fields = null)
    {
        $commission = new Commission();

        if (!empty($where)) {
            foreach ($where as $attribute => $value) {
                $commission = $commission->where($attribute, $value);
            }
        }

        if (count($fields)) {
            foreach ($fields as $field) {
                $commission = $commission->addSelect($field);
            }
        }

        if (!empty($order)) {
            $commission = $commission->orderBy($order);
        }

        if (!empty($limit)) {
            $commission = $commission->take($limit);
        }

        if (!empty($offset)) {
            $commission = $commission->skip($offset);
        }

        return $commission->get();
    }

    public function addCommission($where)
    {
        $commission = new Commission();

        foreach ($where as $attribute => $value) {
            $commission->$attribute = $value;
        }

        $commission->save();

        return $commission;
    }

    public function updateCommission($where, $values)
    {
        $commission = new Commission();

        foreach ($where as $attribute => $value) {
            $commission = $commission->where($attribute, $value);
        }

        $commission = $commission->update($values);

        return $commission;
    }
    
    public function deleteCommission($where)
    {
        $commission = new Commission();

        foreach ($where as $attribute => $value) {
            $commission = $commission->where($attribute, $value);
        }

        $commission->delete();
    }

}