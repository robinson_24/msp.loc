<?php
namespace app\Http\Controllers\Repositories;

use App\Http\Controllers\Contracts\StackingInterface;
use App\Eloquent\Stacking;
use DB;

class StackingRepository implements StackingInterface
{
    public function getStacking($where = null, $order = null, $limit = null, $offset = null, $fields = null)
    {
        $stacking = new Stacking();

        if (!empty($where)) {
            foreach ($where as $attribute => $value) {
                $stacking = $stacking->where($attribute, $value);
            }
        }

        if (count($fields)) {
            foreach ($fields as $field) {
                $stacking = $stacking->addSelect($field);
            }
        }

        if (!empty($order)) {
            $stacking = $stacking->orderBy($order);
        }

        if (!empty($limit)) {
            $stacking = $stacking->take($limit);
        }

        if (!empty($offset)) {
            $stacking = $stacking->skip($offset);
        }

        return $stacking->get();
    }
}