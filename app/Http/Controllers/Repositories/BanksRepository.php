<?php
namespace app\Http\Controllers\Repositories;

use App\Http\Controllers\Contracts\BanksInterface;
use App\Eloquent\Banks;
use App\Eloquent\Users;
use DB;

class BanksRepository implements BanksInterface
{
    public function getBanks($where = null, $order = null, $limit = null, $offset = null, $fields = null)
    {
        $banks = new Banks();

        if (!empty($where)) {
            foreach ($where as $attribute => $value) {
                $banks = $banks->where($attribute, $value);
            }
        }

        if (count($fields)) {
            foreach ($fields as $field) {
                $banks = $banks->addSelect($field);
            }
        }

        if (!empty($order)) {
            $banks = $banks->orderBy($order);
        }

        if (!empty($limit)) {
            $banks = $banks->take($limit);
        }

        if (!empty($offset)) {
            $banks = $banks->skip($offset);
        }

        return $banks->get();
    }

    public function getBanksJoin($where = null)
    {
        $getBanksJoin = new Banks();

        if (!empty($where)) {
            foreach ($where as $attribute => $value) {
                $getBanksJoin = $getBanksJoin->where($attribute, $value);
            }
        }

        $getBanksJoin = $getBanksJoin
            ->leftJoin('users', 'banks.id_bank', '=', 'users.id')
            ->where('users.is_delete', '=', 0)
            ->select('banks.category', 'users.name', 'users.id');

        return $getBanksJoin->get();
    }

    public function addBanks($where)
    {
        $banks = new Banks();

        foreach ($where as $attribute => $value) {
            $banks->$attribute = $value;
        }

        $banks->save();

        return $banks;
    }

    public function updateBanks($where, $values)
    {
        $banks = new Banks();

        foreach ($where as $attribute => $value) {
            $banks = $banks->where($attribute, $value);
        }

        $banks = $banks->update($values);

        return $banks;
    }
    
    public function deleteBanks($where)
    {
        $banks = new Banks();

        foreach ($where as $attribute => $value) {
            $banks = $banks->where($attribute, $value);
        }

        $banks->delete();
    }

}