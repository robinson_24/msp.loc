<?php
namespace app\Http\Controllers\Repositories;

use App\Http\Controllers\Contracts\TermInterface;
use App\Eloquent\Term;
use DB;

class TermRepository implements TermInterface
{
    public function getTerm($where = null, $order = null, $limit = null, $offset = null, $fields = null)
    {
        $term = new Term();

        if (!empty($where)) {
            foreach ($where as $attribute => $value) {
                $term = $term->where($attribute, $value);
            }
        }

        if (count($fields)) {
            foreach ($fields as $field) {
                $term = $term->addSelect($field);
            }
        }

        if (!empty($order)) {
            $term = $term->orderBy($order);
        }

        if (!empty($limit)) {
            $term = $term->take($limit);
        }

        if (!empty($offset)) {
            $term = $term->skip($offset);
        }

        return $term->get();
    }
}