<?php
namespace app\Http\Controllers\Repositories;

use App\Http\Controllers\Contracts\GufAndPartnerBanksInterface;
use App\Eloquent\GufAndPartnerBanks;
use DB;

class GufAndPartnerBanksRepository implements GufAndPartnerBanksInterface
{
    public function getGufAndPartnerBanks($where = null, $order = null, $limit = null, $offset = null, $fields = null)
    {
        $gufAndPartnerBanks = new GufAndPartnerBanks();

        if (!empty($where)) {
            foreach ($where as $attribute => $value) {
                $gufAndPartnerBanks = $gufAndPartnerBanks->where($attribute, $value);
            }
        }

        if (count($fields)) {
            foreach ($fields as $field) {
                $gufAndPartnerBanks = $gufAndPartnerBanks->addSelect($field);
            }
        }

        if (!empty($order)) {
            $gufAndPartnerBanks = $gufAndPartnerBanks->orderBy($order);
        }

        if (!empty($limit)) {
            $gufAndPartnerBanks = $gufAndPartnerBanks->take($limit);
        }

        if (!empty($offset)) {
            $gufAndPartnerBanks = $gufAndPartnerBanks->skip($offset);
        }

        return $gufAndPartnerBanks->get();
    }

    public function addGufAndPartnerBanks($where)
    {
        $gufAndPartnerBanks = new GufAndPartnerBanks();

        foreach ($where as $attribute => $value) {
            $gufAndPartnerBanks->$attribute = $value;
        }

        $gufAndPartnerBanks->save();

        return $gufAndPartnerBanks;
    }

    public function updateGufAndPartnerBanks($where, $values)
    {
        $gufAndPartnerBanks = new GufAndPartnerBanks();

        foreach ($where as $attribute => $value) {
            $gufAndPartnerBanks = $gufAndPartnerBanks->where($attribute, $value);
        }

        $gufAndPartnerBanks = $gufAndPartnerBanks->update($values);

        return $gufAndPartnerBanks;
    }

}