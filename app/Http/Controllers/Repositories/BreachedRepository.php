<?php
namespace app\Http\Controllers\Repositories;

use App\Http\Controllers\Contracts\BreachedInterface;
use App\Eloquent\Breached;
use DB;

class BreachedRepository implements BreachedInterface
{
    public function getBreached($where = null, $order = null, $limit = null, $offset = null, $fields = null)
    {
        $breached = new Breached();

        if (!empty($where)) {
            foreach ($where as $attribute => $value) {
                $breached = $breached->where($attribute, $value);
            }
        }

        if (count($fields)) {
            foreach ($fields as $field) {
                $breached = $breached->addSelect($field);
            }
        }

        if (!empty($order)) {
            $breached = $breached->orderBy($order);
        }

        if (!empty($limit)) {
            $breached = $breached->take($limit);
        }

        if (!empty($offset)) {
            $breached = $breached->skip($offset);
        }

        return $breached->get();
    }

    public function addBreached($where)
    {
        $breached = new Breached();

        foreach ($where as $attribute => $value) {
            $breached->$attribute = $value;
        }

        $breached->save();

        return $breached;
    }
    
    public function deleteBreached($where)
    {
        $breached = new Breached();

        foreach ($where as $attribute => $value) {
            $breached = $breached->where($attribute, $value);
        }

        $breached->delete();
    }

}