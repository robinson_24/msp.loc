<?php
namespace app\Http\Controllers\Repositories;

use App\Http\Controllers\Contracts\ReportsInterface;
use App\Eloquent\Reports;
use DB;

class ReportsRepository implements ReportsInterface
{
    public function getReports($where = null)
    {
        $reports = new Reports();

        if (!empty($where)) {
            foreach ($where as $attribute => $value) {
                $reports = $reports->where($attribute, $value);
            }
        }

        return $reports->get();
    }

    public function addReports($where)
    {
        $reports = new Reports();

        foreach ($where as $attribute => $value) {
            $reports->$attribute = $value;
        }

        $reports->save();

        return $reports;
    }
}