<?php
namespace app\Http\Controllers\Repositories;

use App\Http\Controllers\Contracts\KvedInterface;
use App\Eloquent\Kved;
use DB;

class KvedRepository implements KvedInterface
{
    public function getKved($where = null, $order = null, $limit = null, $offset = null, $fields = null)
    {
        $kved = new Kved();

        if (!empty($where)) {
            foreach ($where as $attribute => $value) {
                $kved = $kved->where($attribute, $value);
            }
        }

        if (count($fields)) {
            foreach ($fields as $field) {
                $kved = $kved->addSelect($field);
            }
        }

        if (!empty($order)) {
            $kved = $kved->orderBy($order);
        }

        if (!empty($limit)) {
            $kved = $kved->take($limit);
        }

        if (!empty($offset)) {
            $kved = $kved->skip($offset);
        }

        return $kved->get();
    }

    public function addKved($where)
    {
        $kved = new Kved();

        foreach ($where as $attribute => $value) {
            $kved->$attribute = $value;
        }

        $kved->save();

        return $kved;
    }

    public function updateKved($where, $values)
    {
        $kved = new Kved();

        foreach ($where as $attribute => $value) {
            $kved = $kved->where($attribute, $value);
        }

        $kved = $kved->update($values);

        return $kved;
    }
    
    public function deleteKved($where)
    {
        $kved = new Kved();

        foreach ($where as $attribute => $value) {
            $kved = $kved->where($attribute, $value);
        }

        $kved->delete();

        return true;
    }
}