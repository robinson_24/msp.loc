<?php
namespace app\Http\Controllers\Repositories;

use App\Http\Controllers\Contracts\TotalInterface;
use App\Eloquent\Total;
use DB;

class TotalRepository implements TotalInterface
{
    public function getTotal($where = null, $order = null, $limit = null, $offset = null, $fields = null)
    {
        $total = new Total();

        if (!empty($where)) {
            foreach ($where as $attribute => $value) {
                $total = $total->where($attribute, $value);
            }
        }

        if (count($fields)) {
            foreach ($fields as $field) {
                $total = $total->addSelect($field);
            }
        }

        if (!empty($order)) {
            $total = $total->orderBy($order);
        }

        if (!empty($limit)) {
            $total = $total->take($limit);
        }

        if (!empty($offset)) {
            $total = $total->skip($offset);
        }

        return $total->get();
    }
}