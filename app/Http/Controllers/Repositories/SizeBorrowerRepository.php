<?php
namespace app\Http\Controllers\Repositories;

use App\Http\Controllers\Contracts\SizeBorrowerInterface;
use App\Eloquent\SizeBorrower;
use DB;

class SizeBorrowerRepository implements SizeBorrowerInterface
{
    public function getSizeBorrower($where = null, $order = null, $limit = null, $offset = null, $fields = null)
    {
        $sizeBorrower = new SizeBorrower();

        if (!empty($where)) {
            foreach ($where as $attribute => $value) {
                $sizeBorrower = $sizeBorrower->where($attribute, $value);
            }
        }

        if (count($fields)) {
            foreach ($fields as $field) {
                $sizeBorrower = $sizeBorrower->addSelect($field);
            }
        }

        if (!empty($order)) {
            $sizeBorrower = $sizeBorrower->orderBy($order);
        }

        if (!empty($limit)) {
            $sizeBorrower = $sizeBorrower->take($limit);
        }

        if (!empty($offset)) {
            $sizeBorrower = $sizeBorrower->skip($offset);
        }

        return $sizeBorrower->get();
    }
}