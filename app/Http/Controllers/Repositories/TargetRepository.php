<?php
namespace app\Http\Controllers\Repositories;

use App\Http\Controllers\Contracts\TargetInterface;
use App\Eloquent\Target;
use DB;

class TargetRepository implements TargetInterface
{
    public function getTarget($where = null, $order = null, $limit = null, $offset = null, $fields = null)
    {
        $target = new Target();

        if (!empty($where)) {
            foreach ($where as $attribute => $value) {
                $target = $target->where($attribute, $value);
            }
        }

        if (count($fields)) {
            foreach ($fields as $field) {
                $target = $target->addSelect($field);
            }
        }

        if (!empty($order)) {
            $target = $target->orderBy($order);
        }

        if (!empty($limit)) {
            $target = $target->take($limit);
        }

        if (!empty($offset)) {
            $target = $target->skip($offset);
        }

        return $target->get();
    }

    public function addTarget($where)
    {
        $target = new Target();

        foreach ($where as $attribute => $value) {
            $target->$attribute = $value;
        }

        $target->save();

        return $target;
    }

    public function updateTarget($where, $values)
    {
        $target = new Target();

        foreach ($where as $attribute => $value) {
            $target = $target->where($attribute, $value);
        }

        $target = $target->update($values);

        return $target;
    }
    
    public function deleteTarget($where)
    {
        $target = new Target();

        foreach ($where as $attribute => $value) {
            $target = $target->where($attribute, $value);
        }

        $target->delete();

        return true;
    }
}