<?php
namespace app\Http\Controllers\Repositories;

use App\Http\Controllers\Contracts\ContractsDPRPWithGufInterface;
use App\Eloquent\ContractsDPRPWithGuf;
use DB;

class ContractsDPRPWithGufRepository implements ContractsDPRPWithGufInterface
{
    public function getContractsDPRPWithGuf($where = null, $order = null, $limit = null, $offset = null, $fields = null)
    {
        $contractsDPRPWithGuf = new ContractsDPRPWithGuf();

        if (!empty($where)) {
            foreach ($where as $attribute => $value) {
                $contractsDPRPWithGuf = $contractsDPRPWithGuf->where($attribute, $value);
            }
        }

        if (count($fields)) {
            foreach ($fields as $field) {
                $contractsDPRPWithGuf = $contractsDPRPWithGuf->addSelect($field);
            }
        }

        if (!empty($order)) {
            $contractsDPRPWithGuf = $contractsDPRPWithGuf->orderBy($order);
        }

        if (!empty($limit)) {
            $contractsDPRPWithGuf = $contractsDPRPWithGuf->take($limit);
        }

        if (!empty($offset)) {
            $contractsDPRPWithGuf = $contractsDPRPWithGuf->skip($offset);
        }

        return $contractsDPRPWithGuf->get();
    }

    public function addContractsDPRPWithGuf($where)
    {
        $contractsDPRPWithGuf = new ContractsDPRPWithGuf();

        foreach ($where as $attribute => $value) {
            $contractsDPRPWithGuf->$attribute = $value;
        }

        $contractsDPRPWithGuf->save();

        return $contractsDPRPWithGuf;
    }

    public function updateContractsDPRPWithGuf($where, $values)
    {
        $contractsDPRPWithGuf = new ContractsDPRPWithGuf();

        foreach ($where as $attribute => $value) {
            $contractsDPRPWithGuf = $contractsDPRPWithGuf->where($attribute, $value);
        }

        $contractsDPRPWithGuf = $contractsDPRPWithGuf->update($values);

        return $contractsDPRPWithGuf;
    }
    
    public function deleteContractsDPRPWithGuf($where)
    {
        $contractsDPRPWithGuf = new ContractsDPRPWithGuf();

        foreach ($where as $attribute => $value) {
            $contractsDPRPWithGuf = $contractsDPRPWithGuf->where($attribute, $value);
        }

        $contractsDPRPWithGuf->delete();
    }

}