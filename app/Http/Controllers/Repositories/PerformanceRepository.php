<?php
namespace app\Http\Controllers\Repositories;

use App\Http\Controllers\Contracts\PerformanceInterface;
use App\Eloquent\Performance;
use App\Eloquent\Users;
use App\Eloquent\SizeBorrower;
use App\Eloquent\Kved;
use DB;

class PerformanceRepository implements PerformanceInterface
{
    public function getPerformance($where = null, $order = null, $limit = null, $offset = null, $fields = null)
    {
        $performance = new Performance();

        if (!empty($where)) {
            foreach ($where as $attribute => $value) {
                $performance = $performance->where($attribute, $value);
            }
        }

        if (count($fields)) {
            foreach ($fields as $field) {
                $performance = $performance->addSelect($field);
            }
        }

        if (!empty($order)) {
            $performance = $performance->orderBy($order);
        }

        if (!empty($limit)) {
            $performance = $performance->take($limit);
        }

        if (!empty($offset)) {
            $performance = $performance->skip($offset);
        }

        return $performance->get();
    }

    public function getPerformanceJoin($where = null, $sum = null, $whereYear = null, $whereMonth = null, $whereQuarte = null)
    {
        $performanceJoin = new Performance();

        $data = [];

        if (!empty($where)) {
            for ($i = 0; $i < count($where); $i++) { 
                $performanceJoin = $performanceJoin->where($where[$i]['field'], $where[$i]['symbol'], $where[$i]['value']);
            }
        }

        if(!empty($whereYear)){
            foreach ($whereYear as $key => $value) {
                $performanceJoin = $performanceJoin->whereYear($key, $value);
            }
        }

        if(!empty($whereMonth)){
            foreach ($whereMonth as $key => $value) {
                $performanceJoin = $performanceJoin->whereMonth($key, $value);
            }
        }

        if(!empty($whereQuarte) && !empty($whereYear)){
            $performanceJoin = $performanceJoin->whereYear($key, $value);
            for($i = 0; $i < count($whereQuarte); $i++){
                $performanceJoin = $performanceJoin->whereMonth($whereQuarte[$i]['field'], $whereQuarte[$i]['symbol'], $whereQuarte[$i]['value']);
            }
        }

        $performanceJoin = $performanceJoin
            ->leftJoin('users', 'performance.id_bank', '=', 'users.id')
            ->leftJoin('size_borrower', 'performance.col_3', '=', 'size_borrower.id')
            ->leftJoin('kved as kved_section', 'performance.col_4_section', '=', 'kved_section.id')
            ->leftJoin('kved as kved_group', 'performance.col_4_group', '=', 'kved_group.id')
            ->leftJoin('kved as kved_clas', 'performance.col_4_clas', '=', 'kved_clas.id')
            ->leftJoin('target', 'performance.col_7', '=', 'target.id')
            ->where('users.is_delete', '=', 0)
            ->select('performance.*', 'users.name', 'size_borrower.size_borrower as size_borrower',
                'kved_section.kved as col_4_section', 'kved_group.kved as col_4_group', 'kved_clas.kved as col_4_clas',
                'target.target as col_7')
            ->orderBy('performance.col_20', 'desc')
            ->get();

        if(!empty($sum)){
            $data['sum_8'] = $performanceJoin->sum('col_8');
            $data['sum_9'] = $performanceJoin->sum('col_9');
            $data['sum_11'] = $performanceJoin->sum('col_11');
            $data['sum_12'] = $performanceJoin->sum('col_12');
            $data['sum_13'] = $performanceJoin->sum('col_13');
            $data['sum_14'] = $performanceJoin->sum('col_14');
            $data['sum_15'] = $performanceJoin->sum('col_15');
            $data['sum_16'] = $performanceJoin->sum('col_16');
            $data['sum_17'] = $performanceJoin->sum('col_17');
            $data['sum_18'] = $performanceJoin->sum('col_18');
        }

        $data = array_merge($data, array(
            'performanceJoin' => $performanceJoin
        ));

        return $data;
    }

    public function addPerformance($where)
    {
        $performance = new Performance();

        foreach ($where as $attribute => $value) {
            $performance->$attribute = $value;
        }

        $performance->save();

        return $performance;
    }

    public function updatePerformance($where, $values)
    {
        $performance = new Performance();

        foreach ($where as $attribute => $value) {
            $performance = $performance->where($attribute, $value);
        }

        $performance = $performance->update($values);

        return $performance;
    }

    public function deletePerformance($where)
    {
        $performance = new Performance();

        foreach ($where as $attribute => $value) {
            $performance = $performance->where($attribute, $value);
        }

        $performance->delete();
    }

    public function getChart_1_3_5()
    {
        $performance = new Performance();

        $performance = $performance
                ->select(
                    DB::raw('sum(col_13) as sumNewPlacePrev'),
                    DB::raw('sum(col_14) as sumNewPlacePres'),
                    DB::raw('sum(col_15) as sumTaxesPrev'),
                    DB::raw('sum(col_16) as sumTaxesPres'),
                    DB::raw('sum(col_17) as sumGoodsPrev'),
                    DB::raw('sum(col_18) as sumGoodsPres'),
                    DB::raw('YEAR(performance.col_20) as year, MONTH(performance.col_20) as month'))
                ->groupBy('year', 'month')
                ->leftJoin('users', 'performance.id_bank', '=', 'users.id')
                ->where('users.is_delete', 0)
                ->get();

        return $performance;
    }

    public function getChart_2_4_6()
    {
        $performance = new Performance();

        $performance = $performance
                ->select('kved.kved as kved',
                    DB::raw('sum(col_13) as sumNewPlacePrev'),
                    DB::raw('sum(col_14) as sumNewPlacePres'),
                    DB::raw('sum(col_15) as sumTaxesPrev'),
                    DB::raw('sum(col_16) as sumTaxesPres'),
                    DB::raw('sum(col_17) as sumGoodsPrev'),
                    DB::raw('sum(col_18) as sumGoodsPres'))
                ->groupBy('performance.col_4_section')
                ->leftJoin('kved', 'performance.col_4_section', '=', 'kved.id')
                ->leftJoin('users', 'performance.id_bank', '=', 'users.id')
                ->where('users.is_delete', 0)
                ->whereNotNull('performance.col_4_section')
                ->get();

        return $performance;
    }

    public function getChart_7_8_9()
    {
        $performance = new Performance();

        $performance = $performance
                ->select('size_borrower.size_borrower as size_borrower',
                    DB::raw('sum(col_13) as sumNewPlacePrev'),
                    DB::raw('sum(col_14) as sumNewPlacePres'),
                    DB::raw('sum(col_15) as sumTaxesPrev'),
                    DB::raw('sum(col_16) as sumTaxesPres'),
                    DB::raw('sum(col_17) as sumGoodsPrev'),
                    DB::raw('sum(col_18) as sumGoodsPres'))
                ->groupBy('performance.col_3')
                ->leftJoin('size_borrower', 'performance.col_3', '=', 'size_borrower.id')
                ->leftJoin('users', 'performance.id_bank', '=', 'users.id')
                ->where('users.is_delete', 0)
                ->whereNotNull('performance.col_3')
                ->get();

        return $performance;
    }

    public function getChart_10_11_12()
    {
        $performance = new Performance();

        $performance = $performance
                ->select('users.name as name',
                    DB::raw('sum(col_13) as sumNewPlacePrev'),
                    DB::raw('sum(col_14) as sumNewPlacePres'),
                    DB::raw('sum(col_15) as sumTaxesPrev'),
                    DB::raw('sum(col_16) as sumTaxesPres'),
                    DB::raw('sum(col_17) as sumGoodsPrev'),
                    DB::raw('sum(col_18) as sumGoodsPres'))
                ->groupBy('performance.id_bank')
                ->leftJoin('users', 'performance.id_bank', '=', 'users.id')
                ->where('users.is_delete', 0)
                ->get();

        return $performance;
    }

}