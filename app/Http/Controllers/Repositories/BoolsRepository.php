<?php
namespace app\Http\Controllers\Repositories;

use App\Http\Controllers\Contracts\BoolsInterface;
use App\Eloquent\Bools;
use DB;

class BoolsRepository implements BoolsInterface
{
    public function getBools($where = null, $order = null, $limit = null, $offset = null, $fields = null)
    {
        $bools = new Bools();

        if (!empty($where)) {
            foreach ($where as $attribute => $value) {
                $bools = $bools->where($attribute, $value);
            }
        }

        if (count($fields)) {
            foreach ($fields as $field) {
                $bools = $bools->addSelect($field);
            }
        }

        if (!empty($order)) {
            $bools = $bools->orderBy($order);
        }

        if (!empty($limit)) {
            $bools = $bools->take($limit);
        }

        if (!empty($offset)) {
            $bools = $bools->skip($offset);
        }

        return $bools->get();
    }
}