<?php
namespace app\Http\Controllers\Repositories;

use App\Http\Controllers\Contracts\ContractsBankWithDPRPInterface;
use App\Eloquent\ContractsBankWithDPRP;
use DB;

class ContractsBankWithDPRPRepository implements ContractsBankWithDPRPInterface
{
    public function getContractsBankWithDPRP($where = null, $order = null, $limit = null, $offset = null, $fields = null)
    {
        $contractsBankWithDPRP = new ContractsBankWithDPRP();

        if (!empty($where)) {
            foreach ($where as $attribute => $value) {
                $contractsBankWithDPRP = $contractsBankWithDPRP->where($attribute, $value);
            }
        }

        if (count($fields)) {
            foreach ($fields as $field) {
                $contractsBankWithDPRP = $contractsBankWithDPRP->addSelect($field);
            }
        }

        if (!empty($order)) {
            $contractsBankWithDPRP = $contractsBankWithDPRP->orderBy($order);
        }

        if (!empty($limit)) {
            $contractsBankWithDPRP = $contractsBankWithDPRP->take($limit);
        }

        if (!empty($offset)) {
            $contractsBankWithDPRP = $contractsBankWithDPRP->skip($offset);
        }

        return $contractsBankWithDPRP->get();
    }

    public function addContractsBankWithDPRP($where)
    {
        $contractsBankWithDPRP = new ContractsBankWithDPRP();

        foreach ($where as $attribute => $value) {
            $contractsBankWithDPRP->$attribute = $value;
        }

        $contractsBankWithDPRP->save();

        return $contractsBankWithDPRP;
    }

    public function updateContractsBankWithDPRP($where, $values)
    {
        $contractsBankWithDPRP = new ContractsBankWithDPRP();

        foreach ($where as $attribute => $value) {
            $contractsBankWithDPRP = $contractsBankWithDPRP->where($attribute, $value);
        }

        $contractsBankWithDPRP = $contractsBankWithDPRP->update($values);

        return $contractsBankWithDPRP;
    }
    
    public function deleteContractsBankWithDPRP($where)
    {
        $contractsBankWithDPRP = new ContractsBankWithDPRP();

        foreach ($where as $attribute => $value) {
            $contractsBankWithDPRP = $contractsBankWithDPRP->where($attribute, $value);
        }

        $contractsBankWithDPRP->delete();
    }

}