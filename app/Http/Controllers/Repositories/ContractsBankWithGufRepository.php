<?php
namespace app\Http\Controllers\Repositories;

use App\Http\Controllers\Contracts\ContractsBankWithGufInterface;
use App\Eloquent\ContractsBankWithGuf;
use DB;

class ContractsBankWithGufRepository implements ContractsBankWithGufInterface
{
    public function getContractsBankWithGuf($where = null, $order = null, $limit = null, $offset = null, $fields = null)
    {
        $contractsBankWithGuf = new ContractsBankWithGuf();

        if (!empty($where)) {
            foreach ($where as $attribute => $value) {
                $contractsBankWithGuf = $contractsBankWithGuf->where($attribute, $value);
            }
        }

        if (count($fields)) {
            foreach ($fields as $field) {
                $contractsBankWithGuf = $contractsBankWithGuf->addSelect($field);
            }
        }

        if (!empty($order)) {
            $contractsBankWithGuf = $contractsBankWithGuf->orderBy($order);
        }

        if (!empty($limit)) {
            $contractsBankWithGuf = $contractsBankWithGuf->take($limit);
        }

        if (!empty($offset)) {
            $contractsBankWithGuf = $contractsBankWithGuf->skip($offset);
        }

        return $contractsBankWithGuf->get();
    }

    public function addContractsBankWithGuf($where)
    {
        $contractsBankWithGuf = new ContractsBankWithGuf();

        foreach ($where as $attribute => $value) {
            $contractsBankWithGuf->$attribute = $value;
        }

        $contractsBankWithGuf->save();

        return $contractsBankWithGuf;
    }

    public function updateContractsBankWithGuf($where, $values)
    {
        $contractsBankWithGuf = new ContractsBankWithGuf();

        foreach ($where as $attribute => $value) {
            $contractsBankWithGuf = $contractsBankWithGuf->where($attribute, $value);
        }

        $contractsBankWithGuf = $contractsBankWithGuf->update($values);

        return $contractsBankWithGuf;
    }
    
    public function deleteContractsBankWithGuf($where)
    {
        $contractsBankWithGuf = new ContractsBankWithGuf();

        foreach ($where as $attribute => $value) {
            $contractsBankWithGuf = $contractsBankWithGuf->where($attribute, $value);
        }

        $contractsBankWithGuf->delete();
    }

}