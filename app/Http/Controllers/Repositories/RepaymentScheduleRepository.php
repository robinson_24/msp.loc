<?php
namespace app\Http\Controllers\Repositories;

use App\Http\Controllers\Contracts\RepaymentScheduleInterface;
use App\Eloquent\RepaymentSchedule;
use DB;

class RepaymentScheduleRepository implements RepaymentScheduleInterface
{
    public function getRepaymentSchedule($where = null, $order = null, $limit = null, $offset = null, $fields = null)
    {
        $repayment_schedule = new RepaymentSchedule();

        if (!empty($where)) {
            foreach ($where as $attribute => $value) {
                $repayment_schedule = $repayment_schedule->where($attribute, $value);
            }
        }

        if (count($fields)) {
            foreach ($fields as $field) {
                $repayment_schedule = $repayment_schedule->addSelect($field);
            }
        }

        if (!empty($order)) {
            $repayment_schedule = $repayment_schedule->orderBy($order);
        }

        if (!empty($limit)) {
            $repayment_schedule = $repayment_schedule->take($limit);
        }

        if (!empty($offset)) {
            $repayment_schedule = $repayment_schedule->skip($offset);
        }

        return $repayment_schedule->get();
    }

    public function addRepaymentSchedule($where)
    {
        $repayment_schedule = new RepaymentSchedule();

        foreach ($where as $attribute => $value) {
            $repayment_schedule->$attribute = $value;
        }

        $repayment_schedule->save();

        return $repayment_schedule;
    }
    
    public function deleteRepaymentSchedule($where)
    {
        $repayment_schedule = new RepaymentSchedule();

        foreach ($where as $attribute => $value) {
            $repayment_schedule = $repayment_schedule->where($attribute, $value);
        }

        $repayment_schedule->delete();
    }
}