<?php
namespace app\Http\Controllers\Repositories;

use App\Http\Controllers\Contracts\RefusalsInterface;
use App\Eloquent\Refusals;
use DB;

class RefusalsRepository implements RefusalsInterface
{
    public function getRefusals($where = null, $order = null, $limit = null, $offset = null, $fields = null)
    {
        $refusals = new Refusals();

        if (!empty($where)) {
            foreach ($where as $attribute => $value) {
                $refusals = $refusals->where($attribute, $value);
            }
        }

        if (count($fields)) {
            foreach ($fields as $field) {
                $refusals = $refusals->addSelect($field);
            }
        }

        if (!empty($order)) {
            $refusals = $refusals->orderBy($order);
        }

        if (!empty($limit)) {
            $refusals = $refusals->take($limit);
        }

        if (!empty($offset)) {
            $refusals = $refusals->skip($offset);
        }

        return $refusals->get();
    }

    public function addRefusals($where)
    {
        $refusals = new Refusals();

        foreach ($where as $attribute => $value) {
            $refusals->$attribute = $value;
        }

        $refusals->save();

        return $refusals;
    }
    
    public function deleteRefusals($where)
    {
        $refusals = new Refusals();

        foreach ($where as $attribute => $value) {
            $refusals = $refusals->where($attribute, $value);
        }

        $refusals->delete();
    }

}