<?php
namespace app\Http\Controllers\Repositories;

use App\Http\Controllers\Contracts\PersonalInterface;
use App\Eloquent\Personal;
use DB;

class PersonalRepository implements PersonalInterface
{
    public function getPersonal($where = null, $order = null, $limit = null, $offset = null, $fields = null)
    {
        $personal = new Personal();

        if (!empty($where)) {
            foreach ($where as $attribute => $value) {
                $personal = $personal->where($attribute, $value);
            }
        }

        if (count($fields)) {
            foreach ($fields as $field) {
                $personal = $personal->addSelect($field);
            }
        }

        if (!empty($order)) {
            $personal = $personal->orderBy($order);
        }

        if (!empty($limit)) {
            $personal = $personal->take($limit);
        }

        if (!empty($offset)) {
            $personal = $personal->skip($offset);
        }

        return $personal->get();
    }
}