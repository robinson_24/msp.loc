<?php
namespace app\Http\Controllers\Repositories;

use App\Http\Controllers\Contracts\UsersInterface;
use App\Eloquent\Users;
use App\Eloquent\Banks;
use DB;

class UsersRepository implements UsersInterface
{
    public function getUsers($where = null, $order = null, $limit = null, $offset = null, $fields = null)
    {
        $users = new Users();

        if (!empty($where)) {
            foreach ($where as $attribute => $value) {
                $users = $users->where($attribute, $value);
            }
        }

        if (count($fields)) {
            foreach ($fields as $field) {
                $users = $users->addSelect($field);
            }
        }

        if (!empty($order)) {
            $users = $users->orderBy($order);
        }

        if (!empty($limit)) {
            $users = $users->take($limit);
        }

        if (!empty($offset)) {
            $users = $users->skip($offset);
        }

        return $users->get();
    }

    public function addUsers($where)
    {
        $users = new Users();

        foreach ($where as $attribute => $value) {
            $users->$attribute = $value;
        }

        $users->save();

        return $users;
    }

    public function updateUsers($where, $values)
    {
        $users = new Users();

        foreach ($where as $attribute => $value) {
            $users = $users->where($attribute, $value);
        }

        $users = $users->update($values);

        return $users;
    }

    public function getUsersBankJoin($where = null, $whereAfter = null)
    {
        $users = new Users();

        if(!empty($where)){
            foreach ($where as $key => $value) {
                $users = $users->where($key, $value);
            }
        }

        $users = $users
            ->select('users.name', 'users.id')
            ->whereNotIn('users.id', function($query) use ($whereAfter){
                $query->select('banks.id_bank')->from('banks')->where('banks.category', '=', $whereAfter);
            })
            ->get();
        return $users;
    }
    
    public function deleteUsers($where)
    {
        $users = new Users();

        foreach ($where as $attribute => $value) {
            $users = $users->where($attribute, $value);
        }

        $users->delete();
    }

}