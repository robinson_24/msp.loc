<?php
namespace app\Http\Controllers\Repositories;

use App\Http\Controllers\Contracts\InformationInterface;
use App\Eloquent\Information;
use App\Eloquent\Users;
use DB;

class InformationRepository implements InformationInterface
{
    public function getInformation($where = null, $order = null, $limit = null, $offset = null, $fields = null)
    {
        $information = new Information();

        if (!empty($where)) {
            foreach ($where as $attribute => $value) {
                $information = $information->where($attribute, $value);
            }
        }

        if (count($fields)) {
            foreach ($fields as $field) {
                $information = $information->addSelect($field);
            }
        }

        if (!empty($order)) {
            $information = $information->orderBy($order);
        }

        if (!empty($limit)) {
            $information = $information->take($limit);
        }

        if (!empty($offset)) {
            $information = $information->skip($offset);
        }

        return $information->get();
    }

    public function getInformationJoin($where = null)
    {
        $getInformationJoin = new Information();

        if (!empty($where)) {
            foreach ($where as $attribute => $value) {
                $getInformationJoin = $getInformationJoin->where($attribute, $value);
            }
        }

        $getInformationJoin = $getInformationJoin
            ->leftJoin('images', 'information.id', '=', 'images.id_information')
            ->select('information.body', 'information.id', 'images.url', 'images.id as idImage');

        return $getInformationJoin->get();
    }

    public function getInformationBankJoin($where = null)
    {
        $getInformationJoin = new Information();

        if (!empty($where)) {
            foreach ($where as $attribute => $value) {
                $getInformationJoin = $getInformationJoin->where($attribute, $value);
            }
        }

        $getInformationJoin = $getInformationJoin
            ->leftJoin('users', 'information.id_bank', '=', 'users.id')
            ->leftJoin('images', 'information.id', '=', 'images.id_information')
            ->where('users.is_delete', '=', 0)
            ->select('information.body', 'users.name', 'users.id', 'images.url');

        return $getInformationJoin->get();
    }

    public function addInformation($where)
    {
        $information = new Information();

        foreach ($where as $attribute => $value) {
            $information->$attribute = $value;
        }

        $information->save();

        return $information;
    }

    public function updateInformation($where, $values)
    {
        $information = new Information();

        foreach ($where as $attribute => $value) {
            $information = $information->where($attribute, $value);
        }

        $information = $information->update($values);

        return $information;
    }
    
    public function deleteInformation($where)
    {
        $information = new Information();

        foreach ($where as $attribute => $value) {
            $information = $information->where($attribute, $value);
        }

        $information->delete();
    }

}