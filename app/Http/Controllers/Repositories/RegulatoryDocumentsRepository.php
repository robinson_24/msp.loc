<?php
namespace app\Http\Controllers\Repositories;

use App\Http\Controllers\Contracts\RegulatoryDocumentsInterface;
use App\Eloquent\RegulatoryDocuments;
use DB;

class RegulatoryDocumentsRepository implements RegulatoryDocumentsInterface
{
    public function getRegulatoryDocuments($where = null, $order = null, $limit = null, $offset = null, $fields = null)
    {
        $regulatoryDocuments = new RegulatoryDocuments();

        if (!empty($where)) {
            foreach ($where as $attribute => $value) {
                $regulatoryDocuments = $regulatoryDocuments->where($attribute, $value);
            }
        }

        if (count($fields)) {
            foreach ($fields as $field) {
                $regulatoryDocuments = $regulatoryDocuments->addSelect($field);
            }
        }

        if (!empty($order)) {
            $regulatoryDocuments = $regulatoryDocuments->orderBy($order);
        }

        if (!empty($limit)) {
            $regulatoryDocuments = $regulatoryDocuments->take($limit);
        }

        if (!empty($offset)) {
            $regulatoryDocuments = $regulatoryDocuments->skip($offset);
        }

        return $regulatoryDocuments->get();
    }

    public function addRegulatoryDocuments($where)
    {
        $regulatoryDocuments = new RegulatoryDocuments();

        foreach ($where as $attribute => $value) {
            $regulatoryDocuments->$attribute = $value;
        }

        $regulatoryDocuments->save();

        return $regulatoryDocuments;
    }
    
    public function deleteRegulatoryDocuments($where)
    {
        $regulatoryDocuments = new RegulatoryDocuments();

        foreach ($where as $attribute => $value) {
            $regulatoryDocuments = $regulatoryDocuments->where($attribute, $value);
        }

        $regulatoryDocuments->delete();
    }

}