<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Contracts\RegulatoryDocumentsInterface;
use Session;
use Request;
use Route;
use Validator;
use App\Libraries\GeneralFunctions;

class RegulatoryDocumentsController extends Controller 
{

    const LAWS = 'laws';
    const RESOLUTIONS = 'resolutions';
    const ORDERS = 'orders';
    const ETCS = 'etcs';

    private $regulatoryDocuments;

    public function __construct(
        RegulatoryDocumentsInterface $regulatoryDocuments
    ){
        $this->request = Request::all();
        $this->regulatoryDocuments = $regulatoryDocuments;
    }

    public function regulatoryDocumentsPage() //вывод страницы "Нормативно-розпорядчі документи щодо Програми ФКП суб'єктів МСП"
    {
        if(Session::has('userId')){
            $data = [];
            $data = array_merge($data, array(
                'title' => 'Нормативно-розпорядчі документи щодо Програми ФКП суб\'єктів МСП'
            ));
            return view('/regulatoryDocuments/regulatoryDocuments', $data);
        } else{
            return redirect('/');
        }
    }

    public function regulatoryDocumentsLorePage() //вывод страницы регуляторных документов
    {

        if(Session::has('userId')){
            if(Route::input('category') && is_string(Route::input('category'))){

                $data = [];

                $category = Route::input('category');

                $whereRegulatoryDocuments = ['regulatory_documents.category' => $category];
                $documents = $this->regulatoryDocuments->getRegulatoryDocuments($whereRegulatoryDocuments);

                if(count($documents)){
                    $data['lore'] = $documents;
                }

                if($category == self::LAWS){
                    $data['title'] = 'Закони України';
                }
                if($category == self::RESOLUTIONS){
                    $data['title'] = 'Постанови КМУ';
                }
                if($category == self::ORDERS){
                    $data['title'] = 'Розпорядження, рішення, накази КМДА';
                }
                if($category == self::ETCS){
                    $data['title'] = 'тощо';
                }

                $data = array_merge($data, array(
                    'category' => $category
                ));

                return view('/regulatoryDocuments/lore', $data);
            }else{
                return redirect('404');
            }
        }else{
            return redirect('/');
        }
    }

    public function regulatoryDocumentsLoreAdd()//добавление Регуляторного документа
    {

        if(Session::has('userId')){
            if(Route::input('category') && is_string(Route::input('category')) && Session::get('status') == 'departament'){

                $category = Route::input('category');

                unset($this->request['_token']);

                $validator = Validator::make(
                    [
                        'name' => $this->request['name']
                    ],[
                        'name' => 'required'
                    ],[
                        'name.required' => 'Поле має бути обов\'язково заповнене.'
                    ]
                );

                if($validator->fails()){
                    return redirect('/regulatoryDocuments'.'/'.$category)->with('error', $validator->messages());
                }else{
                    $data = [];

                    if(!empty($this->request['pdf'])){
                        $pathPDF = 'files/regulatoryDocuments/'.$category.'/';
                        $filePDF = 'pdf';
                        $resultPDF = GeneralFunctions::getNameFile($pathPDF, $filePDF);
                        if($resultPDF != false){
                            $data['url_pdf'] = $resultPDF;
                        }
                    }
                    if(!empty($this->request['word'])){
                        $pathWORD = 'files/regulatoryDocuments/'.$category.'/';
                        $fileWORD = 'word';
                        $resultWORD = GeneralFunctions::getNameFile($pathWORD, $fileWORD);
                        if($resultWORD != false){
                            $data['url_word'] = $resultWORD;
                        }
                    }

                    $data = array_merge($data, array(
                        'category' => $category,
                        'name' => $this->request['name']
                    ));
                    ;

                    $this->regulatoryDocuments->addRegulatoryDocuments($data);

                    return redirect('/regulatoryDocuments'.'/'.$category);
                }

            }else{
                return redirect('404');
            }
        }else{
            return redirect('/');
        }
    }

}