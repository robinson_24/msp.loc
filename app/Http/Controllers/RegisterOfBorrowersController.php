<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Contracts\BanksInterface;
use App\Http\Controllers\Contracts\UsersInterface;
use App\Http\Controllers\Contracts\BorrowersRegisterInterface;
use App\Http\Controllers\Contracts\DefsInterface;
use App\Http\Controllers\Contracts\ReportsInterface;
use App\Http\Controllers\Contracts\KvedInterface;
use Session;
use Request;
use Route;
use Validator;
use App\Libraries\GeneralFunctions;
use Excel;
use Carbon\Carbon;


class RegisterOfBorrowersController extends Controller 
{

    private $banks;
    private $users;
    private $borrowersRegister;
    private $defs;
    private $reports;
    private $kved;

    public function __construct(
        BanksInterface $banks,
        UsersInterface $users,
        BorrowersRegisterInterface $borrowersRegister,
        DefsInterface $defs,
        ReportsInterface $reports,
        KvedInterface $kved

    ){
        $this->request = Request::all();
        $this->banks = $banks;
        $this->users = $users;
        $this->borrowersRegister = $borrowersRegister;
        $this->defs = $defs;
        $this->reports = $reports;
        $this->kved = $kved;

    }

    public function registerOfBorrowersPage() //вывод страницы "Реєстр** позичальників на надання ФКП"
    {
        if(Session::has('userId')){
            
            $data = [];
            
            $where = ['banks.category' => 'registerOfBorrowers'];
            $banks = $this->banks->getBanksJoin($where);
            if(count($banks)){
                $data['banks'] = $banks;
            } else{
                $data['banks'] = '';
            }

            if(Session::get('status') == 'departament'){
                $whereUser = [
                    'users.status' => 'bank',
                    'users.is_delete' => 0
                ];
                $whereUserBank = 'registerOfBorrowers';
                $users = $this->users->getUsersBankJoin($whereUser, $whereUserBank);
                if(count($users)){
                    $data['users'] = $users;
                }
            }

            $data = array_merge($data, array(
                'title' => 'Реєстр** позичальників на надання ФКП'
            ));

            return view('/registerOfBorrowers/registerOfBorrowers', $data);
        } else{
            return redirect('/');
        }
    }

    public function registerOfBorrowersBanksAdd()//автоматическое добавление банка
    {
        $data = [];
        $data['id_bank'] = $_POST['id_bank'];
        $data['category'] = 'registerOfBorrowers';

        $result = $this->banks->addBanks($data);

        return json_encode($result);
    }

    public function registerOfBorrowersGeneralRegisterPage() //вывод страницы "ЗАГАЛЬНИЙ РЕЄСТР"
    {
        if(Session::has('userId')){
            $data = [];

            if(Session::get('status') == 'departament'){

                $generalRegister = $this->borrowersRegister->getBorrowersRegisterJoin();
                if(count($generalRegister)){
                    $data['generalRegister'] = $generalRegister;
                    
                    $whereDefs = ['def.type' => 'registerOfBorrowers'];
                    $order = 'def.created_at';
                    $defs = $this->defs->getDefs($whereDefs, $order);

                    if(count($defs)){
                        $data['budget'] = $defs;
                    } else{
                        $data['budget'] = null;
                    }
                }
                $data['reports'] = $this->reports->getReports(['reports.category' => 'registerOfBorrowers']);

            }

            $data = array_merge($data, array(
                'idBank' =>Session::get('userId'),
                'title' => 'ЗАГАЛЬНИЙ РЕЄСТР',
                'subTitle' => 'позичальників на надання ФКП'
            ));
            return view('/registerOfBorrowers/generalRegister', $data);
        }else{
            return redirect('/');
        }

    }

    public function registerOfBorrowersBankRegisterPage() //вывод страницы "Реєстр позичальників на надання ФКП"
    {
        if(Session::has('userId')){
            if(Route::input('id') && is_numeric(Route::input('id'))){
                $data = [];
                $idBank = Route::input('id');
                
                $whereBank = ['banks.id_bank' => $idBank];
                $bank = $this->banks->getBanksJoin($whereBank);
                if(count($bank)){

                    $where = [];
                    $where[] = [
                    'field'  => 'borrowersRegister.id_bank',
                    'symbol' => '=',
                    'value'  => $idBank
                    ];
                    $borrowersRegister = $this->borrowersRegister->getBorrowersRegisterJoin($where);
                    if(count($borrowersRegister)){
                        $data['borrowersRegister'] = $borrowersRegister;
                    }

                    $data = array_merge($data, array(
                        'name' => $bank[0]->name,
                        'title' => 'РЕЄСТР',
                        'subTitle' => 'позичальників на надання ФКП',
                        'idBank' => $idBank
                    ));
                    
                    return view('/registerOfBorrowers/register', $data);

                } else{
                    return redirect('404');
                }
            } else{
                return redirect('404');
            }
        }else{
            return redirect('/');
        }
    }

    public function registerOfBorrowersAddRow()
    {
        if(Session::has('userId') && Session::get('status') == 'bank'){
            $data['id_bank'] = Session::get('userId');
            $data['col_19'] = Date('Y-m-d');
            $result = $this->borrowersRegister->addBorrowersRegister($data);
            return json_encode($result);
        } else{
            return json_encode('false');
        }
    }

    public function registerOfBorrowersDeleteRow()
    {
        if(Session::has('userId') && Session::get('status') == 'bank'){
            $id = $_POST['id'];
            $where = [
                'id' => $id,
                'id_bank' => Session::get('userId')
            ];
            $this->borrowersRegister->deleteBorrowersRegister($where);
            return json_encode('true');
        }
    }

    public function registerOfBorrowersUpdateRow()
    {
        if(Session::has('userId') && Session::get('status') == 'bank'){
            $id = $_POST['id'];
            $val = $_POST['val'];
            $col = $_POST['col'];
            $id_bank = Session::get('userId');

            $where = [
                'id' => $id,
                'id_bank' => $id_bank
            ];
            $data = [
                'col_'.$col => $val
            ];

            $result = $this->borrowersRegister->updateBorrowersRegister($where, $data);
            return json_encode($result);
        }
    }

    public function registerOfBorrowersUpdateRowKved()//обновление строки в реестре
    {
        if(Session::has('userId') && Session::get('status') == 'bank'){
            $id = $_POST['id'];
            $col = $_POST['col'];
            $section = $_POST['idSection'];
            $group = $_POST['idGroup'];
            $clas = $_POST['idClas'];

            $id_bank = Session::get('userId');

            $where = [
                'id' => $id,
                'id_bank' => $id_bank
            ];
            $data = [
                'col_'.$col.'_section' => $section,
                'col_'.$col.'_group' => $group,
                'col_'.$col.'_clas' => $clas
            ];

            $result = $this->borrowersRegister->updateBorrowersRegister($where, $data);
            return json_encode($result);
        }
    }

    public function registerOfBorrowersGeneralRegisterSearchPage()//поиск по реестру
    {
        if(!empty($_GET)){
            $data = [];
            $where = [];
            $whereYear = [];
            $whereMonth = [];
            $whereQuarte = [];

            if(isset($_GET['field_1']) && isset($_GET['val_1']) && isset($_GET['type_1'])){
                switch ($_GET['type_1']) {

                    case 'term':
                        $where[] = [
                            'field'  => 'borrowersRegister.'.$_GET['field_1'],
                            'symbol' => '=',
                            'value'  => $_GET['val_1']
                        ];
                        break;

                    case 'kved':
                        $vals = explode('_', $_GET['val_1']);
                        if($vals[0] != 0){
                            $where[] = [
                                'field'  => 'borrowersRegister.'.$_GET['field_1'].'_section',
                                'symbol' => '=',
                                'value'  => $vals[0]
                            ];
                        }
                        if($vals[1] != 0 && $vals[1] != 'undefined'){
                            $where[] = [
                                'field'  => 'borrowersRegister.'.$_GET['field_1'].'_group',
                                'symbol' => '=',
                                'value'  => $vals[1]
                            ];
                        }
                        break;

                    case 'target':
                        $where[] = [
                            'field'  => 'borrowersRegister.'.$_GET['field_1'],
                            'symbol' => '=',
                            'value'  => $_GET['val_1']
                        ];
                        break;

                    case 'bank':
                        $where[] = [
                            'field'  => 'borrowersRegister.'.$_GET['field_1'],
                            'symbol' => '=',
                            'value'  => $_GET['val_1']
                        ];
                        break;

                    case 'date':
                        $where[] = [
                            'field'  => 'borrowersRegister.'.$_GET['field_1'],
                            'symbol' => '=',
                            'value'  => date("Y-m-d", strtotime($_GET['val_1']))
                        ];
                        break;

                    case 'text':
                        $where[] = [
                            'field'  => 'borrowersRegister.'.$_GET['field_1'],
                            'symbol' => 'like',
                            'value'  => '%'.$_GET['val_1'].'%'
                        ];
                        break;

                    case 'dateSelect':
                        if($_GET['val_1'] == 'щоквартально'){
                            $whereYear = ['borrowersRegister.created_at' => date('Y')];
                            if(date('m') == '01'|| date('m') == '02'|| date('m') == '03'){
                                $whereQuarte = array([
                                    'field' => 'borrowersRegister.created_at',
                                    'symbol' => '>=',
                                    'value' => '01'
                                ], [
                                    'field' => 'borrowersRegister.created_at',
                                    'symbol' => '<=',
                                    'value' => '03'
                                ]);
                            } elseif(date('m') == '04'|| date('m') == '04'|| date('m') == '06'){
                                $whereQuarte = array([
                                    'field' => 'borrowersRegister.created_at',
                                    'symbol' => '>=',
                                    'value' => '04'
                                ], [
                                    'field' => 'borrowersRegister.created_at',
                                    'symbol' => '<=',
                                    'value' => '06'
                                ]);
                            } elseif(date('m') == '07'|| date('m') == '08'|| date('m') == '09'){
                                $whereQuarte = array([
                                    'field' => 'borrowersRegister.created_at',
                                    'symbol' => '>=',
                                    'value' => '07'
                                ], [
                                    'field' => 'borrowersRegister.created_at',
                                    'symbol' => '<=',
                                    'value' => '09'
                                ]);
                            } elseif(date('m') == '10'|| date('m') == '11'|| date('m') == '12'){
                                $whereQuarte = array([
                                    'field' => 'borrowersRegister.created_at',
                                    'symbol' => '>=',
                                    'value' => '10'
                                ], [
                                    'field' => 'borrowersRegister.created_at',
                                    'symbol' => '<=',
                                    'value' => '12'
                                ]);
                            }
                        } elseif($_GET['val_1'] == 'щорічно'){
                            $whereYear = ['borrowersRegister.created_at' => date('Y')];
                        } elseif($_GET['val_1'] == 'щомісячно'){
                            $whereYear = ['borrowersRegister.created_at' => date('Y')];
                            $whereMonth = ['borrowersRegister.created_at' => date('m')];
                        } else{
                            $where[] = [
                                'field'  => 'borrowersRegister.col_19',
                                'symbol' => '=',
                                'value'  => date("Y-m-d", strtotime($_GET['val_1']))
                            ];
                        }
                        break;

                    default:
                        # code...
                        break;
                }
            }

            if(isset($_GET['field_2']) && isset($_GET['val_2']) && isset($_GET['type_2'])){
                switch ($_GET['type_2']) {

                    case 'term':
                        $where[] = [
                            'field'  => 'borrowersRegister.'.$_GET['field_2'],
                            'symbol' => '=',
                            'value'  => $_GET['val_2']
                        ];
                        break;

                    case 'target':
                        $where[] = [
                            'field'  => 'borrowersRegister.'.$_GET['field_2'],
                            'symbol' => '=',
                            'value'  => $_GET['val_2']
                        ];
                        break;

                    case 'kved':
                        $vals = explode('_', $_GET['val_2']);
                        if($vals[0] != 0){
                            $where[] = [
                                'field'  => 'borrowersRegister.'.$_GET['field_2'].'_section',
                                'symbol' => '=',
                                'value'  => $vals[0]
                            ];
                        }
                        if($vals[1] != 0 && $vals[1] != 'undefined'){
                            $where[] = [
                                'field'  => 'borrowersRegister.'.$_GET['field_2'].'_group',
                                'symbol' => '=',
                                'value'  => $vals[1]
                            ];
                        }
                        break;

                    case 'bank':
                        $where[] = [
                            'field'  => 'borrowersRegister.'.$_GET['field_2'],
                            'symbol' => '=',
                            'value'  => $_GET['val_2']
                        ];
                        break;

                    case 'date':
                        $where[] = [
                            'field'  => 'borrowersRegister.'.$_GET['field_2'],
                            'symbol' => '=',
                            'value'  => date("Y-m-d", strtotime($_GET['val_2']))
                        ];
                        break;

                    case 'text':
                        $where[] = [
                            'field'  => 'borrowersRegister.'.$_GET['field_2'],
                            'symbol' => 'like',
                            'value'  => '%'.$_GET['val_2'].'%'
                        ];
                        break;

                    case 'dateSelect':
                        if($_GET['val_2'] == 'щоквартально'){
                            $whereYear = ['borrowersRegister.created_at' => date('Y')];
                            if(date('m') == '01'|| date('m') == '02'|| date('m') == '03'){
                                $whereQuarte = array([
                                    'field' => 'borrowersRegister.created_at',
                                    'symbol' => '>=',
                                    'value' => '01'
                                ], [
                                    'field' => 'borrowersRegister.created_at',
                                    'symbol' => '<=',
                                    'value' => '03'
                                ]);
                            } elseif(date('m') == '04'|| date('m') == '04'|| date('m') == '06'){
                                $whereQuarte = array([
                                    'field' => 'borrowersRegister.created_at',
                                    'symbol' => '>=',
                                    'value' => '04'
                                ], [
                                    'field' => 'borrowersRegister.created_at',
                                    'symbol' => '<=',
                                    'value' => '06'
                                ]);
                            } elseif(date('m') == '07'|| date('m') == '08'|| date('m') == '09'){
                                $whereQuarte = array([
                                    'field' => 'borrowersRegister.created_at',
                                    'symbol' => '>=',
                                    'value' => '07'
                                ], [
                                    'field' => 'borrowersRegister.created_at',
                                    'symbol' => '<=',
                                    'value' => '09'
                                ]);
                            } elseif(date('m') == '10'|| date('m') == '11'|| date('m') == '12'){
                                $whereQuarte = array([
                                    'field' => 'borrowersRegister.created_at',
                                    'symbol' => '>=',
                                    'value' => '10'
                                ], [
                                    'field' => 'borrowersRegister.created_at',
                                    'symbol' => '<=',
                                    'value' => '12'
                                ]);
                            }
                        } elseif($_GET['val_2'] == 'щорічно'){
                            $whereYear = ['borrowersRegister.created_at' => date('Y')];
                        } elseif($_GET['val_2'] == 'щомісячно'){
                            $whereYear = ['borrowersRegister.created_at' => date('Y')];
                            $whereMonth = ['borrowersRegister.created_at' => date('m')];
                        } else{
                            $where[] = [
                                'field'  => 'borrowersRegister.col_19',
                                'symbol' => '=',
                                'value'  => date("Y-m-d", strtotime($_GET['val_2']))
                            ];
                        }
                        break;

                    default:
                        # code...
                        break;
                }
            }

            if(count($where) == 0){
                $where = null;
            }

            $generalRegister = $this->borrowersRegister->getBorrowersRegisterJoin($where, $whereYear, $whereMonth, $whereQuarte);
            if(count($generalRegister)){
                $data['generalRegister'] = $generalRegister;
                
                $whereDefs = ['def.type' => 'registerOfBorrowers'];
                $order = 'def.created_at';
                $defs = $this->defs->getDefs($whereDefs, $order);

                if(count($defs)){
                    $data['budget'] = $defs;
                } else{
                    $data['budget'] = null;
                }
            }
            $data['reports'] = $this->reports->getReports(['reports.category' => 'registerOfBorrowers']);

            
            $data = array_merge($data, array(
                'idBank'   => Session::get('userId'),
                'title'    => 'ЗАГАЛЬНИЙ РЕЄСТР',
                'subTitle' => 'погодження позичальників, які можуть претендувати на ФКП'
            ));
            return view('/registerOfBorrowers/generalRegister', $data);
        } else{
            return redirect('/registerOfBorrowers/generalRegister');
        }
    }

    public function saveReport($where = null, $whereYear = null, $whereMonth = null, $whereQuarte = null, $name = null)
    {
        $data = [];

        $generalRegister = $this->borrowersRegister->getBorrowersRegisterJoin($where, $whereYear, $whereMonth, $whereQuarte);

        if(count($generalRegister)){
            $data['generalRegister'] = $generalRegister;
        } else{
            $data['generalRegister'] = null;
        }
        
        $whereDefs = ['def.type' => 'registerOfBorrowers'];
        $defs = $this->defs->getDefs($whereDefs);

        $nameFile = self::ExcelReport($data, $defs);

        $value = [
            'name' => $name,
            'category' => 'registerOfBorrowers',
            'url' => $nameFile
        ];
        $this->reports->addReports($value);
        
        return redirect('/registerOfBorrowers/generalRegister/');

    }

    public function ExcelReport($data, $budget)
    {
        $nameFile = md5(microtime());

        Excel::create($nameFile, function($excel) use ($data, $budget) {

            $excel->sheet('ЗАГАЛЬНИЙ РЕЄСТР', function($sheet) use ($data, $budget){
                $sheet->setStyle(array(
                    'font' => array(
                        'name' => 'Arial',
                        'size' => 12
                    ),
                ));
                
                $sheet->setAutoSize(true);

                $sheet->mergeCells('A1:V1');
                $sheet->cell('A1', function($cell) {
                    $cell->setValue('ЗАГАЛЬНИЙ РЕЄСТР');
                    $cell->setAlignment('center');
                    $cell->setValignment('center');
                });

                $sheet->mergeCells('A2:V2');
                $sheet->cell('A2', function($cell) {
                    $cell->setValue('погодження позичальників, які можуть претендувати на ФКП');
                    $cell->setAlignment('center');
                    $cell->setValignment('center');
                });

                $sheet->mergeCells('A3:A4');
                $sheet->cell('A3', function($cell) {
                    $cell->setValue('№');
                    $cell->setAlignment('center');
                    $cell->setValignment('center');
                });

                $sheet->mergeCells('B3:B4');
                $sheet->cell('B3', function($cell) {
                    $cell->setValue('Назва, ЄДРПОУ');
                    $cell->setAlignment('center');
                    $cell->setValignment('center');
                });

                $sheet->mergeCells('C3:C4');
                $sheet->cell('C3', function($cell) {
                    $cell->setValue('Місце державної реєстрації, розташування виробничих потужностей (критерій 1)');
                    $cell->setAlignment('center');
                    $cell->setValignment('center');
                });

                $sheet->mergeCells('D3:D4');
                $sheet->cell('D3', function($cell) {
                    $cell->setValue('КВЕД 2010 (критерій 2)');
                    $cell->setAlignment('center');
                    $cell->setValignment('center');
                });

                $sheet->mergeCells('E3:E4');
                $sheet->cell('E3', function($cell) {
                    $cell->setValue('Цільове призначення (критерій 3)');
                    $cell->setAlignment('center');
                    $cell->setValignment('center');
                });


                $sheet->mergeCells('F3:F4');
                $sheet->cell('F3', function($cell) {
                    $cell->setValue('Власний внесок позичальника (критерій 4)');
                    $cell->setAlignment('center');
                    $cell->setValignment('center');
                });

                $sheet->mergeCells('G3:G4');
                $sheet->cell('G3', function($cell) {
                    $cell->setValue('Дата погодження надання ФКП');
                    $cell->setAlignment('center');
                    $cell->setValignment('center');
                });

                $sheet->mergeCells('H3:Q3');
                $sheet->cell('H3', function($cell) {
                    $cell->setValue('Кредитний догорів');
                    $cell->setAlignment('center');
                    $cell->setValignment('center');
                });
                $sheet->cell('H4', function($cell) {
                    $cell->setValue('№, дата');
                    $cell->setAlignment('center');
                    $cell->setValignment('center');
                });

                $sheet->cell('I4', function($cell) {
                    $cell->setValue('Строк');
                    $cell->setAlignment('center');
                    $cell->setValignment('center');
                });

                $sheet->cell('J4', function($cell) {
                    $cell->setValue('Сума кредиту на початок звітного місяця, тис.грн');
                    $cell->setAlignment('center');
                    $cell->setValignment('center');
                });

                $sheet->cell('K4', function($cell) {
                    $cell->setValue('Відсоткова ставка, %');
                    $cell->setAlignment('center');
                    $cell->setValignment('center');
                });

                $sheet->cell('L4', function($cell) {
                    $cell->setValue('Загальна сума розрахованих відсотків у звітному місяці, %');
                    $cell->setAlignment('center');
                    $cell->setValignment('center');
                });

                $sheet->cell('M4', function($cell) {
                    $cell->setValue('Частка компенсації відсоткової ставки, %');
                    $cell->setAlignment('center');
                    $cell->setValignment('center');
                });

                $sheet->cell('N4', function($cell) {
                    $cell->setValue('Сума ФКП за звітний місяць, грн');
                    $cell->setAlignment('center');
                    $cell->setValignment('center');
                });

                $sheet->cell('O4', function($cell) {
                    $cell->setValue('Загальний розмір ФКП за весь період, грн');
                    $cell->setAlignment('center');
                    $cell->setValignment('center');
                });

                $sheet->cell('P4', function($cell) {
                    $cell->setValue('Плановий розмір ФКП в місяці, наступному за звітним періодом, грн');
                    $cell->setAlignment('center');
                    $cell->setValignment('center');
                });

                $sheet->cell('Q4', function($cell) {
                    $cell->setValue('Залишковий розмір ФКП, тис.грн');
                    $cell->setAlignment('center');
                    $cell->setValignment('center');
                });

                $sheet->mergeCells('R3:R4');
                $sheet->cell('R3', function($cell) {
                    $cell->setValue('Порядковий номер місця участі в ФКП (загальний термін 24 місяці)');
                    $cell->setAlignment('center');
                    $cell->setValignment('center');
                });

                $sheet->mergeCells('S3:S4');
                $sheet->cell('S3', function($cell) {
                    $cell->setValue('Банк-партнер');
                    $cell->setAlignment('center');
                    $cell->setValignment('center');
                });

                $sheet->mergeCells('T3:T4');
                $sheet->cell('T3', function($cell) {
                    $cell->setValue('Дата заповнення реєстру');
                    $cell->setAlignment('center');
                    $cell->setValignment('center');
                });

                $sheet->mergeCells('U3:U4');
                $sheet->cell('U3', function($cell) {
                    $cell->setValue('Сума коштів ФКП, затверджена в бюджеті м.Києва на відповідний рік, грн');
                    $cell->setAlignment('center');
                    $cell->setValignment('center');
                });

                $sheet->mergeCells('V3:V4');
                $sheet->cell('V3', function($cell) {
                    $cell->setValue('Залишок бюджетніх коштів на ФКП');
                    $cell->setAlignment('center');
                    $cell->setValignment('center');
                });

                $sheet->row(5, [
                    1,
                    2,
                    3,
                    4,
                    5,
                    6,
                    7,
                    8,
                    9,
                    10,
                    11,
                    12,
                    13,
                    14,
                    15,
                    16,
                    17,
                    18,
                    19,
                    20,
                    21,
                    22
                ]);
                $sheet->row(5, function($row){
                    $row->setAlignment('center');
                    $row->setValignment('center');
                });

                if($data['generalRegister'] != null){
                    $years = [];
                    $n = 1;
                    $rows = 5;
                    for ($i = 0; $i < count($data['generalRegister']); $i++) { 
                        
                        if(!in_array(date('Y', strtotime($data['generalRegister'][$i]->col_19)), $years)){
                            
                            $years[] = date('Y', strtotime($data['generalRegister'][$i]->col_19));
                            $sum_6 = 0;
                            $sum_10 = 0;
                            $sum_12 = 0;
                            $sum_14 = 0;
                            $sum_15 = 0;
                            $sum_16 = 0;
                            $sum_17 = 0;
                            $budgetNow = 0;
                            $rowsOb = 0;

                            for ($j = 0; $j < count($data['generalRegister']); $j++) { 

                                if(date('Y', strtotime($data['generalRegister'][$i]->col_19)) == date('Y', strtotime($data['generalRegister'][$j]->col_19))){

                                    $sum_6 += $data['generalRegister'][$j]->col_6;
                                    $sum_10 += $data['generalRegister'][$j]->col_10;
                                    $sum_12 += $data['generalRegister'][$j]->col_12;
                                    $sum_14 += $data['generalRegister'][$j]->col_14;
                                    $sum_15 += $data['generalRegister'][$j]->col_15;
                                    $sum_16 += $data['generalRegister'][$j]->col_16;
                                    $sum_17 += $data['generalRegister'][$j]->col_17;

                                    $sheet->row($rows+1, [
                                        $n,
                                        $data['generalRegister'][$j]->col_2,
                                        $data['generalRegister'][$j]->col_3,
                                        $data['generalRegister'][$i]->col_4_section.' / '.$data['generalRegister'][$i]->col_4_group.' / '.$data['generalRegister'][$i]->col_4_clas,
                                        $data['generalRegister'][$j]->col_5,
                                        $data['generalRegister'][$j]->col_6,
                                        \Carbon\Carbon::parse($data['generalRegister'][$j]->col_7)->format('d.m.Y'),
                                        $data['generalRegister'][$j]->col_8,
                                        $data['generalRegister'][$i]->col_9,
                                        $data['generalRegister'][$j]->col_10/1000,
                                        $data['generalRegister'][$j]->col_11,
                                        $data['generalRegister'][$j]->col_12,
                                        $data['generalRegister'][$j]->col_13,
                                        $data['generalRegister'][$j]->col_14,
                                        $data['generalRegister'][$j]->col_15,
                                        $data['generalRegister'][$j]->col_16,
                                        $data['generalRegister'][$j]->col_17/1000,
                                        $data['generalRegister'][$j]->col_18,
                                        $data['generalRegister'][$j]->name,
                                        \Carbon\Carbon::parse($data['generalRegister'][$j]->col_19)->format('d.m.Y')
                                    ]);

                                    if(count($budget)){
                                        
                                        for ($b = 0; $b < count($budget); $b++) { 

                                            if(date('Y', strtotime($data['generalRegister'][$j]->col_19)) == date('Y', strtotime($budget[$b]->created_at))){
                                                if($budget[$b]->value != null){
                                                    $budgetNow = $budget[$b]->value;
                                                }else{
                                                    $budgetNow = 'Дані не внесені';
                                                }
                                            }
                                        }
                                    }else{
                                        $budgetNow = 'Дані не внесені';
                                    }

                                    $n++;
                                    $rows++;
                                    $rowsOb++;
                                }
                                if($j == count($data['generalRegister'])-1){
                                    $sheet->row($rows+1, [
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        $sum_6,
                                        '',
                                        '',
                                        '',
                                        ($sum_10/1000),
                                        '',
                                        $sum_12,
                                        '',
                                        $sum_14,
                                        $sum_15,
                                        $sum_16,
                                        ($sum_17/1000),
                                        '',
                                        '',
                                        ''
                                    ]);
                                    $rows++;

                                    $sheet->mergeCells('U'.($rows-$rowsOb).':U'.$rows);
                                    $sheet->cell('U'.($rows-$rowsOb), function($cell) use($budgetNow) {
                                        $cell->setValue($budgetNow);
                                        $cell->setAlignment('center');
                                        $cell->setValignment('center');
                                    });

                                    $sheet->mergeCells('V'.($rows-$rowsOb).':V'.$rows);
                                    $sheet->cell('V'.($rows-$rowsOb), function($cell) use($budgetNow, $sum_14) {
                                        $cell->setValue(($budgetNow != 'Дані не внесені') ? floatval($budgetNow)-$sum_14 : 'Неможливо розрахувати');
                                        $cell->setAlignment('center');
                                        $cell->setValignment('center');
                                    });
                                }
                            }
                        }
                    }
                } else{
                    $sheet->mergeCells('A6:V6');
                    $sheet->cell('A6', function($cell) {
                        $cell->setValue('Інформації немає');
                        $cell->setAlignment('center');
                        $cell->setValignment('center');
                    });
                }
            });
        
        })->store('xls', 'files/reports');
        
        return $nameFile.'.xls';
    }

    public function chart_1()
    {
        $result = $this->borrowersRegister->getChart_1();

        return json_decode($result);
    }

    public function chart_4_5_6_7()
    {
        $result = $this->borrowersRegister->getChart_4_5_6_7();
        return json_decode($result);
    }

    public function chart_8_9_10()
    {
        $result = $this->borrowersRegister->getChart_8_9_10();
        return json_decode($result);
    }

    public function chart_11_12_13()
    {
        $result = $this->borrowersRegister->getChart_11_12_13();
        return json_decode($result);
    }

}