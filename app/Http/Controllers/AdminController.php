<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
//use App\Http\Controllers\Contracts\BanksInterface;
use App\Http\Controllers\Contracts\UsersInterface;
use App\Http\Controllers\Contracts\DefsInterface;
use App\Http\Controllers\Contracts\TargetInterface;
use App\Http\Controllers\Contracts\KvedInterface;
//use App\Http\Controllers\Contracts\BoolsInterface;
//use App\Http\Controllers\Contracts\PersonalInterface;
//use App\Http\Controllers\Contracts\TotalInterface;
//use App\Http\Controllers\Contracts\TermInterface;
//use App\Http\Controllers\Contracts\StackingInterface;
use Session;
use Request;
use Route;
use Response;
use Validator;
use App\Libraries\GeneralFunctions;

class AdminController extends Controller 
{

    private $users;
    private $defs;
    private $target;
    private $kved;

    public function __construct(
        UsersInterface $users,
        DefsInterface $defs,
        TargetInterface $target,
        KvedInterface $kved
    ){
        $this->request = Request::all();
        $this->users = $users;
        $this->defs = $defs;
        $this->target = $target;
        $this->kved = $kved;
    }

    public function adminPage() //вывод страницы  "Админістрування"
    {
        if(Session::has('userId')){
            if(Session::get('status') == 'departament'){
                $data = [];

                $whereDefault = [
                    'id_user' => Session::get('userId'),
                    'type' => 'registerOfBorrowers'
                ];

                $default = $this->defs->getDefsNowYear($whereDefault);
                if(count($default)){
                    $data['default'] = $default;
                }

                $target = $this->target->getTarget();
                if(count($target)){
                    $data['targets'] = $target;
                }

                $order = 'kved.kved';
                $kved = $this->kved->getKved(null, $order);
                if(count($kved)){
                    $data['kveds'] = $kved;
                }

                $whereUser = [
                    'users.status' => 'bank',
                    'users.is_delete' => 0
                ];
                $data['users'] = $this->users->getUsers($whereUser);

                $data = array_merge($data, array(
                    'title' => 'Адміністрування',
                    'success' => Session::has('success') ? Session::get('success') : null,
                    'error' => Session::has('error') ? Session::get('error') : null,
                    'errorValid' => Session::has('errorValid') ? Session::get('errorValid') : null,
                    'active' => Session::has('active') ? Session::get('active') : 'bank'
                ));
                return view('/admin', $data);
            } else{
                return redirect('404');
            }
        } else{
            return redirect('/');
        }
    }

    public function createBank()//создание банка
    {
        if(Session::has('userId')){
            if(Session::get('status') == 'departament'){
                $login = GeneralFunctions::trimString($this->request['loginBank'], true);
                $name = GeneralFunctions::trimString($this->request['nameBank'], true);
                $password = GeneralFunctions::trimString($this->request['passwordBank'], true);

                $validator = Validator::make(
                    [
                        'login' => $login,
                        'password' => $password,
                        'name' => $name,
                    ],[
                        'login' => 'required|unique:users',
                        'password' => 'required',
                        'name' => 'required',
                    ],[
                        'login.required' => 'Поле "Користувач" має бути заповненим',
                        'login.unique' => 'Такий користувач зареєстрований',
                        'password.required' => 'Поле "Пароль" має бути заповненим',
                        'name.required' => 'Поле "Назва" має бути заповненим',
                    ]
                );
                if($validator->fails()){
                    return redirect('/admin')->with('errorValid', $validator->messages());
                } else{

                    $salt      = GeneralFunctions::createSalt();
                    $password = GeneralFunctions::genPassword($password, $salt);
                    
                    $value = [
                        'login' => $login,
                        'name' => $name,
                        'password' => $password,
                        'status' => 'bank',
                        'salt' => $salt
                    ];

                    $result = $this->users->addUsers($value);
                    if($result){
                        return redirect('/admin')->with('success', 'Ви успішно створили банк')->with('active', 'bank');
                    } else{
                        return redirect('/admin')->with('error', 'Щось пішло не так')->with('active', 'bank');
                    }
                }
            } else{
                return redirect('404');
            }
        } else{
            return redirect('/');
        }
    }

    public function editBank()//редактирование банка
    {
        if(Session::has('userId')){
            if(Session::get('status') == 'departament'){
                $id = $this->request['id'];
                $name = GeneralFunctions::trimString($this->request['nameBank'], true);
                $password = GeneralFunctions::trimString($this->request['passwordBank'], true);

                $value = [];

                if(!empty($name)){
                    $value['name'] = $name;
                }

                if(!empty($password)){
                    $value['salt']     = GeneralFunctions::createSalt();
                    $value['password'] = GeneralFunctions::genPassword($password, $value['salt']);
                }

                if(count($value)){
                    $where = ['users.id' => $id];
                    $result = $this->users->updateUsers($where, $value);
                }

                if($result){
                    return redirect('/admin')->with('success', 'Ви успішно внесли зміни')->with('active', 'bank');
                }else{
                    return redirect('/admin')->with('error', 'Щось пішло не так')->with('active', 'bank');
                }
                return redirect('/admin');
            } else{
                return redirect('404');
            }
        } else{
            return redirect('/');
        }
    }

    public function deleteBank()//удаление банка
    {
        if(Session::has('userId')){
            if(Session::get('status') == 'departament'){
                $id = $this->request['id'];
                $password = GeneralFunctions::trimString($this->request['password'], true);

                $user = $this->users->getUsers(['users.id' => Session::get('userId')]);
                if(count($user)){
                    $salt = $user[0]['salt'];
                    if(GeneralFunctions::genPassword($password, $salt) == $user[0]['password']){
                        $where = ['users.id' => $id];
                        $value = ['users.is_delete' => 1];
                        $result = $this->users->updateUsers($where, $value);
                        if($result){
                            return redirect('/admin')->with('success', 'Успішно видалили банк')->with('active', 'bank');
                        }else{
                            return redirect('/admin')->with('error', 'Щось пішло не так')->with('active', 'bank');
                        }
                        
                    } else{
                        return redirect('/admin')->with('error', 'Не правильний пароль')->with('active', 'bank');
                    }
                } else{
                    return redirect('/admin');
                }
            } else{
                return redirect('404');
            }
        } else{
            return redirect('/');
        }
    }

    public function borrowersDefault()
    {
        if(Session::has('userId')){
            if(Session::get('status') == 'departament'){
                $whereDefault = [
                    'id_user' => Session::get('userId'),
                    'type' => 'registerOfBorrowers'
                ];

                $default = $this->defs->getDefsNowYear($whereDefault);
                if(count($default) == 0){
                    $value = GeneralFunctions::trimString($this->request['borrowersDefault'], true);
                    $data = [
                        'id_user' => Session::get('userId'),
                        'type' => 'registerOfBorrowers',
                        'value' => $value
                    ];
                    $result = $this->defs->addDefs($data);
                    if($result){
                        return redirect('/admin')->with('success', 'Успішно внесені дані')->with('active', 'borrowers');
                    } else{
                        return redirect('/admin')->with('error', 'Щось пішло не так')->with('active', 'borrowers');
                    }
                    
                }
            } else{
                return redirect ('/');
            }
        } else{
            return redirect('/');
        }
    }

    public function createTarget()//создание цели
    {
        if(Session::has('userId')){
            if(Session::get('status') == 'departament'){
                $target = GeneralFunctions::trimString($this->request['nameTarget'], true);
                
                $validator = Validator::make(
                    [
                        'target' => $target,
                    ],[
                        'target' => 'required',
                    ],[
                        'target.required' => 'Поле "Ціль кредиту" має бути заповненим'
                    ]
                );
                if($validator->fails()){
                    return redirect('/admin')->with('errorValid', $validator->messages());
                } else{

                    $value = [
                        'target' => $target
                    ];

                    $result = $this->target->addTarget($value);
                    if($result){
                        return redirect('/admin')->with('success', 'Ви успішно додали ціль кредиту')->with('active', 'target');
                    } else{
                        return redirect('/admin')->with('error', 'Щось пішло не так')->with('active', 'target');
                    }
                }
            } else{
                return redirect('404');
            }
        } else{
            return redirect('/');
        }
    }

    public function editTarget()//редактирование цели
    {
        if(Session::has('userId')){
            if(Session::get('status') == 'departament'){
                $id = $this->request['id'];
                $nameTarget = GeneralFunctions::trimString($this->request['nameTarget'], true);
                
                $value = [];

                if(!empty($nameTarget)){
                    $value['target'] = $nameTarget;
                }

                if(count($value)){
                    $where = ['target.id' => $id];
                    $result = $this->target->updateTarget($where, $value);
                }

                if($result){
                    return redirect('/admin')->with('success', 'Ви успішно внесли зміни')->with('active', 'target');
                }else{
                    return redirect('/admin')->with('error', 'Щось пішло не так')->with('active', 'target');
                }
                return redirect('/admin');
            } else{
                return redirect('404');
            }
        } else{
            return redirect('/');
        }
    }

    public function deleteTarget()//удаление цели
    {
        if(Session::has('userId')){
            if(Session::get('status') == 'departament'){
                $id = $this->request['id'];
                $password = GeneralFunctions::trimString($this->request['password'], true);

                $user = $this->users->getUsers(['users.id' => Session::get('userId')]);
                if(count($user)){
                    if(GeneralFunctions::genPassword($password, $user[0]['salt']) == $user[0]['password']){
                        $where = ['target.id' => $id];
                        $result = $this->target->deleteTarget($where);
                        if($result){
                            return redirect('/admin')->with('success', 'Успішно видалили ціль кредиту')->with('active', 'target');
                        }else{
                            return redirect('/admin')->with('error', 'Щось пішло не так')->with('active', 'target');
                        }
                        
                    } else{
                        return redirect('/admin')->with('error', 'Не правильний пароль')->with('active', 'target');
                    }
                } else{
                    return redirect('/admin');
                }
            } else{
                return redirect('404');
            }
        } else{
            return redirect('/');
        }
    }

    public function createKved()//создание КВЕД 2010
    {
        if(Session::has('userId')){
            if(Session::get('status') == 'departament'){
                $kved = GeneralFunctions::trimString($this->request['newKved'], true);
                
                $validator = Validator::make(
                    [
                        'kved' => $kved,
                    ],[
                        'kved' => 'required',
                    ],[
                        'kved.required' => 'Поле "КВЕД 2010" має бути заповненим'
                    ]
                );
                if($validator->fails()){
                    return redirect('/admin')->with('errorValid', $validator->messages());
                } else{

                    $value = [
                        'kved' => $kved
                    ];

                    $result = $this->kved->addKved($value);
                    if($result){
                        return redirect('/admin')->with('success', 'Ви успішно додали КВЕД 2010')->with('active', 'kved');
                    } else{
                        return redirect('/admin')->with('error', 'Щось пішло не так')->with('active', 'kved');
                    }
                }
            } else{
                return redirect('404');
            }
        } else{
            return redirect('/');
        }
    }

    public function deleteKved()//удаление КВЕД 2010 и всех групп в этом квед и классов
    {
        if(Session::has('userId')){
            if(Session::get('status') == 'departament'){
                $id = explode('-', $this->request['id'])[1];

                $password = GeneralFunctions::trimString($this->request['password'], true);

                $user = $this->users->getUsers(['users.id' => Session::get('userId')]);
                if(count($user)){
                    if(GeneralFunctions::genPassword($password, $user[0]['salt']) == $user[0]['password']){
                        
                        $kveds = $this->kved->getKved();
                        if(count($kveds)){
                            for ($i = 0; $i < count($kveds); $i++) { 
                                if($kveds[$i]['id'] == $id){
                                    try {
                                        for ($j = 0; $j < count($kveds); $j++) { 
                                            if ($kveds[$i]['id'] == $kveds[$j]['parent_kved']) {
                                                $this->kved->deleteKved(['kved.parent_kved' => $kveds[$j]['id']]);
                                                $this->kved->deleteKved(['kved.parent_kved' => $kveds[$i]['id']]);
                                            }
                                        }
                                        $this->kved->deleteKved(['kved.id' => $id]);
                                    } catch (\Exception $e) {
                                        return redirect('/admin')->with('error', 'Щось пішло не так')->with('active', 'kved');
                                    }
                                }
                            }
                            return redirect('/admin')->with('success', 'Успішно видалили КВЕД 2010')->with('active', 'kved');
                        } else{
                            return redirect('/admin')->with('error', 'Щось пішло не так')->with('active', 'kved');
                        }
                        
                    } else{
                        return redirect('/admin')->with('error', 'Не правильний пароль')->with('active', 'kved');
                    }
                } else{
                    return redirect('/admin');
                }
            } else{
                return redirect('404');
            }
        } else{
            return redirect('/');
        }
    }

    public function addGroup()
    {
        $valueAdd = [];
        unset($_POST['_token']);
        foreach ($_POST as $key => $value) {
            $valueAdd['kved.'.$key] = $value;
        }

        $result = $this->kved->addKved($valueAdd);

        return json_encode($result);
    }

    public function removeGroup()
    {
        try {
            $this->kved->deleteKved(['kved.id' => $_POST['id']]);
            $this->kved->deleteKved(['kved.parent_kved' => $_POST['id']]);
        } catch (\Exception $e) {
            return json_encode('false');
        }
        return json_encode('true');
    }

    public function removeClas()
    {
        try {
            $this->kved->deleteKved(['kved.id' => $_POST['id']]);
        } catch (\Exception $e) {
            return json_encode('false');
        }
        return json_encode('true');
    }

    public function editGroup()
    {
        $where = [];
        $value = [];
        $where = ['kved.id' => $_POST['id']];
        $value = ['kved.kved' => $_POST['kved']];
        $result = $this->kved->updateKved($where, $value);

        return json_encode($result);
    }
}