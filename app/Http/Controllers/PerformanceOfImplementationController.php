<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Contracts\BanksInterface;
use App\Http\Controllers\Contracts\UsersInterface;
use App\Http\Controllers\Contracts\PerformanceInterface;
use App\Http\Controllers\Contracts\ReportsInterface;
use App\Http\Controllers\Contracts\KvedInterface;
use Session;
use Request;
use Route;
use Validator;
use App\Libraries\GeneralFunctions;
use Excel;

class PerformanceOfImplementationController extends Controller 
{

    private $banks;
    private $users;
    private $performance;
    private $reports;
    private $kved;

    public function __construct(
        BanksInterface $banks,
        UsersInterface $users,
        PerformanceInterface $performance,
        ReportsInterface $reports,
        KvedInterface $kved

    ){
        $this->request = Request::all();
        $this->banks = $banks;
        $this->users = $users;
        $this->performance = $performance;
        $this->reports = $reports;
        $this->kved = $kved;

    }

    public function performanceOfImplementationPage() //вывод страницы "Інформація щодо показників результативності впровадження ФКП"
    {
         if(Session::has('userId')){
            $data = [];

            $where = ['banks.category' => 'performanceOfImplementation'];
            $banks = $this->banks->getBanksJoin($where);
            if(count($banks)){
                $data['banks'] = $banks;
            } else{
                $data['banks'] = '';
            }

            if(Session::get('status') == 'departament'){
                $whereUser = [
                    'users.status' => 'bank',
                    'users.is_delete' => 0
                ];
                $whereUserBank = 'performanceOfImplementation';
                $users = $this->users->getUsersBankJoin($whereUser, $whereUserBank);
                if(count($users)){
                    $data['users'] = $users;
                }
            }

            $data = array_merge($data, array(
                'title' => 'Інформація щодо показників результативності впровадження ФКП'
            ));
            return view('/performanceOfImplementation/performanceOfImplementation', $data);
        }else{
            return redirect('/');
        }
    }

    public function performanceOfImplementationBanksAdd()//автоматическое добавление банка
    {

        $data = [];
        $data['id_bank'] = $_POST['id_bank'];
        $data['category'] = 'performanceOfImplementation';

        $result = $this->banks->addBanks($data);

        return json_encode($result);
    }

    public function performanceOfImplementationGeneralInformationPage() //вывод страницы ЗАГАЛЬНА ІНФОРМАЦІЯ
    {
        if(Session::has('userId')){
            $data = [];

            if(Session::get('status') == 'departament'){

                $performance = $this->performance->getPerformanceJoin(null, true);
                if(count($performance)){
                    foreach ($performance as $key => $value) {
                        if($key == 'performanceJoin'){
                            $data['performance'] = $performance['performanceJoin'];
                        } else{
                            $data[$key] = $value;
                        }
                    }
                }

                $data['reports'] = $this->reports->getReports(['reports.category' => 'performance']);
            }
            $data = array_merge($data, array(
                'idBank' => Session::get('userId'),
                'title' => 'ЗАГАЛЬНА ІНФОРМАЦІЯ ЩОДО ПОКАЗНИКІВ РЕЗУЛЬТАТИВНОСТІ',
                'subTitle' => 'впровадження ФКП'
            ));
            return view('/performanceOfImplementation/generalInformation', $data);
        }else{
            return redirect('/');
        }
    }

    public function performanceOfImplementationBankInformationPage() //вывод страницы "ІНФОРМАЦІЯ ЩОДО ПОКАЗНИКІВ РЕЗУЛЬТАТИВНОСТІ"
    {
        if(Session::has('userId')){
            if(Route::input('id') && is_numeric(Route::input('id'))){
                $data = [];
                $idBank = Route::input('id');
                
                $whereBank = [
                    'banks.id_bank' => $idBank
                ];
                $bank = $this->banks->getBanksJoin($whereBank);
                if(count($bank)){

                    $where = [];
                    $where[] = [
                    'field' => 'performance.id_bank',
                    'symbol' => '=',
                    'value' => $idBank];
                    $performance = $this->performance->getPerformanceJoin($where);
                    if(count($performance)){
                        $data['performance'] = $performance['performanceJoin'];
                    }

                    $data = array_merge($data, array(
                        'name' => $bank[0]->name,
                        'title' => 'ІНФОРМАЦІЯ ЩОДО ПОКАЗНИКІВ РЕЗУЛЬТАТИВНОСТІ',
                        'subTitle' => 'впровадження ФКП',
                        'idBank' => $idBank
                    ));
                    return view('/performanceOfImplementation/bankInformation', $data);

                } else{
                    return redirect('404');
                }
            } else{
                return redirect('404');
            }
        }else{
            return redirect('/');
        }
    }

    public function performanceOfImplementationAddRow()
    {
        if(Session::has('userId') && Session::get('status') == 'bank'){
            $data['id_bank'] = Session::get('userId');
            $data['col_20'] = Date('Y-m-d');
            $result = $this->performance->addPerformance($data);
            return json_encode($result);
        } else{
            return json_encode('false');
        }
    }

    public function performanceOfImplementationDeleteRow()
    {
        if(Session::has('userId') && Session::get('status') == 'bank'){
            $id = $_POST['id'];
            $where = [
                'id' => $id,
                'id_bank' => Session::get('userId')
            ];
            $this->performance->deletePerformance($where);
            return json_encode('true');
        }
    }

    public function performanceOfImplementationUpdateRow()
    {
        if(Session::has('userId') && Session::get('status') == 'bank'){
            $id = $_POST['id'];
            $val = $_POST['val'];
            $col = $_POST['col'];
            $id_bank = Session::get('userId');

            $where = [
                'id' => $id,
                'id_bank' => $id_bank
            ];
            $data = [
                'col_'.$col => $val
            ];

            $result = $this->performance->updatePerformance($where, $data);
            return json_encode($result);
        }
    }

    public function performanceOfImplementationUpdateRowKved()//обновление строки в реестре КВЕД 2010
    {
        if(Session::has('userId') && Session::get('status') == 'bank'){
            $id = $_POST['id'];
            $col = $_POST['col'];
            $section = $_POST['idSection'];
            $group = $_POST['idGroup'];
            $clas = $_POST['idClas'];

            $id_bank = Session::get('userId');

            $where = [
                'id' => $id,
                'id_bank' => $id_bank
            ];
            $data = [
                'col_'.$col.'_section' => $section,
                'col_'.$col.'_group' => $group,
                'col_'.$col.'_clas' => $clas
            ];

            $result = $this->performance->updatePerformance($where, $data);
            return json_encode($result);
        }
    }

    public function performanceOfImplementationSearchPage()
    {

        if(!empty($_GET)){
            $data = [];
            $where = [];
            $whereYear = [];
            $whereMonth = [];
            $whereQuarte = [];

            if(isset($_GET['field_1']) && isset($_GET['val_1']) && isset($_GET['type_1'])){
                switch ($_GET['type_1']) {

                    case 'bank':
                        $where[] = [
                            'field'  => 'performance.'.$_GET['field_1'],
                            'symbol' => '=',
                            'value'  => $_GET['val_1']
                        ];
                        break;

                    case 'target':
                        $where[] = [
                            'field'  => 'performance.'.$_GET['field_1'],
                            'symbol' => '=',
                            'value'  => $_GET['val_1']
                        ];
                        break;

                    case 'text':
                        $where[] = [
                            'field'  => 'performance.'.$_GET['field_1'],
                            'symbol' => 'like',
                            'value'  => '%'.$_GET['val_1'].'%'
                        ];
                        break;

                    case 'kved':
                        $vals = explode('_', $_GET['val_1']);
                        if($vals[0] != 0){
                            $where[] = [
                                'field'  => 'performance.'.$_GET['field_1'].'_section',
                                'symbol' => '=',
                                'value'  => $vals[0]
                            ];
                        }
                        if($vals[1] != 0 && $vals[1] != undefined){
                            $where[] = [
                                'field'  => 'performance.'.$_GET['field_1'].'_group',
                                'symbol' => '=',
                                'value'  => $vals[1]
                            ];
                        }
                        break;

                    case 'size-borrower':
                        $where[] = [
                            'field'  => 'performance.'.$_GET['field_1'],
                            'symbol' => '=',
                            'value'  => $_GET['val_1']
                        ];
                        break;

                    case 'dateSelect':
                        if($_GET['val_1'] == 'щоквартально'){
                            $whereYear = ['performance.created_at' => date('Y')];
                            if(date('m') == '01'|| date('m') == '02'|| date('m') == '03'){
                                $whereQuarte = array([
                                    'field' => 'performance.created_at',
                                    'symbol' => '>=',
                                    'value' => '01'
                                ], [
                                    'field' => 'performance.created_at',
                                    'symbol' => '<=',
                                    'value' => '03'
                                ]);
                            } elseif(date('m') == '04'|| date('m') == '04'|| date('m') == '06'){
                                $whereQuarte = array([
                                    'field' => 'performance.created_at',
                                    'symbol' => '>=',
                                    'value' => '04'
                                ], [
                                    'field' => 'performance.created_at',
                                    'symbol' => '<=',
                                    'value' => '06'
                                ]);
                            } elseif(date('m') == '07'|| date('m') == '08'|| date('m') == '09'){
                                $whereQuarte = array([
                                    'field' => 'performance.created_at',
                                    'symbol' => '>=',
                                    'value' => '07'
                                ], [
                                    'field' => 'performance.created_at',
                                    'symbol' => '<=',
                                    'value' => '09'
                                ]);
                            } elseif(date('m') == '10'|| date('m') == '11'|| date('m') == '12'){
                                $whereQuarte = array([
                                    'field' => 'performance.created_at',
                                    'symbol' => '>=',
                                    'value' => '10'
                                ], [
                                    'field' => 'performance.created_at',
                                    'symbol' => '<=',
                                    'value' => '12'
                                ]);
                            }
                        } elseif($_GET['val_1'] == 'щорічно'){
                            $whereYear = ['performance.created_at' => date('Y')];
                        } elseif($_GET['val_1'] == 'щомісячно'){
                            $whereYear = ['performance.created_at' => date('Y')];
                            $whereMonth = ['performance.created_at' => date('m')];
                        } else{
                            $where[] = [
                                'field'  => 'performance.col_20',
                                'symbol' => '=',
                                'value'  => date("Y-m-d", strtotime($_GET['val_1']))
                            ];
                        }
                        break;

                    default:
                        # code...
                        break;
                }
            }

            if(isset($_GET['field_2']) && isset($_GET['val_2']) && isset($_GET['type_2'])){
                switch ($_GET['type_2']) {

                    case 'bank':
                        $where[] = [
                            'field'  => 'performance.'.$_GET['field_2'],
                            'symbol' => '=',
                            'value'  => $_GET['val_2']
                        ];
                        break;

                    case 'target':
                        $where[] = [
                            'field'  => 'performance.'.$_GET['field_2'],
                            'symbol' => '=',
                            'value'  => $_GET['val_2']
                        ];
                        break;

                    case 'text':
                        $where[] = [
                            'field'  => 'performance.'.$_GET['field_2'],
                            'symbol' => 'like',
                            'value'  => '%'.$_GET['val_2'].'%'
                        ];
                        break;

                    case 'kved':
                        $vals = explode('_', $_GET['val_2']);
                        if($vals[0] != 0){
                            $where[] = [
                                'field'  => 'performance.'.$_GET['field_2'].'_section',
                                'symbol' => '=',
                                'value'  => $vals[0]
                            ];
                        }
                        if($vals[1] != 0 && $vals[1] != undefined){
                            $where[] = [
                                'field'  => 'performance.'.$_GET['field_2'].'_group',
                                'symbol' => '=',
                                'value'  => $vals[1]
                            ];
                        }
                        break;

                    case 'size-borrower':
                        $where[] = [
                            'field'  => 'performance.'.$_GET['field_1'],
                            'symbol' => '=',
                            'value'  => $_GET['val_1']
                        ];
                        break;

                    case 'dateSelect':
                        if($_GET['val_1'] == 'щоквартально'){
                            $whereYear = ['performance.created_at' => date('Y')];
                            if(date('m') == '01'|| date('m') == '02'|| date('m') == '03'){
                                $whereQuarte = array([
                                    'field' => 'performance.created_at',
                                    'symbol' => '>=',
                                    'value' => '01'
                                ], [
                                    'field' => 'performance.created_at',
                                    'symbol' => '<=',
                                    'value' => '03'
                                ]);
                            } elseif(date('m') == '04'|| date('m') == '04'|| date('m') == '06'){
                                $whereQuarte = array([
                                    'field' => 'performance.created_at',
                                    'symbol' => '>=',
                                    'value' => '04'
                                ], [
                                    'field' => 'performance.created_at',
                                    'symbol' => '<=',
                                    'value' => '06'
                                ]);
                            } elseif(date('m') == '07'|| date('m') == '08'|| date('m') == '09'){
                                $whereQuarte = array([
                                    'field' => 'performance.created_at',
                                    'symbol' => '>=',
                                    'value' => '07'
                                ], [
                                    'field' => 'performance.created_at',
                                    'symbol' => '<=',
                                    'value' => '09'
                                ]);
                            } elseif(date('m') == '10'|| date('m') == '11'|| date('m') == '12'){
                                $whereQuarte = array([
                                    'field' => 'performance.created_at',
                                    'symbol' => '>=',
                                    'value' => '10'
                                ], [
                                    'field' => 'performance.created_at',
                                    'symbol' => '<=',
                                    'value' => '12'
                                ]);
                            }
                        } elseif($_GET['val_1'] == 'щорічно'){
                            $whereYear = ['performance.created_at' => date('Y')];
                        } elseif($_GET['val_1'] == 'щомісячно'){
                            $whereYear = ['performance.created_at' => date('Y')];
                            $whereMonth = ['performance.created_at' => date('m')];
                        } else{
                            $where[] = [
                                'field'  => 'performance.col_20',
                                'symbol' => '=',
                                'value'  => date("Y-m-d", strtotime($_GET['val_1']))
                            ];
                        }
                        break;

                    default:
                        # code...
                        break;
                }
            }

            if(count($where) == 0){
                $where = null;
            }

            $performance = $this->performance->getPerformanceJoin($where, true, $whereYear, $whereMonth, $whereQuarte);
                if(count($performance)){
                    foreach ($performance as $key => $value) {
                        if($key == 'performanceJoin'){
                            $data['performance'] = $performance['performanceJoin'];
                        } else{
                            $data[$key] = $value;
                        }
                        
                    }
                }
            $data = array_merge($data, array(
                'idBank'   => Session::get('userId'),
                'title'    => 'ЗАГАЛЬНА ІНФОРМАЦІЯ ЩОДО ПОКАЗНИКІВ РЕЗУЛЬТАТИВНОСТІ',
                'subTitle' => 'впровадження ФКП'
            ));
            return view('/performanceOfImplementation/generalInformation', $data);
        } else{
            return redirect('/performanceOfImplementation/generalInformation');
        }
    }

    public function saveReport($where = null, $sum = null, $whereYear = null, $whereMonth = null, $whereQuarte = null, $name = null)
    {
        $data = [];

        $generalRegister = $this->performance->getPerformanceJoin($where, $sum, $whereYear, $whereMonth, $whereQuarte);
        if(count($generalRegister)){
            foreach ($generalRegister as $key => $value) {
                if($key == 'performanceJoin'){
                    $data['performance'] = $generalRegister['performanceJoin'];
                } else{
                    $data[$key] = $value;
                }
            }
        }

        $nameFile = self::ExcelReport($data);

        $value = [
            'name' => $name,
            'category' => 'performance',
            'url' => $nameFile
        ];
        $this->reports->addReports($value);
        
        return redirect('performanceOfImplementation/generalInformation');

    }

    public function ExcelReport($data)
    {
        $nameFile = md5(microtime());

        Excel::create($nameFile, function($excel) use ($data) {

            $excel->sheet('ЗАГАЛЬНА ІНФОРМАЦІЯ', function($sheet) use ($data){
                $sheet->setStyle(array(
                    'font' => array(
                        'name' => 'Arial',
                        'size' => 12
                    ),
                ));
                
                $sheet->setAutoSize(true);

                $sheet->mergeCells('A1:U1');
                $sheet->cell('A1', function($cell) {
                    $cell->setValue('ЗАГАЛЬНА ІНФОРМАЦІЯ ЩОДО ПОКАЗНИКІВ РЕЗУЛЬТАТИВНОСТІ');
                    $cell->setAlignment('center');
                    $cell->setValignment('center');

                });

                $sheet->mergeCells('A2:U2');
                $sheet->cell('A2', function($cell) {
                    $cell->setValue('впровадження ФКП');
                    $cell->setAlignment('center');
                    $cell->setValignment('center');
                });

                $sheet->mergeCells('A3:A4');
                $sheet->cell('A3', function($cell) {
                    $cell->setValue('№');
                    $cell->setAlignment('center');
                    $cell->setValignment('center');
                });

                $sheet->mergeCells('B3:B4');
                $sheet->cell('B3', function($cell) {
                    $cell->setValue('Назва постачальника, ЄДРПОУ');
                    $cell->setAlignment('center');
                    $cell->setValignment('center');
                });

                $sheet->mergeCells('C3:C4');
                $sheet->cell('C3', function($cell) {
                    $cell->setValue('Розмір позичальника (мале, середнє)');
                    $cell->setAlignment('center');
                    $cell->setValignment('center');
                });

                $sheet->mergeCells('D3:D4');
                $sheet->cell('D3', function($cell) {
                    $cell->setValue('КВЕД 2010');
                    $cell->setAlignment('center');
                    $cell->setValignment('center');
                });
                
                $sheet->mergeCells('E4:E4');
                $sheet->cell('E4', function($cell) {
                    $cell->setValue('№, дата кредитного договору');
                    $cell->setAlignment('center');
                    $cell->setValignment('center');
                });

                $sheet->mergeCells('F3:F4');
                $sheet->cell('F3', function($cell) {
                    $cell->setValue('Термін кредитного договору');
                    $cell->setAlignment('center');
                    $cell->setValignment('center');
                });

                $sheet->mergeCells('G3:G4');
                $sheet->cell('G3', function($cell) {
                    $cell->setValue('Цільове призначення кредиту');
                    $cell->setAlignment('center');
                    $cell->setValignment('center');
                });

                $sheet->mergeCells('H3:H4');
                $sheet->cell('H3', function($cell) {
                    $cell->setValue('Сума кредитну на початок звітного місяця, тис.грн');
                    $cell->setAlignment('center');
                    $cell->setValignment('center');
                });

                $sheet->mergeCells('I3:I4');
                $sheet->cell('I3', function($cell) {
                    $cell->setValue('Власний внесок позичальника');
                    $cell->setAlignment('center');
                    $cell->setValignment('center');
                });

                $sheet->mergeCells('J3:J4');
                $sheet->cell('J3', function($cell) {
                    $cell->setValue('Відсоткова ставка, %');
                    $cell->setAlignment('center');
                    $cell->setValignment('center');
                });

                $sheet->mergeCells('K3:K4');
                $sheet->cell('K3', function($cell) {
                    $cell->setValue('Загальна сума розрахованих відсотків у звітному місяці, %');
                    $cell->setAlignment('center');
                    $cell->setValignment('center');
                });

                $sheet->mergeCells('L3:L4');
                $sheet->cell('L3', function($cell) {
                    $cell->setValue('Сума ФКП за звітний місяць, грн');
                    $cell->setAlignment('center');
                    $cell->setValignment('center');
                });

                $sheet->mergeCells('M3:N3');
                $sheet->cell('M3', function($cell) {
                    $cell->setValue('Кількість створених позичальником податків і зборів до державного бюджету та бюджету м.Києва');
                    $cell->setAlignment('center');
                    $cell->setValignment('center');
                });
                $sheet->cell('M4', function($cell) {
                    $cell->setValue('попередній період');
                    $cell->setAlignment('center');
                    $cell->setValignment('center');
                });
                $sheet->cell('N4', function($cell) {
                    $cell->setValue('звітний період');
                    $cell->setAlignment('center');
                    $cell->setValignment('center');
                });

                $sheet->mergeCells('O3:P3');
                $sheet->cell('O3', function($cell) {
                    $cell->setValue('Відрахування позичальником податків і зборів до державного бюджету та бюджету м.Києва');
                    $cell->setAlignment('center');
                    $cell->setValignment('center');
                });
                $sheet->cell('O4', function($cell) {
                    $cell->setValue('попередній період');
                    $cell->setAlignment('center');
                    $cell->setValignment('center');
                });
                $sheet->cell('P4', function($cell) {
                    $cell->setValue('звітний період');
                    $cell->setAlignment('center');
                    $cell->setValignment('center');
                });

                $sheet->mergeCells('Q3:R3');
                $sheet->cell('Q3', function($cell) {
                    $cell->setValue('Зростання обсягу виробництва та раелізації товарів, робіт та послуг');
                    $cell->setAlignment('center');
                    $cell->setValignment('center');
                });
                $sheet->cell('Q4', function($cell) {
                    $cell->setValue('попередній період');
                    $cell->setAlignment('center');
                    $cell->setValignment('center');
                });
                $sheet->cell('R4', function($cell) {
                    $cell->setValue('звітний період');
                    $cell->setAlignment('center');
                    $cell->setValignment('center');
                });

                $sheet->mergeCells('S3:S4');
                $sheet->cell('S3', function($cell) {
                    $cell->setValue('Що зроблено позичальником по реалізації кредитного проекту');
                    $cell->setAlignment('center');
                    $cell->setValignment('center');
                });

                $sheet->mergeCells('T3:T4');
                $sheet->cell('T3', function($cell) {
                    $cell->setValue('Банк-партнер');
                    $cell->setAlignment('center');
                    $cell->setValignment('center');
                });

                $sheet->mergeCells('U3:U4');
                $sheet->cell('U3', function($cell) {
                    $cell->setValue('Дата надання інформації');
                    $cell->setAlignment('center');
                    $cell->setValignment('center');
                });

                $sheet->row(5, [
                    1,
                    2,
                    3,
                    4,
                    5,
                    6,
                    7,
                    8,
                    9,
                    10,
                    11,
                    12,
                    13,
                    14,
                    15,
                    16,
                    17,
                    18,
                    19,
                    20,
                    21
                ]);
                $sheet->row(5, function($row){
                    $row->setAlignment('center');
                    $row->setValignment('center');
                });

                if(count($data['performance'])) {
                    for ($i = 0; $i < count($data['performance']); $i++) {
                        $sheet->row($i + 6, [
                            $i + 1,
                            $data['performance'][$i]->col_2,
                            $data['performance'][$i]->size_borrower,
                            $data['performance'][$i]->col_4_section.' / '.$data['performance'][$i]->col_4_group.' / '.$data['performance'][$i]->col_4_clas,
                            $data['performance'][$i]->col_5,
                            $data['performance'][$i]->col_6,
                            $data['performance'][$i]->col_7,
                            $data['performance'][$i]->col_8 / 1000,
                            $data['performance'][$i]->col_9,
                            $data['performance'][$i]->col_10,
                            $data['performance'][$i]->col_11,
                            $data['performance'][$i]->col_12,
                            $data['performance'][$i]->col_13,
                            $data['performance'][$i]->col_14,
                            $data['performance'][$i]->col_15,
                            $data['performance'][$i]->col_16,
                            $data['performance'][$i]->col_17,
                            $data['performance'][$i]->col_18,
                            $data['performance'][$i]->col_19,
                            $data['performance'][$i]->name,
                            $data['performance'][$i]->col_20
                        ]);

                        if ($i == count($data['performance']) - 1) {
                            $sheet->row($i + 7, [
                                '',
                                '',
                                '',
                                '',
                                '',
                                '',
                                '',
                                $data['sum_8'] / 1000,
                                $data['sum_9'],
                                '',
                                $data['sum_11'],
                                $data['sum_12'],
                                $data['sum_13'],
                                $data['sum_14'],
                                $data['sum_15'],
                                $data['sum_16'],
                                $data['sum_17'],
                                $data['sum_18'],
                                '',
                                '',
                                ''
                            ]);
                        }
                    }
                } else{
                    $sheet->mergeCells('A6:U6');
                    $sheet->cell('A6', function($cell) {
                        $cell->setValue('Інформації немає');
                        $cell->setAlignment('center');
                        $cell->setValignment('center');
                    });
                }
            });
        
        })->store('xls', 'public/files/reports/');
        
        return $nameFile.'.xls';
    }

    public function chart_1_3_5()
    {
        $result = $this->performance->getChart_1_3_5();

        return json_decode($result);
    }

    public function chart_2_4_6()
    {
        $result = $this->performance->getChart_2_4_6();

        return json_decode($result);
    }

    public function chart_7_8_9()
    {
        $result = $this->performance->getChart_7_8_9();

        return json_decode($result);
    }

    public function chart_10_11_12()
    {
        $result = $this->performance->getChart_10_11_12();

        return json_decode($result);
    }

}