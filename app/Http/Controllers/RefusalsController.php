<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Contracts\RefusalsInterface;
use Session;
use Request;
use Route;
use Validator;
use App\Libraries\GeneralFunctions;

class RefusalsController extends Controller 
{

    //const LAWS = 'laws';
    //const RESOLUTIONS = 'resolutions';
    //const ORDERS = 'orders';
    //const ETCS = 'etcs';

    private $refusals;

    public function __construct(
        RefusalsInterface $refusals
    ){
        $this->request = Request::all();
        $this->refusals = $refusals;
    }

    public function refusalsPage() //вывод страницы "Суб'єкти МСП, яким відмовили у ФКП"
    {
        if(Session::has('userId')){
            $data = [];

            $refusals = $this->refusals->getRefusals();
            if(count($refusals)){
                $data['refusals'] = $refusals;
            }

            $data = array_merge($data, array(
                'title' => 'Суб\'єкти МСП, яким відмовили у ФКП'
            ));
            return view('/refusals', $data);
        } else{
            return redirect('/');
        }
    }

    public function refusalsAdd() //добавление документа
    {
        if(Session::has('userId')){
                unset($this->request['_token']);

                $validator = Validator::make(
                    [
                        'name' => $this->request['name']
                    ],[
                        'name' => 'required'
                    ],[
                        'name.required' => 'Поле має бути обов\'язково заповнене.'
                    ]
                );

                if($validator->fails()){
                    return redirect('/refusals')->with('error', $validator->messages());
                }else{
                    $data = [];

                    if(!empty($this->request['pdf'])){
                        $pathPDF = 'files/refusals/';
                        $filePDF = 'pdf';
                        $resultPDF = GeneralFunctions::getNameFile($pathPDF, $filePDF);
                        if($resultPDF != false){
                            $data['url_pdf'] = $resultPDF;
                        }
                    }
                    if(!empty($this->request['word'])){
                        $pathWORD = 'files/refusals/';
                        $fileWORD = 'word';
                        $resultWORD = GeneralFunctions::getNameFile($pathWORD, $fileWORD);
                        if($resultWORD != false){
                            $data['url_word'] = $resultWORD;
                        }
                    }

                    $data = array_merge($data, array(
                        'name' => $this->request['name']
                    ));
                    ;

                    $this->refusals->addRefusals($data);

                    return redirect('/refusals');
                }
        }else{
            return redirect('/');
        }
    }

}