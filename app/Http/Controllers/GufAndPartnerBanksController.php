<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
//use App\Http\Controllers\Contracts\GufAndPartnerBanksInterface;
use App\Http\Controllers\Contracts\ContractsDPRPWithGufInterface;
use App\Http\Controllers\Contracts\ContractsBankWithGufInterface;
use App\Http\Controllers\Contracts\ContractsBankWithDPRPInterface;
use App\Http\Controllers\Contracts\BanksInterface;
use App\Http\Controllers\Contracts\UsersInterface;
use App\Http\Controllers\Contracts\InformationInterface;
use App\Http\Controllers\Contracts\ImagesInterface;
use Session;
use Request;
use Route;
use Validator;
use App\Libraries\GeneralFunctions;

class GufAndPartnerBanksController extends Controller 
{

    private $contractsDPRPWithGuf;
    private $contractsBankWithGuf;
    private $contractsBankWithDPRP;
    private $banks;
    private $users;
    private $information;
    private $images;

    public function __construct(
        ContractsDPRPWithGufInterface $contractsDPRPWithGuf,
        ContractsBankWithGufInterface $contractsBankWithGuf,
        ContractsBankWithDPRPInterface $contractsBankWithDPRP,
        BanksInterface $banks,
        UsersInterface $users,
        InformationInterface $information,
        ImagesInterface $images
    ){
        $this->request = Request::all();
        $this->contractsBankWithDPRP = $contractsBankWithDPRP;
        $this->contractsBankWithGuf = $contractsBankWithGuf;
        $this->contractsDPRPWithGuf = $contractsDPRPWithGuf;
        $this->banks = $banks;
        $this->users = $users;
        $this->information = $information;
        $this->images = $images;
    }

    public function gufAndPartnerBanksPage() //вывод страницы "Німецько-український фонд та банки-партнери"
    {
        if(Session::has('userId')){
            $data = [];
            $data = array_merge($data, array(
                'title' => 'Німецько-український фонд та банки-партнери'
            ));
            return view('/gufAndPartnerBanks/gufAndPartnerBanks', $data);
        }else{
            return redirect('/');
        }
    }

    public function gufPage() //вывод страницы "Німецько-український фонд"
    {
        if(Session::has('userId')){
            $data = [];
            $data = array_merge($data, array(
                'title' => 'Німецько-український фонд'
            ));
            return view('/gufAndPartnerBanks/guf', $data);
        }else{
            return redirect('/');
        }
    }

    public function gufInformationPage() //вывод страницы "Інформація"
    {
        if(Session::has('userId')){
            
            $data = [];
            
            $whereInformation = ['information.category' => 'guf'];

            $information = $this->information->getInformationJoin($whereInformation);

            if(count($information)){
                $data['information'] = $information[0];
                $data['images'] = $information;
            }
            $data = array_merge($data, array(
                'name' => 'Німецько-український фонд',
                'title' => 'Інформація',
                'success' => Session::has('success') ? Session::get('success') : null,
                'error' => Session::has('error') ? Session::get('error') : null
            ));
            return view('/gufAndPartnerBanks/information', $data);
        }else{
            return redirect('/');
        }
    }

    public function addGufImage()//добавление картинки
    {
        $idInformation = $_POST['idInformation'];
        $path = 'images/information/';
        $file = 'img';
        try {
            
            $result = GeneralFunctions::getNameFile($path, $file);

            if($result != false){
                $data = [
                    'images.id_information' => $idInformation,
                    'images.url' => $result
                ];

                $addImage = $this->images->addImages($data);

                return redirect('/gufAndPartnerBanks/guf/information');
            }
        } catch (\Exception $e) {
            return redirect('/gufAndPartnerBanks/guf/information');
        }
    }

    public function gufAddInformation()
    {
        if(Session::has('userId')){
            if(Session::get('status') == 'departament'){

                $data = [
                    'information.category' => 'guf',
                    'information.body' => $_POST['editorInformation']
                ];

                $result = $this->information->addInformation($data);
                if($result){
                    return redirect('/gufAndPartnerBanks/guf/information')->with('success', 'Інформацію додано успішно');
                }else{
                    return redirect('/gufAndPartnerBanks/guf/information')->with('error', 'Щось пішло не так');
                }
            }else{
                return redirect('404');
            }
        } else{
            return redirect('/');
        }
    }

    public function gufEditInformation()
    {
        if(Session::has('userId')){
            if(Session::get('status') == 'departament'){
                $where = [];
                $data = [];

                $where = ['information.id' => $_POST['idInformation']];
                
                $data = [
                    'body' => $_POST['editorInformation']
                ];

                $result = $this->information->updateInformation($where, $data);
                if($result){
                    return redirect('/gufAndPartnerBanks/guf/information')->with('success', 'Інформацію змінено успішно');
                }else{
                    return redirect('/gufAndPartnerBanks/guf/information')->with('error', 'Щось пішло не так');
                }
            }else{
                return redirect('404');
            }
        } else{
            return redirect('/');
        }
    }

    public function gufContractPage() //вывод страницы "Договір ДПРП з НУФ"
    {
        if(Session::has('userId')){
            $data = [];

            $contracts = $this->contractsDPRPWithGuf->getContractsDPRPWithGuf();
            if(count($contracts)){
                $data['contracts'] = $contracts;
            }

            $data = array_merge($data, array(
                'title' => 'Договір ДПРП з НУФ'
            ));
            return view('/gufAndPartnerBanks/contract', $data);
        }else{
            return redirect('/');
        }
    }

    public function gufContractAdd() //добавление договора ДПРП з НУФ
    {
        if(Session::has('userId')){

            unset($this->request['_token']);

            $validator = Validator::make(
                $this->request,[
                    'name' => 'required'
                ],[
                    'name.required' => 'Поле має бути обов\'язково заповнене.'
                ]
            );

            if($validator->fails()){
                return redirect('/gufAndPartnerBanks/guf/contract')->with('error', $validator->messages());
            }else{
                $data = [];

                if(!empty($this->request['pdf'])){
                    $pathPDF = 'files/contracts/contractsDPRPWithGuf/';
                    $filePDF = 'pdf';
                    $resultPDF = GeneralFunctions::getNameFile($pathPDF, $filePDF);
                    if($resultPDF != false){
                        $data['url_pdf'] = $resultPDF;
                    }
                }

                $data = array_merge($data, array(
                    'name' => $this->request['name']
                ));
                ;

                $this->contractsDPRPWithGuf->addContractsDPRPWithGuf($data);

                return redirect('/gufAndPartnerBanks/guf/contract');
            }
        } else{
            return redirect('/');
        }
    }

/*------------------------------------------Банки-партнери---------------------------------------*/

    public function partnerBanksPage() //вывод страницы "Банки-партнери"
    {
        if(Session::has('userId')){
            $data = [];

            $where = ['banks.category' => 'bank-partner'];
            $bank = $this->banks->getBanksJoin($where);

            if(count($bank)){
                $data['banks'] = $bank;
            }

            if(Session::get('status') == 'departament'){
                $whereUser = [
                    'users.status' => 'bank',
                    'users.is_delete' => 0
                ];
                $whereUserBank = 'bank-partner';
                $users = $this->users->getUsersBankJoin($whereUser, $whereUserBank);
                if(count($users)){
                    $data['users'] = $users;
                }
            }

            $data = array_merge($data, array(
                'title' => 'Банки-партнери'
            ));
            return view('/gufAndPartnerBanks/partnerBanks', $data);
        }else{
            return redirect('/');
        }
    }

    public function partnerBanksAdd()//автоматическое добавление банка-партнера
    {
        $data = [];
        $data['id_bank'] = $_POST['id_bank'];

        $data['category'] = 'bank-partner';
        $result = $this->banks->addBanks($data);

        $result2 = $this->information->addInformation($data);

        return json_encode($result);
    }

    public function partnerBanksBankPage() //вывод страницы банка
    {
        if(Session::has('userId')){
            if(Route::input('id') && is_numeric(Route::input('id'))){

                $id = Route::input('id');
                $data = [];

                $where = ['banks.id_bank' => $id];
                $bank = $this->banks->getBanksJoin($where);

                if(count($bank)){

                    $name = $bank[0]->name;

                    $data = array_merge($data, array(
                        'id' => $id,
                        'title' => $name,
                        'nameBank' => $name
                    ));
                    return view('/gufAndPartnerBanks/bank', $data);

                } else{
                    return redirect('404');
                }
            } else{
                return redirect('404');
            }
        }else{
            return redirect('/');
        }
    }

    public function partnerBanksBankInformationPage() //вывод страницы "Інформація"
    {
        if(Session::has('userId')){
            if(Route::input('id') && is_numeric(Route::input('id'))){
                $data = [];
                $id   = Route::input('id');

                $whereBank = ['banks.id_bank' => $id];
                $banks     = $this->banks->getBanksJoin($whereBank);
                if(count($banks)){

                    $whereInformation = [
                        'information.id_bank'  => $id,
                        'information.category' => 'bank-partner'
                    ];
                    $information = $this->information->getInformationJoin($whereInformation);
                    if(count($information)){
                        $data['information'] = $information[0];
                        $data['images'] = $information;
                    }

                    $data = array_merge($data, array(
                        'title'   => 'Інформація',
                        'name'    => $banks[0]->name,
                        'id_bank' => $id,
                        'success' => Session::has('success') ? Session::get('success') : null,
                        'error'   => Session::has('error') ? Session::get('error') : null
                    ));
                    return view('/gufAndPartnerBanks/bankInformation', $data);

                } else{
                    return redirect('404');
                }
            }else{
                return redirect('404');
            }
        }else{
            return redirect('/');
        }
    }

    public function partnerBanksBankAddInformation()
    {
        if(Session::has('userId')){
            if(Session::get('status') == 'departament'){
                if(Route::input('id') && is_numeric(Route::input('id'))){
                    $id = Route::input('id');
                    $where = [];
                    $data = [];

                    $data = [
                        'information.category' => 'partner-bank',
                        'information.id_bank'  => $id,
                        'information.body'     => $_POST['editorInformation']
                    ];

                    $result = $this->information->addInformation($data);
                    if($result){
                        return redirect('/gufAndPartnerBanks/partnerBanks/bank/'.$id.'/information')->with('success', 'Інформацію змінено успішно');
                    }else{
                        return redirect('/gufAndPartnerBanks/partnerBanks/bank/'.$id.'/information')->with('error', 'Щось пішло не так');
                    }
                }else{
                    return redirect('404');
                }
            }else{
                return redirect('404');
            }
        } else{
            return redirect('/');
        }
    }

    public function partnerBanksBankEditInformation()
    {
        if(Session::has('userId')){
            if(Session::get('status') == 'departament'){
                if(Route::input('id') && is_numeric(Route::input('id'))){
                    $id = Route::input('id');
                    $where = [];
                    $data = [];

                    $where = ['information.id' => $_POST['idInformation']];

                    $data = [
                        'body' => $_POST['editorInformation']
                    ];

                    $result = $this->information->updateInformation($where, $data);
                    if($result){
                        return redirect('/gufAndPartnerBanks/partnerBanks/bank/'.$id.'/information')->with('success', 'Інформацію змінено успішно');
                    }else{
                        return redirect('/gufAndPartnerBanks/partnerBanks/bank/'.$id.'/information')->with('error', 'Щось пішло не так');
                    }
                }else{
                    return redirect('404');
                }
            }else{
                return redirect('404');
            }
        } else{
            return redirect('/');
        }
    }

    public function addBankImage()//добавление картинки
    {
        $idInformation = $_POST['idInformation'];
        $path = 'images/information/';
        $file = 'img';
        $id_bank = Route::input('id_bank');
        try {
            
            $result = GeneralFunctions::getNameFile($path, $file);

            if($result != false){
                $data = [
                    'images.id_information' => $idInformation,
                    'images.url' => $result
                ];

                $addImage = $this->images->addImages($data);
            }
        } catch (\Exception $e) {
        }
        
        return redirect('/gufAndPartnerBanks/partnerBanks/bank/'.$id_bank.'/information');
    }

    public function partnerBanksBankContractDPRPPage() //вывод страницы "Договір ДПРП з банком"
    {
        if(Session::has('userId')){
            if(Route::input('id') && is_numeric(Route::input('id'))){
                $data = [];
                $id = Route::input('id');

                $whereBank = ['banks.id_bank' => $id];
                $banks = $this->banks->getBanksJoin($whereBank);
                if(count($banks)){

                    $whereContract = ['contracts_bank_with_DPRP.id_bank' => $id];
                    $contracts = $this->contractsBankWithDPRP->getContractsBankWithDPRP($whereContract);
                    if(count($contracts)){
                        $data['contracts'] = $contracts;
                    }

                    $data = array_merge($data, array(
                        'title' => 'Договір ДПРП з банком "'.$banks[0]->name.'"',
                        'id_bank' => $id
                    ));
                    return view('/gufAndPartnerBanks/bankContractDPRP', $data);

                } else{
                    return redirect('404');
                }

            }else{
                return redirect('404');
            }
        }else{
            return redirect('/');
        }
    }

    public function partnerBanksBankContractDPRPAdd()//обработка добавления договоров банка с ДПРП
    {
        if(Session::has('userId')){
            if(Route::input('id') && is_numeric(Route::input('id'))){
                unset($this->request['_token']);

                $id_bank = Route::input('id');

                $validator = Validator::make(
                    $this->request,[
                        'name' => 'required'
                    ],[
                        'name.required' => 'Поле має бути обов\'язково заповнене.'
                    ]
                );

                if($validator->fails()){
                    return redirect('/gufAndPartnerBanks/partnerBanks/bank/'.$id_bank.'/contractDPRP')->with('error', $validator->messages());
                }else{
                    $data = [];

                    if(!empty($this->request['pdf'])){
                        $pathPDF = 'files/contracts/contractsBankWithDPRP/';
                        $filePDF = 'pdf';
                        $resultPDF = GeneralFunctions::getNameFile($pathPDF, $filePDF);
                        if($resultPDF != false){
                            $data['url_pdf'] = $resultPDF;
                        }
                    }

                    $data = array_merge($data, array(
                        'name' => $this->request['name'],
                        'id_bank' => $id_bank
                    ));
                    ;

                    $this->contractsBankWithDPRP->addContractsBankWithDPRP($data);

                    return redirect('/gufAndPartnerBanks/partnerBanks/bank/'.$id_bank.'/contractDPRP');
                }
            } else{
                return redirect('404');
            }
        }else{
            return redirect('/');
        }
    }

    public function partnerBanksBankContractGUFPage() //вывод страницы "Договір банку з НФУ"
    {
        if(Session::has('userId')){
            if(Route::input('id') && is_numeric(Route::input('id'))){
                $data = [];
                $id = Route::input('id');

                $whereBank = ['banks.id_bank' => $id];
                $banks = $this->banks->getBanksJoin($whereBank);
                if(count($banks)){
                    $whereContarcts = ['contracts_bank_with_Guf.id_bank' => $id];
                    $contracts = $this->contractsBankWithGuf->getContractsBankWithGuf($whereContarcts);
                    if(count($contracts)){
                        $data['contracts'] = $contracts;
                    }

                    $data = array_merge($data, array(
                        'title' => 'Договір банку "'.$banks[0]->name.'" з НФУ',
                        'id_bank' => $id
                    ));
                    return view('/gufAndPartnerBanks/bankContractGuf', $data);
                }else{
                    return redirect('404');
                }
            }else{
                return redirect('404');
            }
        }else{
            return redirect('/');
        }
    }

    public function partnerBanksBankContractGUFAdd()//обработка добавления договоров банка с НУФ
    {
        if(Session::has('userId')){
            if(Route::input('id') && is_numeric(Route::input('id'))){
                unset($this->request['_token']);

                $id_bank = Route::input('id');

                $validator = Validator::make(
                    $this->request,[
                        'name' => 'required'
                    ],[
                        'name.required' => 'Поле має бути обов\'язково заповнене.'
                    ]
                );

                if($validator->fails()){
                    return redirect('/gufAndPartnerBanks/partnerBanks/bank/'.$id_bank.'/contractGUF')->with('error', $validator->messages());
                }else{
                    $data = [];

                    if(!empty($this->request['pdf'])){
                        $pathPDF = 'files/contracts/contractsBankWithGuf/';
                        $filePDF = 'pdf';
                        $resultPDF = GeneralFunctions::getNameFile($pathPDF, $filePDF);
                        if($resultPDF != false){
                            $data['url_pdf'] = $resultPDF;
                        }
                    }

                    $data = array_merge($data, array(
                        'name' => $this->request['name'],
                        'id_bank' => $id_bank
                    ));
                    ;

                    $this->contractsBankWithGuf->addContractsBankWithGuf($data);

                    return redirect('/gufAndPartnerBanks/partnerBanks/bank/'.$id_bank.'/contractGUF');
                }
            } else{
                return redirect('404');
            }
        }else{
            return redirect('/');
        }
    }
}