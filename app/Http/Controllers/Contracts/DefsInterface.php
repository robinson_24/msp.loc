<?php
namespace App\Http\Controllers\Contracts;

interface DefsInterface
{
    public function getDefs($where = null, $order = null, $limit = null, $offset = null, $fields = null);
    public function getDefsNowYear($where = null);
    //public function getDefsJoin($where = null);
    public function addDefs($where);
    public function updateDefs($where, $values);
}