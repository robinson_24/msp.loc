<?php
namespace App\Http\Controllers\Contracts;

interface RepaymentScheduleInterface
{
    public function getRepaymentSchedule($where = null, $order = null, $limit = null, $offset = null, $fields = null);
    //public function getRepaymentScheduleBankJoin($where = null, $whereAfter = null);
    public function addRepaymentSchedule($where);
    public function deleteRepaymentSchedule($where);
    //public function updateRepaymentSchedule($where, $values);
}