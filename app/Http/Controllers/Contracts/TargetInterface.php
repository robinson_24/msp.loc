<?php
namespace App\Http\Controllers\Contracts;

interface TargetInterface
{
    public function getTarget($where = null, $order = null, $limit = null, $offset = null, $fields = null);
    public function addTarget($where);
    public function updateTarget($where, $value);
    public function deleteTarget($where);
}