<?php
namespace App\Http\Controllers\Contracts;

interface StackingInterface
{
    public function getStacking($where = null, $order = null, $limit = null, $offset = null, $fields = null);
}