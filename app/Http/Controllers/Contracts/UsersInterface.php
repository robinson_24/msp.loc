<?php
namespace App\Http\Controllers\Contracts;

interface UsersInterface
{
    public function getUsers($where = null, $order = null, $limit = null, $offset = null, $fields = null);
    public function getUsersBankJoin($where = null, $whereAfter = null);
    public function addUsers($where);
    public function updateUsers($where, $values);
    public function deleteUsers($where);
}