<?php
namespace App\Http\Controllers\Contracts;

interface KvedInterface
{
    public function getKved($where = null, $order = null, $limit = null, $offset = null, $fields = null);
    public function addKved($where);
    public function updateKved($where, $value);
    public function deleteKved($where);
}