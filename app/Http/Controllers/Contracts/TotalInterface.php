<?php
namespace App\Http\Controllers\Contracts;

interface TotalInterface
{
    public function getTotal($where = null, $order = null, $limit = null, $offset = null, $fields = null);
}