<?php
namespace App\Http\Controllers\Contracts;

interface RegulatoryDocumentsInterface
{
    public function getRegulatoryDocuments($where = null, $order = null, $limit = null, $offset = null, $fields = null);
    public function addRegulatoryDocuments($where);
    public function deleteRegulatoryDocuments($where);
}