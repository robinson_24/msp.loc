<?php
namespace App\Http\Controllers\Contracts;

interface SizeBorrowerInterface
{
    public function getSizeBorrower($where = null, $order = null, $limit = null, $offset = null, $fields = null);
}