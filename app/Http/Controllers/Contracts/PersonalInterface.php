<?php
namespace App\Http\Controllers\Contracts;

interface PersonalInterface
{
    public function getPersonal($where = null, $order = null, $limit = null, $offset = null, $fields = null);
}