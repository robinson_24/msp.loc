<?php
namespace App\Http\Controllers\Contracts;

interface ImagesInterface
{
    public function getImages($where = null);
    public function addImages($where);
    public function deleteImages($where);
}