<?php
namespace App\Http\Controllers\Contracts;

interface BorrowersRegisterInterface
{
    public function getBorrowersRegister($where = null, $order = null, $limit = null, $offset = null, $fields = null);
    public function getBorrowersRegisterJoin($where = null, $whereYear = null, $whereMonth = null, $whereQuarte = null);
    public function addBorrowersRegister($where);
    public function updateBorrowersRegister($where, $values);
    public function deleteBorrowersRegister($where);
}