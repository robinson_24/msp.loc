<?php
namespace App\Http\Controllers\Contracts;

interface InformationInterface
{
    public function getInformation($where = null, $order = null, $limit = null, $offset = null, $fields = null);
    public function getInformationJoin($where = null);
    public function addInformation($where);
    public function updateInformation($where, $values);
    public function deleteInformation($where);
}