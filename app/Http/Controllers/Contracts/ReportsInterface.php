<?php
namespace App\Http\Controllers\Contracts;

interface ReportsInterface
{
    public function getReports($where = null);
    public function addReports($where);
}