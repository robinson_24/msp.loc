<?php
namespace App\Http\Controllers\Contracts;

interface ContractsDPRPWithGufInterface
{
    public function getContractsDPRPWithGuf($where = null, $order = null, $limit = null, $offset = null, $fields = null);
    public function addContractsDPRPWithGuf($where);
    public function updateContractsDPRPWithGuf($where, $values);
    public function deleteContractsDPRPWithGuf($where);
}