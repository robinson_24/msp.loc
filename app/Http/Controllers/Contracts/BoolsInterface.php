<?php
namespace App\Http\Controllers\Contracts;

interface BoolsInterface
{
    public function getBools($where = null, $order = null, $limit = null, $offset = null, $fields = null);
}