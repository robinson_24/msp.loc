<?php
namespace App\Http\Controllers\Contracts;

interface RefusalsInterface
{
    public function getRefusals($where = null, $order = null, $limit = null, $offset = null, $fields = null);
    public function addRefusals($where);
    public function deleteRefusals($where);
}