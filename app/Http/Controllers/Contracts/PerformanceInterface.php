<?php
namespace App\Http\Controllers\Contracts;

interface PerformanceInterface
{
    public function getPerformance($where = null, $order = null, $limit = null, $offset = null, $fields = null);
    public function getPerformanceJoin($where = null, $sum = null, $whereYear = null, $whereMonth = null, $whereQuarte = null);
    public function addPerformance($where);
    public function updatePerformance($where, $values);
    public function deletePerformance($where);
}