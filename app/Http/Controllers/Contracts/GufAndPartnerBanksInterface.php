<?php
namespace App\Http\Controllers\Contracts;

interface GufAndPartnerBanksInterface
{
    public function getGufAndPartnerBanks($where = null, $order = null, $limit = null, $offset = null, $fields = null);
    public function addGufAndPartnerBanks($where);
    public function updateGufAndPartnerBanks($where, $values);
}