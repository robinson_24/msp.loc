<?php
namespace App\Http\Controllers\Contracts;

interface ApplicantsRegisterInterface
{
    public function getApplicantsRegister($where = null, $order = null, $limit = null, $offset = null, $fields = null);
    public function getApplicantsRegisterJoin($where = null, $sum = null, $between = null, $whereYear = null, $whereMonth = null, $whereQuarte = null);
    public function addApplicantsRegister($where);
    public function updateApplicantsRegister($where, $values);
    public function deleteApplicantsRegister($where);
}