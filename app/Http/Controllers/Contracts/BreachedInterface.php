<?php
namespace App\Http\Controllers\Contracts;

interface BreachedInterface
{
    public function getBreached($where = null, $order = null, $limit = null, $offset = null, $fields = null);
    public function deleteBreached($where);
}