<?php
namespace App\Http\Controllers\Contracts;

interface CommissionInterface
{
    public function getCommission($where = null, $order = null, $limit = null, $offset = null, $fields = null);
    public function addCommission($where);
    public function updateCommission($where, $values);
    public function deleteCommission($where);
}