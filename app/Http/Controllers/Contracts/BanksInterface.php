<?php
namespace App\Http\Controllers\Contracts;

interface BanksInterface
{
    public function getBanks($where = null, $order = null, $limit = null, $offset = null, $fields = null);
    public function getBanksJoin($where = null);
    public function addBanks($where);
    public function updateBanks($where, $values);
    public function deleteBanks($where);
}