<?php
namespace App\Http\Controllers\Contracts;

interface ContractsBankWithGufInterface
{
    public function getContractsBankWithGuf($where = null, $order = null, $limit = null, $offset = null, $fields = null);
    public function addContractsBankWithGuf($where);
    public function updateContractsBankWithGuf($where, $values);
    public function deleteContractsBankWithGuf($where);
}