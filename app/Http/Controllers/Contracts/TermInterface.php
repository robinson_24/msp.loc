<?php
namespace App\Http\Controllers\Contracts;

interface TermInterface
{
    public function getTerm($where = null, $order = null, $limit = null, $offset = null, $fields = null);
}