<?php
namespace App\Http\Controllers\Contracts;

interface ContractsBankWithDPRPInterface
{
    public function getContractsBankWithDPRP($where = null, $order = null, $limit = null, $offset = null, $fields = null);
    public function addContractsBankWithDPRP($where);
    public function updateContractsBankWithDPRP($where, $values);
    public function deleteContractsBankWithDPRP($where);
}