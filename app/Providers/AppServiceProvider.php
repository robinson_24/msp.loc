<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Http\Controllers\Contracts\RegulatoryDocumentsInterface',
            'App\Http\Controllers\Repositories\RegulatoryDocumentsRepository'
        );
        
        $this->app->bind(
            'App\Http\Controllers\Contracts\GufAndPartnerBanksInterface',
            'App\Http\Controllers\Repositories\GufAndPartnerBanksRepository'
        );
        
        $this->app->bind(
            'App\Http\Controllers\Contracts\ContractsDPRPWithGufInterface',
            'App\Http\Controllers\Repositories\ContractsDPRPWithGufRepository'
        );
        
        $this->app->bind(
            'App\Http\Controllers\Contracts\BanksInterface',
            'App\Http\Controllers\Repositories\BanksRepository'
        );
        
        $this->app->bind(
            'App\Http\Controllers\Contracts\ContractsBankWithGufInterface',
            'App\Http\Controllers\Repositories\ContractsBankWithGufRepository'
        );
        
        $this->app->bind(
            'App\Http\Controllers\Contracts\ContractsBankWithDPRPInterface',
            'App\Http\Controllers\Repositories\ContractsBankWithDPRPRepository'
        );
        
        $this->app->bind(
            'App\Http\Controllers\Contracts\CommissionInterface',
            'App\Http\Controllers\Repositories\CommissionRepository'
        );
        
        $this->app->bind(
            'App\Http\Controllers\Contracts\UsersInterface',
            'App\Http\Controllers\Repositories\UsersRepository'
        );
        
        $this->app->bind(
            'App\Http\Controllers\Contracts\ApplicantsRegisterInterface',
            'App\Http\Controllers\Repositories\ApplicantsRegisterRepository'
        );
        
        $this->app->bind(
            'App\Http\Controllers\Contracts\BorrowersRegisterInterface',
            'App\Http\Controllers\Repositories\BorrowersRegisterRepository'
        );
        
        $this->app->bind(
            'App\Http\Controllers\Contracts\PerformanceInterface',
            'App\Http\Controllers\Repositories\PerformanceRepository'
        );
        
        $this->app->bind(
            'App\Http\Controllers\Contracts\BreachedInterface',
            'App\Http\Controllers\Repositories\BreachedRepository'
        );
        
        $this->app->bind(
            'App\Http\Controllers\Contracts\RefusalsInterface',
            'App\Http\Controllers\Repositories\RefusalsRepository'
        );
        
        $this->app->bind(
            'App\Http\Controllers\Contracts\BoolsInterface',
            'App\Http\Controllers\Repositories\BoolsRepository'
        );
        
        $this->app->bind(
            'App\Http\Controllers\Contracts\TermInterface',
            'App\Http\Controllers\Repositories\TermRepository'
        );
        
        $this->app->bind(
            'App\Http\Controllers\Contracts\TotalInterface',
            'App\Http\Controllers\Repositories\TotalRepository'
        );
        
        $this->app->bind(
            'App\Http\Controllers\Contracts\StackingInterface',
            'App\Http\Controllers\Repositories\StackingRepository'
        );
        
        $this->app->bind(
            'App\Http\Controllers\Contracts\PersonalInterface',
            'App\Http\Controllers\Repositories\PersonalRepository'
        );
        
        $this->app->bind(
            'App\Http\Controllers\Contracts\DefsInterface',
            'App\Http\Controllers\Repositories\DefsRepository'
        );
        
        $this->app->bind(
            'App\Http\Controllers\Contracts\RepaymentScheduleInterface',
            'App\Http\Controllers\Repositories\RepaymentScheduleRepository'
        );
        
        $this->app->bind(
            'App\Http\Controllers\Contracts\SizeBorrowerInterface',
            'App\Http\Controllers\Repositories\SizeBorrowerRepository'
        );
        
        $this->app->bind(
            'App\Http\Controllers\Contracts\ReportsInterface',
            'App\Http\Controllers\Repositories\ReportsRepository'
        );
        
        $this->app->bind(
            'App\Http\Controllers\Contracts\InformationInterface',
            'App\Http\Controllers\Repositories\InformationRepository'
        );
        
        $this->app->bind(
            'App\Http\Controllers\Contracts\TargetInterface',
            'App\Http\Controllers\Repositories\TargetRepository'
        );
        
        $this->app->bind(
            'App\Http\Controllers\Contracts\KvedInterface',
            'App\Http\Controllers\Repositories\KvedRepository'
        );
        
        $this->app->bind(
            'App\Http\Controllers\Contracts\ImagesInterface',
            'App\Http\Controllers\Repositories\ImagesRepository'
        );
    }
}
