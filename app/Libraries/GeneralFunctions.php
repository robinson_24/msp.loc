<?php
namespace app\Libraries;

class GeneralFunctions
{

    public static function getNameFile($path, $file)
    {
        $name = md5(microtime()).stristr($_FILES[$file]['name'], '.');
        $uploadFile = $path.$name;
        if(move_uploaded_file($_FILES[$file]['tmp_name'], $uploadFile)){
            return $name;
        }else{
            return false;
        }
    }

    public static function trimString($string, $trimAll = null) //Удаляет пробелы с концов, или заменяет двойные пробелы в одинарные
    {
        if ($trimAll == true) {
            $stringTrim = trim($string);
            $explodeStr = explode(' ', $stringTrim);

            for ($i = 0; $i <= count($explodeStr); $i++) {
                $trimString = str_replace('  ', ' ', $stringTrim);
                $stringTrim = $trimString;
                $i+1;
            }
        } else {
            $trimString = trim($string);
        }

        return $trimString;
    }

    public static function createSalt()
    {
        $salt = substr(sha1(uniqid(rand(), true)), 0, 12);

        return $salt;
    }

    public static function genPassword($password, $salt)
    {
        return hash('sha256', $password . $salt);
    }

}