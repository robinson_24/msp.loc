<?php
namespace app\Eloquent;

use Illuminate\Database\Eloquent\Model;

class Breached extends Model
{
    protected $table = 'breached';
    protected $primaryKey = 'id';
}