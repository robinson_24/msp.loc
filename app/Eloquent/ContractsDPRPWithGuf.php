<?php
namespace app\Eloquent;

use Illuminate\Database\Eloquent\Model;

class ContractsDPRPWithGuf extends Model
{
    protected $table = 'contracts_DPRP_with_Guf';
    protected $primaryKey = 'id';
}