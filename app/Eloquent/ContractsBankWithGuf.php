<?php
namespace app\Eloquent;

use Illuminate\Database\Eloquent\Model;

class ContractsBankWithGuf extends Model
{
    protected $table = 'contracts_bank_with_Guf';
    protected $primaryKey = 'id';
}