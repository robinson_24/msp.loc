<?php
namespace app\Eloquent;

use Illuminate\Database\Eloquent\Model;

class Kved extends Model
{
    protected $table = 'kved';
    protected $primaryKey = 'id';
}
