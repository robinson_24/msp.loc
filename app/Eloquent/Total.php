<?php
namespace app\Eloquent;

use Illuminate\Database\Eloquent\Model;

class Total extends Model
{
    protected $table = 'total';
    protected $primaryKey = 'id';
}
