<?php
namespace app\Eloquent;

use Illuminate\Database\Eloquent\Model;

class Bools extends Model
{
    protected $table = 'bools';
    protected $primaryKey = 'id';
}
