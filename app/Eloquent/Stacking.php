<?php
namespace app\Eloquent;

use Illuminate\Database\Eloquent\Model;

class Stacking extends Model
{
    protected $table = 'stacking';
    protected $primaryKey = 'id';
}
