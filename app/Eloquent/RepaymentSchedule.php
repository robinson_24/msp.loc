<?php
namespace app\Eloquent;

use Illuminate\Database\Eloquent\Model;

class RepaymentSchedule extends Model
{
    protected $table = 'repayment_schedule';
    protected $primaryKey = 'id';
}
