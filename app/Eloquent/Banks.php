<?php
namespace app\Eloquent;

use Illuminate\Database\Eloquent\Model;

class Banks extends Model
{
    protected $table = 'banks';
    protected $primaryKey = 'id';
}
