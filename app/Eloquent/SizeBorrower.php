<?php
namespace app\Eloquent;

use Illuminate\Database\Eloquent\Model;

class SizeBorrower extends Model
{
    protected $table = 'size_borrower';
    protected $primaryKey = 'id';
}
