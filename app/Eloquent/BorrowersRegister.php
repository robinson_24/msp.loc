<?php
namespace app\Eloquent;

use Illuminate\Database\Eloquent\Model;

class BorrowersRegister extends Model
{
    protected $table = 'borrowersRegister';
    protected $primaryKey = 'id';
}
