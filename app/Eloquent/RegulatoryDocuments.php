<?php
namespace app\Eloquent;

use Illuminate\Database\Eloquent\Model;

class RegulatoryDocuments extends Model
{
    protected $table = 'regulatory_documents';
    protected $primaryKey = 'id';
}