<?php
namespace app\Eloquent;

use Illuminate\Database\Eloquent\Model;

class Performance extends Model
{
    protected $table = 'performance';
    protected $primaryKey = 'id';
}
