<?php
namespace app\Eloquent;

use Illuminate\Database\Eloquent\Model;

class Refusals extends Model
{
    protected $table = 'refusals';
    protected $primaryKey = 'id';
}