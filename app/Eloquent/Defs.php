<?php
namespace app\Eloquent;

use Illuminate\Database\Eloquent\Model;

class Defs extends Model
{
    protected $table = 'def';
    protected $primaryKey = 'id';
}
