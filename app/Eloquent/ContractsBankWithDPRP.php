<?php
namespace app\Eloquent;

use Illuminate\Database\Eloquent\Model;

class ContractsBankWithDPRP extends Model
{
    protected $table = 'contracts_bank_with_DPRP';
    protected $primaryKey = 'id';
}