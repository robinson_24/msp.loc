<?php
namespace app\Eloquent;

use Illuminate\Database\Eloquent\Model;

class ApplicantsRegister extends Model
{
    protected $table = 'applicantsRegister';
    protected $primaryKey = 'id';
}
