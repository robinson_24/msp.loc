$(document).ready(function(){

    $(".edit").click(function(){//вывод формы изменения названия банка
        var id = $(this).attr('id');
        $("#oldName_"+id).css('display', 'none');
        $("#newName_"+id).css('display', 'block');
        $("#newName_"+id+">input").focus();
    });

    $(".saveNewNameBank").click(function(){//обработка формы изменения названия банка
        var idBank = $(this).attr('id');
        var val = $("#newNameBank_"+idBank).val();
        if(val.replace(/\s{1,}/g, '').length > 1){
            var token = $('#token').val();
            $.ajax({
                url: '/bank/edit',
                type: 'POST',
                data:{
                    id: idBank,
                    name: val,
                    _token: token
                },
                dataType: 'json',
                success: function(resp){
                    if(resp == 1){
                        $("#newNameBank_"+idBank).val('');
                        $("#newName_"+idBank).css('display', 'none');
                        $("#linkBank_"+idBank).text(val);
                        $("#oldName_"+idBank).css('display', 'block');
                    }
                }
            });
        }
    });

    $(".closedEditBank").click(function(){//закрытие формы изменение названия банка
        $(this).parent().css('display', 'none');
        $(this).parent().prev().css('display', 'block');
    });

    $(".iconPlus").click(function(){ //вывод формы добавления банка
        $(this).css('display', 'none');
        $(this).next().css('display', 'block');
    });

    $(".addBank").click(function(){ //обработка формы добавления банка
        var nameBank = $("[name='bank'] option:selected").text();
        var idBank = $("[name='bank'] option:selected").attr('id');
        var token = $('#token').val();
        var url = $(this).attr('name');
        var hrefs = url.split('/');
        $.ajax({
            url: url,
            type: 'POST',
            data:{
                id_bank: idBank,
                _token: token
            },
            dataType: 'json',
            success: function(resp){
                location.reload();
            }
        });
    });

    $(".closedAddBank").click(function(){ //закрытие формы добавления банка
        $(this).parent().css('display', 'none');
        $(this).parent().prev().css('display', 'block');
    });

    $("[name='editBank']").click(function(){//редактирование банка
        var id = $(this).parent().parent().attr('id');
        if($("*").find("tr[id='bank-edit-"+id+"']").length == 0){
            var token = $("[name='_token']").val();
            var val = $(this).parent().prev().text();
            $(this).parent().parent().after('<tr id="bank-edit-'+id+'">'
                                                +'<td colspan="4">'
                                                    +'<div class="col-xs-12 col-sm-6 col-sm-offset-3">'
                                                        +'<h3>Внести зміни</h3>'
                                                        +'<form action="/admin/editBank" method="POST">'
                                                            +'<input type="hidden" name="_token" value="'+token+'">'
                                                            +'<input type="hidden" name="id" value="'+id+'">'
                                                            +'<div class="form-group">'
                                                                +'<label for="nameBank">Назва</label>'
                                                                +'<input class="form-control" id="nameBank" type="text" name="nameBank" value="'+val+'"/>'
                                                            +'</div>'
                                                            +'<div class="form-group">'
                                                                +'<label for="passwordBank">Пароль</label>'
                                                                +'<input class="form-control" id="passwordBank" type="password" name="passwordBank" value=""/>'
                                                            +'</div>'
                                                            +'<input type="submit" class="btn btn-primary" name="editBank" value="Зберегти"/>'
                                                        +'</form>'
                                                    +'</div>'
                                                +'</td>'
                                            +'</tr>');
        } else{
            $("tr[id='bank-edit-"+id+"']").remove();
        }
    });

    $("[name='deleteBank']").click(function(){//удаление банка
        var id = $(this).parent().parent().attr('id');
        if($("*").find("tr[id='delete-"+id+"']").length == 0){
            var token = $("[name='_token']").val();
            $(this).parent().parent().after('<tr id="delete-'+id+'"><td colspan="4"><div class="col-xs-12 col-sm-6 col-sm-offset-3"><h3>Підтвердити зміни</h3><form action="/admin/deleteBank" method="POST"><input type="hidden" name="_token" value="'+token+'"><input type="hidden" name="id" value="'+id+'"><div class="form-group"><label for="password">Ваш пароль</label><input id="password" class="form-control" type="password" name="password" value=""/></div><input type="submit" class="btn btn-primary" name="confirmBank" value="Підтвердити"/></form></div></td></tr>');
        } else{
            $("tr[id='delete-"+id+"']").remove();
        }
    });
    
    $("[name='editTarget']").click(function(){//редактирование цели
        var id = $(this).parent().parent().attr('id');
        var val = $(this).parent().prev().text();
        if($("*").find("tr[id='target-edit-"+id+"']").length == 0){
            var token = $("[name='_token']").val();
            $(this).parent().parent().after('<tr id="target-edit-'+id+'">'
                                                +'<td colspan="4">'
                                                    +'<div class="col-xs-12 col-sm-6 col-sm-offset-3">'
                                                        +'<h3>Внести зміни</h3>'
                                                        +'<form action="/admin/editTarget" method="POST">'
                                                            +'<input type="hidden" name="_token" value="'+token+'">'
                                                            +'<input type="hidden" name="id" value="'+id+'">'
                                                            +'<div class="form-group">'
                                                                +'<label for="nameTarget">Ціль кредиту</label>'
                                                                +'<input class="form-control" id="nameTarget" type="text" name="nameTarget" value="'+val+'"/>'
                                                            +'</div>'
                                                            +'<input type="submit" class="btn btn-primary" name="editTarget" value="Зберегти"/>'
                                                        +'</form>'
                                                    +'</div>'
                                                +'</td>'
                                            +'</tr>');
        } else{
            $("tr[id='target-edit-"+id+"']").remove();
        }
    });

    $("[name='deleteTarget']").click(function(){//удаление цели
        var id = $(this).parent().parent().attr('id');
        if($("*").find("tr[id='delete-"+id+"']").length == 0){
            var token = $("[name='_token']").val();
            $(this).parent().parent().after('<tr id="delete-'+id+'">'
                                                +'<td colspan="4">'
                                                    +'<div class="col-xs-12 col-sm-6 col-sm-offset-3">'
                                                        +'<h3>Підтвердити зміни</h3>'
                                                        +'<form action="/admin/deleteTarget" method="POST">'
                                                            +'<input type="hidden" name="_token" value="'+token+'">'
                                                            +'<input type="hidden" name="id" value="'+id+'">'
                                                            +'<div class="form-group">'
                                                                +'<label for="password">Ваш пароль</label>'
                                                                +'<input id="password" class="form-control" type="password" name="password" value=""/>'
                                                            +'</div>'
                                                            +'<input type="submit" class="btn btn-primary" name="confirmBank" value="Підтвердити"/>'
                                                        +'</form>'
                                                    +'</div>'
                                                +'</td>'
                                            +'</tr>');
        } else{
            $("tr[id='delete-"+id+"']").remove();
        }
    });
});