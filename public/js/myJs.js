$(document).ready(function(){
    

    $("#alert-close").fadeTo(3000, 500).slideUp(500, function(){//закрытие алерта ошибки
        $("#alert-close").slideUp(500);
    });

    $("[name='alert-close']").fadeTo(3000, 500).slideUp(500, function(){//закрытие алерта ошибки
        $("#alert-close").slideUp(500);
    });

    $(".addRow").click(function(){ //добавление строк
        var countRow = $(".lastNumeroRow").text();
        var token = $("[name='token']").val();
        var urlType = $("input[name='urlType']").attr("data-url");
        if($(".lastRow").prev().length > 0){
            var numero = parseInt($(".lastRow").prev().attr("id"))+1;
        } else{
            var numero = 1;
        }

        $.ajax({
            url: urlType+'addRow',
            method: 'POST',
            data:{
                _token: token
            },
            success:function(resp){
                if(resp != 'false'){
                    var text = '<tr id="'+numero+'" data-id="'+resp['id']+'">';
                    for (var i = 1; i <= countRow; i++) {
                        if(i == 1){
                            text += '<td id="action">'+numero+'<i class="fa fa-times deleteRow" title="Відалити"></i></td>';
                        } else if(i == countRow){
                            var formattedDate = new Date();
                            var d = formattedDate.getDate();
                            if(String(d).length == 1){
                                d = '0'+d;
                            }
                            var m = formattedDate.getMonth();
                            m += 1;
                            if(String(m).length == 1){
                                m = '0'+m;
                            }
                            var y = formattedDate.getFullYear();
                            text += '<td id="'+i+'" data-edit="no">'+d + "." + m + "." + y+'</td>';
                        } else{
                            text += '<td id="'+i+'" data-edit="yes"></td>';
                        }
                    };
                    text += '</tr>';
                    $(".lastRow").before(text);
                }
            },
            dataType: 'json'
        });
    });

    $(document).on('click', '.deleteRow', function(){//удаление строк
        var id = $(this).parent().parent().attr('data-id');
        var idRow = $(this).parent().parent().attr('id');
        var token = $("[name='token']").val();
        var urlType = $("input[name='urlType']").attr("data-url");

        $.ajax({
            url: urlType+'deleteRow',
            method: 'POST',
            data:{
                _token: token,
                id: id
            }, success:function(resp){
                if(resp == 'true'){
                    $("tr[id='"+idRow+"']").remove();

                    /*номерация при удалении строки*/

                }
            },
            dataType: 'json'
        });
    });

    $(document).on("click", ".register>tr>td", function(){//нажатие на тд и появление инпута или селекта с подгруженными данными
        if($(this).parent().attr('class') != 'lastRow' && $(this).attr('class') != 'editTd' && $(this).attr('id') != 'action' && $(this).attr('data-edit') != 'no'){
            $(this).addClass('editTd');
            var val = $(this).text();
            $(this).text('');
            var idCol = $(this).attr('id');
            var typeEnter = $("#td-"+idCol).attr("data-type");
            var idRow = $(this).parent().attr("id");
            var token = $("[name='token']").val();

            switch(typeEnter) {
              case 'text':  // ввод текста
                $(this).append('<input type="text" class="form-control" style="width:250px;" data-type="textInput" name="textTd" value="'+val+'">');
                $(this).children("[name='textTd']").focus();
                break;

              case 'bools':  // выбор ДА/НЕТ
                $.ajax({
                    url: '/getBools',
                    method: 'POST',
                    data: {_token: token},
                    success:function(resp){
                        if(resp.length > 0){
                            var text = '<select type="text" class="form-control" style="width:250px;" name="select"><option id="0">Виберіть</option>';
                            for(var i = 0; i < resp.length; i++){
                                if(resp[i]['bools'] == val){
                                    var sel = 'selected';
                                } else{
                                    var sel = '';
                                }
                                text += '<option id="'+resp[i]['id']+'" '+sel+'>'+resp[i]['bools']+'</option>';
                            }
                            text += '</select>';
                            $("tr[id='"+idRow+"']>td[id='"+idCol+"']").append(text);
                        }
                    },
                    dataType: 'json'
                });
                break;

              case 'numero':  // ввод чисел
                $(this).append('<input type="text" class="form-control" style="width:250px;" data-type="numeroInput" name="textTd" value="'+val+'">');
                $(this).children("[name='textTd']").focus();
                break;

              case 'numero-thousand':  // ввод чисел
                $(this).append('<input type="text" class="form-control" style="width:250px;" data-type="numeroInput" name="textTd" value="'+val+'">');
                $(this).children("[name='textTd']").focus();
                break;

              case 'stacking':  // выбор даты
                $.ajax({
                    url: '/getStacking',
                    method: 'POST',
                    data: {_token: token},
                    success:function(resp){
                        if(resp.length > 0){
                            var text = '<select type="text" class="form-control" style="width:250px;" name="select"><option id="0">Виберіть</option>';
                            for(var i = 0; i < resp.length; i++){
                                if(resp[i]['stacking'] == val){
                                    var sel = 'selected';
                                } else{
                                    var sel = '';
                                }
                                text += '<option id="'+resp[i]['id']+'" '+sel+'>'+resp[i]['stacking']+'</option>';
                            }
                            text += '</select>';
                            $("tr[id='"+idRow+"']>td[id='"+idCol+"']").append(text);
                        }
                    },
                    dataType: 'json'
                });
                break;

              case 'kved':  // выбор квед
                $.ajax({
                    url: '/getKved',
                    method: 'POST',
                    data: {
                        _token: token
                    },
                    success:function(resp){
                        if(resp.length > 0){
                            var text = '<select type="text" class="form-control" style="width:250px;" name="selectKved"><option id="0">Виберіть</option>';
                            vals = val.split(" / ");
                            var section = vals[0];
                            var group = vals[1];
                            var clas = vals[2];
                            for(var i = 0; i < resp.length; i++){
                                if(resp[i]['parent_kved'] == null){
                                    if(resp[i]['kved'] == section){
                                        sel = 'selected';
                                        var idSection = resp[i]['id'];
                                    } else{
                                        sel = '';
                                    }
                                    text += '<option id="'+resp[i]['id']+'" '+sel+'>'+resp[i]['kved']+'</option>';
                                }
                            }
                            text += '</select>';
                        } else{
                            var text = 'КВЕД 2010 немає';
                        }
                        $("tr[id='"+idRow+"']>td[id='"+idCol+"']").append(text);
                        if(typeof(idSection) != 'undefined'){
                            fnGroup(idSection, group, clas);
                            //fnClas(idSection, group);
                        }
                    },
                    dataType: 'json'
                });
                break;

              case 'target':  // выбор цели
                $.ajax({
                    url: '/getTarget',
                    method: 'POST',
                    data: {_token: token},
                    success:function(resp){
                        if(resp.length > 0){
                            var text = '<select type="text" class="form-control" style="width:250px;" name="select"><option id="0">Виберіть</option>';
                            for(var i = 0; i < resp.length; i++){
                                if(resp[i]['target'] == val){
                                    var sel = 'selected';
                                } else{
                                    var sel = '';
                                }
                                text += '<option id="'+resp[i]['id']+'" '+sel+'>'+resp[i]['target']+'</option>';
                            }
                            text += '</select>';
                            $("tr[id='"+idRow+"']>td[id='"+idCol+"']").append(text);
                        }
                    },
                    dataType: 'json'
                });
                break;

              case 'size-borrower':  // выбор даты
                $.ajax({
                    url: '/getSizeBorrower',
                    method: 'POST',
                    data: {_token: token},
                    success:function(resp){
                        if(resp.length > 0){
                            var text = '<select type="text" class="form-control" style="width:250px;" name="select"><option id="0">Виберіть</option>';
                            for(var i = 0; i < resp.length; i++){
                                if(resp[i]['size_borrower'] == val){
                                    var sel = 'selected';
                                } else{
                                    var sel = '';
                                }
                                text += '<option id="'+resp[i]['id']+'" '+sel+'>'+resp[i]['size_borrower']+'</option>';
                            }
                            text += '</select>';
                            $("tr[id='"+idRow+"']>td[id='"+idCol+"']").append(text);
                        }
                    },
                    dataType: 'json'
                });
                break;

              case 'term':  // выбор строка
                $.ajax({
                    url: '/getTerm',
                    method: 'POST',
                    data: {_token: token},
                    success:function(resp){
                        if(resp.length > 0){
                            var text = '<select type="text" class="form-control" style="width:250px;" name="select"><option id="0">Виберіть</option>';
                            for(var i = 0; i < resp.length; i++){
                                if(resp[i]['term'] == val){
                                    var sel = 'selected';
                                } else{
                                    var sel = '';
                                }
                                text += '<option id="'+resp[i]['id']+'" '+sel+'>'+resp[i]['term']+'</option>';
                            }
                            text += '</select>';
                            $("tr[id='"+idRow+"']>td[id='"+idCol+"']").append(text);
                        }
                    },
                    dataType: 'json'
                });
                break;

              case 'repayment_schedule':  // выбор графика погашения
                $.ajax({
                    url: '/getRepaymentSchedule',
                    method: 'POST',
                    data: {_token: token},
                    success:function(resp){
                        if(resp.length > 0){
                            var text = '<select type="text" class="form-control" style="width:250px;" name="select" data-type="repayment_schedule"><option id="noUpdate">Виберіть</option>';
                            var sel = '';
                            var sel2 = '';
                            for(var i = 0; i < resp.length; i++){
                                if(resp[i]['repayment_schedule'] == val){
                                    sel = 'selected';
                                    sel2 = 'selected';
                                } else{
                                    sel = '';
                                }
                                text += '<option id="'+resp[i]['id']+'" '+sel+'>'+resp[i]['repayment_schedule']+'</option>';
                            }
                            if(sel2 == '' && val != ''){
                                text += '<option id="0" selected>Інше</option></select><input type="text" class="form-control" style="width:250px;" data-type="textInput" data-table="repayment_schedule" name="textTd" value="'+val+'">';
                                $("tr[id='"+idRow+"']>td[id='"+idCol+"']").append(text);
                                $('[data-table="repayment_schedule"]').focus();
                            } else{
                                text += '<option id="0">Інше</option></select>';
                                $("tr[id='"+idRow+"']>td[id='"+idCol+"']").append(text);
                            }

                        }
                    },
                    dataType: 'json'
                });
                break;

              case 'date':  // выбор каледарь
                    $(this).append('<input class="form-control" type="date" name="textTd" value="'+val+'">');
                break;

              default:
                //...
                break;
            }

        }
    });

    function fnGroup(idSection, group, clas){
        var token = $("[name='token']").val();
        $.ajax({
            url: '/getKved',
            method: 'POST',
            data: {
                _token: token,
                parent_kved: idSection
            },
            success: function(resp){
                if(resp.length > 0){
                    var text = '<select type="text" class="form-control" style="width:250px;" name="selectGroup"><option id="0">Виберіть</option>';
                    for(var i = 0; i < resp.length; i++){
                        if(resp[i]['kved'] == group){
                            var sel = 'selected';
                            var idGroup = resp[i]['id'];
                        }else{
                            sel = '';
                        }
                        text += '<option id="'+resp[i]['id']+'" '+sel+'>'+resp[i]['kved']+'</option>';
                    }
                    text += '</select>'
                } else{
                    var text = 'груп немає';
                }
                $("select[name='selectKved']").after(text);
                if(typeof(idGroup) != 'undefined'){
                    fnClas(idGroup, clas);
                    //fnClas(idSection, group);
                }
            },
            dataType: 'json'
        });
    }

    function fnClas(idGroup, clas){
        var token = $("[name='token']").val();
        $.ajax({
            url: '/getKved',
            method: 'POST',
            data: {
                _token: token,
                parent_kved: idGroup
            },
            success: function(resp){
                if(resp.length > 0){
                    var text = '<select type="text" class="form-control" style="width:250px;" name="selectClas"><option id="0">Виберіть</option>';
                    for(var i = 0; i < resp.length; i++){
                        if(resp[i]['kved'] == clas){
                            var sel = 'selected';
                        }else{
                            sel = '';
                        }
                        text += '<option id="'+resp[i]['id']+'" '+sel+'>'+resp[i]['kved']+'</option>';
                    }
                    text += '</select>'
                } else{
                    var text = 'класів немає';
                }
                $("select[name='selectGroup']").after(text);
            },
            dataType: 'json'
        });
    }

    $(document).on('keydown', 'input[name="textTd"]', function(e){//скрытие инпута после ввода информации в строку и отправление ajax запроса на добавление
        if(e.keyCode === 13){

            var val = $(this).val();//значение новое
            var idRow = $(this).parent().parent().attr("id");//id строки
            var id = $(this).parent().parent().attr("data-id");//id строки в базе главной
            var idCol = $(this).parent().attr("id");//id колонки
            var token = $("[name='token']").val();
            var urlType = $("input[name='urlType']").attr("data-url");
            var idOld = null;
            if($(this).attr('data-table') == 'repayment_schedule'){
                var idOld = $(this).parent().attr("data-old");
                if(idOld == undefined){
                    idOld = null;
                }
                $.ajax({
                    url: '/registerOfAplicants/updateCol',
                    method: 'POST',
                    data: {
                        _token: token,
                        id: id,
                        val: val,
                        col: idCol,
                        idOld: idOld
                    },
                    success: function(resp){
                        $("tr[id='"+idRow+"']>td[id='"+idCol+"']").attr('data-old', resp);
                        $("tr[id='"+idRow+"']>td[id='"+idCol+"']>input[name='textTd']").parent().removeClass('editTd');
                        $("tr[id='"+idRow+"']>td[id='"+idCol+"']>input[name='textTd']").parent().text(val);
                        $("tr[id='"+idRow+"']>td[id='"+idCol+"']>input[name='textTd']").remove();
                    },
                    dataType: 'json'
                });

            }else{

                var typeEnter = $("#td-"+idCol).attr("data-type");
                switch(typeEnter){
                    case 'date':  // ввод даты
                        var formattedDate = new Date(val);
                        var d = formattedDate.getDate();
                        if(String(d).length == 1){
                            d = '0'+d;
                        }
                        var m =  formattedDate.getMonth();
                        m += 1;
                        if(String(m).length == 1){
                            m = '0'+m;
                        }
                        var y = formattedDate.getFullYear();
                        val_thousand = d+'.'+m+'.'+y;
                        val = y+'-'+m+'-'+d;
                        break;

                    case 'numero'://ввод числа
                        if(val == ''){
                            val = 0;
                        }else{
                            val = val.replace(/,/, '.');
                        }
                        break;

                    case 'numero-thousand'://ввод числа
                        if(val == ''){
                            val = 0;
                        }else{
                            val = val.replace(/,/, '.');
                            var val_thousand = val;
                            val = parseFloat(val)*1000;

                        }
                        break;

                    default:
                        //...
                        break;
                }

                $.ajax({
                    url: urlType+'updateRow',
                    method: 'POST',
                    data: {
                        _token: token,
                        id: id,
                        val: val,
                        col: idCol,
                        idOld: idOld
                    },
                    success: function(resp){
                        $("tr[id='"+idRow+"']>td[id='"+idCol+"']>input[name='textTd']").parent().removeClass('editTd');
                        $("tr[id='"+idRow+"']>td[id='"+idCol+"']>input[name='textTd']").parent().text((val_thousand != undefined) ? val_thousand : val);
                        $("tr[id='"+idRow+"']>td[id='"+idCol+"']>input[name='textTd']").remove();
                    },
                    dataType: 'json'
                });
            }
        }
    });

    $(document).on('change', 'select[name="selectKved"]', function(){//изминение выбора раздела КВЕД 2010
        var idSection = $(this).children(":selected").attr("id");
        if(idSection != 0){
            $("select[name='selectGroup']").remove();
            $("select[name='selectClas']").remove();
            var token = $("[name='token']").val();
            $.ajax({
                url: '/getKved',
                method: 'POST',
                data: {
                    _token: token,
                    parent_kved: idSection
                }, success: function(resp){
                    if(resp.length > 0){
                        var text = '<select type="text" class="form-control" style="width:250px;" name="selectGroup"><option id="0">Виберіть</option>';
                        for(var i = 0; i< resp.length; i++){
                            text += '<option id="'+resp[i]['id']+'">'+resp[i]['kved']+'</option>';
                        }
                        text += '</select>';
                        $('select[name="selectKved"]').after(text);
                    } else{
                        $('select[name="selectKved"]').after("груп немає");
                    }
                },
                dataType: 'json'
            });
        } else{
            $("select[name='selectGroup']").remove();
            $("select[name='selectClas']").remove();
        }
    });

    $(document).on('change', 'select[name="selectGroup"]', function(){//изминение выбора группы КВЕД 2010
        var idGroup = $(this).children(":selected").attr("id");
        if(idGroup != 0){
            $("select[name='selectClas']").remove();
            var token = $("[name='token']").val();
            $.ajax({
                url: '/getKved',
                method: 'POST',
                data: {
                    _token: token,
                    parent_kved: idGroup
                }, success: function(resp){
                    if(resp.length > 0){
                        var text = '<select type="text" class="form-control" style="width:250px;" name="selectClas"><option id="0">Виберіть</option>';
                        for(var i = 0; i< resp.length; i++){
                            text += '<option id="'+resp[i]['id']+'">'+resp[i]['kved']+'</option>';
                        }
                        text += '</select>';
                        $('select[name="selectGroup"]').after(text);
                    } else{
                        $('select[name="selectGroup"]').after("груп немає");
                    }
                },
                dataType: 'json'
            });
        } else{
            $("select[name='selectGroup']").remove();
        }
    });

    $(document).on('change', 'select[name="selectClas"]', function(){//после выбора класа сохранять в базу раздел, группу и класс
        var idClas = $(this).children(":selected").attr("id");
        if(idClas != 0){
            var idSection = $("select[name='selectKved']").children(":selected").attr("id");
            var idGroup = $("select[name='selectGroup']").children(":selected").attr("id");
            var urlType = $("input[name='urlType']").attr("data-url");
            var token = $("[name='token']").val();
            var idRow = $(this).parent().parent().attr("id");
            var id = $(this).parent().parent().attr("data-id");
            var idCol = $(this).parent().attr("id");
            var valSection = $("select[name='selectKved']").val();
            var valGroup = $("select[name='selectGroup']").val();
            var valClas = $("select[name='selectClas']").val();
            $.ajax({
                url: urlType+'updateRowKved',
                method: 'POST',
                data: {
                    _token: token,
                    id: id,
                    col: idCol,
                    idSection: idSection,
                    idGroup: idGroup,
                    idClas: idClas,
                },dataType: 'json',
                success:function(resp){
                    $("tr[id='"+idRow+"']>td[id='"+idCol+"']").removeClass('editTd');
                    $("tr[id='"+idRow+"']>td[id='"+idCol+"']").text(valSection+' / '+valGroup+' / '+valClas);
                    $("tr[id='"+idRow+"']>td[id='"+idCol+"']>select[name='selectKved']").remove();
                    $("tr[id='"+idRow+"']>td[id='"+idCol+"']>select[name='selectGroup']").remove();
                    $("tr[id='"+idRow+"']>td[id='"+idCol+"']>select[name='selectClas']").remove();
                }
            });
        }
    });

    $(document).on('change', 'select[name="select"]', function(){// обновление при выборе с селекта
        var idSelected = $(this).children(":selected").attr("id");
        if($.isNumeric(idSelected) && idSelected != 0){
            var val = $(this).val();
            var idRow = $(this).parent().parent().attr("id");
            var id = $(this).parent().parent().attr("data-id");
            var idCol = $(this).parent().attr("id");
            var token = $("[name='token']").val();
            var urlType = $("input[name='urlType']").attr("data-url");
            var idOld = null;

            if($(this).attr('data-type') == 'repayment_schedule'){
                var idOld = $(this).parent().attr("data-old");

                if(idOld == 1 || idOld == 2 || idOld == 3 || idOld == undefined){
                    idOld = null;
                }
            }

            $.ajax({
                url: urlType+'updateRow',
                method: 'POST',
                data: {
                    _token: token,
                    id: id,
                    val: idSelected,
                    col: idCol,
                    idOld: idOld
                },
                success: function(resp){
                    $("tr[id='"+idRow+"']>td[id='"+idCol+"']>select[name='select']").parent().removeClass('editTd');
                    $("tr[id='"+idRow+"']>td[id='"+idCol+"']>select[name='select']").parent().text(val);
                    $("tr[id='"+idRow+"']>td[id='"+idCol+"']>select[name='select']").remove();
                },
                dataType: 'json'
            });
        } else if(idSelected == 0){
            if($(this).attr('data-type') == 'repayment_schedule'){
                $(this).after('<input type="text" class="form-control" style="width:250px;" data-type="textInput" data-table="repayment_schedule" name="textTd" value="">');
                $(this).next().focus();
            }
        }
    });

    $(document).on('input', 'input[name="textTd"]', function(){
        if($(this).attr('data-type') == 'numeroInput'){
            if ($(this).val().split('.').length > 2) { // если запятая есть
                //$(this).val($(this).val().replace(/.,/g, ''));
            } else{
                $(this).val($(this).val().replace(/[^\d.]/g, ''));
            }
        }
    });


    $(document).on('change', 'input[name="img"]', function(){//появление после выбора картинки
        var val = $(this).val();
        if(val != ''){
            $(this).after('<input type="submit" class="btn btn-primary" value="Завантажити">');
        }
    });

    $('[name="copyUrl"]').click(function(){//копирование удреса картинки
        var val = $(this).attr("data-url");
        var $temp = $("<input>");
        $("body").append($temp);
        $temp.val(val).select();
        document.execCommand("copy");
        $temp.remove();
    });

    $("[name='deleteUrl']").click(function(){//удаление картинки
        var id = $(this).attr('data-id');
        var url = $(this).attr('data-url');
        var token = $("[name='_token']").val();
        $.ajax({
            url: '/deleteImage',
            method: 'POST',
            data: {
                _token: token,
                id: id,
                url: url
            }, dataType: 'json',
            success: function(resp){
                location.reload();
            }
        });
    });

});


