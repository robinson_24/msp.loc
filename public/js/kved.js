$(document).ready(function(){
    
    $("i[name='editKved']").click(function(){//редактирование КВЕД 2010
        var idKved = $(this).attr('data-kved');
        var val = $("span[id='bodyKved-"+idKved+"']").text();
        val = val.replace(/"/g, "&quot;");
        var token = $("[name='_token']").val();
        if($("*").find("tr[id='edit-kved-"+idKved+"']").length == 0){
            var insert = '<tr id="edit-kved-'+idKved+'" data-kved="'+idKved+'"><td colspan="3">'
                    +'<div class="col-xs-12 col-sm-6 col-sm-offset-3 form-group">'
                        +'<label style="float:left">Розділ</label>'
                        +'<i style="float:right" class="fa fa-2x fa-check green" name="saveNameKved" data-kved="'+idKved+'" title="Зберегти"></i>'
                        +'<input class="form-control" type="text" name="editNameKved" data-kved="'+idKved+'" value="'+val+'"/>'
                        +'<p>'
                        +'<span style="text-decoration:underline; color:blue; cursor:pointer;" data-kved="'+idKved+'" class="addGroup">Додати групу</span>&nbsp;'
                        +'</p>';
            $.ajax({
                url: '/getKved',
                method: 'POST',
                data: {
                    _token: token
                },
                dataType: 'json',
                success: function(resp){
                    if(resp.length > 0){
                        for (var i = 0; i < resp.length; i++) {
                            if(resp[i]['parent_kved'] == idKved){
                                var val = resp[i]['kved'].replace(/"/g, "&quot;");
                                insert += '<div style="margin-left: 5%;"><label style="float:left">Група</label>';
                                insert += '<i style="float:right" class="fa fa-2x fa-times red" name="removeGroupKved" data-group="'+resp[i]["id"]+'" title="Видалити"></i>';
                                insert += '<i style="float:right" class="fa fa-2x fa-check green" name="saveGroupKved" data-group="'+resp[i]["id"]+'" title="Зберегти"></i>';
                                insert += '<input class="form-control" type="text" name="nameGroup" data-group="'+resp[i]["id"]+'" value="'+val+'"/></div>';
                                insert += '<p>'
                                insert += '<span style="text-decoration:underline; color:blue; cursor:pointer;" data-group="'+resp[i]["id"]+'" class="addClas">Додати клас</span>&nbsp;'
                                insert += '</p>';
                                for (var j = 0; j < resp.length; j++) {
                                    if(resp[j]['parent_kved'] == resp[i]['id']){
                                        var val = resp[j]['kved'].replace(/"/g, "&quot;");
                                        insert += '<div style="margin-left: 10%;"><label style="float:left">Клас</label>';
                                        insert += '<i style="float:right" class="fa fa-2x fa-times red" name="removeClasKved" data-clas="'+resp[j]["id"]+'" title="Видалити"></i>';
                                        insert += '<i style="float:right" class="fa fa-2x fa-check green" name="saveClasKved" data-clas="'+resp[j]["id"]+'" title="Зберегти"></i>';
                                        insert += '<input class="form-control" type="text" name="nameClas" data-Clas="'+resp[j]["id"]+'" value="'+val+'"/></div>';
                                    }
                                };
                            }
                        };
                        insert +='</div></td></tr>';
                        $("tr[id='kved-"+idKved+"']").after(insert);
                    }else{
                        insert +='</div></td></tr>';
                        $("tr[id='kved-"+idKved+"']").after(insert);
                    }
                }
            });
        } else{
            $("tr[id='edit-kved-"+idKved+"']").remove();
        }
    });

    $("[name='deleteKved']").click(function(){//удаление КВЕД 2010 (раздела)
        var id = $(this).parent().parent().attr('id');
        if($("*").find("tr[id='delete-"+id+"']").length == 0){
            var token = $("[name='_token']").val();
            $(this).parent().parent().after('<tr id="delete-'+id+'"><td colspan="4"><div class="col-xs-12 col-sm-6 col-sm-offset-3"><h3>Підтвердити зміни</h3><form action="/admin/deleteKved" method="POST"><input type="hidden" name="_token" value="'+token+'"><input type="hidden" name="id" value="'+id+'"><div class="form-group"><label for="password">Ваш пароль</label><input id="password" class="form-control" type="password" name="password" value=""/></div><input type="submit" class="btn btn-primary" name="confirmKved" value="Підтвердити"/></form></div></td></tr>');
        } else{
            $("tr[id='delete-"+id+"']").remove();
        }
    });

    $(document).on('click', '.addGroup', function(){//добавление новой группы в раздел
        var idKved = $(this).attr('data-kved');
        $(this).parent().after('<div style="margin-left: 5%"><label style="float:left">Нова група</label>'
        +'<i style="float:right" class="fa fa-2x fa-times red" name="removeNewGroupKved" data-kved="'+idKved+'" title="Видалити"></i>'
        +'<i style="float:right" class="fa fa-2x fa-check green" name="saveNewGroupKved" data-kved="'+idKved+'" title="Зберегти"></i>'
        +'<input class="form-control" type="text" name="addGroup" data-kved="'+idKved+'" value=""/></div>');
    });

    $(document).on('click', '.addClas', function(){//добавление нового класса в группу
        var idKved = $(this).attr('data-group');
        $(this).parent().after('<div style="margin-left: 10%"><label style="float:left">Новий клас</label>'
        +'<i style="float:right" class="fa fa-2x fa-times red" name="removeNewClasKved" data-group="'+idKved+'" title="Видалити"></i>'
        +'<i style="float:right" class="fa fa-2x fa-check green" name="saveNewClasKved" data-group="'+idKved+'" title="Зберегти"></i>'
        +'<input class="form-control" type="text" name="addClas" data-group="'+idKved+'" value=""/></div>');
    });

    $(document).on('click', "i[name='removeNewGroupKved']", function(){//удаление новой группы
        $(this).parent().remove();
    });

    $(document).on('click', "i[name='removeNewClasKved']", function(){//удаление нового класса
        $(this).parent().remove();
    });

    $(document).on('click', "i[name='saveNewGroupKved']", function(){//сохранение новой группы
        var idKved = $(this).attr('data-kved');
        var val = $(this).next().val();
        var token = $("[name='_token']").val();
        $.ajax({
            url: '/admin/addGroup',
            method: 'POST',
            data:{
                _token: token,
                parent_kved: idKved,
                kved: val
            },
            dataType: 'json',
            success:function(resp){
                location.reload();
            }
        });
    });

    $(document).on('click', "i[name='saveNewClasKved']", function(){//сохранение нового класса
        var idKved = $(this).attr('data-group');
        var val = $(this).next().val();
        var token = $("[name='_token']").val();
        $.ajax({
            url: '/admin/addGroup',
            method: 'POST',
            data:{
                _token: token,
                parent_kved: idKved,
                kved: val
            },
            dataType: 'json',
            success:function(resp){
                location.reload();
            }
        });
    });

    $(document).on('click', 'i[name="removeGroupKved"]', function(){//удаление группы
        var id = $(this).attr('data-group');
        var token = $("[name='_token']").val();
        $.ajax({
            url: '/admin/removeGroup',
            method: 'POST',
            data:{
                _token: token,
                id: id
            },
            dataType: 'json',
            success:function(resp){
                location.reload();
            }
        });
    });

    $(document).on('click', 'i[name="removeClasKved"]', function(){//удаление класа
        var id = $(this).attr('data-clas');
        var token = $("[name='_token']").val();
        $.ajax({
            url: '/admin/removeClas',
            method: 'POST',
            data:{
                _token: token,
                id: id
            },
            dataType: 'json',
            success:function(resp){
                location.reload();
            }
        });
    });

    $(document).on('click', 'i[name="saveGroupKved"]', function(){//сохранение изминения группы
        var id = $(this).attr('data-group');
        var val = $(this).next().val();
        var token = $("[name='_token']").val();
        $.ajax({
            url: '/admin/editGroup',
            method: 'POST',
            data:{
                _token: token,
                id: id,
                kved: val
            },
            dataType: 'json',
            success:function(resp){
                location.reload();
            }
        });
    });

    $(document).on('click', 'i[name="saveClasKved"]', function(){//сохранение изминения класса
        var id = $(this).attr('data-clas');
        var val = $(this).next().val();
        var token = $("[name='_token']").val();
        $.ajax({
            url: '/admin/editGroup',
            method: 'POST',
            data:{
                _token: token,
                id: id,
                kved: val
            },
            dataType: 'json',
            success:function(resp){
                location.reload();
            }
        });
    });

    $(document).on('click', 'i[name="saveNameKved"]', function(){//сохранение изминения раздела
        var id = $(this).attr('data-kved');
        var val = $(this).next().val();
        var token = $("[name='_token']").val();
        $.ajax({
            url: '/admin/editGroup',
            method: 'POST',
            data:{
                _token: token,
                id: id,
                kved: val
            },
            dataType: 'json',
            success:function(resp){
                location.reload();
            }
        });
    });
});