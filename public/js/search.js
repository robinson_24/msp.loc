$(document).ready(function(){

    $("button[name='search']").click(function(){ //поиск
        var col_1 = $("select[name='field-1'] option:selected").attr('id');//колонка в базе
        var col_2 = $("select[name='field-2'] option:selected").attr('id');//колонка в базе
        if(col_1 != 0 || col_2 != 0){
            var search = '';
            if(col_1 != 0){
                var dataType = $("select[name='field-1'] option:selected").attr('data-value');
                var value_1 = '';
                switch(dataType){
                    case 'text':
                        value_1 = $("[name='value-1']").val();
                        break;
                    case 'personal':
                        value_1 = $("[name='value-1']").val();
                        break;
                    case 'stacking':
                        value_1 = $("select[name='value-1'] option:selected").attr('id');
                        break;
                    case 'target':
                        value_1 = $("select[name='value-1'] option:selected").attr('id');
                        break;
                    case 'bank':
                        value_1 = $("select[name='value-1'] option:selected").attr('id');
                        break;
                    case 'size-borrower':
                        value_1 = $("select[name='value-1'] option:selected").attr('id');
                        break;
                    case 'total':
                        value_1 = $("[name='value-1']").val();
                        break;
                    case 'term':
                        value_1 = $("select[name='value-1'] option:selected").attr('id');
                        break;
                    case 'kved':
                        value_1 = $("select[name='kved-value-1'] option:selected").attr('id')+'_'+$("select[name='group-value-1'] option:selected").attr('id')+'_'+$("select[name='clas-value-1'] option:selected").attr('id');
                        break;
                    case 'date':
                        var formattedDate = new Date($("[name='value-1']").val());
                        var d = formattedDate.getDate();
                        if(String(d).length == 1){
                            d = '0'+d;
                        }
                        var m =  formattedDate.getMonth();
                        m += 1;
                        if(String(m).length == 1){
                            m = '0'+m;
                        }
                        var y = formattedDate.getFullYear();
                        value_1 = d+'.'+m+'.'+y;
                        break;

                    case 'dateSelect':
                        //$('select.selectized,input.selectized').each(function() {//получить id и ввод
                        //    value_1 = JSON.stringify($(this).val());
                        //});
                        value_1 = $("[name='value-1']").val();
                        break;

                    default:
                        //...
                        break;
                }

                if(value_1 != ''){
                    if(search == ''){
                        search = '?field_1='+col_1+'&val_1='+value_1+'&type_1='+dataType;
                    } else{
                        search = search+'&field_1='+col_1+'&val_1='+value_1+'&type_1='+dataType;
                    }
                }
            }
            if(col_2 != 0){
                var dataType = $("select[name='field-2'] option:selected").attr('data-value');
                var value_2 = '';
                switch(dataType){
                    case 'text':
                        value_2 = $("[name='value-2']").val();
                        break;
                    case 'personal':
                        value_2 = $("select[name='value-2'] option:selected").attr('id');
                        break;
                    case 'stacking':
                        value_2 = $("select[name='value-2'] option:selected").attr('id');
                        break;
                    case 'target':
                        value_2 = $("select[name='value-2'] option:selected").attr('id');
                        break;
                    case 'bank':
                        value_2 = $("select[name='value-2'] option:selected").attr('id');
                        break;
                    case 'size-borrower':
                        value_2 = $("select[name='value-2'] option:selected").attr('id');
                        break;
                    case 'total':
                        value_2 = $("select[name='value-2'] option:selected").attr('id');
                        break;
                    case 'kved':
                        value_2 = $("select[name='kved-value-2'] option:selected").attr('id')+'_'+$("select[name='group-value-2'] option:selected").attr('id')+'_'+$("select[name='clas-value-2'] option:selected").attr('id');
                        break;
                    case 'term':
                        value_2 = $("select[name='value-2'] option:selected").attr('id');
                        break;
                    case 'date':
                        var formattedDate = new Date($("[name='value-2']").val());
                        var d = formattedDate.getDate();
                        if(String(d).length == 1){
                            d = '0'+d;
                        }
                        var m =  formattedDate.getMonth();
                        m += 1;
                        if(String(m).length == 1){
                            m = '0'+m;
                        }
                        var y = formattedDate.getFullYear();
                        value_2 = d+'.'+m+'.'+y;
                        break;

                    case 'dateSelect':
                        $('select.selectized,input.selectized').each(function() {
                            var $input = $(this);
                            value_2 = JSON.stringify($input.val());
                        });
                        break;

                    default:
                        //...
                        break;
                }
                if(value_2 != ''){
                    if(search == ''){
                        search = '?field_2='+col_2+'&val_2='+value_2+'&type_2='+dataType;
                    } else{
                        search = search+'&field_2='+col_2+'&val_2='+value_2+'&type_2='+dataType;
                    }
                }
            }
            //location.href = location.href+'/search'+search;
            
            if(location.pathname.search('/search') > 0){
                location.href = location.pathname+search;
            } else{
                location.href = location.pathname+'/search'+search;
            }
            
            //console.log(location.href+'/search'+search);
            //if(search != ''){
            //    location.href = location.href.substr(0, location.href.search('generalRegister')+15)+'/search'+search;
            //}
        }
    });

    $(document).on('change', "select[name='field-1']", function(){//изминение первого селекта в поиске
        var typeValue = $(this).children(":selected").attr('data-value');//тип по которуму будем создавать поле для ввода информации
        var token = $("[name='_token']").val();
        
        if($("[name='value-1']").attr('data-type') != "dateSelect"){
            $("[name='value-1']").remove();
        } else{
            //$("[name='value-1']").text('');
            $("[name='value-1']").parent().remove();
        }

        if($(this).next().attr('name') == 'kved-value-1'){
            $("[name='kved-value-1']").remove();
            $("[name='group-value-1']").remove();
            $("[name='clas-value-1']").remove();
        }

        switch(typeValue){
            case 'none':
                $("[name='value-1']").remove();
                break;
                
            case 'text'://обычный ввод текста
                $(this).after("<input class='form-control' type='text' data-type='input' name='value-1' value=''/>");
                break;
                
            case 'personal'://селект персонала
                $.ajax({
                    url: '/getPersonal',
                    method: 'POST',
                    data: {_token: token},
                    success:function(resp){
                        if(resp.length > 0){
                            var text = '<select type="text" class="form-control" data-type="select" name="value-1"><option id="0">Виберіть</option>';
                            for(var i = 0; i < resp.length; i++){
                                text += '<option id="'+resp[i]['id']+'">'+resp[i]['personal']+'</option>';
                            }
                            text += '</select>';
                            $("select[name='field-1']").after(text);
                        }
                    },
                    dataType: 'json'
                });
                break;
                
            case 'kved'://селект КВЕД 2010
                $.ajax({
                    url: '/getKved',
                    method: 'POST',
                    data: {_token: token},
                    success:function(resp){
                        if(resp.length > 0){
                            var text = '<select type="text" style="width: 200px;" class="form-control" data-type="select" name="kved-value-1"><option id="0">Виберіть</option>';
                            for(var i = 0; i < resp.length; i++){
                                if(resp[i]['parent_kved'] == null){
                                    text += '<option id="'+resp[i]['id']+'">'+resp[i]['kved']+'</option>';
                                }
                                
                            }
                            text += '</select>';
                            $("select[name='field-1']").after(text);
                        }
                    },
                    dataType: 'json'
                });
                break;
                
            case 'size-borrower'://селект персонала
                $.ajax({
                    url: '/getSizeBorrower',
                    method: 'POST',
                    data: {_token: token},
                    success:function(resp){
                        if(resp.length > 0){
                            var text = '<select type="text" class="form-control" data-type="select" name="value-1"><option id="0">Виберіть</option>';
                            for(var i = 0; i < resp.length; i++){
                                text += '<option id="'+resp[i]['id']+'">'+resp[i]['size_borrower']+'</option>';
                            }
                            text += '</select>';
                            $("select[name='field-1']").after(text);
                        }
                    },
                    dataType: 'json'
                });
                break;
                
            case 'bank'://селект персонала
                var category = $("[name='url']").val();
                $.ajax({
                    url: '/getBank',
                    method: 'POST',
                    data: {
                        _token: token,
                        category: category
                    },success:function(resp){
                        if(resp.length > 0){
                            var text = '<select type="text" class="form-control" data-type="select" name="value-1"><option id="0">Виберіть</option>';
                            for(var i = 0; i < resp.length; i++){
                                text += '<option id="'+resp[i]['id']+'">'+resp[i]['name']+'</option>';
                            }
                            text += '</select>';
                            $("select[name='field-1']").after(text);
                        }
                    },
                    dataType: 'json'
                });
                break;
                
            case 'stacking':
                $.ajax({
                    url: '/getStacking',
                    method: 'POST',
                    data: {_token: token},
                    success:function(resp){
                        if(resp.length > 0){
                            var text = '<select type="text" class="form-control" data-type="select" name="value-1"><option id="0">Виберіть</option>';
                            for(var i = 0; i < resp.length; i++){
                                text += '<option id="'+resp[i]['id']+'">'+resp[i]['stacking']+'</option>';
                            }
                            text += '</select>';
                            $("select[name='field-1']").after(text);
                        }
                    },
                    dataType: 'json'
                });
                break;
                
            case 'target':
                $.ajax({
                    url: '/getTarget',
                    method: 'POST',
                    data: {_token: token},
                    success:function(resp){
                        if(resp.length > 0){
                            var text = '<select type="text" style="width: 200px;" class="form-control" data-type="select" name="value-1"><option id="0">Виберіть</option>';
                            for(var i = 0; i < resp.length; i++){
                                text += '<option id="'+resp[i]['id']+'">'+resp[i]['target']+'</option>';
                            }
                            text += '</select>';
                            $("select[name='field-1']").after(text);
                        }
                    },
                    dataType: 'json'
                });
                break;
                
            case 'term':
                $.ajax({
                    url: '/getTerm',
                    method: 'POST',
                    data: {_token: token},
                    success:function(resp){
                        if(resp.length > 0){
                            var text = '<select type="text" class="form-control" data-type="select" name="value-1"><option id="0">Виберіть</option>';
                            for(var i = 0; i < resp.length; i++){
                                text += '<option id="'+resp[i]['id']+'">'+resp[i]['term']+'</option>';
                            }
                            text += '</select>';
                            $("select[name='field-1']").after(text);
                        }
                    },
                    dataType: 'json'
                });
                break;
                
            case 'total':
                $.ajax({
                    url: '/getTotal',
                    method: 'POST',
                    data: {_token: token},
                    success:function(resp){
                        if(resp.length > 0){
                            var text = '<select type="text" class="form-control" data-type="select" name="value-1"><option id="0">Виберіть</option>';
                            for(var i = 0; i < resp.length; i++){
                                text += '<option id="'+resp[i]['id']+'">'+resp[i]['total']+'</option>';
                            }
                            text += '</select>';
                            $("select[name='field-1']").after(text);
                        }
                    },
                    dataType: 'json'
                });
                break;
                
            case 'date':
                $(this).after("<input class='form-control' type='date' data-type='date' name='value-1' value=''/>");
                break;
                
            case 'dateSelect':
            $.ajax({
                    url: '/getStacking',
                    method: 'POST',
                    data: {_token: token},
                    success:function(resp){
                        if(resp.length > 0){
                            var text = '<div class="dateSelectParent" style="display: inline-block;"><select id="dateSelect" class="form-control" data-type="dateSelect" name="value-1" placeholder="Введіть дату або виберіть"><option id="">Введіть дату або виберіть</option>';
                            for(var i = 0; i < resp.length; i++){
                                text += '<option id="'+resp[i]['id']+'">'+resp[i]['stacking']+'</option>';
                                //text += '<option value="'+resp[i]['id']+'">'+resp[i]['stacking']+'</option>';// получить id и ввод
                            }
                            text += '</select></div>';
                            $("select[name='field-1']").after(text);
                        }
                    },
                    dataType: 'json'
                });
                break;

            default :
                //$("[name='value-1']").remove();
            break;
        }
    });

    $(document).on('change', "select[name='field-2']", function(){//изминение второго селекта в поиске
        var typeValue = $(this).children(":selected").attr('data-value');//тип по которуму будем создавать поле для ввода информации
        var token = $("[name='_token']").val();
        
        if($("[name='value-2']").attr('data-type') != "dateSelect"){
            $("[name='value-2']").remove();
        } else{
            //$("[name='value-1']").text('');
            $("[name='value-2']").parent().remove();
        }

        if($("select[name='field-2']").next().attr('name') == 'kved-value-2'){
            $("[name='kved-value-2']").remove();
            $("[name='group-value-2']").remove();
            $("[name='clas-value-2']").remove();
        }

        switch(typeValue){
            case 'none':
                $("[name='value-2']").remove();
                break;
                
            case 'text'://обычный ввод текста
                $(this).after("<input class='form-control' type='text' data-type='input' name='value-2' value=''/>");
                break;
                
            case 'personal'://селект персонала
                $.ajax({
                    url: '/getPersonal',
                    method: 'POST',
                    data: {_token: token},
                    success:function(resp){
                        if(resp.length > 0){
                            var text = '<select type="text" class="form-control" data-type="select" name="value-2"><option id="0">Виберіть</option>';
                            for(var i = 0; i < resp.length; i++){
                                text += '<option id="'+resp[i]['id']+'">'+resp[i]['personal']+'</option>';
                            }
                            text += '</select>';
                            $("select[name='field-2']").after(text);
                        }
                    },
                    dataType: 'json'
                });
                break;
                
            case 'kved'://селект КВЕД 2010
                $.ajax({
                    url: '/getKved',
                    method: 'POST',
                    data: {_token: token},
                    success:function(resp){
                        if(resp.length > 0){
                            var text = '<select type="text" style="width: 200px;" class="form-control" data-type="select" name="kved-value-2"><option id="0">Виберіть</option>';
                            for(var i = 0; i < resp.length; i++){
                                if(resp[i]['parent_kved'] == null){
                                    text += '<option id="'+resp[i]['id']+'">'+resp[i]['kved']+'</option>';
                                }
                                
                            }
                            text += '</select>';
                            $("select[name='field-2']").after(text);
                        }
                    },
                    dataType: 'json'
                });
                break;

            case 'size-borrower'://селект персонала
                $.ajax({
                    url: '/getSizeBorrower',
                    method: 'POST',
                    data: {_token: token},
                    success:function(resp){
                        if(resp.length > 0){
                            var text = '<select type="text" class="form-control" data-type="select" name="value-2"><option id="0">Виберіть</option>';
                            for(var i = 0; i < resp.length; i++){
                                text += '<option id="'+resp[i]['id']+'">'+resp[i]['size_borrower']+'</option>';
                            }
                            text += '</select>';
                            $("select[name='field-2']").after(text);
                        }
                    },
                    dataType: 'json'
                });
                break;
                
            case 'bank'://селект персонала
                var category = $("[name='url']").val();
                $.ajax({
                    url: '/getBank',
                    method: 'POST',
                    data: {
                        _token: token,
                        category: category
                    },success:function(resp){
                        if(resp.length > 0){
                            var text = '<select type="text" class="form-control" data-type="select" name="value-2"><option id="0">Виберіть</option>';
                            for(var i = 0; i < resp.length; i++){
                                text += '<option id="'+resp[i]['id']+'">'+resp[i]['name']+'</option>';
                            }
                            text += '</select>';
                            $("select[name='field-2']").after(text);
                        }
                    },
                    dataType: 'json'
                });
                break;
                
            case 'stacking':
                $.ajax({
                    url: '/getStacking',
                    method: 'POST',
                    data: {_token: token},
                    success:function(resp){
                        if(resp.length > 0){
                            var text = '<select type="text" class="form-control" data-type="select" name="value-2"><option id="0">Виберіть</option>';
                            for(var i = 0; i < resp.length; i++){
                                text += '<option id="'+resp[i]['id']+'">'+resp[i]['stacking']+'</option>';
                            }
                            text += '</select>';
                            $("select[name='field-2']").after(text);
                        }
                    },
                    dataType: 'json'
                });
                break;
                
            case 'target':
                $.ajax({
                    url: '/getTarget',
                    method: 'POST',
                    data: {_token: token},
                    success:function(resp){
                        if(resp.length > 0){
                            var text = '<select type="text" style="width: 200px;" class="form-control" data-type="select" name="value-2"><option id="0">Виберіть</option>';
                            for(var i = 0; i < resp.length; i++){
                                text += '<option id="'+resp[i]['id']+'">'+resp[i]['target']+'</option>';
                            }
                            text += '</select>';
                            $("select[name='field-2']").after(text);
                        }
                    },
                    dataType: 'json'
                });
                break;
                
            case 'term':
                $.ajax({
                    url: '/getTerm',
                    method: 'POST',
                    data: {_token: token},
                    success:function(resp){
                        if(resp.length > 0){
                            var text = '<select type="text" class="form-control" data-type="select" name="value-2"><option id="0">Виберіть</option>';
                            for(var i = 0; i < resp.length; i++){
                                text += '<option id="'+resp[i]['id']+'">'+resp[i]['term']+'</option>';
                            }
                            text += '</select>';
                            $("select[name='field-2']").after(text);
                        }
                    },
                    dataType: 'json'
                });
                break;
                
            case 'total':
                $.ajax({
                    url: '/getTotal',
                    method: 'POST',
                    data: {_token: token},
                    success:function(resp){
                        if(resp.length > 0){
                            var text = '<select type="text" class="form-control" data-type="select" name="value-2"><option id="0">Виберіть</option>';
                            for(var i = 0; i < resp.length; i++){
                                text += '<option id="'+resp[i]['id']+'">'+resp[i]['total']+'</option>';
                            }
                            text += '</select>';
                            $("select[name='field-2']").after(text);
                        }
                    },
                    dataType: 'json'
                });
                break;
                
            case 'date':
                $(this).after("<input class='form-control' type='date' data-type='date' name='value-2' value=''/>");
                break;
                
            case 'dateSelect':
            $.ajax({
                    url: '/getStacking',
                    method: 'POST',
                    data: {_token: token},
                    success:function(resp){
                        if(resp.length > 0){
                            var text = '<div class="dateSelectParent" style="display: inline-block;"><select id="dateSelect" class="form-control" data-type="dateSelect" name="value-2" placeholder="Введіть дату або виберіть"><option id="">Введіть дату або виберіть</option>';
                            for(var i = 0; i < resp.length; i++){
                                text += '<option id="'+resp[i]['id']+'">'+resp[i]['stacking']+'</option>';
                            }
                            text += '</select></div>';
                            $("select[name='field-2']").after(text);
                        }
                    },
                    dataType: 'json'
                });
                break;

            default :
                //$("[name='value-1']").remove();
            break;
        }
    });

    $(document).on('change', "select[name='kved-value-1']", function(){
        var idSection = $(this).children(":selected").attr('id');
        var token = $("[name='_token']").val();
        $("select[name='group-value-1']").remove();
        if(idSection != 0){
            $.ajax({
                url: '/getKved',
                method: 'POST',
                data: {
                    _token: token,
                    parent_kved: idSection
                },
                dataType: 'json',
                success: function(resp){
                    if(resp.length > 0){
                        var text = '<select type="text" style="width: 200px;" class="form-control" data-type="select" name="group-value-1"><option id="0">Виберіть</option>';
                        for (var i = 0; i < resp.length; i++) {
                            text += '<option id="'+resp[i]['id']+'">'+resp[i]['kved']+'</option>';
                        }
                        text += '</select>';
                    } else{
                        var text = 'груп немає';
                    }
                    $("select[name='kved-value-1']").after(text);
                }
            });
        }
    });

    $(document).on('change', "select[name='group-value-1']", function(){
        var idSection = $(this).children(":selected").attr('id');
        var token = $("[name='_token']").val();
        $("select[name='clas-value-1']").remove();
        if(idSection != 0){
            $.ajax({
                url: '/getKved',
                method: 'POST',
                data: {
                    _token: token,
                    parent_kved: idSection
                },
                dataType: 'json',
                success: function(resp){
                    if(resp.length > 0){
                        var text = '<select type="text" style="width: 200px;" class="form-control" data-type="select" name="clas-value-1"><option id="0">Виберіть</option>';
                        for (var i = 0; i < resp.length; i++) {
                            text += '<option id="'+resp[i]['id']+'">'+resp[i]['kved']+'</option>';
                        }
                        text += '</select>';
                    } else{
                        var text = 'класів немає';
                    }
                    $("select[name='group-value-1']").after(text);
                }
            });
        }
    });

    $(document).on('change', "select[name='group-value-2']", function(){
        var idSection = $(this).children(":selected").attr('id');
        var token = $("[name='_token']").val();
        $("select[name='clas-value-2']").remove();
        if(idSection != 0){
            $.ajax({
                url: '/getKved',
                method: 'POST',
                data: {
                    _token: token,
                    parent_kved: idSection
                },
                dataType: 'json',
                success: function(resp){
                    if(resp.length > 0){
                        var text = '<select type="text" style="width: 200px;" class="form-control" data-type="select" name="clas-value-2"><option id="0">Виберіть</option>';
                        for (var i = 0; i < resp.length; i++) {
                            text += '<option id="'+resp[i]['id']+'">'+resp[i]['kved']+'</option>';
                        }
                        text += '</select>';
                    } else{
                        var text = 'класів немає';
                    }
                    $("select[name='group-value-2']").after(text);
                }
            });
        }
    });

    $(document).on('change', "select[name='kved-value-2']", function(){
        var idSection = $(this).children(":selected").attr('id');
        var token = $("[name='_token']").val();
        $("select[name='group-value-2']").remove();
        if(idSection != 0){
            $.ajax({
                url: '/getKved',
                method: 'POST',
                data: {
                    _token: token,
                    parent_kved: idSection
                },
                dataType: 'json',
                success: function(resp){
                    if(resp.length > 0){
                        var text = '<select type="text" style="width: 200px;" class="form-control" data-type="select" name="group-value-2"><option id="0">Виберіть</option>';
                        for (var i = 0; i < resp.length; i++) {
                            text += '<option id="'+resp[i]['id']+'">'+resp[i]['kved']+'</option>';
                        }
                        text += '</select>';
                    } else{
                        var text = 'груп немає';
                    }
                    $("select[name='kved-value-2']").after(text);
                }
            });
        }
    });

    $(document).on('mouseenter', '#dateSelect', function(){//наведение на поле для ввода даты или выбра
        $(this).selectize({
            create: true,
            sortField: {
                field: 'text',
                direction: 'asc'
            },
            dropdownParent: 'body'
        });
    });
});