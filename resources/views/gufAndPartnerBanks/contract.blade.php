@extends('app')

@section('content')
<style type="text/css">
li{
    font-size: 17px;
}
li:hover{
    font-weight:bold;
}
</style>
    <div class="row">
        <div class="col-xs-12 ">
            @if(!empty($contracts) && count($contracts))
                <ol>
                    @for($i = 0; $i < count($contracts); $i++)
                        <li>{{ $contracts[$i]->name }}
                            @if(!empty($contracts[$i]->url_pdf))
                                <span style="float:right;">
                                    <a href="/download/contractsDPRPWithGuf/{{$contracts[$i]->url_pdf}}" class="btn btn-success btn-xs"><i class="fa fa-download"></i>&nbsp;PDF</a>
                                    @if(Session::get('status') == 'departament')
                                        <a href="/delete/contractsDPRPWithGuf/{{ $contracts[$i]->id }}" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i></a>
                                    @endif
                                </span>
                             @endif
                        </li>
                    @endfor
                </ol>
            @endif
        </div>
    </div>

    @if(Session::has('userId') && Session::get('status') == 'departament')
        <div class="row">
            <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 alert alert-info">
                <form action="/gufAndPartnerBanks/guf/contractsDPRPWithGuf/add" method="post" enctype="multipart/form-data">
                    <input class="form-control" type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-group">
                        <label for="name">Назва документу</label>
                        <input type="text" class="form-control" name="name" id="name" placeholder="Назва документу">
                    </div>
                    @if(!empty($error) && $error->has('name'))
                        <p class="bg-danger">{{$error->first('name')}}</p>
                    @endif
                    <div class="form-group">
                        <label for="pdf">PDF-файл документу</label>
                        <input type="file" id="pdf" name="pdf">
                        <p class="help-block">Файли з форматом pdf.</p>
                    </div>
                    <button type="submit" class="btn btn-default">Додати</button>
                </form>
            </div>
        </div>
    @endif
@endsection