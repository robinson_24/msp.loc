@extends('app')

@section('content')
    <div class="row text-center">
        <div class="col-xs-12 col-sm-6">
            <a href="/gufAndPartnerBanks/guf/information">
                <div class="border">
                    <p>Інформація</p>
                </div>
            </a>
        </div>
        <div class="col-xs-12 col-sm-6">
            <a href="/gufAndPartnerBanks/guf/contract">
                <div class="border">
                    <p>Договір ДПРП з НУФ</p>
                </div>
            </a>
        </div>
    </div>
@endsection