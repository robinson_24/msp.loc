@extends('app')

@section('content')
    <div class="row text-center">
        <div class="col-xs-12 col-sm-4">
            <a href="/gufAndPartnerBanks/partnerBanks/bank/{{ $id }}/information">
                <div class="border">
                    <p>Інформація</p>
                </div>
            </a>
        </div>
        <div class="col-xs-12 col-sm-4">
            <a href="/gufAndPartnerBanks/partnerBanks/bank/{{ $id }}/contractDPRP">
                <div class="border">
                    <p>Договір Департаменту с банком "{{ $nameBank }}"</p>
                </div>
            </a>
        </div>
        <div class="col-xs-12 col-sm-4">
            <a href="/gufAndPartnerBanks/partnerBanks/bank/{{ $id }}/contractGUF">
                <div class="border">
                    <p>Договір банку "{{ $nameBank }}" з НУФ</p>
                </div>
            </a>
        </div>
    </div>
@endsection