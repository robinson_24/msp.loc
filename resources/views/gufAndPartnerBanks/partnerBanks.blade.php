@extends('app')

@section('content')
    <div class="row text-center">
        @if(!empty($banks) && count($banks))
            @for($i = 0; $i < count($banks); $i++)
                <div class="col-xs-12 col-sm-2">
                    <div class="border">
                        <div class="oldName" id="oldName_{{$banks[$i]->id}}">
                            <a href="/gufAndPartnerBanks/partnerBanks/bank/{{$banks[$i]->id}}" class="linkBank" id="linkBank_{{$banks[$i]->id}}">{{$banks[$i]->name}}</a>
                            @if(Session::has('userId') && Session::get('status') == 'departament')
                                <i class="fa fa-pencil edit" id="{{$banks[$i]->id}}" title="Редагувати"></i>
                            @endif
                        </div>
                        @if(Session::has('userId') && Session::get('status') == 'departament')
                            <div class="newName" id="newName_{{$banks[$i]->id}}">
                                <input type="text" style="margin-top:10px;" value="" placeholder="Назва банку" id="newNameBank_{{$banks[$i]->id}}">
                                <i class="fa fa-check saveNewNameBank" id="{{$banks[$i]->id}}" title="Зберегти"></i>
                                <i class="fa fa-close closedEditBank" title="Відмінити"></i>
                            </div>
                        @endif
                    </div>
                </div>
            @endfor
        @endif
        @if(Session::has('userId') && Session::get('status') == 'departament')
            <div class="col-xs-12 col-sm-2 addBlock">
                <input type="hidden" name="token" id="token" value="{{csrf_token()}}">
                <div class="border">
                    <div class="iconPlus" title="Додати банк">
                        <i class="fa fa-plus fa-4x"></i>
                    </div>
                    <div class="addForm">
                        @if(!empty($users) && count($users))
                        <select name="bank" style="display:block; margin:auto;">
                            @for($i = 0; $i < count($users); $i++)
                                <option id="{{ $users[$i]->id }}">{{ $users[$i]->name }}</option>
                            @endfor
                        </select>
                        <i class="fa fa-check green addBank" name="/gufAndPartnerBanks/partnerBanks/add" id="addButton" title="Додати"></i>
                        <i class="fa fa-close closedAddBank" title="Відмінити"></i>
                        @else
                            <p>Немає зареєстрованих банків</p>
                        @endif
                    </div>
                </div>
            </div>
        @endif
    </div>
@endsection