@extends('app')

@section('content')
    <div class="row text-center">
        <div class="col-xs-12 col-sm-6">
            <a href="/gufAndPartnerBanks/guf">
                <div class="border">
                    <p>Німецько-український фонд (НУФ)</p>
                </div>
            </a>
        </div>
        <div class="col-xs-12 col-sm-6">
            <a href="/gufAndPartnerBanks/partnerBanks">
                <div class="border">
                    <p>Банки-партнери</p>
                </div>
            </a>
        </div>
    </div>
@endsection