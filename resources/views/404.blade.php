@extends('app')

@section('content')
    <div class="row text-center second-row">
        <div class="col-xs-12 col-sm-4 col-sm-offset-4">
            <div class="alert alert-info">
                <h2>404</h2>
                <p>Сторінку не знайдено</p><br/>
                <a href="/" title="На головну">Головна</a>
            </div>
        </div>
    </div>
@endsection