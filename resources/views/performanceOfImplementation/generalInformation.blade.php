@extends('app')

@section('content')
    <div class="row">
        @if(Session::get('status') == 'departament')
            <div class="col-xs-12">
                <div class="alert alert-info">
                    <div class="search form-inline">
                        <div class="form-group" style="display: block; margin-bottom: 5px;">
                            <select class="form-control" name="field-1" style="width: 200px;">
                                <option id="0" data-value="none">Поле</option>
                                <option id="col_2" data-value="text">Назва постачальника, ЄДРПОУ</option>
                                <option id="col_3" data-value="size-borrower">Розмір позичальника (мале, середнє)</option>
                                <option id="col_4" data-value="kved">КВЕД 2010</option>
                                <option id="col_5" data-value="text">№, дата кредитного договору</option>
                                <option id="col_6" data-value="text">Термін кредитного договору</option>
                                <option id="col_7" data-value="target">Цільове призначення кредиту</option>
                                <option id="col_8" data-value="text">Сума кредитну на початок звітного місяця, тис.грн</option>
                                <option id="col_9" data-value="text">Власний внесок позичальника</option>
                                <option id="col_10" data-value="text">Відсоткова ставка, %</option>
                                <option id="col_11" data-value="text">Загальна сума розрахованих відсотків у звітному місяці, %</option>
                                <option id="col_12" data-value="text">Сума ФКП за звітний місяць, грн</option>
                                <option id="col_13" data-value="text">Кількість створених позичальником нових робочих місць (попередній період)</option>
                                <option id="col_14" data-value="text">Кількість створених позичальником нових робочих місць (звітний період)</option>
                                <option id="col_15" data-value="text">Відрахування позичальником податків і зборів до державного бюджету та бюджету м.Києва (попередній період)</option>
                                <option id="col_16" data-value="text">Відрахування позичальником податків і зборів до державного бюджету та бюджету м.Києва (звітний період)</option>
                                <option id="col_17" data-value="text">Зростання обсягу виробництва та раелізації товарів, робіт та послуг (попередній період)</option>
                                <option id="col_18" data-value="text">Зростання обсягу виробництва та раелізації товарів, робіт та послуг (звітний період)</option>
                                <option id="id_bank" data-value="bank">Банк-партнер</option>
                                <option id="col_21" data-value="dateSelect">Дата надання інформації</option>
                            </select>
                        </div>
                        <div class="form-group" style="display: block; margin-bottom: 5px;">
                            <select class="form-control" name="field-2" style="width: 200px;">
                                <option id="0" data-value="none">Поле</option>
                                <option id="col_2" data-value="text">Назва постачальника, ЄДРПОУ</option>
                                <option id="col_3" data-value="size-borrower">Розмір позичальника (мале, середнє)</option>
                                <option id="col_4" data-value="kved">КВЕД 2010</option>
                                <option id="col_5" data-value="text">№, дата кредитного договору</option>
                                <option id="col_6" data-value="text">Термін кредитного договору</option>
                                <option id="col_7" data-value="target">Цільове призначення кредиту</option>
                                <option id="col_8" data-value="text">Сума кредитну на початок звітного місяця, тис.грн</option>
                                <option id="col_9" data-value="text">Власний внесок позичальника</option>
                                <option id="col_10" data-value="text">Відсоткова ставка, %</option>
                                <option id="col_11" data-value="text">Загальна сума розрахованих відсотків у звітному місяці, %</option>
                                <option id="col_12" data-value="text">Сума ФКП за звітний місяць, грн</option>
                                <option id="col_13" data-value="text">Кількість створених позичальником нових робочих місць (попередній період)</option>
                                <option id="col_14" data-value="text">Кількість створених позичальником нових робочих місць (звітний період)</option>
                                <option id="col_15" data-value="text">Відрахування позичальником податків і зборів до державного бюджету та бюджету м.Києва (попередній період)</option>
                                <option id="col_16" data-value="text">Відрахування позичальником податків і зборів до державного бюджету та бюджету м.Києва (звітний період)</option>
                                <option id="col_17" data-value="text">Зростання обсягу виробництва та раелізації товарів, робіт та послуг (попередній період)</option>
                                <option id="col_18" data-value="text">Зростання обсягу виробництва та раелізації товарів, робіт та послуг (звітний період)</option>
                                <option id="id_bank" data-value="bank">Банк-партнер</option>
                                <option id="col_21" data-value="dateSelect">Дата надання інформації</option>
                            </select>

                        </div>
                        <button class="btn btn-primary" type="button" name="search">Пошук</button>
                        <a href="/performanceOfImplementation/generalInformation" title="Очистити">Очистити</a>
                    </div>
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <td rowspan='2'>№</td>
                                    <td rowspan='2'>Назва постачальника, ЄДРПОУ</td>
                                    <td rowspan='2'>Розмір позичальника (мале, середнє)</td>
                                    <td rowspan='2'>КВЕД 2010</td>
                                    <td rowspan='2'>№, дата кредитного договору</td>
                                    <td rowspan='2'>Термін кредитного договору</td>
                                    <td rowspan='2'>Цільове призначення кредиту</td>
                                    <td rowspan='2'>Сума кредитну на початок звітного місяця, тис.грн</td>
                                    <td rowspan='2'>Власний внесок позичальника</td>
                                    <td rowspan='2'>Відсоткова ставка, %</td>
                                    <td rowspan='2'>Загальна сума розрахованих відсотків у звітному місяці, %</td>
                                    <td rowspan='2'>Сума ФКП за звітний місяць, грн</td>
                                    <td colspan='2'>Кількість створених позичальником нових робочих місць</td>
                                    <td colspan='2'>Відрахування позичальником податків і зборів до державного бюджету та бюджету м.Києва</td>
                                    <td colspan='2'>Зростання обсягу виробництва та раелізації товарів, робіт та послуг</td>
                                    <td rowspan='2'>Що зроблено позичальником по реалізації кредитного проекту</td>
                                    <td rowspan='2'>Банк-партнер</td>
                                    <td rowspan='2'>Дата надання інформації</td>
                                </tr>
                                <tr>
                                    <td>попередній період</td>
                                    <td>звітний період</td>
                                    <td>попередній період</td>
                                    <td>звітний період</td>
                                    <td>попередній період</td>
                                    <td>звітний період</td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>2</td>
                                    <td>3</td>
                                    <td>4</td>
                                    <td>5</td>
                                    <td>6</td>
                                    <td>7</td>
                                    <td>8</td>
                                    <td>9</td>
                                    <td>10</td>
                                    <td>11</td>
                                    <td>12</td>
                                    <td>13</td>
                                    <td>14</td>
                                    <td>15</td>
                                    <td>16</td>
                                    <td>17</td>
                                    <td>18</td>
                                    <td>19</td>
                                    <td>20</td>
                                    <td>21</td>
                                </tr>
                            </thead>
                            <tbody>
                                @if(isset($performance) && count($performance))
                                    @for($i = 0; $i < count($performance);$i++)
                                        <tr>
                                            <td id="1">{{ $i+1 }}</td>
                                            <td id="2">{{ $performance[$i]->col_2 }}</td>
                                            <td id="3">{{ $performance[$i]->size_borrower }}</td>
                                            <td id="4">{{ $performance[$i]->col_4_section }} / {{ $performance[$i]->col_4_group }} / {{ $performance[$i]->col_4_clas }}</td>
                                            <td id="5">{{ $performance[$i]->col_5 }}</td>
                                            <td id="6">{{ $performance[$i]->col_6 }}</td>
                                            <td id="7">{{ $performance[$i]->col_7 }}</td>
                                            <td id="8">{{ $performance[$i]->col_8/1000 }}</td>
                                            <td id="9">{{ $performance[$i]->col_9 }}</td>
                                            <td id="10">{{ $performance[$i]->col_10 }}</td>
                                            <td id="11">{{ $performance[$i]->col_11 }}</td>
                                            <td id="12">{{ $performance[$i]->col_12 }}</td>
                                            <td id="13">{{ $performance[$i]->col_13 }}</td>
                                            <td id="14">{{ $performance[$i]->col_14 }}</td>
                                            <td id="15">{{ $performance[$i]->col_15 }}</td>
                                            <td id="16">{{ $performance[$i]->col_16 }}</td>
                                            <td id="17">{{ $performance[$i]->col_17 }}</td>
                                            <td id="18">{{ $performance[$i]->col_18 }}</td>
                                            <td id="19">{{ $performance[$i]->col_19 }}</td>
                                            <td id="20">{{ $performance[$i]->name }}</td>
                                            <td id="21">{{ \Carbon\Carbon::parse($performance[$i]->col_20)->format('d.m.Y') }}</td>
                                        </tr>
                                    @endfor
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>{{ $sum_8/1000 }}</td>
                                        <td>{{ $sum_9 }}</td>
                                        <td></td>
                                        <td>{{ $sum_11 }}</td>
                                        <td>{{ $sum_12 }}</td>
                                        <td>{{ $sum_13 }}</td>
                                        <td>{{ $sum_14 }}</td>
                                        <td>{{ $sum_15 }}</td>
                                        <td>{{ $sum_16 }}</td>
                                        <td>{{ $sum_17 }}</td>
                                        <td>{{ $sum_18 }}</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                @else
                                    <tr><td colspan="25">Інформації немає</td></tr>
                                @endif
                                <input type="hidden" name="token" value="{{ csrf_token() }}">
                                <input type="hidden" name="url" value="performanceOfImplementation">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        @else
            <div class="col-xs-12 col-sm-6 col-sm-offset-3">
                <div class="alert alert-danger">
                    <p>
                        Ви не можете переглядати дану інформацію! Повернутися на <a href="/" title="Головна">головну</a>
                    </p>
                </div>
            </div>
        @endif
    </div>

    @if(Session::get('status') == 'departament')
        <div class="row">
            <div class="col-xs-12">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#charts" data-toggle="tab">Графіки</a></li>
                    <li ><a href="#reports" data-toggle="tab">Звіти</a></li>
                </ul>
            </div>
            <div class="col-xs-12">
                <div class="tab-content">
                    <div id="reports" class="tab-pane fade">
                        @if(isset($reports) && count($reports))
                            <ol>
                                @for($i = 0; $i < count($reports); $i++)
                                    <li>{{ $reports[$i]->name }}
                                        <span style="float:right;">
                                            <a href="../download/reports/{{$reports[$i]->url}}" class="btn btn-success btn-xs"><i class="fa fa-download"></i>&nbsp;Excel</a>
                                        </span>
                                    </li>
                                @endfor
                            </ol>
                        @else
                            <div class="col-xs-12 col-sm-6 col-sm-offset-3">
                                <div class="alert alert-info text-center">
                                    <p>Звітів немає</p>
                                </div>
                            </div>
                        @endif
                    </div>
                    <div id="charts" class="tab-pane fade in active">
                        @if(isset($performance) && count($performance))
                            <script src="/js/charts/highcharts.js?"<?php echo time()?>></script>
                            <script src="/js/charts/modules/series-label.js?"<?php echo time()?>></script>
                            <script src="/js/charts/modules/exporting.js?"<?php echo time()?>></script>

                            <div class="row">
                                <div class="col-xs-12 col-sm-6" style="margin-top: 5px;" id="chart-1"></div>
                                <div class="col-xs-12 col-sm-6" style="margin-top: 5px;" id="chart-2"></div>
                                <div class="col-xs-12 col-sm-6" style="margin-top: 5px;" id="chart-3"></div>
                                <div class="col-xs-12 col-sm-6" style="margin-top: 5px;" id="chart-4"></div>
                                <div class="col-xs-12 col-sm-6" style="margin-top: 5px;" id="chart-5"></div>
                                <div class="col-xs-12 col-sm-6" style="margin-top: 5px;" id="chart-6"></div>
                                <div class="col-xs-12 col-sm-6" style="margin-top: 5px;" id="chart-7"></div>
                                <div class="col-xs-12 col-sm-6" style="margin-top: 5px;" id="chart-8"></div>
                                <div class="col-xs-12 col-sm-6" style="margin-top: 5px;" id="chart-9"></div>
                                <div class="col-xs-12 col-sm-6" style="margin-top: 5px;" id="chart-10"></div>
                                <div class="col-xs-12 col-sm-6" style="margin-top: 5px;" id="chart-11"></div>
                                <div class="col-xs-12 col-sm-6" style="margin-top: 5px;" id="chart-12"></div>
                                <div class="col-xs-12 col-sm-6" style="margin-top: 5px;" id="chart-13"></div>
                            </div>

                            <script type="text/javascript">
                                //var datasChart_1 = [];
                                var categoriesData_1_3_5 = [];

                                var seriesData_1 = [];
                                var sumNewPlacePrev_1 = [];
                                var sumNewPlacePres_1 = [];
                                var sumNewPlacePrevData_1 = [];
                                var sumNewPlacePresData_1 = [];
                                sumNewPlacePrev_1['name'] = "Попередній період";
                                sumNewPlacePres_1['name'] = "Звітний період";

                                var seriesData_3 = [];
                                var sumTaxesPrev_3 = [];
                                var sumTaxesPres_3 = [];
                                var sumTaxesPrevData_3 = [];
                                var sumTaxesPresData_3 = [];
                                sumTaxesPrev_3['name'] = "Попередній період";
                                sumTaxesPres_3['name'] = "Звітний період";

                                var seriesData_5 = [];
                                var sumGoodsPrev_5 = [];
                                var sumGoodsPres_5 = [];
                                var sumGoodsPrevData_5 = [];
                                var sumGoodsPresData_5 = [];
                                sumGoodsPrev_5['name'] = "Попередній період";
                                sumGoodsPres_5['name'] = "Звітний період";

                                $.get('/performance/chart_1_3_5').done(function(resp){
                                    if(resp.length > 0){
                                        for (var i = 0; i < resp.length; i++) {
                                            if(String(resp[i]['month']).length == 1){
                                                resp[i]['month'] = '0'+resp[i]['month'];
                                            }
                                            
                                            categoriesData_1_3_5.push(resp[i]['month']+'.'+resp[i]['year']);
                                            
                                            sumNewPlacePrevData_1.push(parseInt(resp[i]['sumNewPlacePrev']));
                                            sumNewPlacePresData_1.push(parseInt(resp[i]['sumNewPlacePres']));
                                            
                                            sumTaxesPrevData_3.push(parseInt(resp[i]['sumTaxesPrev']));
                                            sumTaxesPresData_3.push(parseInt(resp[i]['sumTaxesPres']));
                                            
                                            sumGoodsPrevData_5.push(parseInt(resp[i]['sumGoodsPrev']));
                                            sumGoodsPresData_5.push(parseInt(resp[i]['sumGoodsPres']));
                                        };
                                        sumNewPlacePrev_1['data'] = sumNewPlacePrevData_1;
                                        sumNewPlacePres_1['data'] = sumNewPlacePresData_1;

                                        sumTaxesPrev_3['data'] = sumTaxesPrevData_3;
                                        sumTaxesPres_3['data'] = sumTaxesPresData_3;

                                        sumGoodsPrev_5['data'] = sumGoodsPrevData_5;
                                        sumGoodsPres_5['data'] = sumGoodsPresData_5;

                                        seriesData_1.push(sumNewPlacePrev_1, sumNewPlacePres_1);
                                        seriesData_3.push(sumTaxesPrev_3, sumTaxesPres_3);
                                        seriesData_5.push(sumGoodsPrev_5, sumGoodsPres_5);
                                    }
                                    
                                    Highcharts.chart('chart-1', { //1 Загальна кількість створених нових робочих місць
                                        chart: {
                                            type: 'column'
                                        },
                                        title: {
                                            text: 'Загальна кількість створених нових робочих місць'
                                        },
                                        xAxis: {
                                            categories: categoriesData_1_3_5,
                                            crosshair: true
                                        },
                                        yAxis: {
                                            min: 0,
                                            title: {
                                                text: 'Кількість'
                                            }
                                        },
                                        tooltip: {
                                            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                                            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                                                '<td style="padding:0"><b>{point.y}</b></td></tr>',
                                            footerFormat: '</table>',
                                            shared: true,
                                            useHTML: true
                                        },
                                        plotOptions: {
                                            column: {
                                                pointPadding: 0.2,
                                                borderWidth: 0
                                            }
                                        },
                                        series: seriesData_1
                                    });
                                    
                                    Highcharts.chart('chart-3', { //3 Загальна кількість відрахування позичальником податків і зборів до державного бюджету та бюджету м.Києва
                                        chart: {
                                            type: 'column'
                                        },
                                        title: {
                                            text: 'Загальна кількість відрахування позичальником податків і зборів до державного бюджету та бюджету м.Києва'
                                        },
                                        xAxis: {
                                            categories: categoriesData_1_3_5,
                                            crosshair: true
                                        },
                                        yAxis: {
                                            min: 0,
                                            title: {
                                                text: 'Кількість'
                                            }
                                        },
                                        tooltip: {
                                            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                                            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                                                '<td style="padding:0"><b>{point.y}</b></td></tr>',
                                            footerFormat: '</table>',
                                            shared: true,
                                            useHTML: true
                                        },
                                        plotOptions: {
                                            column: {
                                                pointPadding: 0.2,
                                                borderWidth: 0
                                            }
                                        },
                                        series: seriesData_3
                                    });
                                    
                                    Highcharts.chart('chart-5', { //5 Загальна кількість обсягів виробництва та раелізації товарів, робіт та послуг
                                        chart: {
                                            type: 'column'
                                        },
                                        title: {
                                            text: 'Загальна кількість обсягів виробництва та раелізації товарів, робіт та послуг'
                                        },
                                        xAxis: {
                                            categories: categoriesData_1_3_5,
                                            crosshair: true
                                        },
                                        yAxis: {
                                            min: 0,
                                            title: {
                                                text: 'Кількість'
                                            }
                                        },
                                        tooltip: {
                                            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                                            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                                                '<td style="padding:0"><b>{point.y}</b></td></tr>',
                                            footerFormat: '</table>',
                                            shared: true,
                                            useHTML: true
                                        },
                                        plotOptions: {
                                            column: {
                                                pointPadding: 0.2,
                                                borderWidth: 0
                                            }
                                        },
                                        series: seriesData_5
                                    });
                                });
                            </script>

                            <script type="text/javascript">
                                var categoriesData_2_4_6 = [];
                                
                                var seriesData_2 = [];
                                var sumNewPlacePrev_2 = [];
                                var sumNewPlacePres_2 = [];
                                var sumNewPlacePrevData_2 = [];
                                var sumNewPlacePresData_2 = [];
                                sumNewPlacePrev_2['name'] = "Попередній період";
                                sumNewPlacePres_2['name'] = "Звітний період";
                                
                                var seriesData_4 = [];
                                var sumTaxesPrev_4 = [];
                                var sumTaxesPres_4 = [];
                                var sumTaxesPrevData_4 = [];
                                var sumTaxesPresData_4 = [];
                                sumTaxesPrev_4['name'] = "Попередній період";
                                sumTaxesPres_4['name'] = "Звітний період";

                                var seriesData_6 = [];
                                var sumGoodsPrev_6 = [];
                                var sumGoodsPres_6 = [];
                                var sumGoodsPrevData_6 = [];
                                var sumGoodsPresData_6 = [];
                                sumGoodsPrev_6['name'] = "Попередній період";
                                sumGoodsPres_6['name'] = "Звітний період";

                                $.get('/performance/chart_2_4_6').done(function(resp){
                                    if(resp.length > 0){
                                        for (var i = 0; i < resp.length; i++) {

                                            categoriesData_2_4_6.push('<b>розділ</b> '+resp[i]['kved']);

                                            sumNewPlacePrevData_2.push(parseInt(resp[i]['sumNewPlacePrev']));
                                            sumNewPlacePresData_2.push(parseInt(resp[i]['sumNewPlacePres']));

                                            sumTaxesPrevData_4.push(parseInt(resp[i]['sumTaxesPrev']));
                                            sumTaxesPresData_4.push(parseInt(resp[i]['sumTaxesPres']));

                                            sumGoodsPrevData_6.push(parseInt(resp[i]['sumGoodsPrev']));
                                            sumGoodsPresData_6.push(parseInt(resp[i]['sumGoodsPres']));
                                        };

                                        sumNewPlacePrev_2['data'] = sumNewPlacePrevData_2;
                                        sumNewPlacePres_2['data'] = sumNewPlacePresData_2;

                                        sumTaxesPrev_4['data'] = sumTaxesPrevData_4;
                                        sumTaxesPres_4['data'] = sumTaxesPresData_4;

                                        sumGoodsPrev_6['data'] = sumGoodsPrevData_6;
                                        sumGoodsPres_6['data'] = sumGoodsPresData_6;

                                        seriesData_2.push(sumNewPlacePrev_2, sumNewPlacePres_2);
                                        seriesData_4.push(sumTaxesPrev_4, sumTaxesPres_4);
                                        seriesData_6.push(sumGoodsPrev_6, sumGoodsPres_6);
                                    }
                                    
                                    Highcharts.chart('chart-2', { //2 Кількість створених нових робочих місць на підприємствах за КВЕД
                                        chart: {
                                            type: 'column'
                                        },
                                        title: {
                                            text: 'Кількість створених нових робочих місць на підприємствах за КВЕД'
                                        },
                                        xAxis: {
                                            categories: categoriesData_2_4_6,
                                            crosshair: true
                                        },
                                        yAxis: {
                                            min: 0,
                                            title: {
                                                text: 'Кількість'
                                            }
                                        },
                                        tooltip: {
                                            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                                            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                                                '<td style="padding:0"><b>{point.y}</b></td></tr>',
                                            footerFormat: '</table>',
                                            shared: true,
                                            useHTML: true
                                        },
                                        plotOptions: {
                                            column: {
                                                pointPadding: 0.2,
                                                borderWidth: 0
                                            }
                                        },
                                        series: seriesData_2
                                    });
                                    
                                    Highcharts.chart('chart-4', { //4 Кількість відрахування позичальником податків і зборів до державного бюджету та бюджету м.Києва за КВЕД
                                        chart: {
                                            type: 'column'
                                        },
                                        title: {
                                            text: 'Кількість відрахування позичальником податків і зборів до державного бюджету та бюджету м.Києва за КВЕД'
                                        },
                                        xAxis: {
                                            categories: categoriesData_2_4_6,
                                            crosshair: true
                                        },
                                        yAxis: {
                                            min: 0,
                                            title: {
                                                text: 'Кількість'
                                            }
                                        },
                                        tooltip: {
                                            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                                            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                                                '<td style="padding:0"><b>{point.y}</b></td></tr>',
                                            footerFormat: '</table>',
                                            shared: true,
                                            useHTML: true
                                        },
                                        plotOptions: {
                                            column: {
                                                pointPadding: 0.2,
                                                borderWidth: 0
                                            }
                                        },
                                        series: seriesData_4
                                    });
                                    
                                    Highcharts.chart('chart-6', { //6 Загальна кількість обсягів виробництва та раелізації товарів, робіт та послуг за КВЕД
                                        chart: {
                                            type: 'column'
                                        },
                                        title: {
                                            text: 'Загальна кількість обсягів виробництва та раелізації товарів, робіт та послуг за КВЕД'
                                        },
                                        xAxis: {
                                            categories: categoriesData_2_4_6,
                                            crosshair: true
                                        },
                                        yAxis: {
                                            min: 0,
                                            title: {
                                                text: 'Кількість'
                                            }
                                        },
                                        tooltip: {
                                            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                                            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                                                '<td style="padding:0"><b>{point.y}</b></td></tr>',
                                            footerFormat: '</table>',
                                            shared: true,
                                            useHTML: true
                                        },
                                        plotOptions: {
                                            column: {
                                                pointPadding: 0.2,
                                                borderWidth: 0
                                            }
                                        },
                                        series: seriesData_6
                                    });
                                });
                            </script>

                            <script type="text/javascript">
                                var categoriesData_7_8_9 = [];
                                
                                var seriesData_7 = [];
                                var sumNewPlacePrev_7 = [];
                                var sumNewPlacePres_7 = [];
                                var sumNewPlacePrevData_7 = [];
                                var sumNewPlacePresData_7 = [];
                                sumNewPlacePrev_7['name'] = "Попередній період";
                                sumNewPlacePres_7['name'] = "Звітний період";
                                
                                var seriesData_8 = [];
                                var sumTaxesPrev_8 = [];
                                var sumTaxesPres_8 = [];
                                var sumTaxesPrevData_8 = [];
                                var sumTaxesPresData_8 = [];
                                sumTaxesPrev_8['name'] = "Попередній період";
                                sumTaxesPres_8['name'] = "Звітний період";

                                var seriesData_9 = [];
                                var sumGoodsPrev_9 = [];
                                var sumGoodsPres_9 = [];
                                var sumGoodsPrevData_9 = [];
                                var sumGoodsPresData_9 = [];
                                sumGoodsPrev_9['name'] = "Попередній період";
                                sumGoodsPres_9['name'] = "Звітний період";

                                $.get('/performance/chart_7_8_9').done(function(resp){
                                    if(resp.length > 0){
                                        for (var i = 0; i < resp.length; i++) {

                                            categoriesData_7_8_9.push(resp[i]['size_borrower']);

                                            sumNewPlacePrevData_7.push(parseInt(resp[i]['sumNewPlacePrev']));
                                            sumNewPlacePresData_7.push(parseInt(resp[i]['sumNewPlacePres']));

                                            sumTaxesPrevData_8.push(parseInt(resp[i]['sumTaxesPrev']));
                                            sumTaxesPresData_8.push(parseInt(resp[i]['sumTaxesPres']));

                                            sumGoodsPrevData_9.push(parseInt(resp[i]['sumGoodsPrev']));
                                            sumGoodsPresData_9.push(parseInt(resp[i]['sumGoodsPres']));
                                        };

                                        sumNewPlacePrev_7['data'] = sumNewPlacePrevData_7;
                                        sumNewPlacePres_7['data'] = sumNewPlacePresData_7;

                                        sumTaxesPrev_8['data'] = sumTaxesPrevData_8;
                                        sumTaxesPres_8['data'] = sumTaxesPresData_8;

                                        sumGoodsPrev_9['data'] = sumGoodsPrevData_9;
                                        sumGoodsPres_9['data'] = sumGoodsPresData_9;

                                        seriesData_7.push(sumNewPlacePrev_7, sumNewPlacePres_7);
                                        seriesData_8.push(sumTaxesPrev_8, sumTaxesPres_8);
                                        seriesData_9.push(sumGoodsPrev_9, sumGoodsPres_9);
                                    }
                                    
                                    Highcharts.chart('chart-7', { //7 Кількість створених нових робочих місць малими та середніми підприємствами
                                        chart: {
                                            type: 'column'
                                        },
                                        title: {
                                            text: 'Кількість створених нових робочих місць малими та середніми підприємствами'
                                        },
                                        xAxis: {
                                            categories: categoriesData_7_8_9,
                                            crosshair: true
                                        },
                                        yAxis: {
                                            min: 0,
                                            title: {
                                                text: 'Кількість'
                                            }
                                        },
                                        tooltip: {
                                            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                                            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                                                '<td style="padding:0"><b>{point.y}</b></td></tr>',
                                            footerFormat: '</table>',
                                            shared: true,
                                            useHTML: true
                                        },
                                        plotOptions: {
                                            column: {
                                                pointPadding: 0.2,
                                                borderWidth: 0
                                            }
                                        },
                                        series: seriesData_7
                                    });
                                    
                                    Highcharts.chart('chart-8', { //8 Кількість відрахування позичальником податків і зборів до державного бюджету та бюджету м.Києва малими та середніми підприємствами
                                        chart: {
                                            type: 'column'
                                        },
                                        title: {
                                            text: 'Кількість відрахування позичальником податків і зборів до державного бюджету та бюджету м.Києва малими та середніми підприємствами'
                                        },
                                        xAxis: {
                                            categories: categoriesData_7_8_9,
                                            crosshair: true
                                        },
                                        yAxis: {
                                            min: 0,
                                            title: {
                                                text: 'Кількість'
                                            }
                                        },
                                        tooltip: {
                                            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                                            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                                                '<td style="padding:0"><b>{point.y}</b></td></tr>',
                                            footerFormat: '</table>',
                                            shared: true,
                                            useHTML: true
                                        },
                                        plotOptions: {
                                            column: {
                                                pointPadding: 0.2,
                                                borderWidth: 0
                                            }
                                        },
                                        series: seriesData_8
                                    });
                                    
                                    Highcharts.chart('chart-9', { //9 Кількість обсягів виробництва та раелізації товарів, робіт та послуг малими та середніми підприємствами
                                        chart: {
                                            type: 'column'
                                        },
                                        title: {
                                            text: 'Кількість обсягів виробництва та раелізації товарів, робіт та послуг малими та середніми підприємствами'
                                        },
                                        xAxis: {
                                            categories: categoriesData_7_8_9,
                                            crosshair: true
                                        },
                                        yAxis: {
                                            min: 0,
                                            title: {
                                                text: 'Кількість'
                                            }
                                        },
                                        tooltip: {
                                            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                                            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                                                '<td style="padding:0"><b>{point.y}</b></td></tr>',
                                            footerFormat: '</table>',
                                            shared: true,
                                            useHTML: true
                                        },
                                        plotOptions: {
                                            column: {
                                                pointPadding: 0.2,
                                                borderWidth: 0
                                            }
                                        },
                                        series: seriesData_9
                                    });
                                });
                            </script>

                            <script type="text/javascript">
                                var categoriesData_10_11_12 = [];
                                
                                var seriesData_10 = [];
                                var sumNewPlacePrev_10 = [];
                                var sumNewPlacePres_10 = [];
                                var sumNewPlacePrevData_10 = [];
                                var sumNewPlacePresData_10 = [];
                                sumNewPlacePrev_10['name'] = "Попередній період";
                                sumNewPlacePres_10['name'] = "Звітний період";
                                
                                var seriesData_11 = [];
                                var sumTaxesPrev_11 = [];
                                var sumTaxesPres_11 = [];
                                var sumTaxesPrevData_11 = [];
                                var sumTaxesPresData_11 = [];
                                sumTaxesPrev_11['name'] = "Попередній період";
                                sumTaxesPres_11['name'] = "Звітний період";

                                var seriesData_12 = [];
                                var sumGoodsPrev_12 = [];
                                var sumGoodsPres_12 = [];
                                var sumGoodsPrevData_12 = [];
                                var sumGoodsPresData_12 = [];
                                sumGoodsPrev_12['name'] = "Попередній період";
                                sumGoodsPres_12['name'] = "Звітний період";

                                $.get('/performance/chart_10_11_12').done(function(resp){
                                    if(resp.length > 0){
                                        for (var i = 0; i < resp.length; i++) {

                                            categoriesData_10_11_12.push(resp[i]['name']);

                                            sumNewPlacePrevData_10.push(parseInt(resp[i]['sumNewPlacePrev']));
                                            sumNewPlacePresData_10.push(parseInt(resp[i]['sumNewPlacePres']));

                                            sumTaxesPrevData_11.push(parseInt(resp[i]['sumTaxesPrev']));
                                            sumTaxesPresData_11.push(parseInt(resp[i]['sumTaxesPres']));

                                            sumGoodsPrevData_12.push(parseInt(resp[i]['sumGoodsPrev']));
                                            sumGoodsPresData_12.push(parseInt(resp[i]['sumGoodsPres']));
                                        };

                                        sumNewPlacePrev_10['data'] = sumNewPlacePrevData_10;
                                        sumNewPlacePres_10['data'] = sumNewPlacePresData_10;

                                        sumTaxesPrev_11['data'] = sumTaxesPrevData_11;
                                        sumTaxesPres_11['data'] = sumTaxesPresData_11;

                                        sumGoodsPrev_12['data'] = sumGoodsPrevData_12;
                                        sumGoodsPres_12['data'] = sumGoodsPresData_12;

                                        seriesData_10.push(sumNewPlacePrev_10, sumNewPlacePres_10);
                                        seriesData_11.push(sumTaxesPrev_11, sumTaxesPres_11);
                                        seriesData_12.push(sumGoodsPrev_12, sumGoodsPres_12);
                                    }
                                    
                                    Highcharts.chart('chart-10', { //10 Кількість створених нових робочих місць на підприємствах по банкам
                                        chart: {
                                            type: 'column'
                                        },
                                        title: {
                                            text: 'Кількість створених нових робочих місць на підприємствах по банкам'
                                        },
                                        xAxis: {
                                            categories: categoriesData_10_11_12,
                                            crosshair: true
                                        },
                                        yAxis: {
                                            min: 0,
                                            title: {
                                                text: 'Кількість'
                                            }
                                        },
                                        tooltip: {
                                            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                                            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                                                '<td style="padding:0"><b>{point.y}</b></td></tr>',
                                            footerFormat: '</table>',
                                            shared: true,
                                            useHTML: true
                                        },
                                        plotOptions: {
                                            column: {
                                                pointPadding: 0.2,
                                                borderWidth: 0
                                            }
                                        },
                                        series: seriesData_10
                                    });
                                    
                                    Highcharts.chart('chart-11', { //11 Кількість відрахування позичальником податків і зборів до державного бюджету та бюджету м.Києва по банкам
                                        chart: {
                                            type: 'column'
                                        },
                                        title: {
                                            text: 'Кількість відрахування позичальником податків і зборів до державного бюджету та бюджету м.Києва по банкам'
                                        },
                                        xAxis: {
                                            categories: categoriesData_10_11_12,
                                            crosshair: true
                                        },
                                        yAxis: {
                                            min: 0,
                                            title: {
                                                text: 'Кількість'
                                            }
                                        },
                                        tooltip: {
                                            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                                            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                                                '<td style="padding:0"><b>{point.y}</b></td></tr>',
                                            footerFormat: '</table>',
                                            shared: true,
                                            useHTML: true
                                        },
                                        plotOptions: {
                                            column: {
                                                pointPadding: 0.2,
                                                borderWidth: 0
                                            }
                                        },
                                        series: seriesData_11
                                    });
                                    
                                    Highcharts.chart('chart-12', { //12 Кількість обсягів виробництва та раелізації товарів, робіт та послуг по банкам
                                        chart: {
                                            type: 'column'
                                        },
                                        title: {
                                            text: 'Кількість обсягів виробництва та раелізації товарів, робіт та послуг по банкам'
                                        },
                                        xAxis: {
                                            categories: categoriesData_10_11_12,
                                            crosshair: true
                                        },
                                        yAxis: {
                                            min: 0,
                                            title: {
                                                text: 'Кількість'
                                            }
                                        },
                                        tooltip: {
                                            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                                            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                                                '<td style="padding:0"><b>{point.y}</b></td></tr>',
                                            footerFormat: '</table>',
                                            shared: true,
                                            useHTML: true
                                        },
                                        plotOptions: {
                                            column: {
                                                pointPadding: 0.2,
                                                borderWidth: 0
                                            }
                                        },
                                        series: seriesData_12
                                    });
                                });
                            </script>
                        @else
                            <div class="col-xs-12 col-sm-6 col-sm-offset-3">
                                <div class="alert alert-info text-center">
                                    <p>Для побудови графіків, даних немає</p>
                                </div>
                            </div>
                        @endif

                    </div>
                </div>
            </div>
        </div>
    @endif
@endsection