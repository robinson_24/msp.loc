@extends('app')

@section('content')
    <div class="row">
        @if(Session::get('userId') == $idBank || Session::get('status') == 'departament')
            <div class="col-xs-12">
                <div class="alert alert-info">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <td rowspan='2'>№</td>
                                    <td rowspan='2' id="td-2" data-type="text">Назва постачальника, ЄДРПОУ</td>
                                    <td rowspan='2' id="td-3" data-type="size-borrower">Розмір позичальника (мале, середнє)</td>
                                    <td rowspan='2' id="td-4" data-type="kved">КВЕД 2010</td>
                                    <td rowspan='2' id="td-5" data-type="text">№, дата кредитного договору</td>
                                    <td rowspan='2' id="td-6" data-type="text">Термін кредитного договору</td>
                                    <td rowspan='2' id="td-7" data-type="target">Цільове призначення кредиту</td>
                                    <td rowspan='2' id="td-8" data-type="numero-thousand">Сума кредитну на початок звітного місяця, тис.грн</td>
                                    <td rowspan='2' id="td-9" data-type="numero">Власний внесок позичальника</td>
                                    <td rowspan='2' id="td-10" data-type="numero">Відсоткова ставка, %</td>
                                    <td rowspan='2' id="td-11" data-type="numero">Загальна сума розрахованих відсотків у звітному місяці, %</td>
                                    <td rowspan='2' id="td-12" data-type="numero">Сума ФКП за звітний місяць, грн</td>
                                    <td colspan='2'>Кількість створених позичальником нових робочих місць</td>
                                    <td colspan='2'>Відрахування позичальником податків і зборів до державного бюджету та бюджету м.Києва</td>
                                    <td colspan='2'>Зростання обсягу виробництва та раелізації товарів, робіт та послуг</td>
                                    <td rowspan='2' id="td-19" data-type="text">Що зроблено позичальником по реалізації кредитного проекту</td>
                                    <td rowspan='2' id="td-20" data-type="date">Дата надання інформації</td>
                                </tr>
                                <tr>
                                    <td id="td-13" data-type="numero">попередній період</td>
                                    <td id="td-14" data-type="numero">звітний період</td>
                                    <td id="td-15" data-type="numero">попередній період</td>
                                    <td id="td-16" data-type="numero">звітний період</td>
                                    <td id="td-17" data-type="numero">попередній період</td>
                                    <td id="td-18" data-type="numero">звітний період</td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>2</td>
                                    <td>3</td>
                                    <td>4</td>
                                    <td>5</td>
                                    <td>6</td>
                                    <td>7</td>
                                    <td>8</td>
                                    <td>9</td>
                                    <td>10</td>
                                    <td>11</td>
                                    <td>12</td>
                                    <td>13</td>
                                    <td>14</td>
                                    <td>15</td>
                                    <td>16</td>
                                    <td>17</td>
                                    <td>18</td>
                                    <td>19</td>
                                    <td class="lastNumeroRow">20</td>
                                </tr>
                            </thead>
                            <tbody  class="{{ (Session::get('status') != 'departament') ? 'register' : '' }}">
                                @if(isset($performance) && count($performance))
                                    @for($i = 0; $i < count($performance);$i++)
                                        <tr id="{{ $i+1 }}" data-id="{{ $performance[$i]->id }}">
                                            <td id="action">{{ $i+1 }}
                                                @if(Session::get('userId') == $idBank)
                                                <i class="fa fa-times deleteRow" title="Відалити"></i>
                                                @endif
                                            </td>
                                            <td id="2">{{ $performance[$i]->col_2 }}</td>
                                            <td id="3">{{ $performance[$i]->size_borrower }}</td>
                                            <td id="4">{{ $performance[$i]->col_4_section }} / {{ $performance[$i]->col_4_group }} / {{ $performance[$i]->col_4_clas }}</td>
                                            <td id="5">{{ $performance[$i]->col_5 }}</td>
                                            <td id="6">{{ $performance[$i]->col_6 }}</td>
                                            <td id="7">{{ $performance[$i]->col_7 }}</td>
                                            <td id="8">{{ $performance[$i]->col_8 }}</td>
                                            <td id="9">{{ $performance[$i]->col_9 }}</td>
                                            <td id="10">{{ $performance[$i]->col_10 }}</td>
                                            <td id="11">{{ $performance[$i]->col_11 }}</td>
                                            <td id="12">{{ $performance[$i]->col_12 }}</td>
                                            <td id="13">{{ $performance[$i]->col_13 }}</td>
                                            <td id="14">{{ $performance[$i]->col_14 }}</td>
                                            <td id="15">{{ $performance[$i]->col_15 }}</td>
                                            <td id="16">{{ $performance[$i]->col_16 }}</td>
                                            <td id="17">{{ $performance[$i]->col_17 }}</td>
                                            <td id="18">{{ $performance[$i]->col_18 }}</td>
                                            <td id="19">{{ $performance[$i]->col_19 }}</td>
                                            <td id="20">{{ \Carbon\Carbon::parse($performance[$i]->col_20)->format('d.m.Y') }}</td>
                                        </tr>
                                    @endfor
                                @endif
                                @if(Session::get('status') == 'departament')
                                    @if(!isset($performance) || count($performance) == 0)
                                        <tr><td colspan="24">Немає інформації</td></tr>
                                    @endif
                                @endif
                                @if(Session::get('userId') == $idBank)
                                    <tr class="lastRow">
                                        <td><i class="fa fa-plus green addRow" title="Додати"></i></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <input type="hidden" name="urlType" data-url="/performanceOfImplementation/"/>
                                    <input type="hidden" name="token" value="{{ csrf_token() }}" />
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        @else
            <div class="col-xs-12 col-sm-6 col-sm-offset-3">
                <div class="alert alert-danger">
                    <p>
                        Ви не можете переглядати іформацію даного банку! Повернутися на <a href="/" title="Головна">головну</a>
                    </p>
                </div>
            </div>
        @endif
    </div>
    @if(Session::get('userId') == $idBank)
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-sm-offset-3">
                <div class="alert alert-info">
                    <strong>Увага!</strong> Для того, щоб додати та зберегти дані натисніть клавішу "Enter"
                </div>
            </div>
        </div>
    @endif
@endsection