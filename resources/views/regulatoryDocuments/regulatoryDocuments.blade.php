@extends('app')

@section('content')
        <div class="row text-center">
            <div class="col-xs-12 col-sm-3">
                <a href="regulatoryDocuments/laws">
                    <div class="border pointer">
                        <p>Закони України</p>
                    </div>
                </a>
            </div>
            <div class="col-xs-12 col-sm-3">
                <a href="regulatoryDocuments/resolutions">
                    <div class="border pointer">
                        <p>Постанови КМУ</p>
                    </div>
                </a>
            </div>
            <div class="col-xs-12 col-sm-3">
                <a href="regulatoryDocuments/orders">
                    <div class="border pointer">
                        <p>Розпорядження, рішення, накази КМДА</p>
                    </div>
                </a>
            </div>
            <div class="col-xs-12 col-sm-3">
                <a href="regulatoryDocuments/etcs">
                    <div class="border pointer">
                        <p>тощо</p>
                    </div>
                </a>
            </div>
        </div>
@endsection