@extends('app')

@section('content')
    <div class="row">
        @if(Session::get('status') == 'departament')
            <div class="col-xs-12">
                <div class="alert alert-info">
                    <div class="search form-inline">
                        <div class="form-group" style="display: block; margin-bottom: 5px;">
                            <select class="form-control" name="field-1" style="width: 200px;">
                                <option id="0" data-value="none">Поле</option>
                                <option id="col_2" data-value="text" >Назва, ЄДРПОУ</option>
                                <option id="col_4" data-value="kved" >КВЕД 2010 (критерій 2)</option>
                                <option id="col_5" data-value="target" >Цільове призначення (критерій 3)</option>
                                <option id="col_6" data-value="text" >Власний внесок позичальника (критерій 4)</option>
                                <option id="col_7" data-value="date" >Дата погодження надання ФКП</option>
                                <option id="col_9" data-value="term" >Строк</option>
                                <option id="col_10" data-value="text" >Сума кредиту на початок звітного місяця, тис.грн</option>
                                <option id="col_11" data-value="text" >Відсоткова ставка, %</option>
                                <option id="col_12" data-value="text" >Загальна сума розрахованих відсотків у звітному місяці, %</option>
                                <option id="col_14" data-value="text" >Сума ФКП за звітний місяць, грн</option>
                                <option id="col_15" data-value="text" >Загальний розмір ФКП за весь період, грн</option>
                                <option id="col_16" data-value="text" >Плановий розмір ФКП в місяці, наступному за звітним періодом, грн</option>
                                <option id="col_17" data-value="text" >Залишковий розмір ФКП, тис.грн</option>
                                <option id="col_18" data-value="text" >Порядковий номер місця участі в ФКП (загальний термін 24 місяці)</option>
                                <option id="id_bank" data-value="bank" >Банк-партнер</option>
                                <option id="col_19" data-value="dateSelect" >Дата заповнення реєстру</option>
                            </select>
                        </div>
                        <div class="form-group" style="display: block; margin-bottom: 5px;">
                            <select class="form-control" name="field-2" style="width: 200px;">
                                <option id="0" data-value="none">Поле</option>
                                <option id="col_2" data-value="text" >Назва, ЄДРПОУ</option>
                                <option id="col_4" data-value="kved" >КВЕД 2010 (критерій 2)</option>
                                <option id="col_5" data-value="target" >Цільове призначення (критерій 3)</option>
                                <option id="col_6" data-value="text" >Власний внесок позичальника (критерій 4)</option>
                                <option id="col_7" data-value="date" >Дата погодження надання ФКП</option>
                                <option id="col_9" data-value="term" >Строк</option>
                                <option id="col_10" data-value="text" >Сума кредиту на початок звітного місяця, тис.грн</option>
                                <option id="col_11" data-value="text" >Відсоткова ставка, %</option>
                                <option id="col_12" data-value="text" >Загальна сума розрахованих відсотків у звітному місяці, %</option>
                                <option id="col_14" data-value="text" >Сума ФКП за звітний місяць, грн</option>
                                <option id="col_15" data-value="text" >Загальний розмір ФКП за весь період, грн</option>
                                <option id="col_16" data-value="text" >Плановий розмір ФКП в місяці, наступному за звітним періодом, грн</option>
                                <option id="col_17" data-value="text" >Залишковий розмір ФКП, тис.грн</option>
                                <option id="col_18" data-value="text" >Порядковий номер місця участі в ФКП (загальний термін 24 місяці)</option>
                                <option id="id_bank" data-value="bank" >Банк-партнер</option>
                                <option id="col_19" data-value="dateSelect" >Дата заповнення реєстру</option>
                            </select>
                        </div>
                        <button class="btn btn-primary" type="button" name="search">Пошук</button>
                        <a href="/registerOfBorrowers/generalRegister" title="Очистити">Очистити</a>
                    </div>
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <td rowspan='2'>№</td>
                                    <td rowspan='2'>Назва, ЄДРПОУ</td>
                                    <td rowspan='2'>Місце державної реєстрації, розташування виробничих потужностей (критерій 1)</td>
                                    <td rowspan='2'>КВЕД 2010 (критерій 2)</td>
                                    <td rowspan='2'>Цільове призначення (критерій 3)</td>
                                    <td rowspan='2'>Власний внесок позичальника (критерій 4)</td>
                                    <td rowspan='2'>Дата погодження надання ФКП</td>
                                    <td colspan='10'>Кредитний догорів</td>
                                    <td rowspan='2'>Порядковий номер місця участі в ФКП (загальний термін 24 місяці)</td>
                                    <td rowspan='2'>Банк-партнер</td>
                                    <td rowspan='2'>Дата заповнення реєстру</td>
                                    <td rowspan='2'>Сума коштів ФКП, затверджена в бюджеті м.Києва на відповідний рік, грн</td>
                                    <td rowspan='2'>Залишок бюджетніх коштів на ФКП</td>
                                </tr>
                                <tr>
                                    <td>№, дата</td>
                                    <td>Строк</td>
                                    <td>Сума кредиту на початок звітного місяця, тис.грн</td>
                                    <td>Відсоткова ставка, %</td>
                                    <td>Загальна сума розрахованих відсотків у звітному місяці, %</td>
                                    <td>Частка компенсації відсоткової ставки, %</td>
                                    <td>Сума ФКП за звітний місяць, грн</td>
                                    <td>Загальний розмір ФКП за весь період, грн</td>
                                    <td>Плановий розмір ФКП в місяці, наступному за звітним періодом, грн</td>
                                    <td>Залишковий розмір ФКП, тис.грн</td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>2</td>
                                    <td>3</td>
                                    <td>4</td>
                                    <td>5</td>
                                    <td>6</td>
                                    <td>7</td>
                                    <td>8</td>
                                    <td>9</td>
                                    <td>10</td>
                                    <td>11</td>
                                    <td>12</td>
                                    <td>13</td>
                                    <td>14</td>
                                    <td>15</td>
                                    <td>16</td>
                                    <td>17</td>
                                    <td>18</td>
                                    <td>19</td>
                                    <td>20</td>
                                    <td>21</td>
                                    <td>22</td>
                                </tr>
                            </thead>
                            <tbody>
                                @if(isset($generalRegister) && count($generalRegister))
                                    <?php
                                        $years = [];
                                        $n = 1;
                                        for ($i = 0; $i < count($generalRegister); $i++) { 

                                            if(!in_array(date('Y', strtotime($generalRegister[$i]->col_19)), $years)){
                                                
                                                $years[] = date('Y', strtotime($generalRegister[$i]->col_19));
                                                
                                                $insert = '';
                                                $insert2 = '';
                                                $sum_6 = 0;
                                                $sum_10 = 0;
                                                $sum_12 = 0;
                                                $sum_14 = 0;
                                                $sum_15 = 0;
                                                $sum_16 = 0;
                                                $sum_17 = 0;
                                                $rows = 1;
                                                $budgetNow = 0;

                                                for ($j = 0; $j < count($generalRegister); $j++) { 

                                                    if(date('Y', strtotime($generalRegister[$i]->col_19)) == date('Y', strtotime($generalRegister[$j]->col_19))){
                                                        $sum_6 += $generalRegister[$j]->col_6;
                                                        $sum_10 += $generalRegister[$j]->col_10;
                                                        $sum_12 += $generalRegister[$j]->col_12;
                                                        $sum_14 += $generalRegister[$j]->col_14;
                                                        $sum_15 += $generalRegister[$j]->col_15;
                                                        $sum_16 += $generalRegister[$j]->col_16;
                                                        $sum_17 += $generalRegister[$j]->col_17;

                                                        $insert2 = $insert;

                                                        $insert .= '<tr>'.
                                                            '<td id="1">'.$n.'</td>'.
                                                            '<td id="2">'.$generalRegister[$j]->col_2.'</td>'.
                                                            '<td id="3">'.$generalRegister[$j]->col_3.'</td>'.
                                                            '<td id="4">'.$generalRegister[$j]->col_4_section.' / '.$generalRegister[$j]->col_4_group.' / '.$generalRegister[$j]->col_4_clas.'</td>'.
                                                            '<td id="5">'.$generalRegister[$j]->col_5.'</td>'.
                                                            '<td id="6">'.$generalRegister[$j]->col_6.'</td>'.
                                                            '<td id="7">'.\Carbon\Carbon::parse($generalRegister[$j]->col_7)->format('d.m.Y').'</td>'.
                                                            '<td id="8">'.$generalRegister[$j]->col_8.'</td>'.
                                                            '<td id="9">'.$generalRegister[$i]->col_9.'</td>'.
                                                            '<td id="10">'.($generalRegister[$j]->col_10/1000).'</td>'.
                                                            '<td id="11">'.$generalRegister[$j]->col_11.'</td>'.
                                                            '<td id="12">'.$generalRegister[$j]->col_12.'</td>'.
                                                            '<td id="13">'.$generalRegister[$j]->col_13.'</td>'.
                                                            '<td id="14">'.$generalRegister[$j]->col_14.'</td>'.
                                                            '<td id="15">'.$generalRegister[$j]->col_15.'</td>'.
                                                            '<td id="16">'.$generalRegister[$j]->col_16.'</td>'.
                                                            '<td id="17">'.($generalRegister[$j]->col_17/1000).'</td>'.
                                                            '<td id="18">'.$generalRegister[$j]->col_18.'</td>'.
                                                            '<td id="19">'.$generalRegister[$j]->name.'</td>'.
                                                            '<td id="20">'.\Carbon\Carbon::parse($generalRegister[$j]->col_19)->format('d.m.Y').'</td>';
                                                        
                                                        if($insert2 == ''){
                                                            $insert .= '<td id="21" rowspan="rowspanCount">budgetNow</td>'.
                                                                       '<td id="22" rowspan="rowspanCount">budgetSum</td>';
                                                            
                                                            $budgetNow = 'Дані не внесені';
                                                            if(count($budget)){
                                                                for ($b = 0; $b < count($budget); $b++) { 
                                                                    if(date('Y', strtotime($generalRegister[$j]->col_19)) == date('Y', strtotime($budget[$b]->created_at))){
                                                                        $budgetNow = floatval($budget[$b]->value);
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        $n++;
                                                        $rows++;
                                                    }
                                                    if($j == count($generalRegister)-1){

                                                        $insert .= '</tr><tr>'.
                                                            '<td></td>'.
                                                            '<td></td>'.
                                                            '<td></td>'.
                                                            '<td></td>'.
                                                            '<td></td>'.
                                                            '<td>'.$sum_6.'</td>'.
                                                            '<td></td>'.
                                                            '<td></td>'.
                                                            '<td></td>'.
                                                            '<td>'.($sum_10/1000).'</td>'.
                                                            '<td></td>'.
                                                            '<td>'.$sum_12.'</td>'.
                                                            '<td></td>'.
                                                            '<td>'.$sum_14.'</td>'.
                                                            '<td>'.$sum_15.'</td>'.
                                                            '<td>'.$sum_16.'</td>'.
                                                            '<td>'.($sum_17/1000).'</td>'.
                                                            '<td></td>'.
                                                            '<td></td>'.
                                                            '<td></td>'.
                                                        '</tr>';
                                                    }
                                                }
                                                if(is_Numeric($budgetNow)){
                                                    $bgSum = floatval($budgetNow)-$sum_14;
                                                } else{
                                                    $bgSum = 'Неможливо розрахувати';
                                                }
                                                $insert = str_replace(['rowspan="rowspanCount"', 'budgetNow', 'budgetSum'], ['rowspan="'.$rows.'"', $budgetNow, $bgSum], $insert);
                                                echo $insert;
                                            }
                                        }
                                    ?>

                                @else
                                    <tr><td colspan="22">Інформації немає</td></tr>
                                @endif
                                <input type="hidden" name="token" value="{{ csrf_token() }}">
                                <input type="hidden" name="url" value="registerOfBorrowers">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        @else
            <div class="col-xs-12 col-sm-6 col-sm-offset-3">
                <div class="alert alert-danger">
                    <p>
                        Ви не можете переглядати дану інформацію! Повернутися на <a href="/" title="Головна">головну</a>
                    </p>
                </div>
            </div>
        @endif
    </div>
    @if(Session::get('status') == 'departament')
        <div class="row second-row">
            <div class="col-xs-12">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#charts" data-toggle="tab">Графіки</a></li>
                    <li ><a href="#reports" data-toggle="tab">Звіти</a></li>
                </ul>
            </div>
            <div class="col-xs-12">
                <div class="tab-content">
                    <div id="reports" class="tab-pane fade">
                        @if(isset($reports) && count($reports))
                            <ol>
                                @for($i = 0; $i < count($reports); $i++)
                                    <li>{{ $reports[$i]->name }}
                                        <span style="float:right;">
                                            <a href="../download/reports/{{$reports[$i]->url}}" class="btn btn-success btn-xs"><i class="fa fa-download"></i>&nbsp;Excel</a>
                                        </span>
                                    </li>
                                @endfor
                            </ol>
                        @else
                            <div class="col-xs-12 col-sm-6 col-sm-offset-3">
                                <div class="alert alert-info text-center">
                                    <p>Звітів немає</p>
                                </div>
                            </div>
                        @endif
                    </div>
                    <div id="charts" class="tab-pane fade in active">
                        @if(isset($generalRegister) && count($generalRegister))
                            <script src="/js/charts/highcharts.js?"<?php echo time()?>></script>
                            <script src="/js/charts/modules/series-label.js?"<?php echo time()?>></script>
                            <script src="/js/charts/modules/exporting.js?"<?php echo time()?>></script>

                            <div class="row">
                                <div class="col-xs-12 col-sm-6" style="margin-top: 5px;" id="chart-1"></div>
                                <div class="col-xs-12 col-sm-6" style="margin-top: 5px;" id="chart-2"></div>
                                <div class="col-xs-12 col-sm-6" style="margin-top: 5px;" id="chart-3"></div>
                                <div class="col-xs-12 col-sm-6" style="margin-top: 5px;" id="chart-4"></div>
                                <div class="col-xs-12 col-sm-6" style="margin-top: 5px;" id="chart-5"></div>
                                <div class="col-xs-12 col-sm-6" style="margin-top: 5px;" id="chart-6"></div>
                                <div class="col-xs-12 col-sm-6" style="margin-top: 5px;" id="chart-7"></div>
                                <div class="col-xs-12 col-sm-6" style="margin-top: 5px;" id="chart-8"></div>
                                <div class="col-xs-12 col-sm-6" style="margin-top: 5px;" id="chart-9"></div>
                                <div class="col-xs-12 col-sm-6" style="margin-top: 5px;" id="chart-10"></div>
                                <div class="col-xs-12 col-sm-6" style="margin-top: 5px;" id="chart-11"></div>
                                <div class="col-xs-12 col-sm-6" style="margin-top: 5px;" id="chart-12"></div>
                                <div class="col-xs-12 col-sm-6" style="margin-top: 5px;" id="chart-13"></div>
                            </div>

                            <script type="text/javascript">
                                var datasChart_1 = [];
                                $.get('/chart_1').done(function(resp){
                                    if(resp.length > 0){
                                        for (var i = 0; i < resp.length; i++) {
                                            if(String(resp[i]['month']).length == 1){
                                                resp[i]['month'] = '0'+resp[i]['month'];
                                            }
                                            datasChart_1.push([resp[i]['month']+'.'+resp[i]['year'], parseInt(resp[i]['count'])]);
                                        };
                                    }
                                    
                                    Highcharts.chart('chart-1', { // 1 Кількість підприємств, які отримали ФКП
                                        chart: {
                                            type: 'column'
                                        },
                                        title: {
                                            text: 'Кількість підприємств, які отримали ФКП'
                                        },
                                        xAxis: {
                                            type: 'category',
                                            labels: {
                                                //rotation: -45,
                                                style: {
                                                    fontSize: '13px',
                                                    fontFamily: 'Verdana, sans-serif',
                                                    wordWrap: true
                                                }
                                            }
                                        },
                                        yAxis: {
                                            min: 0,
                                            title: {
                                                text: 'Кількість підприємств'
                                            }
                                        },
                                        legend: {
                                            enabled: false
                                        },
                                        tooltip: {
                                            pointFormat: 'Кількість підприємств: <b>{point.y}</b>'
                                        },
                                        series: [{
                                            name: 'Кількість підприємств',
                                            data:datasChart_1,
                                            dataLabels: {
                                                enabled: true,
                                                rotation: -90,
                                                color: '#FFFFFF',
                                                align: 'right',
                                                format: '{point.y}', // one decimal
                                                y: 10, // 10 pixels down from the top
                                                style: {
                                                    fontSize: '13px',
                                                    fontFamily: 'Verdana, sans-serif'
                                                }
                                            }
                                        }]
                                    });
                                    /*
                                    Highcharts.chart('chart-5', { // 5 Структура ФКП по КВЕД
                                        chart: {
                                            type: 'column'
                                        },
                                        title: {
                                            text: 'Структура ФКП по КВЕД'
                                        },
                                        xAxis: {
                                            type: 'category',
                                            labels: {
                                                //rotation: -45,
                                                style: {
                                                    fontSize: '13px',
                                                    fontFamily: 'Verdana, sans-serif',
                                                    wordWrap: true
                                                }
                                            }
                                        },
                                        yAxis: {
                                            min: 0,
                                            title: {
                                                text: 'Кількість'
                                            }
                                        },
                                        legend: {
                                            enabled: false
                                        },
                                        tooltip: {
                                            pointFormat: 'Кількість ФКП: <b>{point.y}</b>'
                                        },
                                        series: [{
                                            name: 'Кількість ФКП',
                                            data:datasChart_5,
                                            dataLabels: {
                                                enabled: true,
                                                rotation: -90,
                                                color: '#FFFFFF',
                                                align: 'right',
                                                format: '{point.y}', // one decimal
                                                y: 10, // 10 pixels down from the top
                                                style: {
                                                    fontSize: '13px',
                                                    fontFamily: 'Verdana, sans-serif'
                                                }
                                            }
                                        }]
                                    });
                                    
                                    Highcharts.chart('chart-6', { // 6 Сума кредитів по КВЕД
                                        chart: {
                                            type: 'column'
                                        },
                                        title: {
                                            text: 'Сума кредитів по КВЕД'
                                        },
                                        xAxis: {
                                            type: 'category',
                                            labels: {
                                                //rotation: -45,
                                                style: {
                                                    fontSize: '13px',
                                                    fontFamily: 'Verdana, sans-serif',
                                                    wordWrap: true
                                                }
                                            }
                                        },
                                        yAxis: {
                                            min: 0,
                                            title: {
                                                text: 'Сума, тис.грн'
                                            }
                                        },
                                        legend: {
                                            enabled: false
                                        },
                                        tooltip: {
                                            pointFormat: 'Сума кредитів: <b>{point.y:.3f} тис.грн</b>'
                                        },
                                        series: [{
                                            name: 'Сума кредитів',
                                            data:datasChart_6,
                                            dataLabels: {
                                                enabled: true,
                                                rotation: -90,
                                                color: '#FFFFFF',
                                                align: 'right',
                                                format: '{point.y:.3f}', // one decimal
                                                y: 10, // 10 pixels down from the top
                                                style: {
                                                    fontSize: '13px',
                                                    fontFamily: 'Verdana, sans-serif'
                                                }
                                            }
                                        }]
                                    });
                                    
                                    Highcharts.chart('chart-7', { // 7 Сума ФКП по КВЕД
                                        chart: {
                                            type: 'column'
                                        },
                                        title: {
                                            text: 'Сума ФКП по КВЕД'
                                        },
                                        xAxis: {
                                            type: 'category',
                                            labels: {
                                                //rotation: -45,
                                                style: {
                                                    fontSize: '13px',
                                                    fontFamily: 'Verdana, sans-serif',
                                                    wordWrap: true
                                                }
                                            }
                                        },
                                        yAxis: {
                                            min: 0,
                                            title: {
                                                text: 'Сума, грн'
                                            }
                                        },
                                        legend: {
                                            enabled: false
                                        },
                                        tooltip: {
                                            pointFormat: 'Сума ФКП: <b>{point.y:.2f} грн</b>'
                                        },
                                        series: [{
                                            name: 'Сума ФКП',
                                            data:datasChart_7,
                                            dataLabels: {
                                                enabled: true,
                                                rotation: -90,
                                                color: '#FFFFFF',
                                                align: 'right',
                                                format: '{point.y:.2f}', // one decimal
                                                y: 10, // 10 pixels down from the top
                                                style: {
                                                    fontSize: '13px',
                                                    fontFamily: 'Verdana, sans-serif'
                                                }
                                            }
                                        }]
                                    });*/

                                });
                            </script>

                            <script type="text/javascript">
                                var datasChart_4 = [];
                                var datasChart_5 = [];
                                var datasChart_6 = [];
                                var datasChart_7 = [];
                                $.get('/chart_4_5_6_7').done(function(resp){
                                    if(resp.length > 0){
                                        for (var i = 0; i < resp.length; i++) {
                                            datasChart_4.push(['<b>розділ</b> '+resp[i]['kved'], parseFloat(resp[i]['countCredits'])]);
                                            datasChart_5.push(['<b>розділ</b> '+resp[i]['kved'], parseFloat(resp[i]['countFKP'])]);
                                            datasChart_6.push(['<b>розділ</b> '+resp[i]['kved'], parseFloat(resp[i]['sumCredits'])/1000]);
                                            datasChart_7.push(['<b>розділ</b> '+resp[i]['kved'], parseFloat(resp[i]['sumFKP'])]);
                                        };
                                    }
                                    
                                    Highcharts.chart('chart-4', { // 4 Структура кредитів по КВЕД
                                        chart: {
                                            type: 'column'
                                        },
                                        title: {
                                            text: 'Структура кредитів по КВЕД'
                                        },
                                        xAxis: {
                                            type: 'category',
                                            labels: {
                                                //rotation: -45,
                                                style: {
                                                    fontSize: '13px',
                                                    fontFamily: 'Verdana, sans-serif',
                                                    wordWrap: true
                                                }
                                            }
                                        },
                                        yAxis: {
                                            min: 0,
                                            title: {
                                                text: 'Кількість'
                                            }
                                        },
                                        legend: {
                                            enabled: false
                                        },
                                        tooltip: {
                                            pointFormat: 'Кількість кредитів: <b>{point.y}</b>'
                                        },
                                        series: [{
                                            name: 'Кількість кредитів',
                                            data:datasChart_4,
                                            dataLabels: {
                                                enabled: true,
                                                rotation: -90,
                                                color: '#FFFFFF',
                                                align: 'right',
                                                format: '{point.y}', // one decimal
                                                y: 10, // 10 pixels down from the top
                                                style: {
                                                    fontSize: '13px',
                                                    fontFamily: 'Verdana, sans-serif'
                                                }
                                            }
                                        }]
                                    });
                                    
                                    Highcharts.chart('chart-5', { // 5 Структура ФКП по КВЕД
                                        chart: {
                                            type: 'column'
                                        },
                                        title: {
                                            text: 'Структура ФКП по КВЕД'
                                        },
                                        xAxis: {
                                            type: 'category',
                                            labels: {
                                                //rotation: -45,
                                                style: {
                                                    fontSize: '13px',
                                                    fontFamily: 'Verdana, sans-serif',
                                                    wordWrap: true
                                                }
                                            }
                                        },
                                        yAxis: {
                                            min: 0,
                                            title: {
                                                text: 'Кількість'
                                            }
                                        },
                                        legend: {
                                            enabled: false
                                        },
                                        tooltip: {
                                            pointFormat: 'Кількість ФКП: <b>{point.y}</b>'
                                        },
                                        series: [{
                                            name: 'Кількість ФКП',
                                            data:datasChart_5,
                                            dataLabels: {
                                                enabled: true,
                                                rotation: -90,
                                                color: '#FFFFFF',
                                                align: 'right',
                                                format: '{point.y}', // one decimal
                                                y: 10, // 10 pixels down from the top
                                                style: {
                                                    fontSize: '13px',
                                                    fontFamily: 'Verdana, sans-serif'
                                                }
                                            }
                                        }]
                                    });
                                    
                                    Highcharts.chart('chart-6', { // 6 Сума кредитів по КВЕД
                                        chart: {
                                            type: 'column'
                                        },
                                        title: {
                                            text: 'Сума кредитів по КВЕД'
                                        },
                                        xAxis: {
                                            type: 'category',
                                            labels: {
                                                //rotation: -45,
                                                style: {
                                                    fontSize: '13px',
                                                    fontFamily: 'Verdana, sans-serif',
                                                    wordWrap: true
                                                }
                                            }
                                        },
                                        yAxis: {
                                            min: 0,
                                            title: {
                                                text: 'Сума, тис.грн'
                                            }
                                        },
                                        legend: {
                                            enabled: false
                                        },
                                        tooltip: {
                                            pointFormat: 'Сума кредитів: <b>{point.y:.3f} тис.грн</b>'
                                        },
                                        series: [{
                                            name: 'Сума кредитів',
                                            data:datasChart_6,
                                            dataLabels: {
                                                enabled: true,
                                                rotation: -90,
                                                color: '#FFFFFF',
                                                align: 'right',
                                                format: '{point.y:.3f}', // one decimal
                                                y: 10, // 10 pixels down from the top
                                                style: {
                                                    fontSize: '13px',
                                                    fontFamily: 'Verdana, sans-serif'
                                                }
                                            }
                                        }]
                                    });
                                    
                                    Highcharts.chart('chart-7', { // 7 Сума ФКП по КВЕД
                                        chart: {
                                            type: 'column'
                                        },
                                        title: {
                                            text: 'Сума ФКП по КВЕД'
                                        },
                                        xAxis: {
                                            type: 'category',
                                            labels: {
                                                //rotation: -45,
                                                style: {
                                                    fontSize: '13px',
                                                    fontFamily: 'Verdana, sans-serif',
                                                    wordWrap: true
                                                }
                                            }
                                        },
                                        yAxis: {
                                            min: 0,
                                            title: {
                                                text: 'Сума, грн'
                                            }
                                        },
                                        legend: {
                                            enabled: false
                                        },
                                        tooltip: {
                                            pointFormat: 'Сума ФКП: <b>{point.y:.2f} грн</b>'
                                        },
                                        series: [{
                                            name: 'Сума ФКП',
                                            data:datasChart_7,
                                            dataLabels: {
                                                enabled: true,
                                                rotation: -90,
                                                color: '#FFFFFF',
                                                align: 'right',
                                                format: '{point.y:.2f}', // one decimal
                                                y: 10, // 10 pixels down from the top
                                                style: {
                                                    fontSize: '13px',
                                                    fontFamily: 'Verdana, sans-serif'
                                                }
                                            }
                                        }]
                                    });

                                });
                            </script>

                            <script type="text/javascript">
                                var datasChart_8 = [];
                                var datasChart_9 = [];
                                var datasChart_10 = [];
                                $.get('/chart_8_9_10').done(function(resp){
                                    if(resp.length > 0){
                                        for (var i = 0; i < resp.length; i++) {
                                            datasChart_8.push([resp[i]['target'], parseFloat(resp[i]['count'])]);
                                            datasChart_9.push([resp[i]['target'], parseFloat(resp[i]['sumCredits'])/1000]);
                                            datasChart_10.push([resp[i]['target'], parseFloat(resp[i]['sumFKP'])]);
                                        };
                                    }
                                    
                                    Highcharts.chart('chart-8', { // 8 Структура кредитів по цілях
                                        chart: {
                                            type: 'column'
                                        },
                                        title: {
                                            text: 'Структура кредитів по цілях'
                                        },
                                        xAxis: {
                                            type: 'category',
                                            labels: {
                                                //rotation: -45,
                                                style: {
                                                    fontSize: '13px',
                                                    fontFamily: 'Verdana, sans-serif',
                                                    wordWrap: true
                                                }
                                            }
                                        },
                                        yAxis: {
                                            min: 0,
                                            title: {
                                                text: 'Кількість'
                                            }
                                        },
                                        legend: {
                                            enabled: false
                                        },
                                        tooltip: {
                                            pointFormat: 'Кількість кредитів: <b>{point.y}</b>'
                                        },
                                        series: [{
                                            name: 'Кількість кредитів',
                                            data:datasChart_8,
                                            dataLabels: {
                                                enabled: true,
                                                rotation: -90,
                                                color: '#FFFFFF',
                                                align: 'right',
                                                format: '{point.y}', // one decimal
                                                y: 10, // 10 pixels down from the top
                                                style: {
                                                    fontSize: '13px',
                                                    fontFamily: 'Verdana, sans-serif'
                                                }
                                            }
                                        }]
                                    });

                                    Highcharts.chart('chart-9', { // 9 Сума кредитів по цілях
                                        chart: {
                                            type: 'column'
                                        },
                                        title: {
                                            text: 'Сума кредитів по цілях'
                                        },
                                        xAxis: {
                                            type: 'category',
                                            labels: {
                                                //rotation: -45,
                                                style: {
                                                    fontSize: '13px',
                                                    fontFamily: 'Verdana, sans-serif',
                                                    wordWrap: true
                                                }
                                            }
                                        },
                                        yAxis: {
                                            min: 0,
                                            title: {
                                                text: 'Сума, тис.грн'
                                            }
                                        },
                                        legend: {
                                            enabled: false
                                        },
                                        tooltip: {
                                            pointFormat: 'Сума кредитів: <b>{point.y:.3f} тис.грн</b>'
                                        },
                                        series: [{
                                            name: 'Сума кредитів',
                                            data:datasChart_9,
                                            dataLabels: {
                                                enabled: true,
                                                rotation: -90,
                                                color: '#FFFFFF',
                                                align: 'right',
                                                format: '{point.y:.3f}', // one decimal
                                                y: 10, // 10 pixels down from the top
                                                style: {
                                                    fontSize: '13px',
                                                    fontFamily: 'Verdana, sans-serif'
                                                }
                                            }
                                        }]
                                    });

                                    Highcharts.chart('chart-10', { // 10 Сума ФКП по цілях
                                        chart: {
                                            type: 'column'
                                        },
                                        title: {
                                            text: 'Сума ФКП по цілях'
                                        },
                                        xAxis: {
                                            type: 'category',
                                            labels: {
                                                //rotation: -45,
                                                style: {
                                                    fontSize: '13px',
                                                    fontFamily: 'Verdana, sans-serif',
                                                    wordWrap: true
                                                }
                                            }
                                        },
                                        yAxis: {
                                            min: 0,
                                            title: {
                                                text: 'Сума, грн'
                                            }
                                        },
                                        legend: {
                                            enabled: false
                                        },
                                        tooltip: {
                                            pointFormat: 'Сума ФКП: <b>{point.y:.2f} грн</b>'
                                        },
                                        series: [{
                                            name: 'Сума ФКП',
                                            data:datasChart_10,
                                            dataLabels: {
                                                enabled: true,
                                                rotation: -90,
                                                color: '#FFFFFF',
                                                align: 'right',
                                                format: '{point.y:.2f}', // one decimal
                                                y: 10, // 10 pixels down from the top
                                                style: {
                                                    fontSize: '13px',
                                                    fontFamily: 'Verdana, sans-serif'
                                                }
                                            }
                                        }]
                                    });

                                });
                            </script>

                            <script type="text/javascript">
                                var datasChart_11 = [];
                                var datasChart_12 = [];
                                var datasChart_13 = [];
                                $.get('/chart_11_12_13').done(function(resp){
                                    if(resp.length > 0){
                                        for (var i = 0; i < resp.length; i++) {
                                            datasChart_11.push([resp[i]['name'], parseFloat(resp[i]['sumCredits'])/1000]);
                                            datasChart_12.push([resp[i]['name'], parseFloat(resp[i]['sumFKP'])]);
                                            datasChart_13.push([resp[i]['name'], parseFloat(resp[i]['count'])]);
                                        };
                                    }

                                    Highcharts.chart('chart-11', { // 11 Сума кредитів по банках
                                        chart: {
                                            type: 'column'
                                        },
                                        title: {
                                            text: 'Сума кредитів по банках'
                                        },
                                        xAxis: {
                                            type: 'category',
                                            labels: {
                                                //rotation: -45,
                                                style: {
                                                    fontSize: '13px',
                                                    fontFamily: 'Verdana, sans-serif',
                                                    wordWrap: true
                                                }
                                            }
                                        },
                                        yAxis: {
                                            min: 0,
                                            title: {
                                                text: 'Сума, тис.грн'
                                            }
                                        },
                                        legend: {
                                            enabled: false
                                        },
                                        tooltip: {
                                            pointFormat: 'Сума кредитів: <b>{point.y:.3f} тис.грн</b>'
                                        },
                                        series: [{
                                            name: 'Сума кредитів',
                                            data:datasChart_11,
                                            dataLabels: {
                                                enabled: true,
                                                rotation: -90,
                                                color: '#FFFFFF',
                                                align: 'right',
                                                format: '{point.y:.3f}', // one decimal
                                                y: 10, // 10 pixels down from the top
                                                style: {
                                                    fontSize: '13px',
                                                    fontFamily: 'Verdana, sans-serif'
                                                }
                                            }
                                        }]
                                    });

                                    Highcharts.chart('chart-12', { // 12 Сума ФКП по банках
                                        chart: {
                                            type: 'column'
                                        },
                                        title: {
                                            text: 'Сума ФКП по банках'
                                        },
                                        xAxis: {
                                            type: 'category',
                                            labels: {
                                                //rotation: -45,
                                                style: {
                                                    fontSize: '13px',
                                                    fontFamily: 'Verdana, sans-serif',
                                                    wordWrap: true
                                                }
                                            }
                                        },
                                        yAxis: {
                                            min: 0,
                                            title: {
                                                text: 'Сума, грн'
                                            }
                                        },
                                        legend: {
                                            enabled: false
                                        },
                                        tooltip: {
                                            pointFormat: 'Сума ФКП: <b>{point.y:.2f} грн</b>'
                                        },
                                        series: [{
                                            name: 'Сума ФКП',
                                            data:datasChart_12,
                                            dataLabels: {
                                                enabled: true,
                                                rotation: -90,
                                                color: '#FFFFFF',
                                                align: 'right',
                                                format: '{point.y:.2f}', // one decimal
                                                y: 10, // 10 pixels down from the top
                                                style: {
                                                    fontSize: '13px',
                                                    fontFamily: 'Verdana, sans-serif'
                                                }
                                            }
                                        }]
                                    });

                                    Highcharts.chart('chart-13', { // 13 Кількість підприємств які отримали кредити по банках
                                        chart: {
                                            type: 'column'
                                        },
                                        title: {
                                            text: 'Кількість підприємств які отримали кредити по банках'
                                        },
                                        xAxis: {
                                            type: 'category',
                                            labels: {
                                                //rotation: -45,
                                                style: {
                                                    fontSize: '13px',
                                                    fontFamily: 'Verdana, sans-serif',
                                                    wordWrap: true
                                                }
                                            }
                                        },
                                        yAxis: {
                                            min: 0,
                                            title: {
                                                text: 'Кількість'
                                            }
                                        },
                                        legend: {
                                            enabled: false
                                        },
                                        tooltip: {
                                            pointFormat: 'Кількість кредитів: <b>{point.y}</b>'
                                        },
                                        series: [{
                                            name: 'Кількість кредитів',
                                            data:datasChart_13,
                                            dataLabels: {
                                                enabled: true,
                                                rotation: -90,
                                                color: '#FFFFFF',
                                                align: 'right',
                                                format: '{point.y}', // one decimal
                                                y: 10, // 10 pixels down from the top
                                                style: {
                                                    fontSize: '13px',
                                                    fontFamily: 'Verdana, sans-serif'
                                                }
                                            }
                                        }]
                                    });
                                });
                            </script>
                        @else
                            <div class="col-xs-12 col-sm-6 col-sm-offset-3">
                                <div class="alert alert-info text-center">
                                    <p>Для побудови графіків, даних немає</p>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    @endif
@endsection