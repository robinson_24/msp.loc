@extends('app')

@section('content')
    <div class="row">
        @if(Session::get('userId') == $idBank || Session::get('status') == 'departament')
            <div class="col-xs-12">
                <div class="alert alert-info">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <td rowspan='2'>№</td>
                                    <td rowspan='2' id="td-2" data-type="text">Назва, ЄДРПОУ</td>
                                    <td rowspan='2' id="td-3" data-type="text">Місце державної реєстрації, розташування виробничих потужностей (критерій 1)</td>
                                    <td rowspan='2' id="td-4" data-type="kved">КВЕД 2010 (критерій 2)</td>
                                    <td rowspan='2' id="td-5" data-type="target">Цільове призначення (критерій 3)</td>
                                    <td rowspan='2' id="td-6" data-type="numero">Власний внесок позичальника (критерій 4)</td>
                                    <td rowspan='2' id="td-7" data-type="date">Дата погодження надання ФКП</td>
                                    <td colspan='10'>Кредитний догорів</td>
                                    <td rowspan='2' id="td-18" data-type="numero">Порядковий номер місця участі в ФКП (загальний термін 24 місяці)</td>
                                    <td rowspan='2'>Дата заповнення реєстру</td>
                                </tr>
                                <tr>
                                    <td id="td-8" data-type="text">№, дата</td>
                                    <td id="td-9" data-type="term">Строк</td>
                                    <td id="td-10" data-type="numero-thousand">Сума кредиту на початок звітного місяця, тис.грн</td>
                                    <td id="td-11" data-type="numero">Відсоткова ставка, %</td>
                                    <td id="td-12" data-type="numero">Загальна сума розрахованих відсотків у звітному місяці, %</td>
                                    <td id="td-13" data-type="numero">Частка компенсації відсоткової ставки, %</td>
                                    <td id="td-14" data-type="numero">Сума ФКП за звітний місяць, грн</td>
                                    <td id="td-15" data-type="numero">Загальний розмір ФКП за весь період, грн</td>
                                    <td id="td-16" data-type="numero">Плановий розмір ФКП в місяці, наступному за звітним періодом, грн</td>
                                    <td id="td-17" data-type="numero-thousand">Залишковий розмір ФКП, тис.грн</td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>2</td>
                                    <td>3</td>
                                    <td>4</td>
                                    <td>5</td>
                                    <td>6</td>
                                    <td>7</td>
                                    <td>8</td>
                                    <td>9</td>
                                    <td>10</td>
                                    <td>11</td>
                                    <td>12</td>
                                    <td>13</td>
                                    <td>14</td>
                                    <td>15</td>
                                    <td>16</td>
                                    <td>17</td>
                                    <td>18</td>
                                    <td class="lastNumeroRow">19</td>
                                </tr>
                            </thead>
                            <tbody class="{{ (Session::get('status') != 'departament') ? 'register' : '' }}">
                                @if(isset($borrowersRegister) && count($borrowersRegister))
                                    @for($i = 0; $i < count($borrowersRegister);$i++)
                                        <tr id="{{ $i+1 }}" data-id="{{ $borrowersRegister[$i]->id }}">
                                            <td id="action">{{ $i+1 }}
                                                @if(Session::get('userId') == $idBank)
                                                    <i class="fa fa-times deleteRow" title="Відалити"></i></td>
                                                @endif
                                            <td id="2">{{ $borrowersRegister[$i]->col_2 }}</td>
                                            <td id="3">{{ $borrowersRegister[$i]->col_3 }}</td>
                                            <td id="4">{{ $borrowersRegister[$i]->col_4_section }} / {{ $borrowersRegister[$i]->col_4_group }} / {{ $borrowersRegister[$i]->col_4_clas }}</td>
                                            <td id="5">{{ $borrowersRegister[$i]->col_5 }}</td>
                                            <td id="6">{{ $borrowersRegister[$i]->col_6 }}</td>
                                            <td id="7">{{ \Carbon\Carbon::parse($borrowersRegister[$i]->col_7)->format('d.m.Y') }}</td>
                                            <td id="8">{{ $borrowersRegister[$i]->col_8 }}</td>
                                            <td id="9">{{ $borrowersRegister[$i]->col_9 }}</td>
                                            <td id="10">{{ $borrowersRegister[$i]->col_10/1000 }}</td>
                                            <td id="11">{{ $borrowersRegister[$i]->col_11 }}</td>
                                            <td id="12">{{ $borrowersRegister[$i]->col_12 }}</td>
                                            <td id="13">{{ $borrowersRegister[$i]->col_13 }}</td>
                                            <td id="14">{{ $borrowersRegister[$i]->col_14 }}</td>
                                            <td id="15">{{ $borrowersRegister[$i]->col_15 }}</td>
                                            <td id="16">{{ $borrowersRegister[$i]->col_16 }}</td>
                                            <td id="17">{{ $borrowersRegister[$i]->col_17/1000 }}</td>
                                            <td id="18">{{ $borrowersRegister[$i]->col_18 }}</td>
                                            <td id="19">{{ \Carbon\Carbon::parse($borrowersRegister[$i]->col_19)->format('d.m.Y') }}</td>
                                        </tr>
                                    @endfor
                                @endif
                                @if(!isset($borrowersRegister) && Session::get('status') == 'departament')
                                    <tr><td colspan="19">Інформації немає</td></tr>
                                @endif
                                @if(Session::get('userId') == $idBank)
                                    <tr class="lastRow">
                                        <td><i class="fa fa-plus green addRow" title="Додати"></i></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <input type="hidden" name="urlType" data-url="/registerOfBorrowers/"/>
                                    <input type="hidden" name="token" value="{{ csrf_token() }}" />
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        @else
            <div class="col-xs-12 col-sm-6 col-sm-offset-3">
                <div class="alert alert-danger">
                    <p>
                        Ви не можете переглядати іформацію даного банку! Повернутися на <a href="/" title="Головна">головну</a>
                    </p>
                </div>
            </div>
        @endif
    </div>
    @if(Session::get('userId') == $idBank)
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-sm-offset-3">
                <div class="alert alert-info">
                    <strong>Увага!</strong> Для того, щоб додати та зберегти дані натисніть клавішу "Enter"
                </div>
            </div>
        </div>
    @endif
@endsection