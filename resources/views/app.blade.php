<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
{{ csrf_field() }}
        <title>{{ $title }}</title>

        <!-- Fonts -->
        <link href="/css/bootstrap.css" rel="stylesheet">
        <link rel="stylesheet" href="/css/font-awesome/css/font-awesome.css">
        @if(!Session::has('userId'))
            <link href="/css/styleNoAuth.css?<?php echo time();?>" rel="stylesheet">
        @else
            @if($title == 'Он-лайн платформа фінансово-кредитної підтримки суб\'єктів МСП м.Києва')
                <link href="/css/styleHome.css?<?php echo time();?>" rel="stylesheet">
            @else
                <link href="/css/styleDefault.css?<?php echo time();?>" rel="stylesheet">
            @endif
        @endif

        <link href="/css/style.css?<?php echo time();?>" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="/css/selectize.bootstrap3.css" />

        <!-- Scripts -->
        <script type="text/javascript" src="/js/jquery-3.2.1.js"></script>
        <script type="text/javascript" src="/js/bootstrap.min.js"></script>
        
        <script type="text/javascript" src="/js/myJs.js?<?php echo time();?>"></script>
        <script type="text/javascript" src="/js/bank.js?<?php echo time();?>"></script>
        <script type="text/javascript" src="/js/kved.js?<?php echo time();?>"></script>
        <script type="text/javascript" src="/js/search.js?<?php echo time();?>"></script>
        <script type="text/javascript" src="/js/selectize.js?<?php echo time();?>"></script>
        <script type="text/javascript" src="/js/ckeditor/ckeditor.js"></script>
    </head>
    <body>
        @if(Session::has('userId'))
            <div class="container" id="container-home">
                <div class="row" style="margin-bottom: 10px;">

                    <div class="navbar navbar-default hidden-sm hidden-md hidden-lg" style="margin-bottom: 0;">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="/">Головна</a>
                        </div>
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <div class="col-xs-12">
                                <div style="padding:15px;">
                                    <p>Ви увійшли в систему як <strong>{{ Session::get('name') }}</strong></p>
                                    <a href="/logout/{{Session::get('userId')}}" class="btn btn-default btn-sm">Вихід</a>
                                    @if(Session::get('status') == 'departament')
                                        <a href="/admin">Адміністрування</a>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-3 text-center hidden-xs">
                        <a href="/"><img src="/images/backgrounds/gerb.png" width="50%" alt="Герб"></a>
                    </div>

                    <div class="col-xs-12 col-sm-6">
                        @if(isset($name))
                            <h2 class="text-center">{{ $name }}</h2>
                        @endif
                        <h2 class="text-center">{{ $title }}</h2>
                        @if(isset($subTitle))
                            <h4 class="text-center">{{ $subTitle }}</h4>
                        @endif
                    </div>
                    <div class="col-sm-3 hidden-xs">
                        <div class="alert alert-info">
                            <p>Ви увійшли в систему як <strong>{{ Session::get('name') }}</strong></p>
                            <a href="/logout/{{Session::get('userId')}}" class="btn btn-default btn-sm">Вихід</a>
                            @if(Session::get('status') == 'departament')
                                <a href="/admin">Адміністрування</a>
                            @endif
                        </div>
                    </div>
                </div>

                @yield('content')

                <footer>
                    <div class="row text-center">
                        <div class="col-xs-12" style="color: #AEC7E4; margin-top: 6px; padding-bottom: 20px;">
                            <div style="margin-top: 10px">
                                <a target="_blank" style="display: block; width: 100%; color: #000; margin-top: 20px" href="https://itkron.com/" class="footer-developer"><img height="40" src="https://itkron.com/wp-content/uploads/2016/10/new-logo-kron-473.png">Розроблено компанією ITKron</a>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        @else

        <div class="container">
            <div class="row" style="height:100%;">
                <div class="col-xs-12 col-sm-8 col-sm-offset-2" id="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-sm-offset-3 second-row">
                            <div class="alert alert-info" style="background-color:rgba(217, 237, 247, 1.0);">
                                @if(isset($error))
                                    <h5 class="alert alert-danger" id="alert-close" role="alert">{{ $error }}</h5>
                                @endif
                                <form action="/auth" method="post">
                                    <input class="form-control" type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <div class="form-group">
                                        <input type="text" class="form-control input-sm" name="user" placeholder="Користувач" id="user">
                                    </div>
                                    <div class="form-group">
                                        <input type="password" class="form-control input-sm" name="password" placeholder="Пароль" id="password">
                                    </div>
                                        <button type="submit" class="btn btn-default btn-sm">Авторизуватися</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif
    </body>
</html>