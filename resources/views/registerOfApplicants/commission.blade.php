@extends('app')

@section('content')
    <div class="row text-center">
        <div class="col-xs-12 col-sm-6">
            <a href="/registerOfApplicants/commission/commands">
                <div class="border">
                    <p>Наказ</p>
                </div>
            </a>
        </div>
        <div class="col-xs-12 col-sm-6">
            <a href="/registerOfApplicants/commission/protocols">
                <div class="border">
                    <p>Протоколи</p>
                </div>
            </a>
        </div>
    </div>
@endsection