@extends('app')

@section('content')
    <div class="row">
        @if(Session::get('status') == 'departament')
            <div class="col-xs-12">
                <div class="alert alert-info">
                    <div class="search form-inline">
                        <div class="form-group" style="display: block; margin-bottom: 5px;">
                            <select class="form-control" name="field-1">
                                <option id="0" data-value="none">Поле</option>
                                <option id="col_2" data-value="text">Назва</option>
                                <option id="col_3" data-value="text">ЄДРПОУ</option>
                                <option id="col_6" data-value="kved">КВЕД 2010 /розділ/група/клас</option>
                                <option id="col_7" data-value="target">Ціль кредиту</option>
                                <option id="col_12" data-value="personal">Чисельність персоналу</option>
                                <option id="col_13" data-value="text">Номер</option>
                                <option id="col_14" data-value="stacking">Дата</option>
                                <option id="col_15" data-value="term">Строк</option>
                                <option id="col_16" data-value="total">Сума</option>
                                <option id="col_18" data-value="date">Кінцеве погашення</option>
                                <option id="col_19" data-value="text">Ставка, %</option>
                                <option id="col_20" data-value="text">Щомісячна сума, грн</option>
                                <option id="col_21" data-value="text">Період виплат (не більше 24 міс.)</option>
                                <option id="col_22" data-value="text">Загальна сума</option>
                                <option id="id_bank" data-value="bank">Банк-партнер</option>
                                <option id="col_24" data-value="dateSelect">Дата заповнення реєстру</option>
                            </select>
                        </div>
                        <div class="form-group" style="display: block; margin-bottom: 5px;">
                            <select class="form-control" name="field-2">
                                <option id="0" data-value="none">Поле</option>
                                <option id="col_2" data-value="text">Назва</option>
                                <option id="col_3" data-value="text">ЄДРПОУ</option>
                                <option id="col_6" data-value="kved">КВЕД 2010 /розділ/група/клас</option>
                                <option id="col_7" data-value="target">Ціль кредиту</option>
                                <option id="col_12" data-value="personal">Чисельність персоналу</option>
                                <option id="col_13" data-value="text">Номер</option>
                                <option id="col_14" data-value="stacking">Дата</option>
                                <option id="col_15" data-value="term">Строк</option>
                                <option id="col_16" data-value="total">Сума</option>
                                <option id="col_18" data-value="date">Кінцеве погашення</option>
                                <option id="col_19" data-value="text">Ставка, %</option>
                                <option id="col_20" data-value="text">Щомісячна сума, грн</option>
                                <option id="col_21" data-value="text">Період виплат (не більше 24 міс.)</option>
                                <option id="col_22" data-value="text">Загальна сума</option>
                                <option id="id_bank" data-value="bank">Банк-партнер</option>
                                <option id="col_24" data-value="dateSelect">Дата заповнення реєстру</option>
                            </select>
                        </div>
                        <button class="btn btn-primary" type="button" name="search">Пошук</button>
                        <a class="btn btn-default" href="/registerOfApplicants/generalRegister">Очистити</a>
                    </div>
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <td rowspan='3'>№</td>
                                    <td rowspan='3'>Назва</td>
                                    <td rowspan='3'>ЄДРПОУ</td>
                                    <td colspan='2'>Критерій 1</td>
                                    <td>Критерій 2</td>
                                    <td>Критерій 3</td>
                                    <td>Критерій 4</td>
                                    <td rowspan='3'>Відсутність просроченого боргу</td>
                                    <td rowspan='3'>Незалежність МСП</td>
                                    <td rowspan='3'>Річний дохід, тис.грн</td>
                                    <td rowspan='3'>Чисельність персоналу, одиниць</td>
                                    <td colspan='10'>Кредитний догорів</td>
                                    <td rowspan='3'>Результат розгляду (*)</td>
                                    <td rowspan='3'>Банк-партнер</td>
                                    <td rowspan='3'>Дата заповнення реєстру</td>
                                </tr>
                                <tr>
                                    <td rowspan='2'>Місце державної реєстрації</td>
                                    <td rowspan='2'>Розміщення виробничих потужностей</td>
                                    <td rowspan='2'>КВЕД 2010 /розділ/група/клас</td>
                                    <td rowspan='2'>Ціль кредиту</td>
                                    <td rowspan='2'>Особистий вклад МСП</td>
                                    <td rowspan='2'>Номер</td>
                                    <td rowspan='2'>Дата</td>
                                    <td rowspan='2'>Строк</td>
                                    <td rowspan='2'>Сума, грн</td>
                                    <td rowspan='2'>Графік погашення</td>
                                    <td rowspan='2'>Кінцеве погашення</td>
                                    <td rowspan='2'>Ставка, %</td>
                                    <td colspan='3'>ФКП</td>
                                </tr>
                                <tr>
                                    <td>Щомісячна сума, грн</td>
                                    <td>Період виплат (не більше 24 міс.)</td>
                                    <td>Загальна сума, грн</td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>2</td>
                                    <td>3</td>
                                    <td>4</td>
                                    <td>5</td>
                                    <td>6</td>
                                    <td>7</td>
                                    <td>8</td>
                                    <td>9</td>
                                    <td>10</td>
                                    <td>11</td>
                                    <td>12</td>
                                    <td>13</td>
                                    <td>14</td>
                                    <td>15</td>
                                    <td>16</td>
                                    <td>17</td>
                                    <td>18</td>
                                    <td>19</td>
                                    <td>20</td>
                                    <td>21</td>
                                    <td>22</td>
                                    <td>23</td>
                                    <td>24</td>
                                    <td>25</td>
                                </tr>
                            </thead>
                            <tbody>
                                @if(isset($generalRegister) && count($generalRegister))
                                    @for($i = 0; $i < count($generalRegister); $i++)
                                        <tr>
                                            <td id="1">{{ $i+1 }}</td>
                                            <td id="2">{{ $generalRegister[$i]->col_2 }}</td>
                                            <td id="3">{{ $generalRegister[$i]->col_3 }}</td>
                                            <td id="4">{{ $generalRegister[$i]->col_4 }}</td>
                                            <td id="5">{{ $generalRegister[$i]->col_5 }}</td>
                                            <td id="6">{{ $generalRegister[$i]->col_6_section }} / {{ $generalRegister[$i]->col_6_group }} / {{ $generalRegister[$i]->col_6_clas }}</td>
                                            <td id="7">{{ $generalRegister[$i]->col_7 }}</td>
                                            <td id="8">{{ $generalRegister[$i]->col_8 }}</td>
                                            <td id="9">{{ $generalRegister[$i]->col_9 }}</td>
                                            <td id="10">{{ $generalRegister[$i]->col_10 }}</td>
                                            <td id="11">{{ $generalRegister[$i]->col_11/1000 }}</td>
                                            <td id="12">{{ $generalRegister[$i]->col_12 }}</td>
                                            <td id="13">{{ $generalRegister[$i]->col_13 }}</td>
                                            <td id="14">{{ $generalRegister[$i]->col_14 }}</td>
                                            <td id="15">{{ $generalRegister[$i]->col_15 }}</td>
                                            <td id="16">{{ $generalRegister[$i]->col_16 }}</td>
                                            <td id="17">{{ $generalRegister[$i]->col_17 }}</td>
                                            <td id="18">{{ \Carbon\Carbon::parse($generalRegister[$i]->col_18)->format('d.m.Y') }}</td>
                                            <td id="19">{{ $generalRegister[$i]->col_19 }}</td>
                                            <td id="20">{{ $generalRegister[$i]->col_20 }}</td>
                                            <td id="21">{{ $generalRegister[$i]->col_21 }}</td>
                                            <td id="22">{{ $generalRegister[$i]->col_22 }}</td>
                                            <td id="23">{{ $generalRegister[$i]->col_23 }}</td>
                                            <td id="24">{{ $generalRegister[$i]->name }}</td>
                                            <td id="25">{{ \Carbon\Carbon::parse($generalRegister[$i]->col_24)->format('d.m.Y') }}</td>
                                        </tr>
                                    @endfor
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>{{ $sum_11/1000 }}</td>
                                        <td>{{ $sum_12 }}</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>{{ $sum_16 }}</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>{{ $sum_20 }}</td>
                                        <td></td>
                                        <td>{{ $sum_22 }}</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                @else
                                    <tr><td colspan="25">Інформації немає</td></tr>
                                @endif
                                <input type="hidden" name="token" value="{{ csrf_token() }}">
                                <input type="hidden" name="url" value="registerOfApplicants">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            @if(isset($reports) && count($reports))
                <ol>
                    @for($i = 0; $i < count($reports); $i++)
                        <li>{{ $reports[$i]->name }}
                            <span style="float:right;">
                                <a href="../download/reports/{{$reports[$i]->url}}" class="btn btn-success btn-xs"><i class="fa fa-download"></i>&nbsp;Excel</a>
                            </span>
                        </li>
                    @endfor
                </ol>
            @endif
        @else
            <div class="col-xs-12 col-sm-6 col-sm-offset-3">
                <div class="alert alert-danger">
                    <p>
                        Ви не можете переглядати дану інформацію! Повернутися на <a href="/" title="Головна">головну</a>
                    </p>
                </div>
            </div>
        @endif
    </div>
@endsection