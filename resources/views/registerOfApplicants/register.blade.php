@extends('app')

@section('content')
    <div class="row">
        @if(Session::get('userId') == $idBank || Session::get('status') == 'departament')
            <div class="col-xs-12">
                <div class="alert alert-info">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <td rowspan='3'>№</td>
                                    <td rowspan='3' id="td-2" data-type="text">Назва</td>
                                    <td rowspan='3' id="td-3" data-type="text">ЄДРПОУ</td>
                                    <td colspan='2'>Критерій 1</td>
                                    <td>Критерій 2</td>
                                    <td>Критерій 3</td>
                                    <td>Критерій 4</td>
                                    <td rowspan='3' id="td-9" data-type="bools">Відсутність просроченого боргу</td>
                                    <td rowspan='3' id="td-10" data-type="bools">Незалежність МСП</td>
                                    <td rowspan='3' id="td-11" data-type="numero-thousand">Річний дохід, тис.грн</td>
                                    <td rowspan='3' id="td-12" data-type="numero">Чисельність персоналу, одиниць</td>
                                    <td colspan='10'>Кредитний догорів</td>
                                    <td rowspan='3' id="td-23" data-type="bools">Результат розгляду (*)</td>
                                    <td rowspan='3'>Дата заповнення реєстру</td>
                                </tr>
                                <tr>
                                    <td rowspan='2' id="td-4" data-type="text">Місце державної реєстрації</td>
                                    <td rowspan='2' id="td-5" data-type="text">Розміщення виробничих потужностей</td>
                                    <td rowspan='2' id="td-6" data-type="kved">КВЕД 2010 /розділ/група/клас</td>
                                    <td rowspan='2' id="td-7" data-type="target">Ціль кредиту</td>
                                    <td rowspan='2' id="td-8" data-type="text">Особистий вклад МСП</td>
                                    <td rowspan='2' id="td-13" data-type="text">Номер</td>
                                    <td rowspan='2' id="td-14" data-type="stacking">Дата</td>
                                    <td rowspan='2' id="td-15" data-type="term">Строк</td>
                                    <td rowspan='2' id="td-16" data-type="numero">Сума, грн</td>
                                    <td rowspan='2' id="td-17" data-type="repayment_schedule">Графік погашення</td>
                                    <td rowspan='2' id="td-18" data-type="date">Кінцеве погашення</td>
                                    <td rowspan='2' id="td-19" data-type="numero">Ставка, %</td>
                                    <td colspan='3'>ФКП</td>
                                </tr>
                                <tr>
                                    <td id="td-20" data-type="numero">Щомісячна сума, грн</td>
                                    <td id="td-21" data-type="text">Період виплат (не більше 24 міс.)</td>
                                    <td id="td-22" data-type="numero">Загальна сума, грн</td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>2</td>
                                    <td>3</td>
                                    <td>4</td>
                                    <td>5</td>
                                    <td>6</td>
                                    <td>7</td>
                                    <td>8</td>
                                    <td>9</td>
                                    <td>10</td>
                                    <td>11</td>
                                    <td>12</td>
                                    <td>13</td>
                                    <td>14</td>
                                    <td>15</td>
                                    <td>16</td>
                                    <td>17</td>
                                    <td>18</td>
                                    <td>19</td>
                                    <td>20</td>
                                    <td>21</td>
                                    <td>22</td>
                                    <td>23</td>
                                    <td class="lastNumeroRow">24</td>
                                </tr>
                            </thead>
                            <tbody class="{{ (Session::get('status') != 'departament') ? 'register' : '' }}">
                                @if(isset($applicantsRegister) && count($applicantsRegister))
                                    @for($i = 0; $i < count($applicantsRegister);$i++)
                                        <tr id="{{ $i+1 }}" data-id="{{ $applicantsRegister[$i]->id }}">
                                            <td id="action">{{ $i+1 }}
                                                @if(Session::get('userId') == $idBank)
                                                    <i class="fa fa-times deleteRow" title="Видалити"></i>
                                                @endif
                                            </td>
                                            <td id="2">{{ $applicantsRegister[$i]->col_2 }}</td>
                                            <td id="3">{{ $applicantsRegister[$i]->col_3 }}</td>
                                            <td id="4">{{ $applicantsRegister[$i]->col_4 }}</td>
                                            <td id="5">{{ $applicantsRegister[$i]->col_5 }}</td>
                                            <td id="6">{{ $applicantsRegister[$i]->col_6_section }} / {{ $applicantsRegister[$i]->col_6_group }} / {{ $applicantsRegister[$i]->col_6_clas }}</td>
                                            <td id="7">{{ $applicantsRegister[$i]->col_7 }}</td>
                                            <td id="8">{{ $applicantsRegister[$i]->col_8 }}</td>
                                            <td id="9">{{ $applicantsRegister[$i]->col_9 }}</td>
                                            <td id="10">{{ $applicantsRegister[$i]->col_10 }}</td>
                                            <td id="11">{{ $applicantsRegister[$i]->col_11/1000 }}</td>
                                            <td id="12">{{ $applicantsRegister[$i]->col_12 }}</td>
                                            <td id="13">{{ $applicantsRegister[$i]->col_13 }}</td>
                                            <td id="14">{{ $applicantsRegister[$i]->col_14 }}</td>
                                            <td id="15">{{ $applicantsRegister[$i]->col_15 }}</td>
                                            <td id="16">{{ $applicantsRegister[$i]->col_16 }}</td>
                                            <td id="17" data-old="{{ $applicantsRegister[$i]->repayment_schedule_id }}">{{ $applicantsRegister[$i]->col_17 }}</td>
                                            <td id="18">{{ \Carbon\Carbon::parse($applicantsRegister[$i]->col_18)->format('d.m.Y') }}</td>
                                            <td id="19">{{ $applicantsRegister[$i]->col_19 }}</td>
                                            <td id="20">{{ $applicantsRegister[$i]->col_20 }}</td>
                                            <td id="21">{{ $applicantsRegister[$i]->col_21 }}</td>
                                            <td id="22">{{ $applicantsRegister[$i]->col_22 }}</td>
                                            <td id="23">{{ $applicantsRegister[$i]->col_23 }}</td>
                                            <td id="24">{{ \Carbon\Carbon::parse($applicantsRegister[$i]->col_24)->format('d.m.Y') }}</td>
                                        </tr>
                                    @endfor
                                @endif
                                @if(Session::get('status') == 'departament')
                                    @if(!isset($applicantsRegister) || count($applicantsRegister) == 0)
                                        <tr><td colspan="24">Немає інформації</td></tr>
                                    @endif
                                @endif
                                @if(Session::get('userId') == $idBank)
                                    <tr class="lastRow">
                                        <td><i class="fa fa-plus green addRow" title="Додати"></i></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <input type="hidden" name="urlType" data-url="/registerOfApplicants/"/>
                                    <input type="hidden" name="token" value="{{ csrf_token() }}" />
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        @else
            <div class="col-xs-12 col-sm-6 col-sm-offset-3">
                <div class="alert alert-danger">
                    <p>
                        Ви не можете переглядати іформацію даного банку! Повернутися на <a href="/" title="Головна">головну</a>
                    </p>
                </div>
            </div>
        @endif
    </div>
    @if(Session::get('userId') == $idBank)
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-sm-offset-3">
                <div class="alert alert-info">
                    <strong>Увага!</strong> Для того, щоб додати та зберегти дані натисніть клавішу "Enter"
                </div>
            </div>
        </div>
    @endif
@endsection