@extends('app')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <ul class="nav nav-tabs">
              <li class="{{ ($active == 'bank') ? 'active' : ''}}"><a href="#bank"data-toggle="tab">Банк</a></li>
              <li class="{{ ($active == 'borrowers') ? 'active' : ''}}"><a href="#borrowers"data-toggle="tab">Реєстр позичальників</a></li>
              <li class="{{ ($active == 'target') ? 'active' : ''}}"><a href="#target"data-toggle="tab">Ціль кредиту</a></li>
              <li class="{{ ($active == 'kved') ? 'active' : ''}}"><a href="#kved"data-toggle="tab">КВЕД 2010</a></li>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">

            <div class="tab-content alert alert-info">
                @if(isset($error))
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-sm-offset-3 alert alert-danger" name="alert-close" role="alert">
                            <div class="">
                                <h5>{{ $error }}</h5>
                            </div>
                        </div>
                    </div>
                @endif
                @if(isset($success))
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-sm-offset-3 alert alert-success" name="alert-close" role="alert">
                            <div class="">
                                <h5>{{ $success }}</h5>
                            </div>
                        </div>
                    </div>
                @endif

                <div id="bank" class="tab-pane fade {{ ($active == 'bank') ? 'in active' : ''}}">
                    <h3 class="text-center">Банки</h3>
                    <div class="alert-info">
                        <table class="table">
                            <thead>
                                <tr>
                                    <td>#</td>
                                    <td>Користувач</td>
                                    <td>Назва</td>
                                    <td>Дія</td>
                                </tr>
                            </thead>
                            <tbody>
                                @if(isset($users) && count($users))
                                    @for($i = 0; $i < count($users); $i++)
                                        <tr id="{{ $users[$i]->id }}">
                                            <td>{{ $i+1 }}</td>
                                            <td class="text-left">{{ $users[$i]->login }}</td>
                                            <td class="text-left">{{ $users[$i]->name }}</td>
                                            <td>
                                                <i class="fa fa-pencil green" name="editBank" title="Редагувати"></i>
                                                <i class="fa fa-times red" name="deleteBank" title="Видалити"></i>
                                            </td>
                                        </tr>
                                    @endfor
                                @else
                                    <tr><td colspan="4">Банків немає</td></tr>
                                @endif
                                <tr>
                                    <td colspan="4">
                                        <div class="col-xs-12 col-sm-6 col-sm-offset-3">
                                            <h3>Створити банк</h3>
                                            <form action="/admin/createBank" method="POST">
                                                {{  csrf_field()}}
                                                <div class="form-group">
                                                    <label for="loginBank">Користувач (login)</label>
                                                    <input class="form-control" id="loginBank" type="text" name="loginBank" value=""/>
                                                    @if(isset($errorValid) && $errorValid->has('login'))
                                                        <h5 class="alert alert-danger" name="alert-close" role="alert">{{ $errorValid->first('login') }}</h5>
                                                    @endif
                                                </div>
                                                <div class="form-group">
                                                    <label for="nameBank">Назва</label>
                                                    <input class="form-control" id="nameBank" type="text" name="nameBank" value=""/>
                                                    @if(isset($errorValid) && $errorValid->has('name'))
                                                        <h5 class="alert alert-danger" name="alert-close" role="alert">{{ $errorValid->first('name') }}</h5>
                                                    @endif
                                                </div>
                                                <div class="form-group">
                                                    <label for="passwordBank">Пароль</label>
                                                    <input class="form-control" id="passwordBank" type="password" name="passwordBank" value=""/>
                                                    @if(isset($errorValid) && $errorValid->has('password'))
                                                        <h5 class="alert alert-danger" name="alert-close" role="alert">{{ $errorValid->first('password') }}</h5>
                                                    @endif
                                                </div>
                                                <input type="submit" class="btn btn-primary" name="createBank" value="Створити"/>
                                            </form>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div id="borrowers" class="tab-pane fade {{ ($active == 'borrowers') ? 'in active' : ''}}">
                    <h3 class="text-center">Реєстр позичальників на надання ФКП</h3>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-sm-offset-3">
                            <div class="alert alert-info">
                                @if(isset($default) && !empty($default))
                                    <div class="alert alert-success">
                                        <p>На поточний рік дані внесені! Нові зміни не будуть збережені!</p>
                                    </div>
                                @else
                                    <div class="alert alert-danger">
                                        <p>Внесіть дані на поточний рік</p>
                                    </div>
                                @endif
                                <form action="borrowers/default" method="POST">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <label for="borrowersDefault">Сума коштів ФКП, затверджена в бюджеті м.Києва на відповідний рік, грн</label>
                                        <input class="form-control" id="borrowersDefault" name="borrowersDefault" value="{{ isset($default) ? $default[0]->value : '' }}"/>
                                    </div>
                                    <input class="btn btn-primary" type="{{ isset($default) ? 'button' : 'submit' }}" value="Зберегти"/>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="target" class="tab-pane fade {{ ($active == 'target') ? 'in active' : ''}}">
                    <h3 class="text-center">Ціль кредиту</h3>
                    <div class="alert-info">
                        <table class="table">
                            <thead>
                                <tr>
                                    <td>#</td>
                                    <td>Ціль кредиту</td>
                                    <td>Дія</td>
                                </tr>
                            </thead>
                            <tbody>
                                @if(isset($targets) && count($targets))
                                    @for($i = 0; $i < count($targets); $i++)
                                        <tr id="{{ $targets[$i]->id }}">
                                            <td>{{ $i+1 }}</td>
                                            <td class="text-left">{{ $targets[$i]->target }}</td>
                                            <td>
                                                <i class="fa fa-pencil green" name="editTarget" title="Редагувати"></i>
                                                <i class="fa fa-times red" name="deleteTarget" title="Видалити"></i>
                                            </td>
                                        </tr>
                                    @endfor
                                @else
                                    <tr><td colspan="4">Цілей кредиту немає</td></tr>
                                @endif
                                <tr>
                                    <td colspan="4">
                                        <div class="col-xs-12 col-sm-6 col-sm-offset-3">
                                            <h3>Додати ціль кредиту</h3>
                                            <form action="/admin/createTarget" method="POST">
                                                {{  csrf_field()}}
                                                <div class="form-group">
                                                    <label for="nameTarget">Назва</label>
                                                    <input class="form-control" id="nameTarget" type="text" name="nameTarget" value=""/>
                                                    @if(isset($errorValid) && $errorValid->has('name'))
                                                        <h5 class="alert alert-danger" name="alert-close" role="alert">{{ $errorValid->first('name') }}</h5>
                                                    @endif
                                                </div>
                                                <input type="submit" class="btn btn-primary" name="createTarget" value="Створити"/>
                                            </form>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div id="kved" class="tab-pane fade {{ ($active == 'kved') ? 'in active' : ''}}">
                    <h3 class="text-center">КВЕД 2010</h3>
                    <div class="alert-info">
                        <table class="table">
                            <thead>
                                <tr>
                                    <td>#</td>
                                    <td>КВЕД 2010</td>
                                    <td>Дія</td>
                                </tr>
                            </thead>
                            <tbody>
                                @if(isset($kveds) && count($kveds))
                                    @for($i = 0, $n = 0; $i < count($kveds); $i++)
                                        @if($kveds[$i]['parent_kved'] == null)
                                            <tr id="kved-{{ $kveds[$i]['id'] }}" data-kved="{{ $kveds[$i]['id'] }}">
                                                <td>{{ $n = $n + 1 }}</td>
                                                <td class="text-left">
                                                    <b>розділ: </b><span id="bodyKved-{{ $kveds[$i]['id'] }}">{{ $kveds[$i]['kved'] }}</span><br>
                                                    @for($j = 0; $j < count($kveds); $j++)
                                                        @if($kveds[$i]['id'] == $kveds[$j]['parent_kved'])
                                                            <span style="margin-left: 15px;"><b>група: </b>{{ $kveds[$j]['kved'] }}</span></br>
                                                            @for($m = 0; $m < count($kveds); $m++)
                                                                @if($kveds[$j]['id'] == $kveds[$m]['parent_kved'])
                                                                    <span style="margin-left: 30px;"><b>клас: </b>{{ $kveds[$m]['kved'] }}</span></br>
                                                                @endif
                                                            @endfor
                                                        @endif
                                                    @endfor
                                                </td>
                                                <td>
                                                    <i class="fa fa-pencil green" name="editKved" data-kved="{{ $kveds[$i]['id'] }}" title="Редагувати"></i>
                                                    <i class="fa fa-times red" name="deleteKved" title="Видалити"></i>
                                                </td>
                                            </tr>
                                        @endif
                                    @endfor
                                @else
                                    <tr><td colspan="4">КВЕД 2010 немає</td></tr>
                                @endif
                                <tr>
                                    <td colspan="4">
                                        <div class="col-xs-12 col-sm-6 col-sm-offset-3">
                                            <h3>Додати КВЕД 2010</h3>
                                            <form action="/admin/createKved" method="POST">
                                                {{  csrf_field()}}
                                                <div class="form-group">
                                                    <label for="newKved">КВЕД 2010</label>
                                                    <input class="form-control" id="newKved" type="text" name="newKved" value=""/>
                                                    @if(isset($errorValid) && $errorValid->has('name'))
                                                        <h5 class="alert alert-danger" name="alert-close" role="alert">{{ $errorValid->first('name') }}</h5>
                                                    @endif
                                                </div>
                                                <input type="submit" class="btn btn-primary" name="createKved" value="Додати"/>
                                            </form>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection