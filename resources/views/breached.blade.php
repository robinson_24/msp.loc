@extends('app')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            @if(isset($breached) && count($breached))
                <ol>
                    @for($i = 0; $i < count($breached); $i++)
                        <li>{{ $breached[$i]->name }}
                            @if(!empty($breached[$i]->url_pdf) || !empty($breached[$i]->url_word))
                                <span style="float:right;">
                                    @if(!empty($breached[$i]->url_pdf))
                                        <a href="../download/breached/{{$breached[$i]->url_pdf}}" class="btn btn-success btn-xs"><i class="fa fa-download"></i>&nbsp;PDF</a>
                                    @endif
                                    @if(!empty($breached[$i]->url_word))
                                        <a href="../download/breached/{{$breached[$i]->url_word}}" class="btn btn-info btn-xs"><i class="fa fa-download"></i>&nbsp;Word</a>
                                    @endif
                                    @if(Session::get('status') == 'departament')
                                        <a href="/delete/breached/{{ $breached[$i]->id }}" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i></a>
                                    @endif
                                </span>
                            @endif
                        </li>
                    @endfor
                </ol>
            @endif
        </div>
    </div>
    @if(Session::has('userId') && Session::get('status') == 'departament')
        <div class="row">
            <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 alert" style="background-color:rgba(217, 237, 247, 1.0);">
                <form action="/breached/add" method="post" enctype="multipart/form-data">
                    <input class="form-control" type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-group">
                        <input type="text" class="form-control" name="name" id="name" placeholder="Назва документу">
                    </div>
                    @if(!empty($error) && $error->has('name'))
                        <p class="bg-danger">{{$error->first('name')}}</p>
                    @endif
                    <div class="form-group">
                        <label for="pdf">PDF-файл документу</label>
                        <input type="file" id="pdf" name="pdf">
                        <p class="help-block">Файли з форматом pdf.</p>
                    </div>
                    <div class="form-group">
                        <label for="word">Word-файл документу</label>
                        <input type="file" id="word" name="word">
                        <p class="help-block">Файли з форматом doc, docx.</p>
                    </div>
                    <button type="submit" class="btn btn-default">Додати</button>
                </form>
            </div>
        </div>
    @endif
@endsection