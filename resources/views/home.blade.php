@extends('app')

@section('content')
    <div class="row text-center">
        <div class="col-xs-12 col-sm-3 col-sm-offset-3">
            <a href="/regulatoryDocuments">
                <div class="border">
                    <p>Нормативно-розпорядчі документи щодо Програми ФКП суб'єктів МСП</p>
                </div>
            </a>
        </div>
        <div class="col-xs-12 col-sm-3">
            <a href="/gufAndPartnerBanks">
                <div class="border">
                    <p>Німецько-український фонд та банки-партнери</p>
                </div>
            </a>
        </div>
    </div>
    <div class="row text-center second-row">
        <div class="col-xs-12 col-sm-4">
            <a href="/registerOfApplicants">
                <div class="border">
                    <p>Реєстр* погодження позичальників, які претендують на ФКП</p>
                </div>
            </a>
        </div>
        <div class="col-xs-12 col-sm-4">
            <a href="/registerOfBorrowers">
                <div class="border">
                    <p>Реєстр** позичальників на надання ФКП</p>
                </div>
            </a>
        </div>
        <div class="col-xs-12 col-sm-4">
            <a href="/performanceOfImplementation">
                <div class="border">
                    <p>Інформація щодо показників результативності впроваждення ФКП</p>
                </div>
            </a>
        </div>
    </div>
    <div class="row text-center second-row">
        <div class="col-xs-12 col-sm-3">
            <a href="/breached">
                <div class="border">
                    <p>Суб'єкти МСП, які порушили умови кредитних договорів</p>
                </div>
            </a>
        </div>
        <div class="col-xs-12 col-sm-3">
            <a href="/refusals">
                <div class="border">
                    <p>Суб'єкти МСП, яким відмовиили у ФКП</p>
                </div>
            </a>
        </div>
        <div class="col-xs-12 col-sm-3">
            <a href="/examples">
                <div class="border">
                    <p>Цікаві приклади суб'єктів МСП, які отримали ФКП</p>
                </div>
            </a>
        </div>
        <div class="col-xs-12 col-sm-3">
            <a href="/infographics">
                <div class="border">
                    <p>Інфографіка</p>
                </div>
            </a>
        </div>
    </div>
@endsection