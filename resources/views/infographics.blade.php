@extends('app')

@section('content')

    @if(isset($error))
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-sm-offset-3 alert alert-danger" name="alert-close" role="alert">
                <div class="">
                    <h5>{{ $error }}</h5>
                </div>
            </div>
        </div>
    @endif
    @if(isset($success))
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-sm-offset-3 alert alert-success" name="alert-close" role="alert">
                <div class="">
                    <h5>{{ $success }}</h5>
                </div>
            </div>
        </div>
    @endif

    <div class="row">
        <div class="col-xs-12">
            {!! (isset($information)) ? $information->body : '' !!}
        </div>
    </div>
    @if(Session::has('userId') && Session::get('status') == 'departament')
        <div class="row">
            <div class="col-xs-12 text-center">
                <form action="/infographics/{{ (!isset($information)) ? 'addInformation' : 'editInformation' }}" method="POST">
                    {{ csrf_field() }}
                    <input type="hidden" name="idInformation" value="{{ $information->id}}">
                    <textarea name="editorInformation" id="editorInformation" rows="10" cols="80">
                        {{ (isset($information) && !empty($information->body)) ? $information->body : null }}
                    </textarea>
                    <button type="submit" class="btn btn-primary" name="{{ (!isset($information)) ? 'addInformation' : 'editInformation' }}">Зберегти</button>
                </form>
                @for($i = 0; $i < count($images); $i++)
                    @if(!empty($images[$i]['url']))
                        <div class="col-xs-12 col-sm-3">
                            <div class="img-thumbnail">
                                <img src="/images/information/{{ $images[$i]['url'] }}" data-id="{{ $images[$i]['idImage'] }}" alt="Image" class=" img-responsive">
                                <div class="caption">
                                    <p style="margin-top:12px;">
                                        <button class="btn btn-primary" name="copyUrl" data-url="/images/information/{{ $images[$i]['url'] }}">Копіювати посилання</button>
                                        <button class="btn btn-primary" name="deleteUrl" data-url="images/information/{{ $images[$i]['url'] }}" data-id="{{ $images[$i]['idImage'] }}" title="Видалити зображення"><i class="fa fa-times red"></i></button>
                                    </p>
                                </div>
                            </div>
                        </div>
                    @endif
                @endfor
                <p>
                    <form action="/addInfographicsImage" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="hidden" name="idInformation" value="{{ $information->id}}">
                        <label for="addImage" class="btn btn-primary" title="Натисніть, щоб додати">Вибрати зображення</label>
                        <input type="file" style="display:none;" id="addImage" name="img" accept="image/jpeg,image/png,image/gif">
                    </form>
                </p>
            </div>
        </div>
    @endif
    <script>
        CKEDITOR.replace( 'editorInformation' );
    </script>
@endsection