<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBorrowersRegisterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('borrowersRegister', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('id_bank')->comment('id банка');
            $table->text('col_2')->nullable()->comment('назва, ЄДРПОУ');
            $table->text('col_3')->nullable()->comment('місце державної реєстрації, розміщення виробничих потужностей');
            $table->text('col_4_section')->nullable()->comment('КВЕД 2010 розділ');
            $table->text('col_4_group')->nullable()->comment('КВЕД 2010 група');
            $table->text('col_4_clas')->nullable()->comment('КВЕД 2010 клас');
            $table->text('col_5')->nullable()->comment('Цільове призначення');
            $table->double('col_6', 15,2)->default(0)->comment('Власний внесок позичальника');
            $table->date('col_7')->nullable()->comment('Дата погодження надання');
            $table->text('col_8')->nullable()->comment('номер, дата');
            $table->tinyInteger('col_9')->nullable()->comment('связка с таблицей term');
            $table->double('col_10', 15,2)->default(0)->comment('сума кредиту на початок звітного місяця, тис.грн');
            $table->double('col_11', 15,2)->default(0)->comment('Відсоткова ставка, %');
            $table->double('col_12', 15,2)->default(0)->comment('Загальна сума розрахованих відсотків у звітному місяця, грн');
            $table->double('col_13', 15,2)->default(0)->comment('Частка компенсації відсотковоїставки, %');
            $table->double('col_14', 15,2)->default(0)->comment('Сума ФКП за звітний місяць, грн');
            $table->double('col_15', 15,2)->default(0)->comment('Загальний розмір ФКП за весь період, грн');
            $table->double('col_16', 15,2)->default(0)->comment('Плановий розмір ФКП в місяці, наступному за звітним періодом, грн');
            $table->double('col_17', 15,2)->default(0)->comment('Залишковий розмір ФКП, тис.грн');
            $table->integer('col_18')->nullable()->comment('Порядковий номер місця участі в ФКП (загальний термін 24 місяці)');
            $table->date('col_19')->nullable()->comment('Дата надання реєстру');
            $table->timestamps();
            $table->charset = 'utf8';
            $table->collation = 'utf8_general_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('borrowersRegister');
    }
}
