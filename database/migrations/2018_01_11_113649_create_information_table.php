<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('information', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('id_bank')->nullable()->comment('id банка');
            $table->string('category', 255)->nullable()->comment('категория');
            $table->text('body')->nullable()->comment('информация');
            $table->timestamps();
            $table->charset = 'utf8';
            $table->collation = 'utf8_general_ci';
        });

        DB::table('information')->insert(['category' => 'guf']);
        DB::table('information')->insert(['category' => 'examples']);
        DB::table('information')->insert(['category' => 'infographics']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('information');
    }
}
