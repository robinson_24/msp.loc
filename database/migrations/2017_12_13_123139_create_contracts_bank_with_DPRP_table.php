<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContractsBankWithDPRPTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contracts_bank_with_DPRP', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('id_bank')->nullable()->index()->comment('id банка');
            $table->text('name')->comment('наименование контракта');
            $table->string('url_pdf', 500)->nullable()->comment('ссылка на pdf-файл контракта');
            $table->timestamps();
            $table->charset = 'utf8';
            $table->collation = 'utf8_general_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contracts_bank_with_DPRP');
    }
}
