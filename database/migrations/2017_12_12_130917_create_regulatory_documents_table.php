<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegulatoryDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('regulatory_documents', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('category', 255)->nullable()->comment('категория регуляторных документов');
            $table->text('name')->comment('наименование регуляторного документа');
            $table->string('url_pdf', 500)->nullable()->comment('ссылка на pdf-файл регуляторного документа');
            $table->string('url_word', 500)->nullable()->comment('ссылка на word-файл регуляторного документа');
            $table->timestamps();
            $table->charset = 'utf8';
            $table->collation = 'utf8_general_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('regulatory_documents');
    }
}
