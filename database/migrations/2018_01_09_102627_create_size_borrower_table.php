<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSizeBorrowerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('size_borrower', function (Blueprint $table) {
            $table->increments('id');
            $table->string('size_borrower', 10)->comment('мале/середнє');
            $table->timestamps();
            $table->charset = 'utf8';
            $table->collation = 'utf8_general_ci';
        });

        DB::table('size_borrower')->insert(['size_borrower' => 'Мале']);
        DB::table('size_borrower')->insert(['size_borrower' => 'Середнє']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('size_borrower');
    }
}
