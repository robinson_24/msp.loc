<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('login')->comment('login пользователя');
            $table->string('name')->comment('name пользователя');
            $table->string('password', 255)->comment('пароль');
            $table->string('salt', 255);
            $table->string('status', 20)->comment('departament - Департамент, bank - Банк');
            $table->tinyInteger('is_delete')->default(0)->comment('1-удален банк, 0-не удален банк');
            $table->timestamps();
            $table->charset = 'utf8';
            $table->collation = 'utf8_general_ci';
        });

        DB::table('users')->insert([
            'login' => 'departament',
            'name' => 'Департамент',
            'password' => '045353ed44a58ad1f59cc3c8561d569837c62e181df5c006e47132fd055cbacc',
            'salt' => '6d15b28d7dda',
            'status' => 'departament',
            'is_delete' => 0
            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
