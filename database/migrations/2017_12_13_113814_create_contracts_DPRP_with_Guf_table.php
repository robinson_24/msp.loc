<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContractsDPRPWithGufTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contracts_DPRP_with_Guf', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('name')->comment('наименование контракта');
            $table->string('url_pdf', 500)->nullable()->comment('ссылка на pdf-файл регуляторного документа');
            $table->timestamps();
            $table->charset = 'utf8';
            $table->collation = 'utf8_general_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contracts_DPRP_with_Guf');
    }
}
