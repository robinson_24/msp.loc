<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBreachedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('breached', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('name')->comment('наименование файла');
            $table->string('url_pdf', 500)->nullable()->comment('ссылка на pdf-файл');
            $table->string('url_word', 500)->nullable()->comment('ссылка на word-файл');
            $table->timestamps();
            $table->charset = 'utf8';
            $table->collation = 'utf8_general_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('breached');
    }
}
