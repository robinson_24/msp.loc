<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTermTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('term', function (Blueprint $table) {
            $table->increments('id');
            $table->string('term')->comment('строк');
            $table->timestamps();
            $table->charset = 'utf8';
            $table->collation = 'utf8_general_ci';
        });

        DB::table('term')->insert(['term' => '6 місяців']);
        DB::table('term')->insert(['term' => '12 місяців']);
        DB::table('term')->insert(['term' => '18 місяців']);
        DB::table('term')->insert(['term' => '24 місяці']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('term');
    }
}
