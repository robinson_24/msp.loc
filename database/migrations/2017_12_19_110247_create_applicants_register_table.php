<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApplicantsRegisterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('applicantsRegister', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('id_bank')->comment('id банка');
            $table->text('col_2')->nullable()->comment('назва');
            $table->text('col_3')->nullable()->comment('ЄДРПОУ');
            $table->text('col_4')->nullable()->comment('місце державної реєстрації');
            $table->text('col_5')->nullable()->comment('розміщення виробничих потужностей');
            $table->text('col_6_section')->nullable()->comment('КВЕД 2010 розділ');
            $table->text('col_6_group')->nullable()->comment('КВЕД 2010 група');
            $table->text('col_6_clas')->nullable()->comment('КВЕД 2010 клас');
            $table->text('col_7')->nullable()->comment('Ціль кредиту');
            $table->text('col_8')->nullable()->comment('Особистий вклад МСП');
            $table->tinyInteger('col_9')->nullable()->comment('связка с таблицей bool');
            $table->text('col_10')->nullable()->comment('Незалежність МСП');
            $table->double('col_11', 15,2)->default(0)->comment('Річний дохід, грн');
            $table->integer('col_12')->default(0)->comment('чисельність персоналу');
            $table->text('col_13')->nullable()->comment('номер');
            $table->tinyInteger('col_14')->nullable()->comment('связка с таблицей stacking');
            $table->tinyInteger('col_15')->nullable()->comment('связка с таблицей term');
            $table->double('col_16', 15,2)->default(0)->comment('сума');
            $table->text('col_17')->nullable()->comment('Графік погашення');
            $table->text('col_18')->nullable()->comment('Кінцеве погашення');
            $table->double('col_19', 10, 5)->default(0)->comment('ставка');
            $table->double('col_20', 15,2)->default(0)->comment('Щомісячна сума, грн');
            $table->text('col_21')->nullable()->comment('Період виплат (не більше 24 міс.)');
            $table->double('col_22', 15,2)->default(0)->comment('Загальна сума');
            $table->tinyInteger('col_23')->nullable()->comment('связка с таблицей bool');
            $table->date('col_24')->nullable()->comment('Дата заповнення реєстру');
            $table->timestamps();
            $table->charset = 'utf8';
            $table->collation = 'utf8_general_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('applicantsRegister');
    }
}
