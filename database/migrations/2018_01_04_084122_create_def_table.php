<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDefTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('def', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_user')->nullable()->comment('id пользователя который установил значение по умолчанию');
            $table->string('value')->nullable()->comment('знаечение по умолчанию');
            $table->string('type')->nullable()->comment('к чему установлено знаечение по умолчанию');
            $table->timestamps();
            $table->charset = 'utf8';
            $table->collation = 'utf8_general_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('def');
    }
}
