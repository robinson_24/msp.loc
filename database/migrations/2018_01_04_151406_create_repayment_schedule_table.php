<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRepaymentScheduleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('repayment_schedule', function (Blueprint $table) {
            $table->increments('id');
            $table->string('repayment_schedule')->comment('график погашения');
            $table->tinyInteger('is_default')->default(1)->comment('1-по умолчанию, 0-не по умолчанию');
            $table->timestamps();
            $table->charset = 'utf8';
            $table->collation = 'utf8_general_ci';
        });
        
        DB::table('repayment_schedule')->insert([
            'repayment_schedule' => 'Ануїтет',
            'is_default' => 1
        ]);
        
        DB::table('repayment_schedule')->insert([
            'repayment_schedule' => 'Стандарт',
            'is_default' => 1
        ]);
        
        DB::table('repayment_schedule')->insert([
            'repayment_schedule' => 'В кінці року',
            'is_default' => 1
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('repayment_schedule');
    }
}
