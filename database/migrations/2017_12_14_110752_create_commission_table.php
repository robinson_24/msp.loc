<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommissionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('commission', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('category', 255)->comment('категория "Наказ" или "Протокалы"');
            $table->text('name')->comment('наименование "Наказ" или "Протокалы"');
            $table->string('url_pdf', 500)->nullable()->comment('ссылка на pdf-файл "Наказ" или "Протокалы"');
            $table->timestamps();
            $table->charset = 'utf8';
            $table->collation = 'utf8_general_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('commission');
    }
}
