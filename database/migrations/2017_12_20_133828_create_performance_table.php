<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePerformanceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('performance', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('id_bank')->comment('id банка');
            $table->text('col_2')->nullable()->comment('назва позичальника, ЄДРПОУ');
            $table->tinyInteger('col_3')->nullable()->comment('связка с таблицей size_borrower');
            $table->text('col_4_section')->nullable()->comment('КВЕД 2010 розділ');
            $table->text('col_4_group')->nullable()->comment('КВЕД 2010 група');
            $table->text('col_4_clas')->nullable()->comment('КВЕД 2010 клас');
            $table->text('col_5')->nullable()->comment('№, дата кредитного договору');
            $table->text('col_6')->nullable()->comment('Термін кредитного договору');
            $table->text('col_7')->nullable()->comment('Цільове призначення кредиту');
            $table->double('col_8', 15,2)->default(0)->comment('Сума кредитну на початок звітного місяця, тис.грн');
            $table->double('col_9', 15,2)->default(0)->comment('Власний внесок позичальника');
            $table->double('col_10', 15,2)->default(0)->comment('Відсоткова ставка, %');
            $table->double('col_11', 15,2)->default(0)->comment('Загальна сума розрахованих відсотків у звітному місяця, грн');
            $table->double('col_12', 15,2)->default(0)->comment('Сума ФКП за звітний місяць, грн');
            $table->double('col_13', 15,2)->default(0)->comment('Кількість створених позичальником нових робочих місць ->попередній період');
            $table->double('col_14', 15,2)->default(0)->comment('Кількість створених позичальником нових робочих місць ->звітній період');
            $table->double('col_15', 15,2)->default(0)->comment('Відрахування позичальником податків і зборів до державного бюджету та бюджету м.Києва ->попередній період');
            $table->double('col_16', 15,2)->default(0)->comment('Відрахування позичальником податків і зборів до державного бюджету та бюджету м.Києва ->звітній період');
            $table->double('col_17', 15,2)->default(0)->comment('Зростання обсягу виробництва та раелізації товарів, робіт та послуг ->попередній період');
            $table->double('col_18', 15,2)->default(0)->comment('Зростання обсягу виробництва та раелізації товарів, робіт та послуг ->звітній період');
            $table->text('col_19')->nullable()->comment('Що зроблено позичальником по реалізації кредитного проекту');
            $table->date('col_20')->nullable()->comment('Дата надання інформації');
            $table->timestamps();
            $table->charset = 'utf8';
            $table->collation = 'utf8_general_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('performance');
    }
}
