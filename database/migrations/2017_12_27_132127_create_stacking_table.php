<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStackingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stacking', function (Blueprint $table) {
            $table->increments('id');
            $table->string('stacking')->comment('дата укладання кредитного договору');
            $table->timestamps();
            $table->charset = 'utf8';
            $table->collation = 'utf8_general_ci';
        });

        DB::table('stacking')->insert(['stacking' => 'щомісячно']);
        DB::table('stacking')->insert(['stacking' => 'щоквартально']);
        DB::table('stacking')->insert(['stacking' => 'щорічно']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('stacking');
    }
}
