<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTargetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('target', function (Blueprint $table) {
            $table->increments('id');
            $table->text('target')->comment('цель');
            $table->timestamps();
            $table->charset = 'utf8';
            $table->collation = 'utf8_general_ci';
        });

        DB::table('target')->insert(['target' => 'придбання устаткування, обладнаяння та інших основних засобів виробничого призначення']);
        DB::table('target')->insert(['target' => 'модернізацію технологічного процесу виробництва або основних засобів(машин, обладнаня тощо) для розширення діючого або створення нового виробництва, зниження собівартості']);
        DB::table('target')->insert(['target' => 'придбання, будівництво або реконструкцію приміщень, необхідних для розширення діючого або створення нового виробництва']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('target');
    }
}
