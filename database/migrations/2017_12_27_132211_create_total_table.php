<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTotalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('total', function (Blueprint $table) {
            $table->increments('id');
            $table->string('total')->comment('сумма (кредитного договору)');
            $table->timestamps();
            $table->charset = 'utf8';
            $table->collation = 'utf8_general_ci';
        });
        
        DB::table('total')->insert(['total' => 'менше 100 тис.грн']);
        DB::table('total')->insert(['total' => 'від 100 тис.грн до 500 тис.грн']);
        DB::table('total')->insert(['total' => 'від 500 тис.грн до 1000 тис.грн']);
        DB::table('total')->insert(['total' => 'від 1000 тис.грн до 5000 тис.грн']);
        DB::table('total')->insert(['total' => 'більше 5000 тис.грн']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('total');
    }
}
